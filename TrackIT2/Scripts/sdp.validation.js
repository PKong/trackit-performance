﻿function showMessageAlertByName(type, message, Name) {
    if ((type == null && message == null) || (type == "" && message == "")) {
        type = ConstMessageTypes.INFO.type;
        message = ConstMessageTypes.INFO.defaultMessage;
    }

    $(Name).append(
            '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                '<p>' + message + '</p>' +
		        '<a class="close" href="javascript:void(0)">close</a>' +
            '</div>' +
            '<div class="cb"></div>'
        );
}

function generalBeforeSave() 
{
    var msgError = "";
    var check = true;
    if (($("#MainContent_txtSDP_RequestDate").length && $("#MainContent_txtSDP_RequestDate").val().toString() != "" && !isValidDate("#MainContent_txtSDP_RequestDate")) |
        ($("#MainContent_txtSDP_DueDate").length && $("#MainContent_txtSDP_DueDate").val().toString() != "" && !isValidDate("#MainContent_txtSDP_DueDate")) |
        ($("#MainContent_txtFirstSweepDate").length && $("#MainContent_txtFirstSweepDate").val().toString() != "" && !isValidDate("#MainContent_txtFirstSweepDate")) |
        ($("#MainContent_txtSDP_NoticeOfCompletionDate").length && $("#MainContent_txtSDP_NoticeOfCompletionDate").val().toString() != "" && !isValidDate("#MainContent_txtSDP_NoticeOfCompletionDate")) |
        ($("#MainContent_txtSDP_CompletedDate").length && $("#MainContent_txtSDP_CompletedDate").val().toString() != "" && !isValidDate("#MainContent_txtSDP_CompletedDate")) |
        ($("#MainContent_txtFirstMarketDate").length && $("#MainContent_txtFirstMarketDate").val().toString() != "" && !isValidDate("#MainContent_txtFirstMarketDate")) |
        ($("#MainContent_txtSecondMarketDate").length && $("#MainContent_txtSecondMarketDate").val().toString() != "" && !isValidDate("#MainContent_txtSecondMarketDate")) |
        ($("#MainContent_txtThirdMarketDate").length && $("#MainContent_txtThirdMarketDate").val().toString() != "" && !isValidDate("#MainContent_txtThirdMarketDate")) |
        ($("#MainContent_txtEscalationDate").length && $("#MainContent_txtEscalationDate").val().toString() != "" && !isValidDate("#MainContent_txtEscalationDate")) |
        ($("#MainContent_txtEscalationCompletedDate").length && $("#MainContent_txtEscalationCompletedDate").val().toString() != "" && !isValidDate("#MainContent_txtEscalationCompletedDate")) |
        ($("#MainContent_txtVendorOrderReceived").length && $("#MainContent_txtVendorOrderReceived").val().toString() != "" && !isValidDate("#MainContent_txtVendorOrderReceived")) |
        ($("#MainContent_txtVendorOrderPOReceived").length && $("#MainContent_txtVendorOrderPOReceived").val().toString() != "" && !isValidDate("#MainContent_txtVendorOrderPOReceived")) |
        ($("#MainContent_txtCostApprovalDate").length && $("#MainContent_txtCostApprovalDate").val().toString() != "" && !isValidDate("#MainContent_txtCostApprovalDate")) |
        ($("#MainContent_txtPrimeLeaseExecDate").length && $("#MainContent_txtPrimeLeaseExecDate").val().toString() != "" && !isValidDate("#MainContent_txtPrimeLeaseExecDate")) |
        ($("#MainContent_txtPermitAppliedDate").length && $("#MainContent_txtPermitAppliedDate").val().toString() != "" && !isValidDate("#MainContent_txtPermitAppliedDate")) |
        ($("#MainContent_txtConstructionStartDate").length && $("#MainContent_txtConstructionStartDate").val().toString() != "" && !isValidDate("#MainContent_txtConstructionStartDate"))) {
        msgError += messageValidateDateTime;
    }
    
    if (msgError != "") 
    {
       check = false;
       $("#error_general").html("");
       showMessageAlertByName("error", msgError, "#error_general");
       return false;
    }
    else
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };
       
       $("#MainContent_btnSubmitSDP_General").val("Processing...");
       $("#MainContent_btnSubmitSDP_General").attr("disabled", true);
       $("#MainContent_btnSubmitSDP_General2").val("Processing...");
       $("#MainContent_btnSubmitSDP_General2").attr("disabled", true);
       return true;
    }

    return check;
}

function abstractBeforeSave() 
{
    var msgError = "";
    var check = true;

    if (
        ($("#MainContent_txt_Assigned").length && $("#MainContent_txt_Assigned").val().toString() != "" && !isValidDate("#MainContent_txt_Assigned")) |
        ($("#MainContent_txt_Completed").length && $("#MainContent_txt_Completed").val().toString() != "" && !isValidDate("#MainContent_txt_Completed"))
       ) 
    {
        msgError += messageValidateDateTime;
    }

    if (msgError != "") {
       check = false;
       $("#error_abstract").html("");
       showMessageAlertByName("error", msgError, "#error_abstract");
       return false;
    }
    else 
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };

       $("#MainContent_btnSubmitAbstract").val("Processing...");
       $("#MainContent_btnSubmitAbstract").attr("disabled", true);
       $("#MainContent_btnSubmitAbstract2").val("Processing...");
       $("#MainContent_btnSubmitAbstract2").attr("disabled", true);
       return true;
    }
 }

 function documentStatusBeforeSave() 
 {
    // No validation is needed, but the proper processing/disable scripts
    // need to be called.

    // Remove onbeforeunload event since this script is called as a postback.
    window.onbeforeunload = function () { };

    $("#MainContent_btnSubmitDocStatus").val("Processing...");
    $("#MainContent_btnSubmitDocStatus").attr("disabled", true);
    $("#MainContent_btnSubmitDocStatus2").val("Processing...");
    $("#MainContent_btnSubmitDocStatus2").attr("disabled", true);
    return true;
 }

 function upadateShowDocument() {
     var isDisabled = false;
     var cssInnerSwitch = 'onoffswitch-inner';
     if ($('#MainContent_chkShowDocument').attr('disabled') == 'disabled') {
         cssInnerSwitch += '-disabled';
         isDisabled = true;
     }

    $(".onoffswitch-checkbox").each(function () {
        $(this).parent().append(
                '<label class="onoffswitch-label " for="myonoffswitch" >' +
                    '<div class="'+ cssInnerSwitch + '"></div>' +
                    '<div class="onoffswitch-switch "></div>' +
                '</label>'
            );
        // Hide checkbox
        $(this).hide();
    });  // End each()
    // Toggle switch when clicked
    $(".onoffswitch-checkbox").parent().find(".onoffswitch-label").each(function () {
        $(this).click(function (e) {
            if (isDisabled) {
                e.preventDefault();
            }
            else {
                // Toggle state of checkbox
                $('.onoffswitch-checkbox').prop('checked', !$('.onoffswitch-checkbox').prop('checked'));
                $('#MainContent_chkShowDocument').prop($('.onoffswitch-checkbox').prop('checked'));
                $('#MainContent_chkShowDocument').click();
            }
        });
    });
    if ($.browser.msie && $.browser.version == 7 && isDisabled) {
        $('.onoffswitch-inner-disabled').addClass('after');
        $('.onoffswitch-inner-disabled').text('Not Linked');
        $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-switch').css("top", "-25px");
        $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-inner-disabled').css('margin-left', '0px');
    }
    if ($('#MainContent_chkShowDocument').prop('checked')) {
        $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-switch').css("right", "0px");
        setTimeout(function () {
            $('.onoffswitch-checkbox').prop('checked', true);
            $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-switch').animate({ "right": "-91px" }, 300);
            if ($.browser.msie && $.browser.version == 7) {
                $('.onoffswitch-inner').removeClass('after');
                $('.onoffswitch-inner').addClass('before');
                $('.onoffswitch-inner').text('Published');
                $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-switch').css("top", "-25px");
                $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-inner').css('margin-left', '0px');
            }
            else {
                $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-inner').css('margin-left', '0px');
            }
        });
    }
    else {
        $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-inner').css('margin-left', '0px');
        setTimeout(function () {
            $('.onoffswitch-checkbox').prop('checked', false);
            $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-switch').animate({ "right": "0px" }, 300);
            if ($.browser.msie && $.browser.version == 7) {
                $('.onoffswitch-inner').removeClass('before');
                $('.onoffswitch-inner').addClass('after');
                $('.onoffswitch-inner').text('Not Published');
                $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-switch').css("top", "-25px");
                $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-inner').css('margin-left', '0px');
            }
            else {
                $('.onoffswitch-checkbox').parent().find('.onoffswitch-label .onoffswitch-inner').css('margin-left', '-120px');
            }
        }, 0);
    }
}
