﻿
// Create Date 12/11/11
// Update Date 28/11/11

var IsSave_TM = false;

//function PageRequestManagerTM() {
//    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandlerTM);
//}

function EndRequestHandlerTM() {
    var index = getBufferIndexTM();
    //    eventClickCreatePanelTM();
    //    CreateBtnTM(false);
    //    SelectBtnIndexTM(index);
    //    SetDataToUITM(index);
    //   $("#add_Purchase_Panel").button();


    ShowDialog();
    // Create Document link
    createDocumentLink();
    CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');

    setBtnCountTM($("#MainContent_hidden_TM_count").val());
    initEventClickBack();
    initEventClickNext();
    initEventClickAddNew();

    SetDataToUITM(index);
    setBufferIndexTM(index);

    setLabelPageTotal(getCountTM());
    setLabelPageCurrent(index);
//    initEventFocusOut('#MainContent_UpdatePanel1');
    $("#MainContent_btnSummitTowerModPOs1").button();
    $("#MainContent_btnSummitTowerModPOs2").button();
    $("#btnPost").button();
    $("#btnCommentCancel").button();
    // Date Picker
    $(function () {
        $('input').filter('.datepicker').datepicker({
            numberOfMonths: 2,
            showButtonPanel: false
        });
    });

    // Money field
    moneyField();

    // Percent field
    percentField();
    // SetCssBtn
    cssBtnControlPage();


        $(function () {
            $('#txtGeneralContractorID').autocomplete({ source: 'GeneralContractorAutoComplete.axd', select: function (event, ui) {
                try {
                    $(event.target).val(ui.item.value.slice(0, ui.item.value.indexOf(' ')));
                    return false;
                } 
                catch (e) {
                }
            }
        });
    });
      
}
var LastSelectedTab = ""
function DynamicUpdate(sTabName) {

   $("hiddenCurrentTab").val(sTabName);
    if (LastSelectedTab == 'PO') {
        var index = getBufferIndexTM();
        SetDataFromUI(index);
    }
    LastSelectedTab = sTabName;
}

function cssBtnControlPage() {
    $("#btn_Previous").button();
    $("#btn_Next").button();
    $("#btn_AddNew").button();
    $("#btnTMPODelete").button();
}

function initEventClickNext() {
    $('#btn_Next').unbind('click');
    $("#btn_Next").click(function () {
        funcClickNext();
    });
}

function initEventClickBack() {
    $('#btn_Previous').unbind('click');
    $("#btn_Previous").click(function () {
        funcClickBack();
    });
}

function initEventClickAddNew() {
    $('#btn_AddNew').unbind('click');
    $("#btn_AddNew").click(function () {
        funcClickAddNew();
    });
}


function funcClickNext() {
    var index = getBufferIndexTM();
    var count = getCountTM();
    var intI = (parseInt(index) + 1);

    if (intI < count) {
        if (intI - 1 >= 0) SetDataFromUI(intI - 1);
        setBufferIndexTM(intI);
        SetDataToUITM(intI);
        setLabelPageCurrent(intI)
        $("#slideshow2").show("slide", { direction: "right" }, 220);
    }
    else {
        // do something for last page
    }

    //hide all document link
    $("[id^=lst_EbidComment_]").css('display', 'none');
    $("[id^=lst_BidAmount_]").css('display', 'none');

    //set display list document
    var tmopo = '';
    var jsonTMOStr = $("#MainContent_hidden_TM").val();
    var tmo_data = jQuery.parseJSON(jsonTMOStr);
    if (index < tmo_data.length && intI < count) {
        tmopo = tmo_data[intI].ID;
    }
    if ($('#lst_EbidComment_' + tmopo).length > 0) {
        $('#lst_EbidComment_' + tmopo).css('display', 'block');
    }
    if ($('#lst_BidAmount_' + tmopo).length > 0) {
        $('#lst_BidAmount_' + tmopo).css('display', 'block');
    }
}

function funcClickBack() {
    var index = getBufferIndexTM();
    var intI = (parseInt(index) - 1);
    var count = getCountTM();
    $("#txtSiteID").val("A");

    if (intI >= 0) {
        if (intI + 1 < getCountTM()) SetDataFromUI(intI + 1);
        setBufferIndexTM(intI);
        SetDataToUITM(intI);
        setLabelPageCurrent(intI)
        $("#slideshow2").show("slide", { direction: "left" }, 220);
        $("#txtSiteID").val("C");
    }
    else {
        // do something for first page

    }
    //set display list document
    var tmopo = '';

    //hide all document link
    $("[id^=lst_EbidComment_]").css('display', 'none');
    $("[id^=lst_BidAmount_]").css('display', 'none');

    var jsonTMOStr = $("#MainContent_hidden_TM").val();
    var tmo_data = jQuery.parseJSON(jsonTMOStr);
    if (index < tmo_data.length && intI != -1) {
        tmopo = tmo_data[intI].ID;
        if ($('#lst_EbidComment_' + tmopo).length > 0) {
            $('#lst_EbidComment_' + tmopo).css('display', 'block');
        }

        if ($('#lst_BidAmount_' + tmopo).length > 0) {
            $('#lst_BidAmount_' + tmopo).css('display', 'block');
        }
    }
}

function funcClickAddNew() {
    var IsNew = parseInt(getIsNewTM());
    if (IsNew == 0) {
        var oldIndex = getBufferIndexTM();
        SetDataFromUI(oldIndex);
        increaseCountTM();
        ClearUITM();
        var c = getCountTM();
        var i = (parseInt(c) - 1);
        setBufferIndexTM(i);
        setIsNewTM(1);

        setLabelPageCurrent(i);
        setLabelPageTotal(getCountTM());
        $("#slideshow2").show("slide", { direction: "right" }, 220);
        $("#txtSiteID").val("D");


        //hide all document link
        $("[id^=lst_EbidComment_]").css('display', 'none')
        $("[id^=lst_BidAmount_]").css('display', 'none')

    }
    else {

    }
}

function setTMdocument() {
    var tmo_id = getTMID();

    //hide all document link
    $("[id^=lst_EbidComment_]").css('display', 'none');
    $("[id^=lst_BidAmount_]").css('display', 'none');

    var jsonTMOStr = $("#MainContent_hidden_TM").val();
    var tmo_data = jQuery.parseJSON(jsonTMOStr);
    if (tmo_data != null) {
        if ($('#lst_EbidComment_' + tmo_id).length > 0) {
            $('#lst_EbidComment_' + tmo_id).css('display', 'block');
        }

        if ($('#lst_BidAmount_' + tmo_id).length > 0) {
            $('#lst_BidAmount_' + tmo_id).css('display', 'block');
        }
    }
}

function setLabelPageCurrent(data) {
    var i = (parseInt(data) + 1);
    $("#current-page").text(i);
}

function setLabelPageTotal(data) {
    $("#total-page").text(data);
}

function increaseCountTM() {
    var count = $("#hidden_count").text();
    $("#hidden_count").text("");
    count++;
    $("#hidden_count").text(count);
    return count;
}

function setBtnCountTM(count) {

    if (count == 0) {
        $("#hidden_count").text(0);
        ClearUITM();
        setLabelPageTotal(0);
        setLabelPageCurrent(-1);
        setIsNewTM(1); 
    }
    else {
        $("#hidden_count").text(count);
        SetDataToUITM(count - 1);
        setLabelPageTotal(count);
        setLabelPageCurrent(count - 1);
        setIsNewTM(0); 
    }

}

function getCountTM() {
    var count = $("#hidden_count").text();
    return count;
}

function SelectBtnIndexTM(intdex) {
    if ($("#btn_page_" + intdex).attr('class') != "activeSlide") {
        $("[id^=btn_page_]").removeClass("activeSlide");
        $("#btn_page_" + intdex).addClass("activeSlide");
        setBufferIndexTM(intdex);
    }
}

function setBufferIndexTM(data) {
    $("#BufferIndex").html("");
    $("#BufferIndex").html(data);
}

function getBufferIndexTM() {
    var data = $("#BufferIndex").html();
    return data;
}

function getTMID() {
    var id = $("#MainContent_hidden_TM_ID").val();
    return id;
}

function addEvenPagingTM() {

}

function SetIndexPagingStartUpTM() {
    var count = getCount();
}

function setPageIndexTM(index) {
    $("#PageIndex").html("");
    $("#PageIndex").html(index);
}

function getPageIndexTM() {
    var index = $("#PageIndex").html();
    return index;
}

function getIsNewTM() {
    var val = $("#hidden_TM_IsNew").val();
    return val;
}

function getIsSaveTM() {
    var val = $("#hidden_TM_IsSave").val();
    return val;
}

function setIsNewTM(data) {
    $("#hidden_TM_IsNew").val(data);
}

function setIsSaveTM(data) {
    $("#hidden_TM_IsSave").val(data);
}

///////////  Control Data ////////////

function SelectIndexOrderedTM() {
    $("#MainContent_ddlProcessDates_ColLeft1").val("3");
}


function SetDataToUITM(index) {
    var data = GetDataTM(index);
    if (data != null) {

        //Tower Mod POs
        $("#MainContent_ddlTowerModPhase1").val(data.tower_mod_phase_type_id);

        // eBid
        $("#MainContent_txtRequestReceived1").val((data.ebid_req_rcvd_date));
        $("#MainContent_txtPacketReady1").val((data.ebid_packet_ready_date));
        $("#MainContent_txtScheduledPublished1").val((data.ebid_scheduled_published_date));
        $("#MainContent_txtScheduledClosed1").val((data.ebid_scheduled_close_date));
        $("#MainContent_txtMaturityDate1").val((data.ebid_maturity_date));
        $("#MainContent_txt_eBidComments1").val(data.ebid_comment);

        // Pricing
        $("#MainContent_txtEstConstructionCostAmount1").val(data.estimated_construction_cost_amount);
        $("#MainContent_txtBidAmount1").val(data.bid_amount);
        $("#MainContent_txtTaxAmount1").val(data.tax_amount);
        $("#MainContent_txtMarkupAmount1").val(data.markup_amount);
        $("#MainContent_txtTotalfromCustomerAmount1").val(data.total_from_customer_amount);


        // PO Creation
       // $("#MainContent_ddlGeneralContractor1").val(data.general_contractor_id);
        $("#MainContent_txtSendToCSPM1").val((data.sent_to_cs_pm_date));
        $("#MainContent_txtPOtoGC1").val((data.po_to_gc_requested_date));
        $("#MainContent_txtShoppingCartID1").val(data.shopping_cart_nbr);
        $("#MainContent_txtPOtoGCReceived1").val((data.po_to_gc_rcvd_date));
        $("#MainContent_txtPONumber1").val(data.po_nbr);
        $("#txtGeneralContractorID").val(data.general_contractor_name);



        if (data.bid_amount == "" || data.bid_amount == null) {
            $("#MainContent_txtBidAmount1").val("$");
        }
        else {
            $("#MainContent_txtBidAmount1").val(data.bid_amount);
        }


        if (data.estimated_construction_cost_amount == "" || data.estimated_construction_cost_amount == null) {
            $("#MainContent_txtEstConstructionCostAmount1").val("$");
        }
        else {
            $("#MainContent_txtEstConstructionCostAmount1").val(data.estimated_construction_cost_amount);
        }

        if (data.tax_amount == "" || data.tax_amount == null) {
            $("#MainContent_txtTaxAmount1").val("$");
        }
        else {
            $("#MainContent_txtTaxAmount1").val(data.tax_amount);
        }


        if (data.markup_amount == "" || data.markup_amount == null) {
            $("#MainContent_txtMarkupAmount1").val("$");
        }
        else {
            $("#MainContent_txtMarkupAmount1").val(data.markup_amount);
        }

        if (data.total_from_customer_amount == "" || data.total_from_customer_amount == null) {
            $("#MainContent_txtTotalfromCustomerAmount1").val("$");
        }
        else {
            $("#MainContent_txtTotalfromCustomerAmount1").val(data.total_from_customer_amount);
        }

       
        //hide all document link
        $("[id^=lst_EbidComment_]").css('display', 'none')
        $("[id^=lst_BidAmount_]").css('display', 'none')

        //display current document
        $('#lst_BidAmount_' + data.ID).css('display', 'block');
        $('#lst_EbidComment_' + data.ID).css('display', 'block');

        $("#MainContent_hidden_TM_ID").val(data.ID);

        $("#MainContent_txtEstConstructionCostAmount1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_txtBidAmount1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_txtTaxAmount1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_txtMarkupAmount1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_txtTotalfromCustomerAmount1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
    }
    else {
        ClearUITM();
    }

}

function ClearUITM() {

    //Tower Mod POs
    $("#MainContent_ddlTowerModPhase1").val("");

    // eBid
    $("#MainContent_txtRequestReceived1").val("");
    $("#MainContent_txtPacketReady1").val("");
    $("#MainContent_txtScheduledPublished1").val("");
    $("#MainContent_txtScheduledClosed1").val("");
    $("#MainContent_txtMaturityDate1").val("");
    $("#MainContent_txt_eBidComments1").val("");

    // Pricing
    $("#MainContent_txtEstConstructionCostAmount1").val("$");
    $("#MainContent_txtBidAmount1").val("$");
    $("#MainContent_txtTaxAmount1").val("$");
    $("#MainContent_txtMarkupAmount1").val("$");
    $("#MainContent_txtTotalfromCustomerAmount1").val("$");

    // PO Creation
    $("#MainContent_txtSendToCSPM1").val("");
    $("#MainContent_txtPOtoGC1").val("");
    $("#MainContent_txtShoppingCartID1").val("");
    $("#MainContent_txtPOtoGCReceived1").val("");
    $("#MainContent_txtPONumber1").val("");

    $("#txtGeneralContractorID").val("");

    $("#MainContent_hidden_TM_ID").val("0");
}

function SetDataFromUI(index) {

    var data = GetAllJsonData();

    //    if (data != null ) {
    //Temp use.
    if (data == null && index == 0) {
        data = new Array();
    }
    if (data != null && index <= data.length) {
        if (index == data.length) {
            var newData = GetCtrlData("#MainContent_hidden_TM_template");
            data[index] = newData;
        }
        
        
        
        //Tower Mod POs
        data[index].tower_mod_phase_type_id = $("#MainContent_ddlTowerModPhase1").val();

        // eBid
        data[index].ebid_req_rcvd_date = ($("#MainContent_txtRequestReceived1").val());
        data[index].ebid_packet_ready_date = ($("#MainContent_txtPacketReady1").val());
        data[index].ebid_scheduled_published_date = ($("#MainContent_txtScheduledPublished1").val());
        data[index].ebid_scheduled_close_date = ($("#MainContent_txtScheduledClosed1").val());
        data[index].ebid_maturity_date = ($("#MainContent_txtMaturityDate1").val());
        data[index].ebid_comment = $("#MainContent_txt_eBidComments1").val();

        // Pricing
        data[index].estimated_construction_cost_amount = $("#MainContent_txtEstConstructionCostAmount1").val().toString().replace('$', '');
        data[index].bid_amount = $("#MainContent_txtBidAmount1").val().toString().replace('$', '');
        data[index].tax_amount = $("#MainContent_txtTaxAmount1").val().toString().replace('$', '');
        data[index].markup_amount = $("#MainContent_txtMarkupAmount1").val().toString().replace('$', '');
        data[index].total_from_customer_amount = $("#MainContent_txtTotalfromCustomerAmount1").val().toString().replace('$', '');


        // PO Creation
        // $("#MainContent_ddlGeneralContractor1").val(data.general_contractor_id);
        data[index].sent_to_cs_pm_date = ($("#MainContent_txtSendToCSPM1").val());
        data[index].po_to_gc_requested_date = ($("#MainContent_txtPOtoGC1").val());
        data[index].shopping_cart_nbr = $("#MainContent_txtShoppingCartID1").val();
        data[index].po_to_gc_rcvd_date = ($("#MainContent_txtPOtoGCReceived1").val());
        data[index].po_nbr = $("#MainContent_txtPONumber1").val();
        data[index].general_contractor_name = $("#txtGeneralContractorID").val();

        SetData("#MainContent_hidden_TM", data);
    }
    else {
        ClearUITM();
    }

}

function CheckSavingTM() {
    var IsValid = funcBeforeSaveTowerModPO();
    
    if (IsValid) {
        setIsNewTM(0); // not New
        IsSave_TM = true;
    }

    return IsValid;
}

function GetDataTM(index) {
    var jsonStr = $("#MainContent_hidden_TM").val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        var i = parseInt(index);
        return data[i];
    }
}

function GetAllJsonData() {
    var jsonStr = $("#MainContent_hidden_TM").val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}
function GetCtrlData(sCtrlName) {
    var jsonStr = $(sCtrlName).val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}

function SetData(ctrlSource, data) {
    var json_string = JSON.stringify(data);
    $(ctrlSource).val(json_string);
}

function setPageName(name) {
    $('body').append("<input type='hidden' id='hidden_PageName' value='" + name + "' >");
}

function getPageName(name) {
    var data = $("#hidden_PageName").val();
    return data;
}

function ShowDialog() {
    if (IsSave_TM == true) {
        var sText = $('#hidden_popup').val();
        UpdatePanelDialog('Status', sText);
        IsSave_TM = false;
    }

}
function UpdatePanelDialog(title, message) {
    // Delete Modal Dialog
    var delDialogHtml = '<div id="dialog-message" title="' + title + '">' +
                        '<p>' + message + '</p></div>';
    $('#dialog-message').remove();
    $(delDialogHtml).dialog({
//        buttons: {
//            'OK': function () {
//                $(this).dialog('close');
//            }
//        }
    });
    $('#dialog-message').dialog('open');
}

function funcBeforeSaveGen() 
{
    var messageErr = "";
    var check = true;
    var data = $("#MainContent_txtCapacity").val();
    var IsCurrencyErr = false;

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    // Date validation    
    if (
        ($("#MainContent_txtPackageSent").length && $("#MainContent_txtPackageSent").val().toString() != "" && !isValidDate("#MainContent_txtPackageSent")) |
        ($("#MainContent_txtPermitReceived").length && $("#MainContent_txtPermitReceived").val().toString() != "" && !isValidDate("#MainContent_txtPermitReceived")) |
        ($("#MainContent_txtMaterialsOrdered").length && $("#MainContent_txtMaterialsOrdered").val().toString() != "" && !isValidDate("#MainContent_txtMaterialsOrdered")) |
        ($("#MainContent_txtMaterialsReceived").length && $("#MainContent_txtMaterialsReceived").val().toString() != "" && !isValidDate("#MainContent_txtMaterialsReceived")) |
        ($("#MainContent_txtWorkStart").length && $("#MainContent_txtWorkStart").val().toString() != "" && !isValidDate("#MainContent_txtWorkStart")) |
        ($("#MainContent_txtWorkCompletion").length && $("#MainContent_txtWorkCompletion").val().toString() != "" && !isValidDate("#MainContent_txtWorkCompletion")) |
        ($("#MainContent_txtInspection").length && $("#MainContent_txtInspection").val().toString() != "" && !isValidDate("#MainContent_txtInspection"))
      ) 
      {
        check = false;
        messageErr += messageValidateDateTime;
    }

    // Percent validation
     if ($("#MainContent_txtCapacity").length && $("#MainContent_txtCapacity").val().toString() != "") 
     {
        if (!validatePercent("#MainContent_txtCapacity", 0)) {
            check = false;
            messageErr += messageValidatePercent;
        }
    }
    
    // Double validation
    if ($("#MainContent_txtTowerHeight").length && $("#MainContent_txtTowerHeight").val().toString() != "" && !validateDecimal("#MainContent_txtTowerHeight")) 
    {
        check = false;
        messageErr += messageValidateDecimal;
    }


    if (!check)
    {
        $("#message_alert").html("");
        showMessageAlertByName("error", messageErr, "#message_alert");
        return false;
    }

    else
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };

       // Change save button to display processing message and disable it to
       // prevent "double submissions".
       $("#MainContent_btnSubmitSummary").val("Processing...");
       $("#MainContent_btnSubmitSummary").attr("disabled", true);
       $("#MainContent_btnSubmitSummary2").val("Processing...");
       $("#MainContent_btnSubmitSummary2").attr("disabled", true);
        
        var oldIndex = getBufferIndexTM();
        SetDataFromUI(oldIndex);
                
        return true;
    }
}

function funcBeforeSaveTowerModPO() {
    var msgError = "";
    var check = true;

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    // Date validation

    //Date validation.
    if (($("#MainContent_txtRequestReceived1").length && $("#MainContent_txtRequestReceived1").val().toString() != "" && !isValidDate("#MainContent_txtRequestReceived1")) |
        ($("#MainContent_txtPacketReady1").length && $("#MainContent_txtPacketReady1").val().toString() != "" && !isValidDate("#MainContent_txtPacketReady1")) |
        ($("#MainContent_txtScheduledPublished1").length && $("#MainContent_txtScheduledPublished1").val().toString() != "" && !isValidDate("#MainContent_txtScheduledPublished1")) |
        ($("#MainContent_txtScheduledClosed1").length && $("#MainContent_txtScheduledClosed1").val().toString() != "" && !isValidDate("#MainContent_txtScheduledClosed1")) |
        ($("#MainContent_txtMaturityDate1").length && $("#MainContent_txtMaturityDate1").val().toString() != "" && !isValidDate("#MainContent_txtMaturityDate1")) |
        ($("#MainContent_txtSendToCSPM1").length && $("#MainContent_txtSendToCSPM1").val().toString() != "" && !isValidDate("#MainContent_txtSendToCSPM1")) |
        ($("#MainContent_txtPOtoGC1").length && $("#MainContent_txtPOtoGC1").val().toString() != "" && !isValidDate("#MainContent_txtPOtoGC1")) |
        ($("#MainContent_txtPOtoGCReceived1").length && $("#MainContent_txtPOtoGCReceived1").val().toString() != "" && !isValidDate("#MainContent_txtPOtoGCReceived1"))) {
        msgError += messageValidateDateTime;
    }
    //Currency validation.
    if (($("#MainContent_txtEstConstructionCostAmount1").length && $("#MainContent_txtEstConstructionCostAmount1").val().toString() != "" && !validateCurrency("#MainContent_txtEstConstructionCostAmount1")) |
        ($("#MainContent_txtBidAmount1").length && $("#MainContent_txtBidAmount1").val().toString() != "" && !validateCurrency("#MainContent_txtBidAmount1")) |
        ($("#MainContent_txtTaxAmount1").length && $("#MainContent_txtTaxAmount1").val().toString() != "" && !validateCurrency("#MainContent_txtTaxAmount1")) |
        ($("#MainContent_txtMarkupAmount1").length && $("#MainContent_txtMarkupAmount1").val().toString() != "" && !validateCurrency("#MainContent_txtMarkupAmount1")) |
        ($("#MainContent_txtTotalfromCustomerAmount1").length && $("#MainContent_txtTotalfromCustomerAmount1").val().toString() != "" && !validateCurrency("#MainContent_txtTotalfromCustomerAmount1"))) {
        msgError += messageValidateCurrency;
    }

    if (msgError != "") 
    {
        check = false;
        $("#error_twmodpo").html("");
        showMessageAlertByName("error", msgError, "#error_twmodpo");

        return false;
    }
     else
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };

       // Change save button to display processing message and disable it to
       // prevent "double submissions".
       $("#MainContent_btnSummitTowerModPOs1").val("Processing...");
       $("#MainContent_btnSummitTowerModPOs1").attr("disabled", true);
       $("#MainContent_btnSummitTowerModPOs2").val("Processing...");
       $("#MainContent_btnSummitTowerModPOs2").attr("disabled", true);
        
       var oldIndex = getBufferIndexTM();
       SetDataFromUI(oldIndex);

       return true;
     }
}

function showMessageAlertByName(type, message, Name) {
    if ((type == null && message == null) || (type == "" && message == "")) {
        type = ConstMessageTypes.INFO.type;
        message = ConstMessageTypes.INFO.defaultMessage;
    }

    $(Name).append(
            '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                '<p>' + message + '</p>' +
		        '<a class="close" href="javascript:void(0)">close</a>' +
            '</div>' +
            '<div class="cb"></div>'
        );
}

