﻿/** Dialog **/

/** Constants **/
var ListViewConstants = {
    dialogWidth: 740,
    dialogIframeMinWidth: 700,
    dialogIframeMinHeight: 520,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "Add.aspx"
};
var TMID = 0;
/** /Constants **/

// Create resizable modal function to apply before call 
function setModalSize(dWidth, iframe_minWidth, iframe_minHeight) {
    ListViewConstants = null;
    ListViewConstants = {
        dialogWidth: dWidth,
        dialogIframeMinWidth: iframe_minWidth,
        dialogIframeMinHeight: iframe_minHeight,
        modalLoadingMessage: "LOADING... PLEASE WAIT",
        modalAddPageUrl: "Add.aspx"
    }
}

// Setup List Create Modal
function setupListCreateModal(title, id) {
    TMID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstants.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeId').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    //
    $("#modalExternal").dialog(optionsCreate);
    //
    $('#modalBtnExternal').click(function () {
        showCreateDialog();
    });
}
//-->

// Show Create Dialog
function showCreateDialog() {

    $("#modalExternal").html('<iframe id="modalIframeId" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:' + ListViewConstants.dialogIframeMinHeight + 'px;min-width:' + ListViewConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading">' + ListViewConstants.modalLoadingMessage + '</div>').dialog("open");
    $("#modalIframeId").attr("src", ListViewConstants.modalAddPageUrl + "?id=" + TMID);
    return false;
}
//-->

/** /Dialog **/

