﻿/*Constants*/
var enumInfo = {
    ControlTypeAbbre: 0,
    Name: 1,
    Label: 2,
    DBName: 3
};
var AutoCompleteType = {
    City: 'autCity',
    State: 'autState',
    Country: 'autCounty',
    CustomerSiteUID: 'autCustomerSiteUID'
};

var ControlType = {
    TEXT_BOX: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Type: 'text',
            css: 'adv_search-input'
        }],
        Prefix: 'txt'
    },
    DROP_DOWN: {
        Elements: [{
            ElementName: 'select',
            Prefix: 'ddl',
            Type: null,
            css: 'select-field'
        }],
        Prefix: 'ddl'
    },
    DATE_TIME: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'datepicker split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        }, {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'datepicker split'
        }],
        Prefix: 'dat'
    },
    DATE_TIME_WITH_STATUS: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'datepicker split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        }, {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'datepicker split'
        },
        {
            ElementName: 'select',
            Prefix: 'ddl',
            Subfix: 'Status',
            Type: null,
            css: 'split'
        }],
        Prefix: 'dst'
    },
    NUMERIC: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'adv_search-input split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        },
        {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'adv_search-input split'
        }],
        Prefix: 'num'
    },
    DECIMAL: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'moneyfield split'
        },{
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        },
        {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'moneyfield split'
        }],
        Prefix: 'dec'
    },
    AUTO_COMPLETE: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: null,
            Type: 'text',
            css: 'input - autocomplete'
        }],
        Prefix: 'aut',
        ScriptKeyName: '',
        DataSourceUrl: ''
    },
    DOUBLE_TABLE_DATE_TIME_WITH_STATUS: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'datepicker split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        }, {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'datepicker split'
        },
        {
            ElementName: 'select',
            Prefix: 'ddl',
            Subfix: 'Status',
            Type: null,
            css: 'split'
        }],
        Prefix: 'dds'
    },
    Share_DROP_DOWN: {
        Elements: [{
            ElementName: 'select',
            Prefix: 'ddl',
            Type: null,
            css: 'select-field'
        }],
        Prefix: 'sdl'
    },
    Share_DATE_TIME: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'datepicker split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        }, {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'datepicker split'
        }],
        Prefix: 'sat'
    },
    Share_DECIMAL: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'moneyfield split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        },
        {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'moneyfield split'
        }],
        Prefix: 'sec'
    },
    Share_TEXT_BOX: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Type: 'text',
            css: 'adv_search-input'
        }],
        Prefix: 'sxt'
    },
    Share_NUMERIC: {
        Elements: [{
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'From',
            Type: 'text',
            css: 'adv_search-input split'
        }, {
            ElementName: 'label',
            Prefix: 'lbl',
            Type: null,
            css: 'date_time_label',
            Value: 'to'
        },
        {
            ElementName: 'input',
            Prefix: 'txt',
            Subfix: 'To',
            Type: 'text',
            css: 'adv_search-input split'
        }],
        Prefix: 'snm'
    }
};

var DIR_SEARCH_PAGE = "/Search.aspx";
var CtrlContainer = {};
var ScriptContainer = {};
/*Control adding*/
//Test on adding textbox
function addControl(container, fieldDDL, itemType, itemName, itemLabel, value1, value2, value3) {
    var page = GetCurrentPage();
    var ctrlDDL;                //Use for carry the DDL control to scripts setting.
    var ctrlDateFrom;
    var ctrlDateTo;
    var breakline = document.createElement('div');
    breakline.className = "cb";
    var compItemName = itemType.Prefix + itemName;
    //    if(container.innerHTML == "") {
    //        //Add some label to container.
    //        var header = document.createElement('b');
    //        header.innerHTML = "Advance Search Criteria";
    //        container.appendChild(header);
    //        container.appendChild(breakline);
    //    }
    //Add field label.
    var curLabel = document.createElement('label');
    curLabel.name = "lbl" + itemName;
    curLabel.id = "lbl" + itemName;
    curLabel.innerHTML = itemLabel;

    container.appendChild(curLabel);
    RegisterControl(compItemName, curLabel.id);
    //Add Control(s) to container.
    for (var element in itemType.Elements) {
        var currentElement = itemType.Elements[element];
        var ctrl = document.createElement(currentElement.ElementName);

        ctrl.name = currentElement.Prefix + itemName;
        if (currentElement.Subfix != null) {
            ctrl.name += currentElement.Subfix;
        }
        ctrl.id = ctrl.name;
        if (currentElement.Type != null) ctrl.type = currentElement.Type;
        if (currentElement.css != null) ctrl.className = currentElement.css;

        if (currentElement.ElementName == 'label') ctrl.innerHTML = currentElement.Value;
        container.appendChild(ctrl);
        RegisterControl(compItemName, ctrl.id);
        //Store current control for late binding.
        if (currentElement.ElementName == 'select') ctrlDDL = ctrl;
        if ((itemType == ControlType.DATE_TIME || itemType == ControlType.DATE_TIME_WITH_STATUS
        || itemType == ControlType.DECIMAL || itemType == ControlType.NUMERIC
        || itemType == ControlType.Share_DECIMAL || itemType == ControlType.Share_NUMERIC
        // add support control for shared date time and double date time with status.
        || itemType == ControlType.Share_DATE_TIME || itemType == ControlType.DOUBLE_TABLE_DATE_TIME_WITH_STATUS)
         && currentElement.Subfix != null) {
            if (currentElement.Subfix == 'From') {
                ctrlDateFrom = ctrl;
            }
            else if (currentElement.Subfix == 'To') {
                ctrlDateTo = ctrl;
            }
        }
    }
    //Some control need to initialize scripts.
    switch (itemType) {
        case ControlType.TEXT_BOX:
        case ControlType.Share_TEXT_BOX:
            if (value1 != null) {
                ctrl.value = value1;
            }
            break;
        case ControlType.DROP_DOWN:
        case ControlType.Share_DROP_DOWN:
            var jsonData;
            $.post(page + DIR_SEARCH_PAGE, { result: compItemName }, function (data) {
                jsonData = data;

            })
            .success(function (data) {
                //Add condtion check if value1 is null then use value 3
                if (value1 != null) {
                    AddDataToDDL(jsonData, ctrlDDL, value1);
                }
                else {
                    AddDataToDDL(jsonData, ctrlDDL, value3);
                }
            });

            break;
        case ControlType.DATE_TIME:
        case ControlType.Share_DATE_TIME:
            DateTimeInit();
            if (value1 != null) ctrlDateFrom.value = value1;
            if (value2 != null) ctrlDateTo.value = value2;
            break;
        case ControlType.DATE_TIME_WITH_STATUS:
        case ControlType.DOUBLE_TABLE_DATE_TIME_WITH_STATUS:
            DateTimeInit();
            if (value1 != null) ctrlDateFrom.value = value1;
            if (value2 != null) ctrlDateTo.value = value2;
            $.post(page + DIR_SEARCH_PAGE, { result: 'apn' }, function (data) {
                AddDataToDDL(data, ctrlDDL, value3);

            });
        case ControlType.AUTO_COMPLETE:
            if (ScriptContainer[compItemName] != 1) {
                var DataSource = "";
                var Delimiter = ' ';
                var InnerHTML = "";
                //TODO: Need to re-factor this InnerHTML into easy looking string Format.
                switch (compItemName) {
                    case AutoCompleteType.City:
                        DataSource = "CityAutoComplete.axd";
                        InnerHTML = "$(function () { $('#" + ctrl.id + "').autocomplete({ source: '" + DataSource +
                        "'" + ",select: function(event, ui) { try { $(event.target).val(ui.item.value); return false; }catch(e){} }})})";
                        break;
                    case AutoCompleteType.State:
                        DataSource = "StateAutoComplete.axd";
                        Delimiter = '-';
                        InnerHTML = "$(function () { $('#" + ctrl.id + "').autocomplete({ source: '" + DataSource +
                        "'" + ",select: function(event, ui) { try { var ret;if (ui.item.value.indexOf('" + Delimiter +
                        "') == -1){ret = ui.item.value;}else{ret = ui.item.value.slice(0, ui.item.value.indexOf('" + Delimiter +
                        "'));}$(event.target).val(ret); return false; }catch(e){} }})})";
                        break;
                    case AutoCompleteType.Country:
                        DataSource = "CountryAutoComplete.axd";
                        InnerHTML = "$(function () { $('#" + ctrl.id + "').autocomplete({ source: '" + DataSource +
                        "'" + ",select: function(event, ui) { try { $(event.target).val(ui.item.value); return false; }catch(e){} }})})";
                        break;
                    case AutoCompleteType.CustomerSiteUID:
                        DataSource = "SiteIdAutoComplete.axd";
                        Delimiter = ' ';
                        InnerHTML = "$(function () { $('#" + ctrl.id + "').autocomplete({ source: '" + DataSource +
                        "'" + ",select: function(event, ui) { try { var ret;if (ui.item.value.indexOf('" + Delimiter +
                        "') == -1){ret = ui.item.value;}else{ret = ui.item.value.slice(0, ui.item.value.indexOf('" + Delimiter +
                        "'));}$(event.target).val(ret); return false; }catch(e){} }})})";
                        break;
                }

                var headID = document.getElementsByTagName('head')[0];
                var newScript = document.createElement('script');
                newScript.type = 'text/javascript';
                newScript.language = 'javascript';
                newScript.id = 'script' + compItemName;

                newScript.innerHTML = InnerHTML;
                headID.appendChild(newScript);
                ScriptContainer[compItemName] = 1;
            }
            if (value1 != null) {
                ctrl.value = value1;
            }
            break;
        case ControlType.DECIMAL:
        case ControlType.Share_DECIMAL:
            if (value1 != null) ctrlDateFrom.value = value1;
            if (value2 != null) ctrlDateTo.value = value2;
            moneyField();   //Init money fields from JQuery
            FillSymbol();

            break;
        case ControlType.NUMERIC:
        case ControlType.Share_NUMERIC:
            if (value1 != null) ctrlDateFrom.value = value1;
            if (value2 != null) ctrlDateTo.value = value2;
            break;
        default:
            ClearAdvanceCriteria(container);
            break;
    }
    var deleteBtn = document.createElement('button');
    deleteBtn.onclick = function () { DeleteControl(container, fieldDDL, compItemName); };

    deleteBtn.id = "btnDelete" + itemName;
    deleteBtn.innerHTML = "x";
    deleteBtn.className = "fancy-button";
    container.appendChild(deleteBtn);
    RegisterControl(compItemName, deleteBtn.id);
}

function AddDataToDDL(data, ddl, val) {

    if (!String(data).isNullOrEmpty()) {
        
        var result = jQuery.parseJSON(data);
        var nullOption = new Option(' - Select - ', '');
        InsertNewOption(nullOption, ddl);
        
        for (var data in result) {
            var currentObj = result[data];
            var option = new Option(currentObj.html, currentObj.value);
            InsertNewOption(option, ddl);
        }

        if (val != null) {
            for (var i = 0; i < ddl.options.length; i++) {
                if (ddl.options[i].value == val) {
                    if (ddl.selectedIndex != i) {
                        ddl.selectedIndex = i;
                    }
                }
            }
        }

        // Clear Last Field To Check Loaded (so the load can continue and filters be applied)
        // if hidden filed contain item will remove that item 
        if ($('#hiddenLastFieldToCheckLoaded').val().indexOf($(ddl).attr('id'))) {
            var newValue = $('#hiddenLastFieldToCheckLoaded').val().replace(',' + $(ddl).attr('id'),'');
            $('#hiddenLastFieldToCheckLoaded').val(newValue);
        }
    }
    else {
        alert("Error");
    }
}

function DeleteControl(container, ddl, ctrlName) {
    var ctrlNames = CtrlContainer[ctrlName].split(",");
    if (ctrlNames.length > 0) {
        for (var nameIndex in ctrlNames) {
            var currName = ctrlNames[nameIndex];
            var child = document.getElementById(currName);
            container.removeChild(child);
        }
        CtrlContainer[ctrlName] = null;
        for (var optionIndex = 0; optionIndex < ddl[0].length; optionIndex++) {
            var currOption = ddl[0].options[optionIndex];
            var infos = currOption.value.split(",");
            if (infos[enumInfo.ControlTypeAbbre] + infos[enumInfo.Name] == ctrlName) {
                ddl[0][optionIndex].disabled = false;
            }
        }
    }
    //delete script.
    if (ScriptContainer[ctrlName] == 1) {
        var ctrlScript = document.getElementById('script' + ctrlName);
        var HeadID = document.getElementsByTagName('head')[0];
        HeadID.removeChild(ctrlScript);
        ScriptContainer[ctrlName] = 0;
    }
}

function ClearAdvanceCriteria(container) {
    container.innerHTML = "";
    CtrlContainer = {};
}

function GetCurrentAddedControlString() {
    var ret = "";
    var count = 0;
    //    for(var ctrl in CtrlCount)
    //    {
    //        count = CtrlCount[ctrl];
    //        for(i = 1; i<= count; i++)
    //        {
    //            if(ret == "")
    //            {
    //                ret += ctrl + i;
    //            }
    //            else
    //            {
    //                ret += ',' + ctrl + i;
    //            }
    //        }
    //    }
    for (var ctrl in CtrlContainer) {
        if (CtrlContainer[ctrl] != null) {
            var Type = ctrl.substr(0, 3);
            var fieldName = new String(ctrl.substr(3, ctrl.length));
            if (Type == 'dat' || Type == 'sat') {
                var dateFrom = 'txt' + fieldName + 'From';
                var dateTo = 'txt' + fieldName + 'To';
                ctrl = dateFrom + ',' + dateTo;
            } else if (Type == 'dst' || Type == 'dds') {
                var dateFrom = 'txt' + fieldName + 'From';
                var dateTo = 'txt' + fieldName + 'To';
                var ddlStatus = 'ddl' + fieldName + 'Status';
                ctrl = dateFrom + ',' + dateTo + ',' + ddlStatus;
            } else if (Type == 'aut') {
                ctrl = 'txt' + fieldName;
            } else if (Type == 'dec' || Type == 'num' || Type == 'sec' || Type == 'snm') {
                var ctrlFrom = 'txt' + fieldName + 'From';
                var ctrlTo = 'txt' + fieldName + 'To';
                ctrl = ctrlFrom + ',' + ctrlTo;
            } else if (Type == 'sdl') {
                var ddlStatus = 'ddl' + fieldName;
                ctrl = ddlStatus;
            } else if (Type == 'sxt') {
                var txt = 'txt' + fieldName;
                ctrl = txt;
            } 

            if (ret == "") {
                ret += ctrl;
            }
            else {
                ret += ',' + ctrl;
            }
        }
    }
    return ret;
}
function InsertNewOption(newOption, selectElement) {
    try {
        // for IE earlier than version 8
        selectElement.add(newOption, selectElement.options[null]);
    }
    catch (e) {
        selectElement.add(newOption, null);
    }
}

function DateTimeInit() {
    // Date Picker initializing.
    $('input').filter('.datepicker').datepicker({
        numberOfMonths: 2,
        showButtonPanel: false
    });
}
//Calculate and count current dynamic control number.
function getCountForControl(ctrlName) {
    if (CtrlCount[ctrlName] != null && CtrlCount[ctrlName] != "") {
        CtrlCount[ctrlName]++;
    }
    else {
        CtrlCount[ctrlName] = 1;
    }
    return CtrlCount[ctrlName];

}

function RegisterControl(fieldName, ctrlName) {
    if (CtrlContainer[fieldName] != null) {
        CtrlContainer[fieldName] += "," + ctrlName;
    }
    else {
        CtrlContainer[fieldName] = ctrlName;
    }
}

/*/Control adding*/

/*ADV Search Related Utilities*/
//Init the hashtable for control type.
function InitializeControlTypeArray(aryCtrlType) {
    if (typeof aryCtrlType != "object") {
        aryCtrlType = {};
    }
    //Init Type.
    aryCtrlType['txt'] = ControlType.TEXT_BOX;
    aryCtrlType['ddl'] = ControlType.DROP_DOWN;
    aryCtrlType['dat'] = ControlType.DATE_TIME;
    aryCtrlType['dst'] = ControlType.DATE_TIME_WITH_STATUS;
    aryCtrlType['num'] = ControlType.NUMERIC;
    aryCtrlType['aut'] = ControlType.AUTO_COMPLETE;
    aryCtrlType['dec'] = ControlType.DECIMAL;
    aryCtrlType['dds'] = ControlType.DOUBLE_TABLE_DATE_TIME_WITH_STATUS;
    aryCtrlType['sdl'] = ControlType.Share_DROP_DOWN;
    aryCtrlType['sat'] = ControlType.Share_DATE_TIME;
    aryCtrlType['sec'] = ControlType.Share_DECIMAL;
    aryCtrlType['sxt'] = ControlType.Share_TEXT_BOX;
    aryCtrlType['snm'] = ControlType.Share_NUMERIC;
    return aryCtrlType;
}
var m_htControlTypes;
//Read only.
function GetControlTypes() {
    if (typeof m_htControlTypes != "object") {
        m_htControlTypes = InitializeControlTypeArray(m_htControlTypes);
    }
    return m_htControlTypes;
}
//Accessor for Reference Drop down list.
var m_ddlCurrent;
var m_pagCurrent;
function GetCurrentDDL() {
    return m_ddlCurrent;
}
function SetCurrentDDL(ddlRef) {
    m_ddlCurrent = ddlRef;
}
function GetCurrentPage() {
    return m_pagCurrent;
}
function SetCurrentPage(sPage) {
    m_pagCurrent = sPage;
}
//--> end accessor
//Initialize advance search related params
function ADVSearchInit(ddlRef, sPage) {
    SetCurrentDDL(ddlRef);
    SetCurrentPage(sPage);
}

// Adding criteria on clicking add button script.
function OnAddingCriteria(val1, val2, val3) {

    var curDDL = GetCurrentDDL(); //TODO : Don't forgot to verify the DDL later.
    //Not add on added items.
    if (curDDL[0][curDDL[0].selectedIndex].disabled != true) {
        var infos = curDDL.val().split(",");
        var TypeAbbre = infos[enumInfo.ControlTypeAbbre];
        var Name = infos[enumInfo.Name];
        var Label = infos[enumInfo.Name].camelCaseToStringWithSpacesTest();
        Label = Label.replace("("," (");
        curDDL[0][curDDL[0].selectedIndex].disabled = "disabled";  //Disable selected option.
        var ctrlType = GetControlTypes()[TypeAbbre];
        var curPanel = document.getElementById('pnlAdvanceCriteria');

        addControl(curPanel, curDDL, ctrlType, Name, Label, val1, val2, val3);
        return true;
    }
    return false;
}
// Load Advance Search
function LoadAdvanceSearch(postText, sSearchFilterFields) {

    var queries = postText.split("|");
    var blnIsProcess = false;

    for (var i = 0; i < queries.length; i += 2) {
        if (sSearchFilterFields.indexOf(queries[i]) == -1) //control not found on default
        {
            var curName = queries[i];
            var nextName;

            // If dropdown add the field to check loaded (before filters are applied)
            // add all dropdown control name to make sure all dropdown has filtering
            if (curName.substr(0, 3) === ControlType.DROP_DOWN.Prefix) {
                $('#hiddenLastFieldToCheckLoaded').val($('#hiddenLastFieldToCheckLoaded').val() + ',' + curName);
            }

            //Checking for 2/3 value
            if (curName.substr(curName.length - 4, curName.length).indexOf('From') == -1) {             //Didn't have control DateFrom.
                if (curName.substr(curName.length - 2, curName.length).indexOf('To') != -1) {
                    nextName = queries[i + 2];
                    if (i + 2 < queries.length && nextName.substr(nextName.length - 6, nextName.length).indexOf('Status') != -1) {
                        //To & Status
                        AddCriteria(queries[i], null, queries[i + 1], queries[i + 3]);
                        i += 2;
                    }
                    else {
                        //To only.
                        AddCriteria(queries[i], null, queries[i + 1], null);
                    }
                }
                else {
                    if (curName.substr(curName.length - 6, curName.length).indexOf('Status') != -1) {
                        //Status Only.
                        AddCriteria(queries[i], null, null, queries[i + 1]);
                    }
                    else {
                        //Other single value.
                        AddCriteria(queries[i], queries[i + 1], null, null);
                    }
                }
            }
            else {
                nextName = queries[i + 2];
                //May be Having 2/3 value finding match value.
                if (i + 2 < queries.length && nextName.substr(nextName.length - 2, nextName.length).indexOf('To') != -1) {
                    //From & To
                    var lastName = queries[i + 4];
                    if (i + 4 < queries.length && lastName.substr(lastName.length - 6, lastName.length).indexOf('Status') != -1) {
                        //From & To & Status  aka. DateTime with APN Status.
                        AddCriteria(queries[i], queries[i + 1], queries[i + 3], queries[i + 5]);
                        i += 4;
                    }
                    else {
                        AddCriteria(queries[i], queries[i + 1], queries[i + 3], null);
                        i += 2;
                    }

                }
                else if (i + 2 < queries.length && nextName.substr(nextName.length - 6, nextName.length).indexOf('Status') != -1) {
                    //From & Status
                    AddCriteria(queries[i], queries[i + 1], null, queries[i + 3]);
                    i += 2;
                }
                else {// Only From
                    AddCriteria(queries[i], queries[i + 1], null, null);
                }
            }
            blnIsProcess = true;
        }
    }
    return blnIsProcess;
}
//
// Add Criteria
function AddCriteria(ctrlName, Value, Value2, Value3) {
    var curDDL = GetCurrentDDL()[0];

    //                                var fieldType = ctrlName.substr(0, 3);
    var fieldName = new String(ctrlName.substr(3, ctrlName.length));
    var fieldNameWithoutStatus = fieldName;
    if (Value != null) { //&& (Value2 != null || Value3 != null)
        if (fieldName.substr(fieldName.length - 4, fieldName.length).indexOf('From') != -1) {
            fieldName = new String(fieldName.substr(0, fieldName.length - 4));

        }
    } else if (Value2 != null) {
        if (fieldName.substr(fieldName.length - 2, fieldName.length).indexOf('To') != -1) {
            fieldName = new String(fieldName.substr(0, fieldName.length - 2));

        }
    }
    else {
        if (fieldName.substr(fieldName.length - 6, fieldName.length).indexOf('Status') != -1) {
            fieldName = new String(fieldName.substr(0, fieldName.length));
            fieldNameWithoutStatus = new String(fieldName.substr(0, fieldName.length - 6));
        }
    }

    //Separate condition to set criteria dropdown and add control in case not found dropdown status will search dropdown name with out status
    var bIsAddControl = false;
    for (var optionIndex = 0; optionIndex < curDDL.length; optionIndex++) {
        var currOption = curDDL.options[optionIndex];
        var infos = currOption.value.split(",");
        if ((infos[enumInfo.Name] == fieldName)) {
            curDDL[optionIndex].selected = "selected";
            OnAddingCriteria(Value, Value2, Value3);
            bIsAddControl = true;
        }

    }

    if (!bIsAddControl) {
        for (var optionIndex = 0; optionIndex < curDDL.length; optionIndex++) {
            var currOption = curDDL.options[optionIndex];
            var infos = currOption.value.split(",");
            if ((infos[enumInfo.Name] == fieldNameWithoutStatus)) {
                curDDL[optionIndex].selected = "selected";
                OnAddingCriteria(Value, Value2, Value3);
            }

        }
    }
    return true;
}
//-->
/*/ADV Search Related Utilities*/

