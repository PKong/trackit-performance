/*********************
* Saved Filters - JS
**********************/

/** Saved Filters **/

    /** Constants **/
    var SavedFilterConstants = {
        dialogWidth: 520,
        dialogIframeMinWidth: 480,
        dialogIframeMinHeight: 220,
        filtersQs: "filter",
        listViewQs: "listView",
        urlQs: "url",
        buttonCreateNew: "Create New Saved Filter",
        modalSaveFilterManagePageUrl: "/Account/SavedFiltersManage.aspx",
        messageNoFiltersToSave: "You have no filters applied to save!",
        messageValidateCreateNewSavedFilterError: "Please enter a Filter Name!",
        messageFilterNameExists: "A Saved Filter with this name already exists!",
        messageSavedFilterCreated: "Success: your new Saved Filter has been saved.",
        listViews:
        {
            leaseApps:
            {
                name: "Lease Applications",
                url: "/LeaseApplications/"
            },
            towerMods:
            {
                name: "Tower Modifications",
                url: "/TowerModifications/"
            },
            sites:
            {
                name: "Sites",
                url: "/Sites/"
            },
            sdp:
            {
                name: "Site Data Packages",
                url: "/SiteDataPackages/"
            },
            issueTracker:
            {
                name: "Issue Tracker",
                url: "/IssueTracker/"
            },
            LeaseAdminTracker:
            {
                name: "Lease Admin Tracker",
                url: "/LeaseAdminTracker/"
            }
        }
    };
    /** /Constants **/

    // Setup Saved Filters Modal
    function setupSavedFilterModal(pageTitle, listPageName, url) {

        // Create New Dialog
        var optionsCreate = {
            autoOpen: false,
            width: SavedFilterConstants.dialogWidth,
            modal: true,
            title: pageTitle,
            open: function (event, ui) {
                $('#modal-loading-SavedFilterManage').show();
                $('#modalIframeIdSavedFilterManage').load(function () {
                    $('#modal-loading-SavedFilterManage').fadeOut();
                });
            }
        };
        //
        $("#modalExternalSavedFilterManage").dialog(optionsCreate);
        //
        $('#modalBtnExternalSaveFilter').click(function () {
            showSavedFilterManageDialog(listPageName, savedFiltersQueryString, url);
        });
    }
    //-->

    // Show Saved Filter Manage Dialog
    function showSavedFilterManageDialog(listPageName, filter, url) {

        //alert(listPageName + " : " + filter);

        $("#modalExternalSavedFilterManage").html('<iframe id="modalIframeIdSavedFilterManage" scrolling="no" style="width:100%;height:auto;overflow:hidden;min-height:' + SavedFilterConstants.dialogIframeMinHeight + 'px;min-width:' + SavedFilterConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-SavedFilterManage">' + ListViewConstants.modalLoadingMessage + '</div>').dialog("open");
        $("#modalIframeIdSavedFilterManage").attr("src", (SavedFilterConstants.modalSaveFilterManagePageUrl + "?" + SavedFilterConstants.listViewQs + "=" + escape(listPageName) + "&" + SavedFilterConstants.filtersQs + "=" + escape(filter) + "&" + SavedFilterConstants.urlQs + "=" + escape(url)));
        return false;
    }
    //-->

    // Validate Create New Saved Filter
    function validateCreateNewSavedFilter() {

        var sErrorMessage = "";
        var check = true;
        var sTitle = "";
        var sfilter = $.urlParam(SavedFilterConstants.filtersQs);
        var sListView = $.urlParam(SavedFilterConstants.listViewQs);
        var sUrl = $.urlParam(SavedFilterConstants.urlQs);

        if (String(sfilter).isNullOrEmpty()) {
            sErrorMessage = SavedFilterConstants.messageNoFiltersToSave;
            check = false;
        } else if (!validateRequiredField("#txtFilterName")) {
            check = false;
            sErrorMessage = SavedFilterConstants.messageValidateCreateNewSavedFilterError;
        }

        //
        if (check) {

            hideMessageAlert();
            sTitle = $("#txtFilterName").val();

            // Lease Apps - Types
            var pc = parent.$.urlParam(ConstGlobal.GENERAL.projectCategoryQueryString);
            pcFilterValue = "";
            if (!String(pc).isNullOrEmpty()) {
                pcFilterValue = ConstGlobal.GENERAL.projectCategoryQueryString + "=" + pc;
            }

            $.post("/Account/SavedFiltersManage.aspx", { title: sTitle, url: sUrl, filter: sfilter, listView: sListView, pcFilter: pcFilterValue },
            function (data) {
                if (!String(data).isNullOrEmpty()) {
                    if (data == "success") {
                        // Success
                        hideMessageAlert();
                        showMessageAlert(ConstMessageTypes.SUCCESS.type, SavedFilterConstants.messageSavedFilterCreated, false);
                        $("#savedFilterForm").hide();
                        $("#closeWindowDiv").show();
                    } else if (data == "exists") {
                        // Exists
                        sErrorMessage = SavedFilterConstants.messageFilterNameExists;
                        hideMessageAlert();
                        showMessageAlert(ConstMessageTypes.ERROR.type, sErrorMessage, false);
                    }
                }
            });
        } else {
            hideMessageAlert();
            showMessageAlert(ConstMessageTypes.ERROR.type, sErrorMessage, false);
        }

        return false; // check;
    }
    //-->

/** /Saved Filters **/

