﻿

// Hide/Show application
function OnHiddenApplication(tmoid, obj) {
    // Load JSON
    var delDialogHtml;

    delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Hide this item?"><p><span class="ui-icon ui-icon-alert"' +
    'style="float:left; margin:0 7px 20px 0;"></span>This application will be hidden <b>for all users</b> and will not be shown again. There is no unhide.</p></div>';

    $(delDialogHtml).dialog({
        buttons: {
            'Confirm': function () {
                var ajax_url = "/ImportApplicationsList.axd?tmoid=" + tmoid + "&hidden=" + $(obj).text(); ;
                $.ajax({
                    dataType: 'json',
                    url: ajax_url,
                    cache: false,
                    success: function (data) {
                        $(obj).parent().parent().parent().remove();
                    }
                });
                $(this).dialog('close');
                return false;
            },
            'Cancel': function () {
                $(this).dialog('close');
                return false;
            }
        },
        width: 360,
        heigth: 100,
        position: ['top', 70]
    });
}

// Flexigrid
//
function fgBeforeSendData(data) {

    // Global show none associate
    var gs = $.urlParam(GlobalSearch.searchQueryString);
    addGlobal:
    if (!String(gs).isNullOrEmpty()) {
        for (var i = 0; i < arrayOfFilters.length; i++) {
            if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
        }
        addFilterPostParam(GlobalSearch.searchQueryString, gs);
    }

    // Project Category
    var pc = $.urlParam(ConstGlobal.GENERAL.projectCategoryQueryString);

    //
    addCategory:
    if (!String(pc).isNullOrEmpty()) {
        for (i = 0; i < arrayOfFilters.length; i++) {
            if (arrayOfFilters[i].name == ConstGlobal.GENERAL.projectCategoryQueryString) break addCategory;
        }
        addFilterPostParam(ConstGlobal.GENERAL.projectCategoryQueryString, pc);
    }

    // Search Filter Data
    sendSearchFilterDataToHandler(data);
}


//FG Data Load
//
function fgDataLoad(sender, args) {

    // Hide - Loading Spinner 
    $('.h1').activity(false);

}

//FG Row Click
//
function fgRowClick(sender, args) {

    return false;
}

//FG No Data
//
function fgNoData() {

    // Hide - Loading Spinner 
    $('.h1').activity(false);
}

//FG Row Render
//
function fgRowRender(tdCell, data, dataIdx) {

    var cVal = new String(data.cell[dataIdx]);

    switch (Number(dataIdx)) {
        // Standard String    
        default:
            if (cVal.isNullOrEmpty()) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {
                tdCell.innerHTML = cVal;
            }
            break;
    }
}