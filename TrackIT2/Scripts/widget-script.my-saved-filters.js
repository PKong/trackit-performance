/************************************
* Widget Script - My Saved Filters
*************************************/

// On Create / Load 
$(function () {

    $('.deletable tbody td:last-child').wrapInner('<div class="widget-td-wrapper" style="width:100%; height:100%; position:relative;"></div>');
    $('.deletable tbody .widget-td-wrapper').append('<a class="remove-entry-button" href="#">Delete</a>');

    $('.deletable tbody tr').mouseenter(function () {
        $('.remove-entry-button', this).show();
    }).mouseleave(function () {
        $('.remove-entry-button').hide();
    });

    $(".remove-entry-button").bind("click", function (e) {
        return handleRemoveSavedFilter(this);
    });

});
//-->

// handle Remove Saved Filter
function handleRemoveSavedFilter(item) {

    var filterTitle = $(item).parent().parent().parent().find("td").eq(0).find("a").text();
    var delDialogHtml = '<div id="dialog-confirm-saved-filter-delete" title="Delete this Saved Filter?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This Saved Filter will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
    
    $(delDialogHtml).dialog({
        open: function (type, data) {
            $(this).parent().appendTo("form[0]");
        },
        buttons: {
            'Confirm': function () {

                var target = $(this);

                $.post("/Account/SavedFiltersManage.aspx", { title: filterTitle, action: "delete" },
                function (data) {
                    if (!String(data).isNullOrEmpty()) {
                        if (data == "success") {
                            // Success
                            target.dialog('destroy');
                            $(item).parent().parent().parent().remove();
                        }
                    }
                });

                return true;
            },
            'Cancel': function () {
                $(this).dialog('destroy');
                return false;
            }
        }
    });
    $('#dialog-confirm-saved-filter-delete').dialog('open');
}
//-->

