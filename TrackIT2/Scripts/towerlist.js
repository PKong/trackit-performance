﻿/** Dialog **/

/** Constants **/
var TowerListConstants = {
    TowerListHandler: "/Handlers/TowerList.axd",
    dialogWidth: 780,
    dialogIframeMinWidth: 740,
    dialogIframeMinHeight: 500,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "Add.aspx"
};
var towerListID = 0;
/** Constants **/

// Setup List Create Modal
function setupTowerListCreateModal(title, id) {
    towerListID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: TowerListConstants.dialogWidth,
        height: TowerListConstants.dialogIframeMinHeight,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeId').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    //
    $("#modalExternal").dialog(optionsCreate);
    //
    $('#modalBtnExternal').click(function () {
        showTowerListCreateDialog();
    });
}
//-->

// Show Create Dialog
function showTowerListCreateDialog() {

    $("#modalExternal").html('<iframe id="modalIframeId" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:' + TowerListConstants.dialogIframeMinHeight + 'px;min-width:' + TowerListConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading">' + TowerListConstants.modalLoadingMessage + '</div>').dialog("open");
    $("#modalIframeId").attr("src", TowerListConstants.modalAddPageUrl + "?id=" + towerListID);
    return false;
}
//-->

// Flexigrid
//
// FG Before Send Data
function fgBeforeSendData(data) {

    // Show - Loading Spinner
    $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

    // disable export button
    $("#MainContent_exportButton").val("Processing...");
    $("#MainContent_exportButton").attr("disabled", true);

    // Search Filter Data
    sendSearchFilterDataToHandler(data);
}
//
// FG Data Load
function fgDataLoad(sender, args) {

    // Hide - Loading Spinner 
    $('#h1').activity(false);
    $('#modal-init-page').fadeOut();

    // Enable export button
    $("#MainContent_exportButton").val("Export to Excel");
    $('#MainContent_exportButton').removeAttr("disabled");

    // Workflow Button
    $('.tower-status-button').click(function () {

        var a = $(this).parent().find('.tower-status-menu');
        $('.tower-status-menu').not(a).slideUp();
        $(a).slideToggle('fast');

        $(a).focusout(function () { $('.tower-status-menu').slideUp(); });
        $(this).parent().find('.statusLink').click(function () {
            OnNewStatusClick(this);
        });

    }).focusout(function () {
        setTimeout(function () {
            $('.tower-status-menu').slideUp()
        }, 100);
    });
}
//
// FG Row Click
function fgRowClick(sender, args) {

    return false;
}
//
// FG Row Render
function fgRowRender(tdCell, data, dataIdx) {

    var cVal = new String(data.cell[dataIdx]);

    switch (Number(dataIdx)) {
        // Standard String   
        default:
            if ((cVal.isNullOrEmpty()) || (cVal == "<undefined>")) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {
                tdCell.innerHTML = cVal;
            }
            break;
    }
    //>
}
//
// FG No Data
function fgNoData() {

    // Hide - Loading Spinner 
    $('#h1').activity(false);

    // Disable export button
    $("#MainContent_exportButton").val("Export to Excel");
    $("#MainContent_exportButton").attr("disabled", true);
}
//-->

function setupTowerList() {
    $('#modal-init-page').show();

    $("input:radio").removeAttr("checked");

    $("#trackitOn").button();
    $("#trackitOff").button();
    $("#mapOn").button();
    $("#mapOff").button();


    // Disable export button
    $("#MainContent_exportButton").attr("disabled", true);
}

$("#txtSearch").live("keypress", function (e) {
    if (e.which == 13) {
        ClearFilterParam();
        GetFilterParam();
        reloadDataGrid();
        $('#modal-init-page').fadeOut();
        e.preventDefault();
    }
});

function SetupFilter() {

    $('.filter-checkbox').click(function () {
        ClearFilterParam();

        if ($(this).hasClass('alreadyCheck')) {
            $(this).prop('checked', false);
            $(this).removeClass('alreadyCheck');
            var clickBtn = $(this).parent().find("label[for='" + this.id + "']"); //.removeClass('TowerClick');
            clickBtn[0].className = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
            $(clickBtn).attr("aria-pressed", "false");
            $(clickBtn).removeClass('TowerClick');
        }
        else {
            $(this).parent().find('input').each(function () {
                if (this.checked) {
                    $(this).addClass('alreadyCheck');
                    $(this).parent().find("label[for='" + this.id + "']").addClass('TowerClick');
                }
                else {
                    $(this).removeClass('alreadyCheck');
                    $(this).parent().find("label[for='" + this.id + "']").removeClass('TowerClick');
                }
            });
        }

        GetFilterParam();
        reloadDataGrid();

        $('#modal-init-page').fadeOut();
    });

}

function GetFilterParam() {
    var trackitOn = $('#trackitOn').is(':checked');
    var trackitOff = $('#trackitOff').is(':checked');
    var mapOn = $('#mapOn').is(':checked');
    var mapOff = $('#mapOff').is(':checked');
    var searchValue = $("#txtSearch").val();

    if (searchValue != '') {
        addFilterPostParam('TowerListSearch', searchValue);
    }

    if (trackitOn) {
        addFilterPostParam('TrackiTOn', trackitOn);
    }
    if (trackitOff) {
        addFilterPostParam('TrackiTOff', trackitOff);
    }
    if (mapOn) {
        addFilterPostParam('MapOn', mapOn);
    }
    if (mapOff) {
        addFilterPostParam('MapOff', mapOff);
    }
}

function ClearFilterParam() {
    removeFilterPostParam('TowerListSearch');
    removeFilterPostParam('TrackiTOn');
    removeFilterPostParam('TrackiTOff');
    removeFilterPostParam('MapOn');
    removeFilterPostParam('MapOff');
}

function OnNewStatusClick(selectObj) {
    var type = $(selectObj).attr('type');
    var siteId = $(selectObj).attr('siteid');
    var newstatus = $(selectObj).attr('newstatus');
    var isSuccess = false;
    var backUpSelectObj = selectObj;

    var data = PrepareData(selectObj, type, siteId, newstatus);
    if (data.length == 5 && data[4]) {
        CallHandler(type, siteId, newstatus, data, selectObj, data[0]);
    }
}

function PrepareData(selectObj, type, siteId, newstatus) {
    var isSuccess = false;
    var returnResult = new Array();
    var currentStatus = $('span[current-text-' + type + '="' + siteId + '"]');
    var currentImg = $('img[current-img-' + type + '="' + siteId + '"]');
    try {
        var selectStatus = $(selectObj)[0].childNodes[0];
        var selectImg = $(selectObj).find('img')[0].src;

        var tmpCurrentStatus = $(currentStatus[0]).text();
        var tmpCurrentImgSrc = currentImg[0].src;

        returnResult.push(tmpCurrentStatus);
        returnResult.push(tmpCurrentImgSrc);
        returnResult.push(newstatus);
        returnResult.push(selectImg);
        returnResult.push(true);
    }
    catch (err) {
        returnResult = new Array();
        returnResult.push(false);
    }
    return returnResult;
}

function ChangeUI(selectObj, type, siteId, newstatus, uiData) {
    var isSuccess = false;
    var returnResult = new Array();
    var currentStatus = $('span[current-text-' + type + '="' + siteId + '"]');
    var currentImg = $('img[current-img-' + type + '="' + siteId + '"]');
    var selectStatus = uiData[2];
    var selectImg = uiData[3];

    try {
        if (currentStatus.length > 0 && currentImg.length > 0) {
            
            if (selectStatus == '') {
                $(currentImg)[0].style.cssText = ''
            }

            currentStatus = currentStatus[0];
            currentImg = currentImg[0];

            // hide select li , display old status li
            var selectLi = $("li[siteid='" + siteId + "'][status='" + selectStatus + "'][type='" + type + "']");
            var oldLi = $("li[siteid='" + siteId + "'][status='" + currentImg.alt.trim() + "'][type='" + type + "']");
            if (selectLi.length > 0 && oldLi.length > 0) {
                currentImg.alt = selectStatus;
                currentImg.src = selectImg;
                $(currentStatus).text(selectStatus);

                $(selectLi[0]).css('display','none')
                $(oldLi[0]).css('display', 'block')
            }
            isSuccess = true;
        }
    }
    catch (err) {
        isSuccess = false;
    }

    return isSuccess;
}

function CallHandler(type, siteId, newstatus, uiData, selectObj, CurrentStatus) {
    var isSuccess = false;
    var actions = '?siteId=' + siteId + '&newstatus=' + newstatus + '&type=' + type + '&currentstatus=' + CurrentStatus;

    var messageError = "An error occured updating the status. Please try again.";

    $.ajax({
        type: 'POST',
        url: TowerListConstants.TowerListHandler + actions,
        async: false,
        dataType: 'json',
        success: function (data) {
            if (data.rows.length > 0) {
                if (data.rows[0].cell[0] != "error") {
                    if (ChangeUI(selectObj, type, siteId, newstatus, uiData)) {
                        isSuccess = true;
                        $('.tower-status-menu').slideUp()
                    }
                }
                else {
                    messageError = data.rows[0].cell[1];
                }
            }

            if (!isSuccess) {
                UpdateTowerListDialog('Fail', messageError);
            }
        },
        error: function (error) {
            UpdateTowerListDialog('Fail', messageError);
        }
    });
    return isSuccess;
}

function UpdateTowerListDialog(title, message) {
    var delDialogHtml = '<div id="dialog-message" title="' + title + '">' +
                        '<p>' + message + '</p></div>';
    $('#dialog-message').remove();
    $(delDialogHtml).dialog({ close: function (event, ui) { $('.tower-status-menu').slideUp(); } });
    $('#dialog-message').dialog('open');
}

//--------------------Error Message--------------------------------//
function showTowerListMessageAlertByName(type, message, Name) {
    if ((type == null && message == null) || (type == "" && message == "")) {
        type = ConstMessageTypes.INFO.type;
        message = ConstMessageTypes.INFO.defaultMessage;
    }

    var alreadyIncreaseSize = false;
    if ($(".ms").length > 0 || $("#HiddenDialogHeightFlag").val() == "true") {
        alreadyIncreaseSize = true;
        $("#HiddenDialogHeightFlag").val("true")
    }
    else {
        alreadyIncreaseSize = false;
        $("#HiddenDialogHeightFlag").val("false")
    }

    $(Name).find(".ms").each(function () {
        $(this).remove()
    });

    $(Name).append(
            '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                '<p>' + message + '</p>' +
		        '<a class="close" href="javascript:void(0)">close</a>' +
            '</div>' +
            '<div class="cb"></div>'
    );

    $("#HiddenDialogHeightFlag").val("true")

    $('.close').click(function () {
        var msgalert = $(this).parent();
        var fHeight = $(Name).height();
        $("#HiddenDialogHeightFlag").val('false');
        $(msgalert).fadeOut('slow', function () {
            //resize dailog when show error message
            $(".ms").remove();
            setTowerListDefaultSize();
        });
    });
    //resize dailog when show error message
    var fHeight = $(Name).height();
    if (!alreadyIncreaseSize) {
        resizeModal(fHeight+20);
    }
}
//--------------------Error Message--------------------------------//

function setTowerListDefaultSize() {
    parent.$('iframe').height(TowerListConstants.dialogIframeMinHeight);
    parent.$('iframe').parent().height(TowerListConstants.dialogIframeMinHeight);
    parent.$('iframe').parent().parent().height(TowerListConstants.dialogIframeMinHeight);
}