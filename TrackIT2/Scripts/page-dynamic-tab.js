﻿//Control References
var TabsReferences = {};

//Initialize method
function InitTab(sTabName, refLabelCurrentPage, refLabelTotalPage, refBtnAdd, refBtnNext, refBtnPrev, refHidJSONData
                ,refHidJSONCount,refDivSlideShow,IsReadOnly) {

    if ($(refHidJSONData).val() != null && $(refHidJSONData).val() != "") { 
        TabsReferences[sTabName] = {
            ControlReferences: {
                ctrlLabelCurrentPage: refLabelCurrentPage,
                ctrlLabelTotalPage: refLabelTotalPage,
                ctrlBtnAdd: refBtnAdd,
                ctrlBtnNext: refBtnNext,
                ctrlBtnPrev: refBtnPrev,
                ctrlHidJSONData: refHidJSONData,
                ctrlHidJSONCount: refHidJSONCount,
                ctrlDivSlideShow: refDivSlideShow
            },
            JSONData: $(refHidJSONData).val(),
            JSONDllItemsData: null,
            IsReadOnly: IsReadOnly,
            IsInitial: document.getElementById("div" + sTabName + "Mod") == null,
            CurrentIndex: 0,
            OtherMetadata: null
        };
        //Initialize button
        $(TabsReferences[sTabName].ControlReferences.ctrlBtnNext).button();
        initEventClick(TabsReferences[sTabName].ControlReferences.ctrlBtnNext, sTabName, true);
        $(TabsReferences[sTabName].ControlReferences.ctrlBtnPrev).button();
        initEventClick(TabsReferences[sTabName].ControlReferences.ctrlBtnPrev, sTabName, false);
    }
}
function CreateNewTab(sTabName) {
    if (TabsReferences[sTabName] == null) {
        TabsReferences[sTabName] = TabTemplate;
    }
}
function initEventClick(sBtnName,sTabName,IsNext) {
    $(sBtnName).unbind('click');
    $(sBtnName).click(function () {
        BindPageData(sTabName, IsNext);
    });
}
function BindingData() {
    for (var tab in TabsReferences) {
        BindPageData(tab);
    }
}

function BindPageData(tabName,IsNext) {
    var ctrlPrefix = "";
    var ctrlSubfix = "init";
    var JSONData = null;
    var iPageMultiply = 1;
    var currentTab = null;
    var currentObj = null;
    var index = 0;
    var groupID = "";
    
    currentTab = TabsReferences[tabName];
    ctrlPrefix = (currentTab.IsReadOnly ? "lab" : "txt");
    ctrlSubfix = "init";
    var TotalPage = $(currentTab.ControlReferences.ctrlHidJSONCount).val();
    iPageMultiply = 1;
    if (!currentTab.IsInitial && tabName != "Gsp") {
        iPageMultiply = 2;
    }
   
    if(IsNext != null) {
        if (IsNext  ) {
            if ((currentTab.CurrentIndex + 1) <= TotalPage - 1) {
                currentTab.CurrentIndex = currentTab.CurrentIndex + 1;
            }
            else {
                //Exceed limit. Nothing to do here.
                return;
            }
        }
        else {
            if ((currentTab.CurrentIndex - 1) >=0 ) {
                currentTab.CurrentIndex = currentTab.CurrentIndex - 1;
            }
            else {
                //lower limit. Nothing to do here.
                return;
            }   
        }
    }
    index =  currentTab.CurrentIndex * iPageMultiply
    JSONData = GetAllDynamicJsonData(currentTab.JSONData);
    currentObj = JSONData[index];

    //hide and reset height antena line type
    if (tabName == "Ant") {
        HideAllLineType(JSONData, index);
    }

    //hide gps row when location not 'Tower'
    if (tabName == "GPS") {
        HideGPSDetail(currentObj, JSONData, index);
    }

    if (!currentTab.IsInitial) {
        if (currentObj.IsExist) {
            //Existing data
            var tmpDimension = "";
            var tmpMountingDimensions

            //check tab for display typeLine
            if(currentObj.hasOwnProperty("AllLineCount"))
            {
                for (x = 0; x < currentObj["AllLineCount"]; x++) {
                    $("#MainContent_AntPnLine_" + currentObj["ID"] + "_" + x).css("display", 'block')
                }
            }

            for (var key in currentObj) {
                if (currentObj.hasOwnProperty(key)) {
                    if (ctrlPrefix == "lab") {
                        if (currentObj[key] != null) {
                            if (key.match(/Dimension/g)) {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'inline')
                            }
                            else {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                            }
                        }
                        else {
                            if (key.match(/Dimension/g)) {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'none')
                            }
                            else {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                            }
                        }
                    }
                    else {
                        //Need implement.
                    }
                }
            }
            //Proposed data

            if ((currentObj.EquipmentGroup != "") && JSONData[(currentTab.CurrentIndex * iPageMultiply) + 1].EquipmentGroup == currentObj.EquipmentGroup
               && !JSONData[(currentTab.CurrentIndex * iPageMultiply) + 1].IsExist) {
                ctrlSubfix = "mod";
                currentObj = JSONData[(currentTab.CurrentIndex * iPageMultiply) + 1];
                //Binding Modification
                if (currentObj.hasOwnProperty("AllLineCount")) {
                    for (x = 0; x < currentObj["AllLineCount"]; x++) {
                        $("#MainContent_AntPnLine_" + currentObj["ID"] + "_" + x).css('display', 'block')
                    }
                }
                for (var key in currentObj) {
                    if (currentObj.hasOwnProperty(key)) {
                        if (ctrlPrefix == "lab") {
                            if (currentObj[key] != null) {
                                if (key.match(/Dimension/g)) {
                                    $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                                    $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'inline')
                                }
                                else {
                                    $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                                }
                            }
                            else {
                                if (key.match(/Dimension/g)) {
                                    $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                                    $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'none')
                                }
                                else {
                                    $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                                }
                            }
                        }
                        else {
                            //Need implement.
                        }
                    }
                }
            }
        }
        else {
            //Proposed data
            ctrlSubfix = "mod";
            for (var key in currentObj) {
                if (currentObj.hasOwnProperty(key)) {
                    if (ctrlPrefix == "lab") {
                        if (currentObj[key] != null) {
                            if (key.match(/Dimension/g)) {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'inline')
                            }
                            else {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                            }
                        }
                        else {
                            if (key.match(/Dimension/g)) {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'none')
                            }
                            else {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                            }
                        }
                    }
                    else {
                        //Need implement.
                    }
                }
            }
        }
    }
    else {
        //check tab for display typeLine
        if (currentObj.hasOwnProperty("AllLineCount")) {
            for (x = 0; x < currentObj["AllLineCount"]; x++) {
                $("#MainContent_AntPnLine_" + currentObj["ID"] + "_" + x).css("display", 'block')
            }
        }

        for (var key in currentObj) {
            if (currentObj.hasOwnProperty(key)) {
                if (ctrlPrefix == "lab") {
                    if (tabName == "Gsp") {
                        if (currentObj[key] != null) {
                            $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                        }
                        else {
                            $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                        }
                    }
                    else {
                        if (currentObj[key] != null) {
                            if (key.match(/Dimensions/g)) {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'inline')
                            }
                            else {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text(currentObj[key]);
                            }
                        }
                        else {
                            if (key.match(/Dimensions/g)) {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix + "_after").css("display", 'none')
                            }
                            else {
                                $("#" + ctrlPrefix + tabName + key + "_" + ctrlSubfix).text("");
                            }
                        }
                    }
                }
                else {
                    //Need implement.
                }
            }
        }
    }

    //Set page
    if (TotalPage > 1) {
        $(currentTab.ControlReferences.ctrlLabelCurrentPage).text(currentTab.CurrentIndex + 1);
        $(currentTab.ControlReferences.ctrlLabelTotalPage).text(TotalPage);
    }
    else {
        if (tabName != "Gsp") {
            $("#" + tabName + "PageCtrl").hide();
        }
    }

    $(currentTab.ControlReferences.ctrlDivSlideShow).show("slide", { direction: "right" }, 220);
    if (tabName == "Ant") {
        //increase antena panel height
        SetDivHeight(JSONData, index)
    }
}



function GetAllDynamicJsonData(sJSONData) {
    var data = null;
    if (sJSONData != "" &&  sJSONData != null) {
        data = jQuery.parseJSON(sJSONData);
    }
    return data;
}

//Hide summary and editButton
function HideSummary() {
    if (document.getElementById) {
        document.getElementById("summary").style.display = "none";
        document.getElementById("summaryLi").style.display = "none";
        document.getElementById("modalBtnSummary").style.display = "none";
    }
}

function HideSummaryOnly() {
    if (document.getElementById) {
        document.getElementById("summary").style.display = "none";
        document.getElementById("summaryLi").style.display = "none";
    }
}

function DisplayEquipHistory() {
    $('#equipmentHistory').css('display', 'inline-block');
}

//Hide commentDiv
function HideCommentDiv() {
    if (document.getElementById) {
        document.getElementById("commentDiv").style.visibility = 'hidden';
        obj = document.getElementById("divNoData");
        obj.style.display = "block";
    }
}

//Hide divNoData
function HideDivNoData() {
    if (document.getElementById) {
        obj = document.getElementById("commentDiv");
        obj.style.display = "block";
        obj = document.getElementById("divNoData");
        obj.style.display = "none";
    }
}

//TransmissionLine table create
function ResetHeight() {
    $("#sliAnt").height(460)    //minimum height of sliAnt = 460px
}
function HideAllLineType(jsonData,index) {
    var lenget = jsonData.length;
    ResetHeight();
    //hide all panel
    for (x = 0; x < lenget; x++) {
        var allObj = jsonData[x];
        if (allObj.hasOwnProperty("AllLineCount")) {
            for (inObj = 0; inObj < allObj["AllLineCount"]; inObj++) {
                $("#MainContent_AntPnLine_" + allObj["ID"] + "_" + inObj).css("display", 'none')
            }
        }
    }
}
function SetDivHeight(jsonData, index)
{
    //set height for div
    currentObj = jsonData[index];
    if (currentObj.hasOwnProperty("AllLineCount")) {
        for (x = 0; x < currentObj["AllLineCount"]; x++) {
            IncreaseDivHeight();
        }
    }
}
function IncreaseDivHeight() {
    $("#sliAnt").height($("#sliAnt").height() + 200);
}

function HideGPSDetail(currentObj, tmpJsondata, index) {
    if (currentObj["TMO_AppID"].match("-M-")) {
        var tmpCurrentObj = tmpJsondata[index + 1];
        if (currentObj["InstalledLocation"] == "Tower") {
            if (currentObj["IsExist"] == true) {
                $("#TowerRad_init").css('display', '');
                $("#TowerWeight_init").css('display', '');
                $("#TowerDimensions_init").css('display', '');
            }
            else {
                $("#TowerRad_mod").css('display', '');
                $("#TowerWeight_mod").css('display', '');
                $("#TowerDimensions_mod").css('display', '');
            }
        }
        else {
            if (currentObj["IsExist"] == true) {
                $("#TowerRad_init").css('display', 'none');
                $("#TowerWeight_init").css('display', 'none');
                $("#TowerDimensions_init").css('display', 'none');
            }
            else {
                $("#TowerRad_mod").css('display', 'none');
                $("#TowerWeight_mod").css('display', 'none');
                $("#TowerDimensions_mod").css('display', 'none');
            }
        }
        if (tmpCurrentObj["InstalledLocation"] == "Tower") {
            if (tmpCurrentObj["IsExist"] == true) {
                $("#TowerRad_init").css('display', '');
                $("#TowerWeight_init").css('display', '');
                $("#TowerDimensions_init").css('display', '');
            }
            else {
                $("#TowerRad_mod").css('display', '');
                $("#TowerWeight_mod").css('display', '');
                $("#TowerDimensions_mod").css('display', '');
            }
        }
        else {
            if (tmpCurrentObj["IsExist"] == true) {
                $("#TowerRad_init").css('display', 'none');
                $("#TowerWeight_init").css('display', 'none');
                $("#TowerDimensions_init").css('display', 'none');
            }
            else {
                $("#TowerRad_mod").css('display', 'none');
                $("#TowerWeight_mod").css('display', 'none');
                $("#TowerDimensions_mod").css('display', 'none');
            }
        }
    }
    else {
        if (currentObj["InstalledLocation"] == "Tower") {
            $("#TowerRad_init").css('display', '');
            $("#TowerWeight_init").css('display', '');
            $("#TowerDimensions_init").css('display', '');
        }
    }
}

