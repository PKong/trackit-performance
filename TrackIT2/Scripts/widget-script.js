/*****************
* Widget Script
******************/

// Globals
var user_widget_json = [];
var widget = [];
var layout;

// Doc Ready!
$(document).ready(function () {

    // Iterate and setup Widgets
    $('.widget').each(function () {

        $('.edit-links-list-item').each(function () {
            $('.edit-link-button').click(function () {
                slideInSubOptions();

                var linkTitle = $('.link-title', this).html();
                var linkDescr = $('.link-description', this).html();

                $('.sub-options .link-title-field').attr('value', linkTitle);
                $('.sub-options .link-description-field').attr('value', linkDescr);
            });
        });

        function slideInSubOptions() {
            $('.sub-options').show();
        }

        function slideOutSubOptions() {
            $('.sub-options').hide();
        }

        $('.sub-options-button-cancel, .sub-options-button-save').click(function () {
            slideOutSubOptions();
        });

        $('.edit-links-list-item .edit-link-button').click(function () {
            slideInSubOptions();

            var linkTitle = $(this).parent().parent().find('.link-title').html();
            var linkDescr = $(this).parent().parent().find('.link-description').html();

            $('.sub-options .link-title-field').attr('value', linkTitle);
            $('.sub-options .link-description-field').attr('value', linkDescr);
        });

    });
    //>

    // Widget Table
    $('.deletable tbody td:last-child').wrapInner('<div class="widget-td-wrapper" style="width:100%; height:100%; position:relative;"></div>');
    $('.deletable tbody .widget-td-wrapper').append('<a class="remove-entry-button" href="#">Delete</a>');

    $('.deletable tbody tr').mouseenter(function () {
        $('.remove-entry-button', this).show();
    }).mouseleave(function () {
        $('.remove-entry-button').hide();
    });
    //>

    // Dashboard Comments 
    $('.comment').hover(
	function () {
	    $(this).find('.comment-source-link-wrapper').show();
	},
	function () {
	    $(this).find('.comment-source-link-wrapper').hide();
	});
    //>

    // Edit Links List
    $('.edit-links-list-item .edit-link-button').click(function () {
        slideInSubOptions();

        var linkTitle = $(this).parent().parent().find('.link-title').html();
        var linkDescr = $(this).parent().parent().find('.link-description').html();

        $('.sub-options .link-title-field').attr('value', linkTitle);
        $('.sub-options .link-description-field').attr('value', linkDescr);
    });
    //>

    // Setup User Widget JSON
    setupUserWidgetJSON();

    // Drag & Drop - Widget Dropped Event Handler
    $('.widget').live('widgetDropped', function () {

        // Save Dashboard & Widgets
        saveDashboardAndWidgets();
    });
    //>

    // Delete - Widget Deleted Event Handler
    $('.widget').live('widgetDeleted', function () {

        // Save Dashboard & Widgets
        saveDashboardAndWidgets();
    });
    //>

    // Auto resize widget when the layout changes
    $('.dashboard').live('widgetOpenFullScreen widgetCloseFullScreen widgetDropped dashboardLayoutChanged', function ()
    {
        $('.widget').trigger('resize');
    });
    //>

});
//-->

// Setup User Widget JSON
function setupUserWidgetJSON() {

    var sJson = $("#hidMyWidgetJSON").val();
    var json = jQuery.parseJSON(sJson.toString());
    var index = 0;

    while (index < json.result.data.length) {
        addNewWidgetToUserWidgetJSON(json.result.data[index].title, json.result.data[index].column,
                                        json.result.data[index].id, json.result.data[index].url,
                                    json.result.data[index].editurl, json.result.data[index].open,
                                        index, json.result.data[index].metadata);
        index++;
    }
}
//-->

// Add New Widget To User Widget JSON
function addNewWidgetToUserWidgetJSON(title, column, id, url, editurl, open, index, metadata) {

    user_widget_json.push({
        "title": title,
        "column": column,
        "id": id,
        "url": url,
        "editurl": editurl,
        "open": open,
        "new_position": index,
        "double_height": false,
        "metadata": metadata
    });
}
//-->

// Return index old widget
function getIndexByID(id) {
    var i = 0;
    while (i < user_widget_json.length) {
        if (id == user_widget_json[i].id) {
            return i;
        }
        else {
            i++;
        }
    }
}
//-->

// Save Dashboard & Widgets
function saveDashboardAndWidgets(sWidgetID,sWidgetName) {

    // Setup
    $(".outputa").html('');
    var index_of_space = $(".layout").attr("class").lastIndexOf(" ");
    layout = $(".layout").attr("class").substring(index_of_space + 1);
    var layout_id;
    // Layout type?
    switch (layout) {
        case "layout-a":
            layout_id = "layout1";
            break;
        case "layout-aa":
            layout_id = "layout2";
            break;
        case "layout-ab":
            layout_id = "layout3";
            break;
        case "layout-ba":
            layout_id = "layout4";
            break;
        case "layout-aaa":
            layout_id = "layout5";
            break;
    }
    //alert(layout);
    //>

    // Reset Var
    widget = [];
    var count = 0;

    // Iterate Dashboard Columns
    $('#dashboard .column').each(function () {

        var column;
        var id;
        var title;

        if ($(this).hasClass("column-first")) {
            column = "first";
        }
        else if ($(this).hasClass("column-second")) {
            column = "second";
        }
        else if ($(this).hasClass("column-third")) {
            column = "third";
        }
        else {
            column = 'unspecified';
        }
        var IsTitleChanged = sWidgetID != undefined && sWidgetID != null && sWidgetName != undefined && sWidgetName != null;
        // Optain widget in each column
        $(this).find("div.widget").each(function () {

            id = $(this).attr("id");
            title = $(this).attr("title");
            var WidgetTitle = sWidgetName;
            var target_index = getIndexByID($(this).attr("id"));
            var double_height_flag = false;

            //////////////////////////////
            //fixed bug ie not support cast type open to boolean by Kantorn J. 2012-08-17
            var windowopen = false;
            if ($(this).find("div.widgetcontent").children().length > 0) {
                windowopen = true;
            }
            else {
                windowopen = false;
            }
            /////////////////////////////////////

            var defaults = $('#' + id + '-hfWidgetMetaDataDefaults', this).val();
            if ((defaults != null) && (defaults != '')) {
                user_widget_json[target_index].metadata = {};
                user_widget_json[target_index].metadata.defaults = defaults;
            }
            if ((IsTitleChanged && id == sWidgetID)) {
                user_widget_json[target_index].title = WidgetTitle;
            }
            else {
                WidgetTitle = user_widget_json[target_index].title;
            }

            if ($(this).height() != null && $(this).height() != undefined) {
                if ($(this).height() > 300) {
                    double_height_flag = true
                }
            }

            //Only for Recent comment
            if ($('#isAssign').length > 0) {
                if ($('#isAssign').prop('checked')) {
                    user_widget_json[target_index].editurl = 'true';
                }
                else {
                    user_widget_json[target_index].editurl = 'false';
                }
            }

            widget.push({
                'title': WidgetTitle,
                'column': column,
                'id': user_widget_json[target_index].id,
                'url': user_widget_json[target_index].url,
                'editurl': user_widget_json[target_index].editurl,
                'open': windowopen,
                'new_position': count,
                'double_height': double_height_flag,
                'metadata': user_widget_json[target_index].metadata
            });

            // UnComment this if you want to see the raw json result
            /*
            $(".outputa").append(
            widget[count].title + '<br>' +
            widget[count].column + '<br>' +
            widget[count].id + '<br>' +
            widget[count].url + '<br>' +
            widget[count].editurl + '<br>' +
            widget[count].open + '<br>' +
            widget[count].new_position + '<br>' +
            '--------------------------' + '<br>'
            );
            */

            count++;
        });
    });                 //>
    //>

    // Temp Object to Save
    var save_result = {
        'layout': layout_id,
        'data': widget
    };
    // Use 'stringify' to convert array to json form
    save_result = JSON.stringify(save_result);

    // Ajax send JSON to save layout since widget dropped
    $.post("/Account/LayoutWidget.aspx", { result: save_result }, function (data) {
        if (!String(data).isNullOrEmpty()) {
            //                if (data == "success")
            //                {
            //                    // Success
            //                    alert("Saved");
            //                }
            //                else
            //                {
            //                    alert(data);
            //                }
        }
        else {
            alert("Error");
        }
    });
    //>
}
//-->

