﻿/** Dialog **/

//--------------------Constant Parameter-------------------------//
var ListViewConstants = {
    dialogWidth: 740,
    dialogIframeMinWidth: 700,
    dialogIframeMinHeight: 520,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "Add.aspx"
};
var LeaseAdminID = 0;
var HasId = false;
//--------------------Constant Parameter-------------------------//

//--------------------Resize Modal------------------------------//
function resizeModal(fHeight) {
    originalHeight = parent.$('iframe').parent().parent().height();
    parent.$('iframe').height(originalHeight + fHeight);
    parent.$('iframe').parent().height(originalHeight + fHeight);
    parent.$('iframe').parent().parent().height(originalHeight + fHeight);
}
//--------------------Resize Modal------------------------------//

function setDefaultSize() {
    parent.$('iframe').height(400);
    parent.$('iframe').parent().height(400);
    parent.$('iframe').parent().parent().height(440);
}
//--------------------Set Size Modal------------------------------//
function setModalSize(dWidth, iframe_minWidth, iframe_minHeight) {
    ListViewConstants = null;
    ListViewConstants = {
        dialogWidth: dWidth,
        dialogIframeMinWidth: iframe_minWidth,
        dialogIframeMinHeight: iframe_minHeight,
        modalLoadingMessage: "LOADING... PLEASE WAIT",
        modalAddPageUrl: "Add.aspx"
    }
}
//--------------------Resize Modal------------------------------//

//--------------------Setup List Create Modal-------------------//
function setupListLeaseAdminCreateModal(title) {
    HasId = false;
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstants.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeId').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    $("#modalExternal").dialog(optionsCreate);
    $('#modalBtnExternal').click(function () {
        showCreateDialog();
    });
}
//--------------------Setup List Create Modal-------------------//

//----------------Setup List Create Modal with ID---------------//
function setupListLeaseAdminCreateModalWithId(title, id) {
    HasId = true;
    LeaseAdminID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstants.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeId').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    //
    $("#modalExternal").dialog(optionsCreate);
    //
    $('#modalBtnExternal').click(function () {
        showCreateDialog();
    });
}
//----------------Setup List Create Modal with ID---------------//

//----------------------Show Create Dialog---------------------//
function showCreateDialog() {
    $("#modalExternal").html('<iframe id="modalIframeId" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:' + ListViewConstants.dialogIframeMinHeight + 'px;min-width:' + ListViewConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading">' + ListViewConstants.modalLoadingMessage + '</div>').dialog("open");
    if (HasId) {
        $("#modalIframeId").attr("src", ListViewConstants.modalAddPageUrl + "?id=" + LeaseAdminID);
    }
    else {
        $("#modalIframeId").attr("src", ListViewConstants.modalAddPageUrl);
    }
    return false;
}
//----------------------Show Create Dialog---------------------//

/** /Dialog **/

