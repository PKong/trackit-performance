﻿
// Create Date 19/11/12
// Flexigrid
//
//--------------------FG Before Send Data-------------------------//
function fgBeforeSendData(data) {

    // Show - Loading Spinner
    if(( $('#MainContent_div_site').length == 0) || ($('#MainContent_div_site').css('display') != 'block') ){
        $('.h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });
    }
    // disable export button
    $("#MainContent_exportButton").val("Processing...");
    $("#MainContent_exportButton").attr("disabled", true);

    // Global show none associate
    var gs = $.urlParam(GlobalSearch.searchQueryString);
    addGlobal:
    if (!String(gs).isNullOrEmpty()) {
        for (var i = 0; i < arrayOfFilters.length; i++) {
            if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
        }
        addFilterPostParam(GlobalSearch.searchQueryString, gs);
    }

    // Project Category
    var pc = $.urlParam(ConstGlobal.GENERAL.projectCategoryQueryString);

    //
    addCategory:
    if (!String(pc).isNullOrEmpty()) {
        for (i = 0; i < arrayOfFilters.length; i++) {
            if (arrayOfFilters[i].name == ConstGlobal.GENERAL.projectCategoryQueryString) break addCategory;
        }
        addFilterPostParam(ConstGlobal.GENERAL.projectCategoryQueryString, pc);
    }

    // Set Default filter
    removeFilterPostParam(ConstGlobal.DEFAULT.returnPage);
    addFilterPostParam(ConstGlobal.DEFAULT.returnPage, savedFiltersQueryString);

    // Search Filter Data
    sendSearchFilterDataToHandler(data);
}
//--------------------FG Before Send Data-------------------------//

//--------------------FG Data Load-------------------------------//
function fgDataLoad(sender, args) {

    // Hide - Loading Spinner 
    $('.h1').activity(false);

    // Enable export button
    $("#MainContent_exportButton").val("Export to Excel");
    $('#MainContent_exportButton').removeAttr("disabled");

}
//--------------------FG Data Load-------------------------------//

//--------------------FG Row Click-------------------------------//
function fgRowClick(sender, args) {

    return false;
}
//--------------------FG Row Click-------------------------------//

//--------------------FG Row Render-------------------------------//
function fgRowRender(tdCell, data, dataIdx) {

    var cVal = new String(data.cell[dataIdx]);

    switch (Number(dataIdx)) {
        //Display revenue share 
        case 5:
            if (cVal.isNullOrEmpty()) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {
                //alert(cVal);
                tdCell.innerHTML = cVal;
            }
            break;

        // Standard String  
        default:
            if (cVal.isNullOrEmpty()) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {
                tdCell.innerHTML = cVal;
            }
            break;
    }
}
//--------------------FG Row Render-------------------------------//

//--------------------Grid Sites Row Render-----------------------//
function fgSiteRowRender(tdCell, data, dataIdx) {

    var cVal = new String(data.cell[dataIdx]);

    // Type?
    switch (Number(dataIdx)) {

        // Tower Mod     
        case 5: ColumnTypes
            sHtml = getColumnImageIcon(ColumnTypes.TOWER_MOD, cVal, data.cell[0]);
            tdCell.innerHTML = sHtml;
            break;

        // Rogue Equipment      
        case 6:
            sHtml = getColumnImageIcon(ColumnTypes.ROGUE_EQUIP, cVal, null);
            tdCell.innerHTML = sHtml;
            break;

        // Standard String    
        default:
            if ((cVal.isNullOrEmpty()) || (cVal == "<undefined>")) {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else {
                tdCell.innerHTML = cVal;
            }
            break;
    }
    //>
}
//--------------------Grid Sites Row Render-----------------------//

//--------------------FG No Data---------------------------------//
function fgNoData() {

    // Hide - Loading Spinner 
    $('.h1').activity(false);

    // Disable export button
    $("#MainContent_exportButton").val("Export to Excel");
}
//--------------------FG No Data--------------------------------//


//------------Bind Delete Discussion Button Actions-------------//
function bindDeleteDiscussionButtonActions() {
    // Delete Modal Dialog
    var delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Delete this item?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This discussion item will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
    $('.btn-comment-delete').click(function (e) {
        e.preventDefault();
        $('#dialog-confirm-comment-delete').remove();
        $(delDialogHtml).dialog({
            open: function (type, data) {

                $(this).parent().appendTo("form[0]");
            },
            buttons: {
                'Confirm': function () {

                    //debugger;

                    $(this).dialog('close');
                    showLoadingSpinner('h1-discussion');
                    __doPostBack('upDiscussion', e.target.alt);
                    return true;

                },
                'Cancel': function () {
                    $(this).dialog('close');
                    return false;
                }
            }
        });
        $('#dialog-confirm-comment-delete').dialog('open');
    });
}
//------------Bind Delete Discussion Button Actions-------------//

//------------Appned Data on multiselect site=-----------------//
function appendSiteSelected(siteId) {
    if (!CheckDuplicateSiteId(siteId)) {
        //Check existing
        var check = true;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "/Handlers/SiteIDHandler.axd",
            async: false,
            data: {
                term: siteId
            },
            error: function (xhr, status, error) {
                //alert("AJAX Error!=" + error);
                message = "Site Id not found!";
                check = false;
            },
            success: function (data) {
                if (data != "[]") {
                    message = "Site Id";
                    var notFoundData = jQuery.parseJSON(data);
                    for (x = 0; x < notFoundData.length; x++) {
                        message += " " + notFoundData[x];
                        if (x == notFoundData.length - 1) {
                            message += " ";
                        }
                        else {
                            message += ",";
                        }
                    }
                    
                    message += " not found!";
                    changeBGColorError("#txtSiteID");
                    check = false;

                    if (!check) {
                        showMessageAlertByName("error", message, "#message_alert");
                        return message;
                    }
                }
            }
        });

        if (check) {
            changeBGColorDefault("#txtSiteID");

            var listItemClass = "asmListItem";
            var removeClass = "asmListItemRemove";
            var listItemLabelClass = "asmListItemLabel";
            var removeLabel = "remove";
            var $removeLink = $("<a></a>")
					    .attr("href", "#")
					    .addClass(removeClass)
					    .prepend(removeLabel)
					    .click(function () {
					        var fHeight = $(this).parent().height();
					        resizeModal(-fHeight);
					        $(this).parent().remove();

					        if ($("#txtSiteID").val().length == 8) {
					            if (appendSiteSelected($("#txtSiteID").val().toUpperCase())) {
					                $("#txtSiteID").val('');
					            }
					        }
					        return false;
					    });

            var $itemLabel = $("<span></span>")
					    .addClass(listItemLabelClass)
					    .html(siteId);

            var $item = $("<li></li>")
					    .attr('rel', siteId)
					    .addClass(listItemClass)
					    .append($itemLabel)
					    .append($removeLink);
            $('#MainContent_ListSiteId').find('ol').append($item);
            var fHeight = $item.height();
            resizeModal(fHeight);

            return true;
        }
    }
    else {

        changeBGColorError("#txtSiteID");
        return false;
    }
}
//------------Appned Data on multiselect site=-----------------//

//---Toggle display associate and none associate Lease admin---//
function ManegeToggle(obj) {
    var IS_SHOW_MESSAGE_ALERT = false;
    var alert_height = 0;
    var div_height = 0;
    $("#message_alert").find(".ms").each(function () {
        alert_height = $("#message_alert").height();
        $(this).remove()
    });
    $("#txtAssocSiteID").val("");
    $("#txtSiteID").val("");
    changeBGColorDefault("#txtAssocSiteID");
    changeBGColorDefault("#txtSiteID");
    changeBGColorDefault("#MainContent_txtSiteIds");
    $("#MainContent_txtSiteIds").val('');
    $("#HiddenDialogHeightFlag").val("false")

    if ($('#MainContent_radNoAssoc').attr('checked') == 'checked') {
        $('#MainContent_associate').css("display", "none");
        $('#MainContent_no_associate').css("display", "block");
        $('#MainContent_ddlAssociatedLeaseApps').find('option').remove();
        $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
        $('#MainContent_lblNotfound').css("display", "block");

        if ($('#MainContent_radNoAssoc').attr('alreadyClick') == undefined) {
            $('#MainContent_radNoAssoc').attr('alreadyClick', 0);
        }
        else {
            var cickCount = parseInt($('#MainContent_radNoAssoc').attr('alreadyClick')) + 1;
            $('#MainContent_radNoAssoc').attr('alreadyClick', cickCount);
        }

        $('#MainContent_radAssoc').attr('alreadyClick', -1);
        div_height = 75;
    }
    else {
        $('#MainContent_no_associate').css("display", "none");
        $('#MainContent_associate').css("display", "block");
        if ($('#MainContent_radAssoc').attr('alreadyClick') != undefined) {
            var cickCount = parseInt($('#MainContent_radAssoc').attr('alreadyClick')) + 1;
            $('#MainContent_radAssoc').attr('alreadyClick', cickCount);
        }
        $('#MainContent_radNoAssoc').removeAttr('alreadyClick');
        div_height = -75;
    }

    if (obj != null) {
        if ($(obj).attr('alreadyClick') == 0) {
            var fHeight = $("#divUserFunction").height() - alert_height + div_height;
            resizeModal(fHeight);
        }
    }
}
//---Toggle display associate and none associate Lease admin---//

//--------------- Get JSON Data from Hidden field--------------//
function GetData(ctrlSource) {
    var jsonStr = $(ctrlSource).val();

    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}
//--------------- Get JSON Data from Hidden field--------------//

//---------------Get Base from current URL--------------------//
function getBaseURL() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));
    if (baseURL.indexOf('http://localhost') != -1) {
        // Base Url for localhost
        var url = location.href;  // window.location.href;
        var pathname = location.pathname;  // window.location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index1);

        return baseLocalUrl + "/";
    }
    else {
        // Root Url for domain name
        return baseURL + "/";
    }

}
//---------------Get Base from current URL--------------------//

//---------------Ajax call associate LeaseApp-----------------//
function AjaxGetAssociateLeaseApp(siteId){
    var check = false;
    var message = "Site Id " + siteId + " not found!";

    var ajaxResult = AjaxCallSiteIDHandler(siteId+"|", "");
    check = ajaxResult[0];
    message = ajaxResult[1];
    var url = "AssociateLeaseAppHandler.axd?siteid=" + siteId 
    if (check) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            error: function (xhr, status, error) {
                //alert("AJAX Error!=" + error);
                message = "Site Id " + siteId + " not found!";
                check = false;
            },
            success: function (result) {
                if (result != null) {
                    if (result.length == 0) {
                        $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
                        $('#MainContent_lblNotfound').css("display", "block");

                        message = "Lease Application not found!";
                        check = false;
                    }
                    else {

                        $('#MainContent_ddlAssociatedLeaseApps').css("display", "block");
                        $('#MainContent_lblNotfound').css("display", "none");
                        $('#MainContent_ddlAssociatedLeaseApps').find('option').each(function () {
                            $(this).remove();
                        });
                        changeBGColorDefault("#txtAssocSiteID");
                        changeBGColorDefault("#MainContent_ddlAssociatedLeaseApps");
                        check = true;
                        message = "";


                        var data = new Array();
                        if (result.length > 0) {
                            $('#MainContent_ddlAssociatedLeaseApps').append($('<option></option>').val('').html(' Click to Select an Application '));
                        }
                        for (i = 0; i < result.length; i++) {
                            $('#MainContent_ddlAssociatedLeaseApps').append($('<option></option>').val(result[i].id).html(result[i].Descriptions));
                        }
                    }
                }
                else {
                    message = "Lease Application not found!";
                    check = false;
                }
            }
        });
    }
    if (!check) {
        changeBGColorError("#txtAssocSiteID");
        $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
        $('#MainContent_lblNotfound').css("display", "block");
        showMessageAlertByName("error", message, "#message_alert");
    }
}

//---------------Ajax call associate LeaseApp-----------------//

function AjaxLeaseAdminCreateAndEditHandler() {
    var id = $("#Hidden_Lease_Admin_ID").val();
    var isRadAssoc = $('#MainContent_radAssoc').is(':checked');
    var Hidden_selected_leaseapp_value = $('#MainContent_ddlAssociatedLeaseApps').val();
    var pnlAssocLeaseAppText = $('#txtAssocSiteID').val();
    var Hidden_selected_site_ids = $('#Hidden_selected_site_ids').val();
    var Hidden_selected_customer = $('#Hidden_selected_customer').val();
    var isCreate = true;
    if (id != "") {
        isCreate = false;
    }

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/Handlers/LeaseAdminCreateAndEditHandler.axd",
        async: false,
        data: {
            id: id,
            isRadAssoc: isRadAssoc,
            isCreate: isCreate,
            Hidden_selected_leaseapp_value: Hidden_selected_leaseapp_value,
            pnlAssocLeaseAppText: pnlAssocLeaseAppText,
            Hidden_selected_site_ids: Hidden_selected_site_ids,
            Hidden_selected_customer: Hidden_selected_customer
        },
        error: function (xhr, status, error) {
            //alert("AJAX Error!=" + error);
        },
        success: function (result) {
            var objResult = jQuery.parseJSON(result);
            if (objResult.length == 4) {
                var isSuccess = objResult[0];
                var msgResult = objResult[1];
                var targetObj = objResult[2];
                var isCreate = objResult[3];

                if (isSuccess == "true") {
                    changeBGColorDefault("#txtAssocSiteID");
                    changeBGColorDefault("#MainContent_ddlAssociatedLeaseApps");
                    changeBGColorDefault("#MainContent_txtSiteIds");
                    $('.close').click();

                    window.parent.location.href = 'Edit.aspx?id=' + msgResult;
                }
                else if (isSuccess == "false") {
                    showMessageAlertByName("error", msgResult, "#message_alert");
                    if (targetObj != "") {
                        changeBGColorError("#" + targetObj);
                    }

                    if (isRadAssoc) {
                        $("#MainContent_btnSubmitCreateAssoc").removeAttr("disabled");
                        if (isCreate == "true") {
                            $("#MainContent_btnSubmitCreateAssoc").val("Create");
                        }
                        else {
                            $("#MainContent_btnSubmitCreateAssoc").val("Save");
                        }
                    }
                    else {
                        $("#MainContent_btnSubmitCreateApps").removeAttr("disabled");
                        if (isCreate == "true") {
                            $("#MainContent_btnSubmitCreateApps").val("Create");
                        }
                        else {
                            $("#MainContent_btnSubmitCreateApps").val("Save");
                        }
                    }
                }
            }
        }
    });
}

//--------------Initial Add Form------------------------------//
function initAddLeaseAdmin() {
    $("#txtAssocSiteID").unbind("change");
    $("#txtAssocSiteID").change(function (e) {
        if ($(this).val().length == 8) {
            AjaxGetAssociateLeaseApp($("#txtAssocSiteID").val());
        }
    });
    $("#txtAssocSiteID").live("keyup", function (e) {
        if ($(this).val().length == 8) {
            $(this).change();
        }
    });
    $("li.ui-menu-item").live("click", function (e) {
        if ($("#txtSiteID").val().length == 8) {
            var check = appendSiteSelected($("#txtSiteID").val().toUpperCase());
            $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
            $('#MainContent_lblNotfound').css("display", "block");
            if (check) {
                $("#txtSiteID").val('');
            }
        }
        else if ($("#txtAssocSiteID").val().length == 8) {
            $("#txtAssocSiteID").change();
        }
    });

    $("#txtSiteID").live("blur", function () {
        if ($(this).val().length == 8) {
            var check = appendSiteSelected($(this).val().toUpperCase());
            $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
            $('#MainContent_lblNotfound').css("display", "block");
            if (check) {
                $("#txtSiteID").val('');
            }
        }
        else if ($(this).val().length == 0) {
            changeBGColorDefault("#txtSiteID");
        }
    });

    $("#txtSiteID").live("keypress", function (e) {
        if (e.which == 13) {
            if ($(this).val().length == 8) {
                var check = appendSiteSelected($(this).val().toUpperCase());
                $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
                $('#MainContent_lblNotfound').css("display", "block");
                if (check) {
                    $("#txtSiteID").val('');
                }
            }
            else if ($(this).val().length == 0)
            {
                changeBGColorDefault("#txtSiteID");
            }
            e.preventDefault();
        }
    });

    $("#txtSiteID").live("textchange", function (e) {
        if ($(this).val().length == 8) {
            var check = appendSiteSelected($(this).val().toUpperCase());
            $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
            $('#MainContent_lblNotfound').css("display", "block");
            if (check) {
                $("#txtSiteID").val('');
            }
        }
        else if ($(this).val().length == 0) {
            changeBGColorDefault("#txtSiteID");
        }
    });
    $("#txtSiteID").live("focusOut", function (e) {
        if ($(this).val().length == 8) {
            appendSiteSelected($(this).val().toUpperCase());
            $('#MainContent_ddlAssociatedLeaseApps').css("display", "none");
            $('#MainContent_lblNotfound').css("display", "block");
            if (check) {
                $("#txtSiteID").val('');
            }
        }
        else if ($(this).val().length == 0) {
            changeBGColorDefault("#txtSiteID");
        }
    });


    $('#lblAssocSiteID').css('display', 'none')
}
//--------------Initial Add Form------------------------------//

