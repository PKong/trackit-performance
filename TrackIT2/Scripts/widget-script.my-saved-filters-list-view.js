/*********************************************
* Widget Script - My Saved Filters List View
**********************************************/

// Setup My Saved Filter List View
function setupMySavedFilterListView(context, savedFiltersIsEmpty, title) {

    // Variables
    var widgetMetaData = $('#' + ($(context).attr("id") + "-hfWidgetMetaData"), context).val();
    var mySavedFilterListViewPrefix = "My Saved Filter: ";
    var optionsWindow = $('.widget-options', context);
    var editButton = $('.edit-widget-button', context);
    var saveButton = $('.save-widget-button', context);
    var cancelButton = $('.widget-options-cancel', context);
    var optionsBar = $('.widget-options-bar', context);
    var widgetH = $(context).outerHeight();
    var headerH = $('.widgetheader', context).outerHeight();
    var footerH = $('.widget-footer', context).outerHeight();
    var finalH = widgetH - footerH - headerH - 3;  // default: (- 5)
    //
    var selMySavedFiltersClass = ".sel-my-saved-filters";
    var selMySavedFilters = $(selMySavedFiltersClass, context);
    var selMySavedFiltersIndex;
    //
    var titleValue = (title != null) ? title : null;
    //
    optionsWindow.css('height', finalH);
    //
    var classNavLinkFirst = "widget-nav-link-first";
    var classNavLinkPrevious = "widget-nav-link-previous";
    var classNavLinkNext = "widget-nav-link-next";
    var classNavLinkLast = "widget-nav-link-last";
    var classNavLinkDisabled = "widget-nav-link-disabled";
    //
    var resultsPerPage = 20;
    var resultsCurrentPage = 1;
    var resultsTotalRecords = 0;
    var resultsTotalPages = 0;
    //>

    // Open Options Window
    function openOptionsWindow() {

        optionsWindow.slideDown();
        optionsBar.show();
        editButton.hide();
    }
    //>

    // Close Options Window
    function closeOptionsWindow(refreshWidget) {

        optionsWindow.slideUp(
            function () {
                if (refreshWidget) {
                    loadMySavedFiltersListView(context);
                }
            }
        );
        optionsBar.hide();
        editButton.show();
    }
    //>

    // Button Actions
    editButton.click(openOptionsWindow);
    //
    // Save Button
    saveButton.click(function () {

        selMySavedFiltersIndex = selMySavedFilters[0].selectedIndex;

        // Filter selected?
        if (selMySavedFiltersIndex > 0) {
            $(".dashboard-inline-error", context).fadeOut("fast",
                function () {
                    //Changed widget title property for on mouse hover popup text.
                    //TODO : Should check the best way to get current Widget from button.

                    var sWidgetName = "My Saved Filter: " + selMySavedFilters[0][selMySavedFiltersIndex].text;
                    var sWidgetID = $(this).parent().parent().parent().parent().attr("id");
                    $(this).parent().parent().parent().parent().attr("title", sWidgetName);
                    saveDashboardAndWidgets(sWidgetID, sWidgetName);
                    closeOptionsWindow(true);
                }
            );
        } else {
            // Validation Error
            $(".dashboard-inline-error", context).fadeIn();
        }
        //>
    });
    //
    cancelButton.click(function () { closeOptionsWindow(false) });
    //>

    // Select My Saved Filters - Changed Val
    selMySavedFilters.change(function () {
        titleValue = null;
    });
    //>

    // Widget Initial State
    if (savedFiltersIsEmpty) {
        optionsWindow.show();
        optionsBar.show();
        editButton.hide();
    } else {
        optionsWindow.hide();
        optionsBar.hide();
        editButton.show();
        loadMySavedFiltersListView(context);
    }
    //>

    // Load My Saved Filters List View
    function loadMySavedFiltersListView(context, pageToLoad) {

        clearWidgetResults();

        //debugger;
        var pageToLoadValue = 1;
        if (pageToLoad != null) pageToLoadValue = pageToLoad;

        var loadingHtml = '<div class="loading"><img alt="Loading, please wait" src="/Styles/images/loading.gif" /><p>Loading...</p></div>';
        $(".my-saved-filters-list-view-container", context).html(loadingHtml);

        var titleParam = (titleValue != null) ? titleValue : $(selMySavedFiltersClass + " option:selected", context).text();

        selMySavedFiltersIndex = selMySavedFilters[0].selectedIndex;
        var currentListViewNameValue = (selMySavedFiltersIndex > 0) ? selMySavedFilters.val() : 'null';

        // check if comment change title to Recent Comments
        if (unescape(titleParam).indexOf("Comment") != -1) {
            $('.widgettitle', context).html("Recent Comments");
        }
        else if (unescape(titleParam).indexOf("Document") != -1) {
            $('.widgettitle', context).html("Fields Missing Documents");
        }
        else {
            $('.widgettitle', context).html(mySavedFilterListViewPrefix + unescape(titleParam));
        }

        //check isAssignCheckBox for leaseApp comment
        if ($('#isAssign').length > 0) {
            if ($('#isAssign').prop('checked')) {
                currentListViewNameValue += '|true';
            }
            else {
                currentListViewNameValue += '|false';
            }
        }

        // Load List View Data
        $.post("/Dashboard/Widgets/MySavedFiltersListViewDataProvider.aspx", { title: titleParam, currentListViewName: currentListViewNameValue, page: pageToLoadValue },
            function (data) {
                if (!String(data).isNullOrEmpty()) {
                    if (data == "error") {
                        // Error
                        alert("Error!");
                    } else {
                        // Success
                        $(".my-saved-filters-list-view-container", context).html(data);
                        $('#' + ($(context).attr("id") + "-hfWidgetMetaDataDefaults"), context).val('&title=' + encodeURIComponent(titleParam));

                        // Setup Nav Links
                        setupNavLinks(context);

                        // Save Dashboard & Widgets
                        saveDashboardAndWidgets();
                    }
                }
            }
        );
        //>
    }
    //-->

    // Setup Nav Links
    function setupNavLinks(context) {

        var arrNavState = String($(".my-saved-filters-list-view-container .hf-widget-nav-state", context).text()).split('|');
        var bindForwardLinks = false;
        var bindBackLinks = false;

        resultsPerPage = Number(arrNavState[0]);
        resultsCurrentPage = Number(arrNavState[1]);
        resultsTotalRecords = Number(arrNavState[2]);
        resultsTotalPages = calculateNumberOfPages(resultsTotalRecords, resultsPerPage);

        // Start of Paging
        if (resultsCurrentPage <= 1) {

            // Inactive Links
            $("." + classNavLinkFirst, context).unbind("click");
            $("." + classNavLinkFirst, context).addClass(classNavLinkDisabled);
            $("." + classNavLinkPrevious, context).unbind("click");
            $("." + classNavLinkPrevious, context).addClass(classNavLinkDisabled);

            if (resultsTotalPages > 1) bindForwardLinks = true;

        } else {
            $("." + classNavLinkFirst, context).removeClass(classNavLinkDisabled);
            $("." + classNavLinkPrevious, context).removeClass(classNavLinkDisabled);

            if (resultsCurrentPage > 1) bindBackLinks = true;
        }
        //>

        // End of Paging
        if (resultsCurrentPage == resultsTotalPages) {

            // Inactive Links
            $("." + classNavLinkNext, context).unbind("click");
            $("." + classNavLinkNext, context).addClass(classNavLinkDisabled);
            $("." + classNavLinkLast, context).unbind("click");
            $("." + classNavLinkLast, context).addClass(classNavLinkDisabled);

            if (resultsCurrentPage > 1) bindBackLinks = true;

        } else {
            if (resultsTotalPages > 1) {
                $("." + classNavLinkNext, context).removeClass(classNavLinkDisabled);
                $("." + classNavLinkLast, context).removeClass(classNavLinkDisabled);

                if (resultsTotalPages > 1) bindForwardLinks = true;
            }
        }
        //>

        if (bindForwardLinks) {

            // Active Links
            $("." + classNavLinkNext, context).unbind("click");
            $("." + classNavLinkNext, context).bind("click", function () {
                loadMySavedFiltersListView(context, resultsCurrentPage + 1);
            });
            //
            $("." + classNavLinkLast, context).unbind("click");
            $("." + classNavLinkLast, context).bind("click", function () {
                loadMySavedFiltersListView(context, resultsTotalPages);
            });
        }

        if (bindBackLinks) {

            // Active Links
            $("." + classNavLinkPrevious, context).unbind("click");
            $("." + classNavLinkPrevious, context).bind("click", function () {
                loadMySavedFiltersListView(context, resultsCurrentPage - 1);
            });
            //
            $("." + classNavLinkFirst, context).unbind("click");
            $("." + classNavLinkFirst, context).bind("click", function () {
                loadMySavedFiltersListView(context, 1);
            });
        }

        // Paging Counter / Totals
        $(".widget-footer .widget-nav-links-page-count", context).html("Page " + resultsCurrentPage + " of " + resultsTotalPages);

        if (resultsTotalPages > 0) {
           $('#divListViewNoRecords', context).hide();
           $('.widget-footer-nav-container', context).show();
        }
        else {
           $('.widget-footer-nav-container', context).hide();
           $('#divListViewNoRecords', context).show();           
        }
    }
    //-->

    // Clear Widget Results
    function clearWidgetResults() {
        $(".my-saved-filters-list-view-container", context).empty();
    }
    //-->

    // Calculate Number Of Pages
    function calculateNumberOfPages(totalNumberOfItems, pageSize)
    {
	    var result = totalNumberOfItems % pageSize;
	    if (result == 0)
		    return Math.floor(totalNumberOfItems / pageSize);
	    else
		    return Math.floor(totalNumberOfItems / pageSize + 1);
	}
    //-->
}
//-->

