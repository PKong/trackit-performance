﻿function prelimBeforeSave()
{
    var msgError = "";
    var check = true;

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    if (($("#MainContent_txtPreliminaryDecision_ColLeft1").length && $("#MainContent_txtPreliminaryDecision_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtPreliminaryDecision_ColLeft1")) |
        ($("#MainContent_txtSDPtoCustomer_ColLeft1").length && $("#MainContent_txtSDPtoCustomer_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtSDPtoCustomer_ColLeft1")) |
        ($("#MainContent_txtPreliminarySitewalk_ColLeft1").length && $("#MainContent_txtPreliminarySitewalk_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtPreliminarySitewalk_ColLeft1"))) {
       msgError += messageValidateDateTime;
    }

    if (msgError != "") 
    {
       check = false;
       $("#error_prelim").html("");
       showMessageAlertByName("error", msgError, "#error_prelim");
       return false;
    }
    else 
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };

       // Change save button to display processing message and disable it to
       // prevent "double submissions".
       $("#MainContent_btnSubmit_PreliminaryReview_upper").val("Processing...");
       $("#MainContent_btnSubmit_PreliminaryReview_upper").attr("disabled", true);
       $("#MainContent_btnSubmit_PreliminaryReview").val("Processing...");
       $("#MainContent_btnSubmit_PreliminaryReview").attr("disabled", true);
       return true;
    }

    return check;
}

function docReviewBeforeSave() {
    var msgError = "";
    var check = true;

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    if (($("#MainContent_txtLeaseExhibit_Ordered").length && $("#MainContent_txtLeaseExhibit_Ordered").val().toString() != "" && !isValidDate("#MainContent_txtLeaseExhibit_Ordered")) |
        ($("#MainContent_txtLeaseExhibit_Received").length && $("#MainContent_txtLeaseExhibit_Received").val().toString() != "" && !isValidDate("#MainContent_txtLeaseExhibit_Received")) |
        ($("#MainContent_txtLeaseExhibit_Decision").length && $("#MainContent_txtLeaseExhibit_Decision").val().toString() != "" && !isValidDate("#MainContent_txtLeaseExhibit_Decision")) |
        ($("#MainContent_txtLandlordConsent_ConsentRequested").length && $("#MainContent_txtLandlordConsent_ConsentRequested").val().toString() != "" && !isValidDate("#MainContent_txtLandlordConsent_ConsentRequested")) |
        ($("#MainContent_txtLandlordConsent_ReceivedDate").length && $("#MainContent_txtLandlordConsent_ReceivedDate").val().toString() != "" && !isValidDate("#MainContent_txtLandlordConsent_ReceivedDate")) |
        ($("#MainContent_txtConstructionDrawings_Received").length && $("#MainContent_txtConstructionDrawings_Received").val().toString() != "" && !isValidDate("#MainContent_txtConstructionDrawings_Received")) |
        ($("#MainContent_txtConstructionDrawings_Decision").length && $("#MainContent_txtConstructionDrawings_Decision").val().toString() != "" && !isValidDate("#MainContent_txtConstructionDrawings_Decision"))) {
        msgError += messageValidateDateTime;
     }

     if (msgError != "") 
    {
       check = false;
       $("#error_docreview").html("");
       showMessageAlertByName("error", msgError, "#error_docreview");
       return false;
    }
    else 
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };

       // Change save button to display processing message and disable it to
       // prevent "double submissions".       
       $("#MainContent_btnSubmit_DocumentReview_Upper").val("Processing...");
       $("#MainContent_btnSubmit_DocumentReview_Upper").attr("disabled", true);
       $("#MainContent_btnSubmit_DocumentReview").val("Processing...");
       $("#MainContent_btnSubmit_DocumentReview").attr("disabled", true);
       return true;
    }
}

function constructionBeforeSave() {
    var msgError = "";
    var check = true;

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    if (($("#MainContent_txtPreconstruction_PreconstructionSitewalk").length && $("#MainContent_txtPreconstruction_PreconstructionSitewalk").val().toString() != "" && !isValidDate("#MainContent_txtPreconstruction_PreconstructionSitewalk")) |
        ($("#MainContent_txtPreconstruction_NTPIssuedDateStatus").length && $("#MainContent_txtPreconstruction_NTPIssuedDateStatus").val().toString() != "" && !isValidDate("#MainContent_txtPreconstruction_NTPIssuedDateStatus")) |
        ($("#MainContent_txtPreconstruction_NTPVerified").length && $("#MainContent_txtPreconstruction_NTPVerified").val().toString() != "" && !isValidDate("#MainContent_txtPreconstruction_NTPVerified")) |
        ($("#MainContent_txtEarlyInstallation_ViolaitonDiscovered").length && $("#MainContent_txtEarlyInstallation_ViolaitonDiscovered").val().toString() != "" && !isValidDate("#MainContent_txtEarlyInstallation_ViolaitonDiscovered")) |
        ($("#MainContent_txtEarlyInstallation_ViolationLetterSent").length && $("#MainContent_txtEarlyInstallation_ViolationLetterSent").val().toString() != "" && !isValidDate("#MainContent_txtEarlyInstallation_ViolationLetterSent")) |
        ($("#MainContent_txtEarlyInstallation_ViolationInvoiceCreatedByFinops").length && $("#MainContent_txtEarlyInstallation_ViolationInvoiceCreatedByFinops").val().toString() != "" && !isValidDate("#MainContent_txtEarlyInstallation_ViolationInvoiceCreatedByFinops")) |
        ($("#MainContent_txtEarlyInstallation_ViolationFeeReceived").length && $("#MainContent_txtEarlyInstallation_ViolationFeeReceived").val().toString() != "" && !isValidDate("#MainContent_txtEarlyInstallation_ViolationFeeReceived")) |
        ($("#MainContent_txtExtensionNotice_Received").length && $("#MainContent_txtExtensionNotice_Received").val().toString() != "" && !isValidDate("#MainContent_txtExtensionNotice_Received")) |
        ($("#MainContent_txtExtensionNotice_FeeReceived").length && $("#MainContent_txtExtensionNotice_FeeReceived").val().toString() != "" && !isValidDate("#MainContent_txtExtensionNotice_FeeReceived")) |
        ($("#MainContent_txtExtensionNotice_InvoiceCreatedByFinops").length && $("#MainContent_txtExtensionNotice_InvoiceCreatedByFinops").val().toString() != "" && !isValidDate("#MainContent_txtExtensionNotice_InvoiceCreatedByFinops")) |
        ($("#MainContent_txtEquipment_Removed").length && $("#MainContent_txtEquipment_Removed").val().toString() != "" && !isValidDate("#MainContent_txtEquipment_Removed")) |
        ($("#MainContent_txtEquipment_FailureToRemoveViolationLetterSent").length && $("#MainContent_txtEquipment_FailureToRemoveViolationLetterSent").val().toString() != "" && !isValidDate("#MainContent_txtEquipment_FailureToRemoveViolationLetterSent")) |
        ($("#MainContent_txtEquipment_FailureToRemoveViolationLetterSubmittedToCst").length && $("#MainContent_txtEquipment_FailureToRemoveViolationLetterSubmittedToCst").val().toString() != "" && !isValidDate("#MainContent_txtEquipment_FailureToRemoveViolationLetterSubmittedToCst")) |
        ($("#MainContent_txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst").length && $("#MainContent_txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst").val().toString() != "" && !isValidDate("#MainContent_txtEquipment_ViolationEquipmentRemovalConfirmedSubmittedToCst")) |
        ($("#MainContent_txtPostConstruction_Ordered").length && $("#MainContent_txtPostConstruction_Ordered").val().toString() != "" && !isValidDate("#MainContent_txtPostConstruction_Ordered")) |
        ($("#MainContent_txtPostConstruction_SiteWalk").length && $("#MainContent_txtPostConstruction_SiteWalk").val().toString() != "" && !isValidDate("#MainContent_txtPostConstruction_SiteWalk")) |
        ($("#MainContent_txtPostConstruction_ConstructionCompleted").length && $("#MainContent_txtPostConstruction_ConstructionCompleted").val().toString() != "" && !isValidDate("#MainContent_txtPostConstruction_ConstructionCompleted")) |
        ($("#MainContent_txtCloseOut_Received").length && $("#MainContent_txtCloseOut_Received").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_Received")) |
        ($("#MainContent_txtCloseOut_NoticeSent").length && $("#MainContent_txtCloseOut_NoticeSent").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_NoticeSent")) |
        ($("#MainContent_txtCloseOut_FeeInvoiceSent").length && $("#MainContent_txtCloseOut_FeeInvoiceSent").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_FeeInvoiceSent")) |
        ($("#MainContent_txtCloseOut_FeeInvoiceCreatedByFinops").length && $("#MainContent_txtCloseOut_FeeInvoiceCreatedByFinops").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_FeeInvoiceCreatedByFinops")) |
        ($("#MainContent_txtCloseOut_FeeReceived").length && $("#MainContent_txtCloseOut_FeeReceived").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_FeeReceived")) |
        ($("#MainContent_txtCloseOut_FinalSiteApproval").length && $("#MainContent_txtCloseOut_FinalSiteApproval").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_FinalSiteApproval")) |
        ($("#MainContent_txtCloseOut_AsBuilt").length && $("#MainContent_txtCloseOut_AsBuilt").val().toString() != "" && !isValidDate("#MainContent_txtCloseOut_AsBuilt")) |
        ($("#MainContent_txtMarketHandoff_Date").length && $("#MainContent_txtMarketHandoff_Date").val().toString() != "" && !isValidDate("#MainContent_txtMarketHandoff_Date"))) {
        msgError += messageValidateDateTime;
    }

    if (msgError != "") 
    {
       check = false;
       $("#error_construction").html("");
       showMessageAlertByName("error", msgError, "#error_construction");
       return false;
    }
    else
    {
       // Remove onbeforeunload event since this script is called as a postback.
       window.onbeforeunload = function () { };

       // Change save button to display processing message and disable it to
       // prevent "double submissions".
       $("#MainContent_btnSubmit_Construction_Upper").val("Processing...");
       $("#MainContent_btnSubmit_Construction_Upper").attr("disabled", true);
       $("#MainContent_btnSubmit_Construction").val("Processing...");
       $("#MainContent_btnSubmit_Construction").attr("disabled", true);
       return true;
    }
}

