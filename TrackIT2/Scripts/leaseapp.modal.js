﻿/** Dialog **/

/** Constants **/
var ListViewConstants = {
    dialogWidth: 740,
    dialogIframeMinWidth: 700,
    dialogIframeMinHeight: 650,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "Add.aspx"
};

var leaseAppID = 0;
/** /Constants **/

// Setup List Create Modal
function setupListCreateModal(title,id) {
    leaseAppID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstants.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeId').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    //
    $("#modalExternal").dialog(optionsCreate);
    //
    $('#modalBtnExternal').click(function () {
        showCreateDialog();
    });
}
//-->

function showCreateDialog() {

    $("#modalExternal").html('<iframe id="modalIframeId" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:' + ListViewConstants.dialogIframeMinHeight + 'px;min-width:' + ListViewConstants.dialogIframeMinWidth + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading">' + ListViewConstants.modalLoadingMessage + '</div>').dialog("open");
    $("#modalIframeId").attr("src", ListViewConstants.modalAddPageUrl + "?id=" + leaseAppID);
    return false;
}

///////////////////
//// Summary /////
/////////////////

/** Constants **/
var ListViewConstantsSummary = {
    dialogWidth: 730,
    dialogIframeMinWidth: 700,
    dialogIframeMinHeight: 550,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "LASummaryInfo.aspx"
};
/** /Constants **/

// Setup List Create Modal
function setupListCreateModalSummary(title,id) {
    leaseAppID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstantsSummary.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading-summary').show();
            $('#modalIframeIdSummary').load(function () {
                $('#modal-loading-summary').fadeOut();
            });
        }
    };
    //
    $("#modalExternalSummary").dialog(optionsCreate);
    //
    $('#modalBtnSummary').click(function () {
        showCreateDialogSummary ();
    });
}
//-->

// Show Create Dialog
function showCreateDialogSummary () {
    $("#modalExternalSummary").html('<iframe id="modalIframeIdSummary" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:'
         + ListViewConstantsSummary.dialogIframeMinHeight 
         + 'px;min-width:' 
         + ListViewConstantsSummary.dialogIframeMinWidth 
         + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-summary">' 
         + ListViewConstantsSummary.modalLoadingMessage 
         + '</div>').dialog("open");
    $("#modalIframeIdSummary").attr("src", ListViewConstantsSummary.modalAddPageUrl + "?id=" + leaseAppID);
    return false;
}
//-->

/** /Dialog **/

/////////////////////
//// End Summary ////
/////////////////////


/////////////////////////////
//// Customer Contacts /////
///////////////////////////

/** Constants **/
var ListViewConstantsCustomerContacts = {
    dialogWidth: 380,
    dialogIframeMinWidth: 350,
    dialogIframeMinHeight: 385,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "LAClientInfo.aspx"
};
/** /Constants **/

// Setup List Create Modal
function setupListCreateModalCustomerContacts(title, id) {
    leaseAppID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstantsCustomerContacts.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading-CustomerContacts').show();
            $('#modalIframeIdCustomerContacts').load(function () {
                $('#modal-loading-CustomerContacts').fadeOut();
            });
        }
    };
    //
    $("#modalExternalCustomerContacts").dialog(optionsCreate);
    //
    $('#modalBtnCustomerContacts').click(function () {
        showCreateDialogCustomerContacts();
    });
}
//-->

// Show Create Dialog
function showCreateDialogCustomerContacts() {
    $("#modalExternalCustomerContacts").html('<iframe id="modalIframeIdCustomerContacts" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:'
         + ListViewConstantsCustomerContacts.dialogIframeMinHeight
         + 'px;min-width:'
         + ListViewConstantsCustomerContacts.dialogIframeMinWidth
         + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-CustomerContacts">'
         + ListViewConstantsCustomerContacts.modalLoadingMessage
         + '</div>').dialog("open");
    $("#modalIframeIdCustomerContacts").attr("src", ListViewConstantsCustomerContacts.modalAddPageUrl + "?id=" + leaseAppID);
    return false;
}
//-->

/** /Dialog **/

/////////////////////////////////
//// End Customer Contacts /////
///////////////////////////////

//////////////////
//// History /////
//////////////////

/** Constants **/
var ListViewConstantsHistory = {
    dialogWidth: 990,
    dialogIframeMinWidth: 960,
    dialogIframeMinHeight: 420,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "History.aspx"
};
/** /Constants **/

// Setup List Create Modal
function setupModalHistory(title, id)
{
    leaseAppID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstantsHistory.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui)
        {
            $('#modal-loading-History').show();
            $('#modalIframeIdHistory').load(function ()
            {
                $('#modal-loading-History').fadeOut();
            });
        }
    };
    //
    $("#modalExternalHistory").dialog(optionsCreate);
    //
    $('#modalHisExternal').click(function ()
    {
        showCreateDialogHistory();
    });
    //
    $('#equipmentHistory').click(function () {
        showCreateDialogHistory();
    });
}
//-->

// Show Create Dialog
function showCreateDialogHistory()
{
    $("#modalExternalHistory").html('<iframe id="modalIframeIdHistory" scrolling="no" style="width:100%;height:auto;overflow:hidden;min-height:'
         + ListViewConstantsHistory.dialogIframeMinHeight
         + 'px;min-width:'
         + ListViewConstantsHistory.dialogIframeMinWidth
         + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading-History">'
         + ListViewConstantsHistory.modalLoadingMessage
         + '</div>').dialog("open");

    if ($('#LiEquipmentInformation').attr('class').indexOf('ui-tabs-selected') > -1) {
        if ($('#ApplicationID').val() != 'null') {
            $("#modalIframeIdHistory").attr("src", ListViewConstantsHistory.modalAddPageUrl + "?id=" + leaseAppID + "&ApplicationID=" + $('#ApplicationID').val())
        }
        else {
            $("#modalIframeIdHistory").attr("src", ListViewConstantsHistory.modalAddPageUrl + "?id=" + leaseAppID + "&ApplicationID=nodata");
        }
    }
    else {
        $("#modalIframeIdHistory").attr("src", ListViewConstantsHistory.modalAddPageUrl + "?id=" + leaseAppID);
    }
    return false;
}
//-->

//////////////////////
//// end History /////
//////////////////////


//////////////////////////
//// Create Towermod /////
//////////////////////////

/** Constants **/
var ListViewConstantsCreateTowermod = {
    //Kongpat change dialogWidth and dialogIframeMinHeight 
    dialogWidth: 520,
    dialogIframeMinWidth: 470,
    dialogIframeMinHeight: 380,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "/TowerModifications/Add.aspx"
};
/** /Constants **/

// Setup Create Modal
function setupModalCreateTowerMod(title, id) {
    leaseAppID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstantsCreateTowermod.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeIdCreateTowermod').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    //
    $("#modalExternalCreateTowermod").dialog(optionsCreate);
    //
    $('#modalCreateTowermodExternal').click(function () {
        showCreateCreateTowerMod();
    });
}
//-->

// Show Create Dialog
function showCreateCreateTowerMod() {
    $("#modalExternalCreateTowermod").html('<iframe id="modalIframeIdCreateTowermod" scrolling="no" style="width:100%;height:auto;overflow:hidden;min-height:'
         + ListViewConstantsCreateTowermod.dialogIframeMinHeight
         + 'px;min-width:'
         + ListViewConstantsCreateTowermod.dialogIframeMinWidth
         + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading">'
         + ListViewConstantsCreateTowermod.modalLoadingMessage
         + '</div>').dialog("open");
    $("#modalIframeIdCreateTowermod").attr("src", ListViewConstantsCreateTowermod.modalAddPageUrl + "?laid=" + leaseAppID);
    return false;
}
//-->

//////////////////////////////
//// end Create Towermod /////
//////////////////////////////

/////////////////////
//// Create SDP /////
/////////////////////

/** Constants **/
var ListViewConstantsCreateSDP = {
    dialogWidth: 520,
    dialogIframeMinWidth: 470,
    dialogIframeMinHeight: 400,
    modalLoadingMessage: "LOADING... PLEASE WAIT",
    modalAddPageUrl: "/SiteDataPackages/Add.aspx"
};
/** /Constants **/

// Setup List Create Modal
function setupListCreateModalCreateSDP(title, id) {
    leaseAppID = id;
    // Create New Dialog
    var optionsCreate = {
        autoOpen: false,
        width: ListViewConstantsCreateSDP.dialogWidth,
        modal: true,
        title: title,
        open: function (event, ui) {
            $('#modal-loading').show();
            $('#modalIframeIdCreateSDP').load(function () {
                $('#modal-loading').fadeOut();
            });
        }
    };
    //
    $("#modalExternalCreateSDP").dialog(optionsCreate);
    //
    $('#modalCreateSDPExternal').click(function () {
        showCreateDialogCreateSDP();
    });
}
//-->

// Show Create Dialog
function showCreateDialogCreateSDP() {
    $("#modalExternalCreateSDP").html('<iframe id="modalIframeIdCreateSDP" scrolling="no" style="width:100%;height:100%;overflow:hidden;min-height:'
         + ListViewConstantsCreateSDP.dialogIframeMinHeight
         + 'px;min-width:'
         + ListViewConstantsCreateSDP.dialogIframeMinWidth
         + 'px;" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" /><div id="modal-loading">'
         + ListViewConstantsCreateSDP.modalLoadingMessage
         + '</div>').dialog("open");
    $("#modalIframeIdCreateSDP").attr("src", ListViewConstantsCreateSDP.modalAddPageUrl + "?laid=" + leaseAppID);
    return false;
}
//-->

/** /Dialog **/

/////////////////////
//// Create SDP /////
/////////////////////
/** /Dialog **/

