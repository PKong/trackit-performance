﻿// TMP: workaround for deleting comments / refreshing the UI.
var excludeCommentsIdArray = new Array();
var addingNewSaComment = false;
var COMMENT_ID = 0;
var IsSave_SA = false;
var IsSave_RV = false;

//for checking which tab is post.
var SA_TAB = "SA";
var REVISION_TAB = "RV";
var sCurrentTab = "";

function EndRequestHandlerSA() {
    var index_sa, index_rv;
    index_sa = getBufferIndex();
    index_rv = getRVBufferIndex();

    // Show Status Message
    ShowDialog();

    // Create Document link
    createDocumentLink();
    $("#hiddenCurrentSaCommentCount").val('0');
    CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');

    $("#hidden_count").text($("#MainContent_hidden_SA_count").val());
    $("#hidden_rv_count").text($("#MainContent_hidden_revision_count").val());


    initEventClickBack();
    initEventClickNext();
    initEventClickAddNew();
    initEventClickRVAddNew();
    initEventClickRVBack();
    initEventClickRVNext();

    SetDataToUI(index_sa);
    CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');
    createLeaseAdminCommentUI();

    if ($('#hidden_deleted_comment_id').val() != "") {
        var data = $('#hidden_deleted_comment_id').val().split('|');
        var type = data[0];
        var id = data[1];
        switch (type) {
            case "sa":
                var ui_id = $("div[sacommentid='SA_" + id + "']").attr('id');
                if (ui_id == undefined) {
                    // in case delete at side comment panel. can't find ui_id.
                    DeleteSaCommentOnNotUiFound(id);
                }
                else {
                    var Comment_index = ui_id.replace("Comment_Field_inner_SA_", "");
                    var index = getBufferIndex();
                    var data = GetAllJsonData();
                    data[index].comments[parseInt(Comment_index)].exist = false;
                    SetData("#MainContent_hidden_SA", data);    //Save to content
                    $("div[sacommentid='SA_" + id + "']").remove();
                }
                break;
            case "admin":
                var ui_id = $("div[leaseadmincommentid='LeaseAdmin_" + id + "']").attr('id');
                var Comment_index = ui_id.replace("Comment_Field_inner_Lease_", "");
                var data = jQuery.parseJSON($("#MainContent_hidden_lease_comments").val());
                data.splice(parseInt(Comment_index), 1);

                //Save to content.
                $("#MainContent_hidden_lease_comments").val(JSON.stringify(data));
                $("div[leaseadmincommentid='LeaseAdmin_" + id + "']").remove();

                if ($("div[leaseadmincommentid]").length == 0) {
                    $('#MainContent_link_addnew_lease').click();
                }
                break;
        }

        $('#hidden_deleted_comment_id').val("");
    }

    setRVDataToUI(index_rv);

    setBufferIndex(index_sa);
    setRVBufferIndex(index_rv);

    setLabelPageTotal(getCount());
    setLabelPageCurrent(index_sa);

    setRVPageTotal(getRVCount());
    setRVPageCurrent(index_rv);

    $("#MainContent_btnSubmit_StructuralAnalysis1").button();
    $("#MainContent_btnSubmit_StructuralAnalysis_Upper").button();
    $("#MainContent_btnSubmitRevisionTop").button();
    $("#MainContent_btnSubmitRevisionBottom").button();

    // Date Picker
    $(function () {
        $('input').filter('.datepicker').datepicker({
            numberOfMonths: 2,
            showButtonPanel: false
        });
    });

    // Comment //
    reloadSAComment(index_sa);
    if ($("div[sacommentid]").length == 0) {
        $('#MainContent_link_addnew').click();
    }

    // end Comment //
    // SetCssBtn
    cssBtnControlPage();

    // Money field
    moneyField();
    FillSymbol();

    // percent Field
    percentField();
    sCurrentTab = "";
}

function DeleteSaCommentOnNotUiFound(id) {
    var current_hidden_sa = GetAllJsonData();
    for (var index = 0; index < current_hidden_sa.length; index++) {
        for (var saIndex = 0; saIndex < current_hidden_sa[index].comments.length; saIndex++) {
            if (current_hidden_sa[index].comments[saIndex].comment_id == id) {
                current_hidden_sa[index].comments[saIndex].exist = false;
                break;
            }
        }
    }
    SetData("#MainContent_hidden_SA", current_hidden_sa);    //Save to content
}

var LastSelectedTab = ""
function DynamicUpdate(sTabName) {
    if (LastSelectedTab == 'SA') {
        var index = getBufferIndex();
        SetDataFromUI(index);
    }
    else if (LastSelectedTab == 'RE') {
        var index = getRVBufferIndex();
        RVSetDataFromUI(index);
    }
    LastSelectedTab = sTabName;
}
function cssBtnControlPage() {
    $("#btn_Previous").button();
    $("#btn_Next").button();
    $("#btn_AddNew").button();
    $("#btnRevisionPrevious").button();
    $("#btnRevisionNext").button();
    $("#btnRevisionAdd").button();
    $("#btnSADelete").button();
    $("#btnRVDelete").button();
    //For comment
    $("#btnPost").button();
    $("#btnCommentCancel").button();
    //SA comment
    $("#btnSAPost").button();
    $("#btnSACancel").button();

}

function initEventClickNext() {
    $('#btn_Next').unbind('click');
    $("#btn_Next").click(function () {
        //hide current doc
        var currentSaID = getSAID();

        $('#lst_SAWToSAC_' + currentSaID).css('display', 'none');
        $('#lst_SA_Received_' + currentSaID).css('display', 'none');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'none');
        $('#lst_DelayedOrder_Cancelled_' + currentSaID).css('display', 'none');
        $('#lst_DelayedOrder_OnHold_' + currentSaID).css('display', 'none');
        $('#lst_PurchaseOrder_Received_' + currentSaID).css('display', 'none');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'none');
        $("#hiddenCurrentSaCommentCount").val('0');

        funcClickNext();
        CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-', true);
//        setTimeout(CreateCommentLinkDocument(),100);
    });
}

function initEventClickBack() {
    $('#btn_Previous').unbind('click');
    $("#btn_Previous").click(function () {
        // hide current doc
        var currentSaID = getSAID();

        $('#lst_SAWToSAC_' + currentSaID).css('display', 'none');
        $('#lst_SA_Received_' + currentSaID).css('display', 'none');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'none');
        $('#lst_DelayedOrder_Cancelled_' + currentSaID).css('display', 'none');
        $('#lst_DelayedOrder_OnHold_' + currentSaID).css('display', 'none');
        $('#lst_PurchaseOrder_Received_' + currentSaID).css('display', 'none');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'none');
        $("#hiddenCurrentSaCommentCount").val('0');

        funcClickBack();

        CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-', true);
//        setTimeout(CreateCommentLinkDocument(), 100);
    });
}

function setSA_RV_Document() {
    var currentSaID = getSAID();

    $("[id^=lst_SAWToSAC_]").css('display', 'none');
    $("[id^=lst_SA_Received_]").css('display', 'none');
    $("[id^=lst_Structural_Decision_]").css('display', 'none');
    $("[id^=lst_DelayedOrder_Cancelled_]").css('display', 'none');
    $("[id^=lst_DelayedOrder_OnHold_]").css('display', 'none');
    $("[id^=lst_PurchaseOrder_Received_]").css('display', 'none');
    $("[id^=lst_Structural_Decision_]").css('display', 'none');

    $('#lst_SAWToSAC_' + currentSaID).css('display', 'block');
    $('#lst_SA_Received_' + currentSaID).css('display', 'block');
    $('#lst_Structural_Decision_' + currentSaID).css('display', 'block');
    $('#lst_DelayedOrder_Cancelled_' + currentSaID).css('display', 'block');
    $('#lst_DelayedOrder_OnHold_' + currentSaID).css('display', 'block');
    $('#lst_PurchaseOrder_Received_' + currentSaID).css('display', 'block');
    $('#lst_Structural_Decision_' + currentSaID).css('display', 'block');

    $("[id^=lst_RevisionReveivedDate_]").css('display', 'none');

    var revID = $("#MainContent_hidden_revision_id").val();
    $('#lst_RevisionReveivedDate_' + revID).css('display', 'block');
}

function initEventClickAddNew() {
    $('#btn_AddNew').unbind('click');
    $("#btn_AddNew").click(function () {
        // hide current doc
        var currentSaID = getSAID();

        $('#lst_SAWToSAC_' + currentSaID).css('display', 'none');
        $('#lst_SA_Received_' + currentSaID).css('display', 'none');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'none');
        $('#lst_DelayedOrder_Cancelled_' + currentSaID).css('display', 'none');
        $('#lst_DelayedOrder_OnHold_' + currentSaID).css('display', 'none');
        $('#lst_PurchaseOrder_Received_' + currentSaID).css('display', 'none');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'none');

        funcClickAddNew();
    });
}

function initEventClickRVAddNew() {
    $('#btnRevisionAdd').unbind('click');
    $("#btnRevisionAdd").click(function () {
        funcRVClickAddNew();
    });
}


function initEventClickRVBack() {
    $('#btnRevisionPrevious').unbind('click');
    $('#btnRevisionPrevious').click(function () {
        $("[id^=lst_RevisionReveivedDate_]").css('display', 'none');
        funcClickRVBack();
        var revID = $("#MainContent_hidden_revision_id").val();
        $('#lst_RevisionReveivedDate_' + revID).css('display', 'block');
    });
}

function initEventClickRVNext() {
    $('#btnRevisionNext').unbind('click');
    $('#btnRevisionNext').click(function () {
        $("[id^=lst_RevisionReveivedDate_]").css('display', 'none');
        funcRVClickNext();
        var revID = $("#MainContent_hidden_revision_id").val();
        $('#lst_RevisionReveivedDate_' + revID).css('display', 'block');
    });
}
function ShowDialog() {
    if (IsSave_SA == true || IsSave_RV == true) {
        var sText = $('#hidden_SA_popup').val();
        if (sText == null || sText == "") {
            sText = $('#hidden_revisions_popup').val();
        }

        UpdatePanelDialog('Status', sText);
        IsSave_SA = false;
    }

}

function UpdatePanelDialog(title, message) {
    // Delete Modal Dialog
    var delDialogHtml = '<div id="dialog-message" title="' + title + '">' +
                        '<p>' + message + '</p></div>';
    $('#dialog-message').remove();
    $(delDialogHtml).dialog({});
    $('#dialog-message').dialog('open');
}

function funcClickNext() {
    var index = getBufferIndex();
    var count = getCount();
    var intI = (parseInt(index) + 1);
    if (intI < count) {
        if (intI - 1 >= 0) SetDataFromUI(intI - 1);
        setLabelPageCurrent(intI);
        setBufferIndex(intI);
        SetDataToUI(intI);
        $("#MainContent_slideshow2").show("slide", { direction: "right" }, 220);
        initComment(intI);
    }
    else {
        // do something for last page
        var currentSaID = getSAID();

        $('#lst_SAWToSAC_' + currentSaID).css('display', 'block');
        $('#lst_SA_Received_' + currentSaID).css('display', 'block');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'block');
        $('#lst_DelayedOrder_Cancelled_' + currentSaID).css('display', 'block');
        $('#lst_DelayedOrder_OnHold_' + currentSaID).css('display', 'block');
        $('#lst_PurchaseOrder_Received_' + currentSaID).css('display', 'block');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'block');

    }
}
function funcRVClickNext() {
    var index = getRVBufferIndex();
    var count = getRVCount();
    var intI = (parseInt(index) + 1);
    if (intI < count) {
        if (intI - 1 >= 0) RVSetDataFromUI(intI - 1);

        setRVPageCurrent(intI);
        setRVBufferIndex(intI);
        setRVDataToUI(intI);
        $("#Test-div").show("slide", { direction: "right" }, 220); //
    }
}
function funcClickBack() {
    var index = getBufferIndex();
    var intI = (parseInt(index) - 1);
    if (intI >= 0) {
        if (intI + 1 < getCount()) SetDataFromUI(intI + 1);
        setLabelPageCurrent(intI);
        setBufferIndex(intI);
        SetDataToUI(intI);
        $("#MainContent_slideshow2").show("slide", { direction: "left" }, 220);
        initComment(intI);
    }
    else {
        // do something for first page
        // display current document data
        var currentSaID = getSAID();

        $('#lst_SAWToSAC_' + currentSaID).css('display', 'block');
        $('#lst_SA_Received_' + currentSaID).css('display', 'block');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'block');
        $('#lst_DelayedOrder_Cancelled_' + currentSaID).css('display', 'block');
        $('#lst_DelayedOrder_OnHold_' + currentSaID).css('display', 'block');
        $('#lst_PurchaseOrder_Received_' + currentSaID).css('display', 'block');
        $('#lst_Structural_Decision_' + currentSaID).css('display', 'block');
    }
}
function funcClickRVBack() {
    var index = getRVBufferIndex();
    var intI = (parseInt(index) - 1);

    if (intI >= 0) {
        if (intI + 1 < getRVCount()) RVSetDataFromUI(intI + 1);
        setRVPageCurrent(intI);
        setRVBufferIndex(intI);
        setRVDataToUI(intI);
        $("#Test-div").show("slide", { direction: "left" }, 220);
    }
    else {
        // do something for first page
    }
}

function funcClickAddNew() {
    var IsNew = parseInt(getIsNew());
    if (IsNew == 0) {
        var oldIndex = getBufferIndex();
        SetDataFromUI(oldIndex);
        increaseCount();
        ClearUI();
        var c = getCount();
        var i = (parseInt(c) - 1);
        setBufferIndex(i);
        setIsNew(1);
        setLabelPageCurrent(i);
        setLabelPageTotal(getCount());
        $("#MainContent_slideshow2").show("slide", { direction: "right" }, 220);

        //hide all document link
        $("[id^=lst_SAWToSAC_]").css('display', 'none');
        $("[id^=lst_SA_Received_]").css('display', 'none');
        $("[id^=lst_Structural_Decision_]").css('display', 'none');
        $("[id^=lst_DelayedOrder_Cancelled_]").css('display', 'none');
        $("[id^=lst_DelayedOrder_OnHold_]").css('display', 'none');
        $("[id^=lst_PurchaseOrder_Received_]").css('display', 'none');
        $("[id^=lst_Structural_Decision_]").css('display', 'none');


        $("#MainContent_link_cancel").hide();
        $("#MainContent_link_post").hide();
        $("#MainContent_link_addnew").hide();
        $("#div-add-new-comment").hide();
        $("#hiddenCurrentSaCommentCount").val('0');
    }
    else {

    }
}

function funcRVClickAddNew() {
    var IsNew = parseInt(getRVIsNew());

    //hide all document link
    if (IsNew == 0) {
        var oldIndex = getRVBufferIndex();
        RVSetDataFromUI(oldIndex);
        increaseRVCount();
        ClearRVUI();
        var c = getRVCount();
        var i = (parseInt(c) - 1);
        setRVBufferIndex(i);
        setRVIsNew(1);
        setRVPageCurrent(i);
        setRVPageTotal(getRVCount());
        $("[id^=lst_RevisionReveivedDate_]").css('display', 'none');
        $("#Test-div").show("slide", { direction: "right" }, 220);
    }
    else {

    }
}

function setLabelPageCurrent(data) {
    var i = (parseInt(data) + 1);
    if (getCount() == 0) {
        i = 0;
    }

    $("#current-page").text(i);
}

function setLabelPageTotal(data) {
    $("#total-page").text(data);
}

function setRVPageCurrent(data) {
    var i = (parseInt(data) + 1);
    if (getRVCount() == 0) {
        i = 0;
    }
    $("#spanPageNumberCurrent").text(i);
}
function setRVPageTotal(data) {
    $("#spanPageNumberMax").text(data);
}

function increaseCount() {
    var count = $("#hidden_count").text();
    $("#hidden_count").text("");
    count++;
    $("#hidden_count").text(count);
    return count;
}

function increaseRVCount() {
    var count = $("#hidden_rv_count").text();
    $("#hidden_rv_count").text("");
    count++;
    $("#hidden_rv_count").text(count);
    return count;
}

function setBtnCount(count) {
    if (count == 0) {
        $("#hidden_count").text(0);
        ClearUI();
        reloadSAComment(0);
        setLabelPageTotal(0);
        setLabelPageCurrent(-1);
        setIsNew(1);
    }
    else {
        $("#hidden_count").text(count);
        SetDataToUI(count - 1);
        reloadSAComment(count - 1);
        setLabelPageTotal(count);
        setLabelPageCurrent(count - 1);
        setIsNew(0);
    }
}

function getCount() {
    var count = $("#hidden_count").text();
    return count;
}

function getRVCount() {
    var count = $("#hidden_rv_count").text();
    return count;
}
function SelectBtnIndex(intdex) {
    if ($("#btn_page_" + intdex).attr('class') != "activeSlide") {
        $("[id^=btn_page_]").removeClass("activeSlide");
        $("#btn_page_" + intdex).addClass("activeSlide");
        setBufferIndex(intdex);
    }
}

function setBufferIndex(data) {
    excludeCommentsIdArray = new Array();
    $("#BufferIndex").html("");
    $("#BufferIndex").html(data);
}

function getBufferIndex() {
    var data = $("#BufferIndex").html();
    return data;
}

function setRVBufferIndex(data) {
    $("#rv_bufferIndex").html("");
    $("#rv_bufferIndex").html(data);
}

function getRVBufferIndex() {
    var data = $("#rv_bufferIndex").html();
    return data;
}

function setBtnRVCount(count) {
    if (count == 0) {
        $("#hidden_rv_count").text(0);
        ClearRVUI();
        setRVPageTotal(0);
        setRVPageCurrent(-1);
        setRVIsNew(1);
    }
    else {
        $("#hidden_rv_count").text(count);
        setRVDataToUI(count - 1);
        setRVPageTotal(count);
        setRVPageCurrent(count - 1);
        setRVIsNew(0);
    }
}
function getSAID() {
    var id = $("#MainContent_hidden_SA_ID").val();
    return id;
}
function getRVID() {
    var id = $("#MainContent_hidden_revision_id").val();
    return id;
}

function addEvenPaging() {

}

function SetIndexPagingStartUp() {
    var count = getCount();
}

function setPageIndex(index) {
    $("#PageIndex").html("");
    $("#PageIndex").html(index);
}

function getPageIndex() {
    var index = $("#PageIndex").html();
    return index;
}

function getIsNew() {
    var val = $("#hidden_SA_IsNew").val();
    return val;
}

function getIsSave() {
    var val = $("#hidden_SA_IsSave").val();
    return val;
}

function setIsNew(data) {
    $("#hidden_SA_IsNew").val(data);
}

function setIsSave(data) {
    $("#hidden_SA_IsSave").val(data);
}


///////////Revision Tab //////////////

function setRVIsNew(data) {
    $("#hidden_RV_IsNew").val(data);
}
function getRVIsNew() {
    var val = $("#hidden_RV_IsNew").val();
    return val;
}
function setRVIsSave(data) {
    $("#hidden_RV_IsSave").val(data);
}
function getRVIsSave() {
    var val = $("#hidden_RV_IsSave").val();
    return val;
}

///////////  Control Data ////////////

function SelectIndexOrdered() {
    $("#MainContent_ddlProcessDates_ColLeft1").val("3");
}

function setRVDataToUI(index) {
    var data = GetData(index, "#MainContent_hidden_revisions");
    $("[id^=lst_RevisionReveivedDate_]").css('display', 'none');

    if (data != null) {
        $('#lst_RevisionReveivedDate_' + data.ID).css('display', 'block');

        $("#MainContent_txtRevisionReceivedDate").val(data.revision_rcvd_date);
        $("#MainContent_txtRevisionFeeReceivedDate").val(data.revision_fee_rcvd_date);
        $("#MainContent_txtRevisionCheckNo").val(data.revision_fee_check_po_nbr);

        if (data.revision_fee_amount == "" || data.revision_fee_amount == null) {
            $("#MainContent_txtRevisionFeeAmount").val("$");
        }
        else {
            $("#MainContent_txtRevisionFeeAmount").val(data.revision_fee_amount);
        }

        $("#MainContent_hidden_revision_id").val(data.ID);
        $("#MainContent_txtRevisionFeeAmount").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_hidden_document_RevisionUpdatedAt").val(data.revision_updated_at);
    }
    else {
        ClearRVUI();
    }
}
function SetDataToUI(index) {
    var data = GetData(index, "#MainContent_hidden_SA");

    if (data != null) {
        // Purchase Order
        $("#MainContent_ddlPurchaseOrder_Vendor_ColLeft1").val(data.sa_vendor_id);

        $("#MainContent_txtPurchaseOrder_Ordered_ColLeft1").val(data.po_ordered_date);
        $("#MainContent_txtPurchaseOrder_Received_ColLeft1").val(data.po_received_date);

        $("#MainContent_txtPurchaseOrder_Number_ColLeft1").val(data.po_number);

        // Delayed Order
        $("#MainContent_txtDelayedOrder_Onhold_ColRight1").val(data.sa_order_onhold_date);
        $("#MainContent_txtDelayedOrder_Cancelled_ColRight1").val(data.sa_order_cancelled_date);

        //Process Dates
        $("#MainContent_ddlProcessDates_Type_ColLeft1").val(data.structural_analysis_type_id);
        $("#MainContent_ddlProcessDates_ColLeft1").val(data.sa_ordered_date_status_id);
        $("#MainContent_txtProcessDates_Ordered_ColLeft1").val(data.sa_ordered_date);
        $("#MainContent_txtProcessDates_Received_ColLeft1").val(data.sa_received_date);
        $("#MainContent_ddlProcess_ReceivedStatus_ColLeft1").val(data.sa_received_date_status_id);
        $("#MainContent_txtProcessDates_TowerPercent_ColLeft1").val(data.sa_tower_prcnt);

        //Payment
        $("#MainContent_ddlPayment_PayorType_ColRight1").val(data.structural_analysis_fee_payor_type_id);
        $("#MainContent_txtPayment_FeeProcessed_ColRight1").val(data.sa_fee_received_date);
        $("#MainContent_txtPayment_FeeAmount_ColRight1").val(data.sa_fee_amount);
        $("#MainContent_txtPayment_CheckNumber_ColRight1").val(data.sa_check_nbr);

        //Other
        $("#MainContent_ddlProcessDates_SACDescription_ColLeft1").val(data.sac_description_id);
        $("#MainContent_txtRequest_SAWToCustomer_ColLeft1").val(data.saw_to_customer_date);
        $("#MainContent_txtRequest_SAWApproved_ColLeft1").val(data.saw_approved_date);
        $("#MainContent_txtRequest_SAWToSAC_ColLeft1").val(data.saw_to_sac_date);
        $("#MainContent_txtRequest_SAWRequest_ColLeft1").val(data.saw_request_date);
        $("#MainContent_ddlRequest_SACName_ColLeft1").val(data.sac_name_emp_id);
        $("#MainContent_ddlRequest_SACPriority_ColLeft1").val(data.sac_priority_id);

        $("#MainContent_txtPurchaseOrder_ShoppingCartNumber_ColLeft1").val(data.shopping_cart_number);
        $("#MainContent_ddlPurchaseOrder_BlanketPOMonth_ColLeft1").val(data.blanket_po_month);
        $("#MainContent_ddlPurchaseOrder_BlanketPOYear_ColLeft1").val(data.blanket_po_year);
        $("#MainContent_ddlPurchaseOrder_POReleaseStatus_ColLeft1").val(data.po_release_status_id);
        $("#MainContent_txtPurchaseOrder_PORelease_ColLeft1").val(data.po_release_date);
        $("#MainContent_txtPurchaseOrder_PONotes_ColLeft1").val(data.po_notes);

        $("#MainContent_hidden_SA_updated_at").val(data.sa_updated_at);

        setVisibleList(data.sa_vendor_id, "MainContent_ddlPurchaseOrder_Vendor_ColLeft1", "(historical)");

        setVisibleList(data.sac_description_id, "MainContent_ddlOther_SACDescription_ColRight1", "(historical)");

        $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_txtPayment_FeeAmount_ColRight1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });

        $("#MainContent_hidden_SA_ID").val(data.ID);

        if (data.po_amount == "" || data.po_amount == null) {
            $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").val("$");
        }
        else {
            $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").val(data.po_amount);
        }

        if (data.sa_fee_amount == "" || data.sa_fee_amount == null) {
            $("#MainContent_txtPayment_FeeAmount_ColRight1").val("$");
        }
        else {
            $("#MainContent_txtPayment_FeeAmount_ColRight1").val(data.sa_fee_amount);
        }

        $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });
        $("#MainContent_txtPayment_FeeAmount_ColRight1").formatCurrency({ colorize: true, negativeFormat: '-%s%n', roundToDecimalPlace: 2 });

        $("#MainContent_txt_sa_decision").val(data.sa_decision_date);
        $("#MainContent_ddl_sa_decision").val(data.sa_decision_type_id);

        //hide all document link
        $("[id^=lst_SAWToSAC_]").css('display', 'none')
        $("[id^=lst_SA_Received_]").css('display', 'none')
        $("[id^=lst_Structural_Decision_]").css('display', 'none')
        $("[id^=lst_DelayedOrder_Cancelled_]").css('display', 'none')
        $("[id^=lst_DelayedOrder_OnHold_]").css('display', 'none')
        $("[id^=lst_PurchaseOrder_Received_]").css('display', 'none')
        $("[id^=lst_Structural_Decision_]").css('display', 'none')

        //display current document
        $('#lst_SAWToSAC_' + data.ID).css('display', 'block');
        $('#lst_SA_Received_' + data.ID).css('display', 'block');
        $('#lst_Structural_Decision_' + data.ID).css('display', 'block');
        $('#lst_DelayedOrder_Cancelled_' + data.ID).css('display', 'block');
        $('#lst_DelayedOrder_OnHold_' + data.ID).css('display', 'block');
        $('#lst_PurchaseOrder_Received_' + data.ID).css('display', 'block');
        $('#lst_Structural_Decision_' + data.ID).css('display', 'block');

        if (data.ID == 0) {
            $('#emptySAWToSACDocDiv').css('display', 'block');
            $('#emptySA_ReceivedDocDiv').css('display', 'block');
        }
        else {
            $('#emptySAWToSACDocDiv').css('display', 'none');
            $('#emptySA_ReceivedDocDiv').css('display', 'none');
        }

        createCommentUI(index);
    }
    else {
        ClearUI();
    }

}

function SetDataFromUI(index) {

    var data = GetAllJsonData();

    if (data == null && index == 0) {
        data = new Array();
    }

    if (data != null && index <= data.length) {
        if (index == data.length) {
            var newData = GetCtrlData("#MainContent_hidden_SA_Template");
            data[index] = newData;
        }
        // Purchase Order
        data[index].sa_vendor_id = $("#MainContent_ddlPurchaseOrder_Vendor_ColLeft1").val();
        data[index].po_ordered_date = $("#MainContent_txtPurchaseOrder_Ordered_ColLeft1").val();
        data[index].po_received_date = $("#MainContent_txtPurchaseOrder_Received_ColLeft1").val();
        data[index].po_number = $("#MainContent_txtPurchaseOrder_Number_ColLeft1").val();

        // Delayed Order
        data[index].sa_order_onhold_date = $("#MainContent_txtDelayedOrder_Onhold_ColRight1").val();
        data[index].sa_order_cancelled_date = $("#MainContent_txtDelayedOrder_Cancelled_ColRight1").val();

        //Process Dates
        data[index].structural_analysis_type_id = $("#MainContent_ddlProcessDates_Type_ColLeft1").val();
        data[index].sa_ordered_date_status_id = $("#MainContent_ddlProcessDates_ColLeft1").val();
        data[index].sa_ordered_date = $("#MainContent_txtProcessDates_Ordered_ColLeft1").val();
        data[index].sa_received_date = $("#MainContent_txtProcessDates_Received_ColLeft1").val();
        data[index].sa_received_date_status_id = $("#MainContent_ddlProcess_ReceivedStatus_ColLeft1").val();
        data[index].sa_tower_prcnt = $("#MainContent_txtProcessDates_TowerPercent_ColLeft1").val();

        //Payment
        data[index].structural_analysis_fee_payor_type_id = $("#MainContent_ddlPayment_PayorType_ColRight1").val();
        data[index].sa_fee_received_date = $("#MainContent_txtPayment_FeeProcessed_ColRight1").val();
        data[index].sa_fee_amount = $("#MainContent_txtPayment_FeeAmount_ColRight1").val();
        data[index].sa_check_nbr = $("#MainContent_txtPayment_CheckNumber_ColRight1").val();

        //OTher
        data[index].sac_description_id = $("#MainContent_ddlProcessDates_SACDescription_ColLeft1").val();
        data[index].saw_to_customer_date = $("#MainContent_txtRequest_SAWToCustomer_ColLeft1").val();
        data[index].saw_approved_date = $("#MainContent_txtRequest_SAWApproved_ColLeft1").val();
        data[index].saw_to_sac_date = $("#MainContent_txtRequest_SAWToSAC_ColLeft1").val();
        data[index].saw_request_date = $("#MainContent_txtRequest_SAWRequest_ColLeft1").val();
        data[index].sac_name_emp_id = $("#MainContent_ddlRequest_SACName_ColLeft1").val();
        data[index].sac_priority_id = $("#MainContent_ddlRequest_SACPriority_ColLeft1").val();
        data[index].shopping_cart_number = $("#MainContent_txtPurchaseOrder_ShoppingCartNumber_ColLeft1").val();
        data[index].blanket_po_month = $("#MainContent_ddlPurchaseOrder_BlanketPOMonth_ColLeft1").val();
        data[index].blanket_po_year = $("#MainContent_ddlPurchaseOrder_BlanketPOYear_ColLeft1").val();
        data[index].po_release_status_id = $("#MainContent_ddlPurchaseOrder_POReleaseStatus_ColLeft1").val();
        data[index].po_release_date = $("#MainContent_txtPurchaseOrder_PORelease_ColLeft1").val();
        data[index].po_notes = $("#MainContent_txtPurchaseOrder_PONotes_ColLeft1").val();
        data[index].sa_updated_at = $("#MainContent_hidden_SA_updated_at").val();

        //currency format
        data[index].po_amount = $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").val().toString().replace('$', '');
        data[index].sa_fee_amount = $("#MainContent_txtPayment_FeeAmount_ColRight1").val().toString().replace('$', '');

        //SA Decision
        data[index].sa_decision_date = $("#MainContent_txt_sa_decision").val();
        data[index].sa_decision_type_id = $("#MainContent_ddl_sa_decision").val();

        SetData("#MainContent_hidden_SA", data);
    }
    else {
        ClearUI();
    }

}

function RVSetDataFromUI(index) {

    var data = GetAllRVJsonData();

    if (data == null && index == 0) {
        data = new Array();
    }
    if (data != null && index <= data.length) {
        if (index == data.length) {
            var newData = GetCtrlData("#MainContent_hidden_revision_template");
            data[index] = newData;
        }

        data[index].revision_rcvd_date = $("#MainContent_txtRevisionReceivedDate").val();
        data[index].revision_fee_rcvd_date = $("#MainContent_txtRevisionFeeReceivedDate").val();
        data[index].revision_fee_check_po_nbr = $("#MainContent_txtRevisionCheckNo").val();
        data[index].revision_fee_amount = $("#MainContent_txtRevisionFeeAmount").val().toString().replace('$', '');
        data[index].revision_updated_at = $("#MainContent_hidden_document_RevisionUpdatedAt").val();

        SetData("#MainContent_hidden_revisions", data);
    }
    else {
        ClearRVUI();
    }

}

function ClearUI() {
    clearComment();
    // Purchase Order
    $("#MainContent_ddlPurchaseOrder_Vendor_ColLeft1").val("");
    $("#MainContent_txtPurchaseOrder_Ordered_ColLeft1").val("");
    $("#MainContent_txtPurchaseOrder_Received_ColLeft1").val("");
    $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").val("$");
    $("#MainContent_txtPurchaseOrder_Number_ColLeft1").val("");

    // Delayed Order
    $("#MainContent_txtDelayedOrder_Onhold_ColRight1").val("");
    $("#MainContent_txtDelayedOrder_Cancelled_ColRight1").val("");

    //Process Dates
    $("#MainContent_ddlProcessDates_Type_ColLeft1").val("");
    $("#MainContent_ddlProcessDates_ColLeft1").val("");
    $("#MainContent_txtProcessDates_Ordered_ColLeft1").val("");
    $("#MainContent_txtProcessDates_Received_ColLeft1").val("");
    $("#MainContent_ddlProcess_ReceivedStatus_ColLeft1").val("");
    $("#MainContent_txtProcessDates_TowerPercent_ColLeft1").val("");

    //Payment
    $("#MainContent_ddlPayment_PayorType_ColRight1").val("");
    $("#MainContent_txtPayment_FeeProcessed_ColRight1").val("");
    $("#MainContent_txtPayment_FeeAmount_ColRight1").val("$");
    $("#MainContent_txtPayment_CheckNumber_ColRight1").val("");

    //OTher
    $("#MainContent_ddlRequest_SACName_ColLeft1").val("");
    $("#MainContent_txtRequest_SAWToCustomer_ColLeft1").val("");
    $("#MainContent_txtRequest_SAWApproved_ColLeft1").val("");
    $("#MainContent_txtRequest_SAWToSAC_ColLeft1").val("");
    $("#MainContent_txtRequest_SAWRequest_ColLeft1").val("");
    $("#MainContent_ddlRequest_SACPriority_ColLeft1").val("");
    $("#MainContent_ddlProcessDates_SACDescription_ColLeft1").val("");
    $("#MainContent_txtPurchaseOrder_ShoppingCartNumber_ColLeft1").val("");
    $("#MainContent_ddlPurchaseOrder_BlanketPOMonth_ColLeft1").val("");
    $("#MainContent_ddlPurchaseOrder_BlanketPOYear_ColLeft1").val("");
    $("#MainContent_ddlPurchaseOrder_POReleaseStatus_ColLeft1").val("");
    $("#MainContent_txtPurchaseOrder_PORelease_ColLeft1").val("");
    $("#MainContent_txtPurchaseOrder_PONotes_ColLeft1").val("");
    $("#MainContent_hidden_SA_updated_at").val("");

    $("#MainContent_txt_sa_decision").val("");
    $("#MainContent_ddl_sa_decision").val("");

    setVisibleList("", "MainContent_ddlPurchaseOrder_Vendor_ColLeft1", "(historical)");
    setVisibleList("", "MainContent_ddlOther_SACDescription_ColRight1", "(historical)");

    $("#MainContent_hidden_SA_ID").val("0");
}

function ClearRVUI() {
    $("#MainContent_txtRevisionReceivedDate").val("");
    $("#MainContent_txtRevisionFeeReceivedDate").val("");
    $("#MainContent_txtRevisionCheckNo").val("");
    $("#MainContent_txtRevisionFeeAmount").val("$");

    $("#MainContent_hidden_revision_id").val("0");
    $("#MainContent_hidden_document_RevisionUpdatedAt").val("");
}

function funcSACheckBeforeSave() {
    var messageErr = "";
    var check = true;
    var IsCurrencyErr = false;

    // Checking for the .length of the object is a quick jQuery way to check if an object exists.        
    // Date Checking.
    if (
            ($("#MainContent_txtRequest_SAWRequest_ColLeft1").length && $("#MainContent_txtRequest_SAWRequest_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtRequest_SAWRequest_ColLeft1")) |
            ($("#MainContent_txtRequest_SAWToCustomer_ColLeft1").length && $("#MainContent_txtRequest_SAWToCustomer_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtRequest_SAWToCustomer_ColLeft1")) |
            ($("#MainContent_txtRequest_SAWApproved_ColLeft1").length && $("#MainContent_txtRequest_SAWApproved_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtRequest_SAWApproved_ColLeft1")) |
            ($("#MainContent_txtRequest_SAWToSAC_ColLeft1").length && $("#MainContent_txtRequest_SAWToSAC_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtRequest_SAWToSAC_ColLeft1")) |
            ($("#MainContent_txtPurchaseOrder_Ordered_ColLeft1").length && $("#MainContent_txtPurchaseOrder_Ordered_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtPurchaseOrder_Ordered_ColLeft1")) |
            ($("#MainContent_txtPurchaseOrder_Received_ColLeft1").length && $("#MainContent_txtPurchaseOrder_Received_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtPurchaseOrder_Received_ColLeft1")) |
            ($("#MainContent_txtPurchaseOrder_PORelease_ColLeft1").length && $("#MainContent_txtPurchaseOrder_PORelease_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtPurchaseOrder_PORelease_ColLeft1")) |
            ($("#MainContent_txtProcessDates_Ordered_ColLeft1").length && $("#MainContent_txtProcessDates_Ordered_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtProcessDates_Ordered_ColLeft1")) |
            ($("#MainContent_txtProcessDates_Received_ColLeft1").length && $("#MainContent_txtProcessDates_Received_ColLeft1").val().toString() != "" && !isValidDate("#MainContent_txtProcessDates_Received_ColLeft1")) |
            ($("#MainContent_txtDelayedOrder_Onhold_ColRight1").length && $("#MainContent_txtDelayedOrder_Onhold_ColRight1").val().toString() != "" && !isValidDate("#MainContent_txtDelayedOrder_Onhold_ColRight1")) |
            ($("#MainContent_txtDelayedOrder_Cancelled_ColRight1").length && $("#MainContent_txtDelayedOrder_Cancelled_ColRight1").val().toString() != "" && !isValidDate("#MainContent_txtDelayedOrder_Cancelled_ColRight1")) |
            ($("#MainContent_txtPayment_FeeProcessed_ColRight1").length && $("#MainContent_txtPayment_FeeProcessed_ColRight1").val().toString() != "" && !isValidDate("#MainContent_txtPayment_FeeProcessed_ColRight1"))
           ) {
        check = false;
        messageErr += messageValidateDateTime;
    }

    // Percent.
    if ($("#MainContent_txtProcessDates_TowerPercent_ColLeft1").length && $("#MainContent_txtProcessDates_TowerPercent_ColLeft1").val().toString() != "")
        if (!validatePercent("#MainContent_txtProcessDates_TowerPercent_ColLeft1", 0)) {
            check = false;
            messageErr += messageValidatePercent;
        }

    // Currenncy.
    if ($("#MainContent_txtPurchaseOrder_Amount_ColLeft1").length && $("#MainContent_txtPurchaseOrder_Amount_ColLeft1").val().toString() != "") {
        if (!validateCurrency("#MainContent_txtPurchaseOrder_Amount_ColLeft1")) {
            check = false;
            messageErr += messageValidateCurrency;
            IsCurrencyErr = true;
        }
    }

    if ($("#MainContent_txtPayment_FeeAmount_ColRight1").length && $("#MainContent_txtPayment_FeeAmount_ColRight1").val().toString() != "") {
        if (!validateCurrency("#MainContent_txtPayment_FeeAmount_ColRight1")) {
            check = false;
            if (!IsCurrencyErr) messageErr += messageValidateCurrency;
        }
    }

    sCurrentTab = SA_TAB;

    if (check) {
        setIsNew(0); // not New
        IsSave_SA = true;
        var index = getBufferIndex();
        SetDataFromUI(index);

        $("#MainContent_btnSubmit_StructuralAnalysis").val("Processing...");
        $("#MainContent_btnSubmit_StructuralAnalysis").attr("disabled", true);
        $("#MainContent_btnSubmit_StructuralAnalysis_Upper").val("Processing...");
        $("#MainContent_btnSubmit_StructuralAnalysis_Upper").attr("disabled", true);
        return true;
    }
    else {
        $("#message_alert").html("");
        showMessageAlert("error", messageErr);
        return false;
    }
}

//Validation for Revisions tab.
function funcRVCheckBeforeSave() {
    var check = true;
    var msgError = "";

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    //Date fields.
    if ($("#MainContent_txtRevisionReceivedDate").length && $("#MainContent_txtRevisionReceivedDate").val().toString() != "" && !isValidDate("#MainContent_txtRevisionReceivedDate")) {
        check = false;
        msgError += messageValidateDateTime;
    }

    if ($("#MainContent_txtRevisionFeeReceivedDate").length && $("#MainContent_txtRevisionFeeReceivedDate").val().toString() != "" && !isValidDate("#MainContent_txtRevisionFeeReceivedDate")) {
        check = false;
        msgError += messageValidateDateTime;
    }

    //Currency field.
    if ($("#MainContent_txtRevisionFeeAmount").length && $("#MainContent_txtRevisionFeeAmount").val().toString() != "" && !validateCurrency("#MainContent_txtRevisionFeeAmount")) {
        check = false;
        msgError += messageValidateCurrency;
    }

    //validateOnlyNumber
    if ($("#MainContent_txtRevisionCheckNo").length && $("#MainContent_txtRevisionCheckNo").val().toString() != "" && !validateOnlyNumber("#MainContent_txtRevisionCheckNo")) {
        check = false;
        msgError += messageValidateInteger;
    }

    sCurrentTab = REVISION_TAB;

    if (check) {
        setRVIsNew(0);
        IsSave_RV = true;
        var oldIndex = getRVBufferIndex();
        RVSetDataFromUI(oldIndex);

        $("#MainContent_btnSubmitRevisionTop").val("Processing...");
        $("#MainContent_btnSubmitRevisionTop").attr("disabled", true);
        $("#MainContent_btnSubmitRevisionBottom").val("Processing...");
        $("#MainContent_btnSubmitRevisionBottom").attr("disabled", true);
        return true;
    }
    else {
        $("#rv_message_alert").html("");
        showMessageAlertByName("error", msgError, "#rv_message_alert");
        return false;
    }
}

function GetData(index, ctrlSource) {
    var jsonStr = $(ctrlSource).val();

    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        var i = parseInt(index);
        return data[i];
    }
}

function SetData(ctrlSource, data) {
    var json_string = JSON.stringify(data);
    $(ctrlSource).val(json_string);
}

function GetAllJsonData() {
    var jsonStr = $("#MainContent_hidden_SA").val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}

function GetAllRVJsonData() {
    var jsonStr = $("#MainContent_hidden_revisions").val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}

function GetCtrlData(sCtrlName) {
    var jsonStr = $(sCtrlName).val();
    if (jsonStr == "") {

        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        return data;
    }
}

function GetRVData(index) {
    var jsonStr = $("#MainContent_hidden_revisions").val();
    if (jsonStr == "") {
        return null;
    }
    else if (jsonStr == null) {
        return null;
    }
    else {
        var data = jQuery.parseJSON(jsonStr);
        var i = parseInt(index);
        return data[i];
    }
}

function setPageName(name) {
    $('body').append("<input type='hidden' id='hidden_PageName' value='" + name + "' >");
}

function getPageName(name) {
    var data = $("#hidden_PageName").val();
    return data;
}

///////// Comment /////////

var isPost = false;

function initComment(index) {
    var data = GetData(index, "#MainContent_hidden_SA");
    var count = 0;
    if (data == null) {
        $("#MainContent_link_cancel").hide();
        $("#MainContent_link_post").hide();
        $("#MainContent_link_addnew").hide();
        $("#div-add-new-comment").hide();
        return;
    }
    else {
        if (data.comments == null) {
            $("#MainContent_link_cancel").hide();
            $("#MainContent_link_post").hide();
            $("#MainContent_link_addnew").hide();
            $("#div-add-new-comment").hide();
            return;
        }
        else {
            count = parseInt(data.comments.length);
        }
    }

    if (count == 0) {
        linkAddNewClick();
    }
    else {
        linkCancelClick();
        if (isPost) {
            $("#div-add-new-comment").slideUp();
        }
        else {
            $("#div-add-new-comment").hide();
        } //HideAddComment
    }
    isPost = false;
}

function reloadSAComment(index) {
    initComment(index);
}

function linkPostClick() {

    $('#btnSAPost').val("Processing...");

    var data = $("#MainContent_txt_comment_SA").val();
    var check = false;
    if (data != "") {
        if (data.toString() != ConstComments.textAreaDefaultMessage.toString()) {
            check = true;
            isPost = true;
            addingNewSaComment = true;
        }
        else {
            check = false;
            isPost = false;
        }
    }
    else {
        check = false;
        isPost = false;
    }

    return check;
}

function linkCancelClick() {


    var count = parseInt(getCount());
    if (count == 0) {
        $("#MainContent_link_addnew").hide();
    }
    else {
        $("#MainContent_link_addnew").show();
        $("#MainContent_link_cancel").hide();
        $("#MainContent_link_post").hide();
        $("#div-add-new-comment").hide();
    }

    return false;
}

function HideAddComment() {
    //   $("#div-add-new-comment").slideUp();
    //   prepareCommentsTextArea(true);
}

function linkAddNewClick() {
    $("#MainContent_link_cancel").show();
    $("#MainContent_link_post").show();
    $("#MainContent_link_addnew").hide();
    $("#div-add-new-comment").slideDown();
    return false;
}

function addUIComment(text_Msg, Creator, CreateTime, CreatorId, index, saCommentID, defaultId) {

    $(".application-comments-inner-sa").append("<div class='comment' id='Comment_Field_inner_SA_" + index + "' saCommentId='SA_" + saCommentID + "'>" +
        "<div class='balloon'>" +
            "<p id='pComment_inner_SA_" + index + "'>" + text_Msg + "</p>" +
            "<div class='balloon-bottom'></div>" +
         "</div>" +
         "<div class='author-info' style='margin-top: 15px; font-size: 10pt; color: #a1a1a1;'>" +
            "<img id='imgUserComment_inner_SA_Png_" + index + "' title='profile-picture' src='/images/avatars/" + CreatorId + ".png' width='25' onload='document.getElementById(\"" + defaultId + "\").style.display=\"none\";' onerror='this.style.display=\"none\";return false;'>" +
            "<img id='imgUserComment_inner_SA_Jpg_" + index + "' title='profile-picture' src='/images/avatars/" + CreatorId + ".jpg' width='25' onload='document.getElementById(\"" + defaultId + "\").style.display=\"none\";' onerror='this.style.display=\"none\";return false;'>" +
            "<img id='" + defaultId + "' title='profile-picture' src='/images/avatars/default.png' width='25'>" +
            "<p id='pCommenter_inner_SA_" + index + "'>" + Creator + ", " + CreateTime + "</p>" +
            "<img type='image'  id='btnDeleteComment_inner_SA_" + index + "' title='Delete Comment' class='btn-comment-delete-inner-sa' src='/images/icons/Erase-Greyscale.png' style='width:12px;' />" +
            "</div>" +
    "</div>");
}

function createCommentUI(index) {
    clearComment();
    var data = GetData(index, "#MainContent_hidden_SA");
    try {
        var count = parseInt(data.comments.length);
        var currentCount = parseInt($("#hiddenCurrentSaCommentCount").val());

        for (var i = 0; i < 10; i++) {
            if (data.comments[currentCount].exist == true) {
                addUIComment(data.comments[currentCount].comment, data.comments[currentCount].creator, (data.comments[currentCount].created_at), data.comments[currentCount].creator_id, currentCount, data.comments[currentCount].comment_id, currentCount);
                currentCount++;
            }
        }

        $("#hiddenCurrentSaCommentCount").val(currentCount);

        if (currentCount >= count) {
            $("#div-more-sa-comment").css('display', 'none');
        }
        else {
            $("#div-more-sa-comment").css('display', 'block');
        }
    }
    catch (err) {
    }

    bindDeleteSACommentButtonActions();

    if (addingNewSaComment) {
        refreshMainCommentsPanel();
        addingNewSaComment = false;

        $('#btnSAPost').val("Post");
    }
}

function loadMoreSaComment() {
   
    var index = getBufferIndex()
    var data = GetData(index, "#MainContent_hidden_SA");
    try {
        
        var count = parseInt(data.comments.length);
        var currentCount = parseInt($("#hiddenCurrentSaCommentCount").val());

        for (var i = 0; i < 10; i++) {
            if (data.comments[currentCount].exist == true) {
                if ($("div[id='Comment_Field_inner_SA_" + currentCount + "']").length == 0) {
                    addUIComment(data.comments[currentCount].comment, data.comments[currentCount].creator, (data.comments[currentCount].created_at), data.comments[currentCount].creator_id, currentCount, data.comments[currentCount].comment_id, currentCount);
                }
            }
            currentCount++;
            $("#hiddenCurrentSaCommentCount").val(currentCount);
        }
    }
    catch (err) {
    }

    if (currentCount >= count) {
        $("#div-more-sa-comment").css('display', 'none');
    }
    else {
        $("#div-more-sa-comment").css('display', 'block');
    }

    CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');

    bindDeleteSACommentButtonActions();

    if (addingNewSaComment) {
        refreshMainCommentsPanel();
        addingNewSaComment = false;

        $('#btnSAPost').val("Post");
    }
}

function EventClickDelComment() {
    bindDeleteSACommentButtonActions();
}

function HandlerDeleteComment(btn) {
    return false;
}

function clearComment() {
    $("[id^=Comment_Field_inner_SA_]").remove();
}

function getIsShowAddComment() {
    $("#hidden_SA_IsShow_Add_Comment").val();
}

function setIsShowAddComment(value) {
    $("#hidden_SA_IsShow_Add_Comment").val(value);
}

function getCommentByID(ui_id) {
    var Comment_index = ui_id.replace("btnDeleteComment_inner_SA_", "");
    var index = getBufferIndex();
    var data = GetData(index, "#MainContent_hidden_SA");
    return data.comments[parseInt(Comment_index)].comment_id;
}

// Bind Delete Comment Button Actions
function bindDeleteSACommentButtonActions() {

    // Delete Modal Dialog
    var delDialogHtml = '<div id="dialog-confirm-comment-delete" title="Delete this comment?"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This comment will be permanently deleted and cannot be recovered. Are you sure?</p></div>';
    $('.btn-comment-delete-inner-sa').unbind('click');
    $('.btn-comment-delete-inner-sa').click(function (e) {
        var ui_id = this.id;
        COMMENT_ID = getCommentByID(this.id);
        e.preventDefault();
        $('#dialog-confirm-comment-delete').remove();
        $(delDialogHtml).dialog({
            buttons: {
                'Confirm': function () {

                    //debugger;
                    var id = window.location.toString().substr(window.location.toString().indexOf('=') + 1);
                    var url = getBaseURL() + "LeaseApplications/SAComment/Delete.aspx?id=" + COMMENT_ID;

                    $.get(url.toString(), function (data) {
                        $("div[sacommentid='SA" + COMMENT_ID + "']").remove();
                        // Refresh Main Comments Panel
                        refreshMainCommentsPanel();
                    });

                    //Get Comment object.                    
                    var Comment_index = ui_id.replace("btnDeleteComment_inner_SA_", "");
                    var index = getBufferIndex();
                    var data = GetAllJsonData();
                    data[index].comments[parseInt(Comment_index)].exist = false;
                    SetData("#MainContent_hidden_SA", data);    //Save to content.

                    excludeCommentsIdArray.push(COMMENT_ID);
                    var index = getBufferIndex();
                    $("#hiddenCurrentSaCommentCount").val('0')
                    createCommentUI(index);

                    $(this).dialog('close');
                    return true;
                },
                'Cancel': function () {
                    $(this).dialog('close');
                    return false;
                }
            }
        });
        $('#dialog-confirm-comment-delete').dialog('open');
    });
    //>
}

function getBaseURL() {
    var url = location.href;  // entire url including querystring - also: window.location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14));
    if (baseURL.indexOf('http://localhost') != -1) {
        // Base Url for localhost
        var url = location.href;  // window.location.href;
        var pathname = location.pathname;  // window.location.pathname;
        var index1 = url.indexOf(pathname);
        var index2 = url.indexOf("/", index1 + 1);
        var baseLocalUrl = url.substr(0, index1);

        return baseLocalUrl + "/";
    }
    else {
        // Root Url for domain name
        return baseURL + "/";
    }

}

function setVisibleList(buffer_id, name, filter) {
    $("#" + name + " option[value]").each(function () {
        var text = $(this).text();
        var check = text.search(filter);
        if (check != -1) {
            var id = $(this).val();
            if (id != buffer_id) {
                $(this).remove();
            }
        }
    });

    if (buffer_id != null) {
        $("#" + name).val(buffer_id);
    }
    else {
        $("#" + name).val("");
    }
}

// SA Link //
function SACommentClick(id) {
    var data = GetAllJsonData();
    $(function () {
        $("#tabs2").tabs({ selected: 1 });
    });
    var intI = 0;
    $("#hiddenCurrentSaCommentCount").val('0');
    for (var i = 0; i < data.length; i++) {
        if (data[i].ID.toString() == id.toString()) {
            intI = i;
            setLabelPageCurrent(intI)
            setBufferIndex(intI);
            SetDataToUI(intI);
            $("#MainContent_slideshow2").show("slide", { direction: "right" }, 220);
            initComment(intI);
            CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-', true);
        }
        else {

            $('#lst_SAWToSAC_' + data.ID).css('display', 'none');
            $('#lst_SA_Received_' + data.ID).css('display', 'none');
            $('#lst_Structural_Decision_' + data.ID).css('display', 'none');
            $('#lst_DelayedOrder_Cancelled_' + data.ID).css('display', 'none');
            $('#lst_DelayedOrder_OnHold_' + data.ID).css('display', 'none');
            $('#lst_PurchaseOrder_Received_' + data.ID).css('display', 'none');
        }
    }
}

// SA Link //
function LECommentClick(id) {
    var data = GetAllJsonData();
    $(function () {
        $("#tabs2").tabs({ selected: 3 });
    });

}
function beforeSaveLeaseTab() {
    var messageErr = "";
    var check = true;
    var IsCurrencyErr = false;

    // Checking for the .length of an object is a quick jQuery way to check if an object exists.
    //Date Checking.
    if (
        ($("#MainContent_txtExecutablesSenttoCustomer").length && $("#MainContent_txtExecutablesSenttoCustomer").val().toString() != "" && !isValidDate("#MainContent_txtExecutablesSenttoCustomer")) |
        ($("#MainContent_txtLOEHoldupReasonTypes_DocsBackfromCustomer").length && $("#MainContent_txtLOEHoldupReasonTypes_DocsBackfromCustomer").val().toString() != "" && !isValidDate("#MainContent_txtLOEHoldupReasonTypes_DocsBackfromCustomer")) |
        ($("#MainContent_txtLOEHoldupReasonTypes_SignedbyCustomer").length && $("#MainContent_txtLOEHoldupReasonTypes_SignedbyCustomer").val().toString() != "" && !isValidDate("#MainContent_txtLOEHoldupReasonTypes_SignedbyCustomer")) |
        ($("#MainContent_txtLOEHoldupReasonTypes_DocstoFSC").length && $("#MainContent_txtLOEHoldupReasonTypes_DocstoFSC").val().toString() != "" && !isValidDate("#MainContent_txtLOEHoldupReasonTypes_DocstoFSC")) |
        ($("#MainContent_txtLOEHoldupReasonTypes_LeaseExecution").length && $("#MainContent_txtLOEHoldupReasonTypes_LeaseExecution").val().toString() != "" && !isValidDate("#MainContent_txtLOEHoldupReasonTypes_LeaseExecution")) |
        ($("#MainContent_txtLOEHoldupReasonTypes_AmendmentExecution").length && $("#MainContent_txtLOEHoldupReasonTypes_AmendmentExecution").val().toString() != "" && !isValidDate("#MainContent_txtLOEHoldupReasonTypes_AmendmentExecution")) |
        ($("#MainContent_txtFinancial_CommencementForecast").length && $("#MainContent_txtFinancial_CommencementForecast").val().toString() != "" && !isValidDate("#MainContent_txtFinancial_CommencementForecast")) |
        ($("#MainContent_txtLOEHoldupReasonTypes_TerminationDate").length && $("#MainContent_txtLOEHoldupReasonTypes_TerminationDate").val().toString() != "" && !isValidDate("#MainContent_txtLOEHoldupReasonTypes_TerminationDate"))
       ) {
        messageErr += messageValidateDateTime;
        check = false;
    }

    //Currency checking.
    if ($("#MainContent_txtFinancial_RentForecastAmount").length && $("#MainContent_txtFinancial_RentForecastAmount").val().toString() != "") {
        if (!validateCurrency("#MainContent_txtFinancial_RentForecastAmount", 0)) {
            check = false;
            messageErr += messageValidateCurrency;
        }
    }

    if ($("#MainContent_txtFinancial_CostShareAgreementAmount").length && $("#MainContent_txtFinancial_CostShareAgreementAmount").val().toString() != "") {
        if (!validateCurrency("#MainContent_txtFinancial_CostShareAgreementAmount", 0)) {
            check = false;
            messageErr += messageValidateCurrency;
        }
    }

    if ($("#MainContent_txtFinancial_CapCostRecoveryAmount").length && $("#MainContent_txtFinancial_CapCostRecoveryAmount").val().toString() != "") {
        if (!validateCurrency("#MainContent_txtFinancial_CapCostRecoveryAmount", 0)) {
            check = false;
            messageErr += messageValidateCurrency;
        }
    }

    if (!check) {
        $("#message_alert_Lease").html("");
        showMessageAlertByName("error", messageErr, "#message_alert_Lease");
        return false;
    }
    else {
        // Remove onbeforeunload event since this script is called as a postback.
        window.onbeforeunload = function () { };

        // Change save button to display processing message and disable it to
        // prevent "double submissions".
        $("#MainContent_btnSubmit_Lease_Upper").val("Processing...");
        $("#MainContent_btnSubmit_Lease_Upper").attr("disabled", true);
        $("#MainContent_btnSubmit_Lease").val("Processing...");
        $("#MainContent_btnSubmit_Lease").attr("disabled", true);
        return true;
    }
}

function showMessageAlertByName(type, message, Name) {
    if ((type == null && message == null) || (type == "" && message == "")) {
        type = ConstMessageTypes.INFO.type;
        message = ConstMessageTypes.INFO.defaultMessage;
    }

    $(Name).append(
            '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                '<p>' + message + '</p>' +
		        '<a class="close" href="javascript:void(0)">close</a>' +
            '</div>' +
            '<div class="cb"></div>'
        );
}

function windowBindingBeforeUnload() {
    $(window).bind('beforeunload', function () {
        return "Do you really want to leave now?";
    });

    $("input.ui-button").click(function () {
        $(window).unbind("beforeunload");
    });
}