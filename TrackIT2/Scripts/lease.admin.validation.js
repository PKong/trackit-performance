﻿
//--------------------Error Message--------------------------------//
function showMessageAlertByName(type, message, Name) {
    if ((type == null && message == null) || (type == "" && message == "")) {
        type = ConstMessageTypes.INFO.type;
        message = ConstMessageTypes.INFO.defaultMessage;
    }

    var alreadyIncreaseSize = false;
    if ($(".ms").length > 0 || $("#HiddenDialogHeightFlag").val() == "true") {
        alreadyIncreaseSize = true;
        $("#HiddenDialogHeightFlag").val("true")
    }
    else {
        alreadyIncreaseSize = false;
        $("#HiddenDialogHeightFlag").val("false")
    }

    $(Name).find(".ms").each(function () {
        $(this).remove()
    });

    $(Name).append(
            '<div class="ms ' + type + '" style="margin-bottom:15px;">' +
                '<p>' + message + '</p>' +
		        '<a class="close" href="javascript:void(0)">close</a>' +
            '</div>' +
            '<div class="cb"></div>'
    );

    $("#HiddenDialogHeightFlag").val("true")

    $('.close').click(function () {
        var msgalert = $(this).parent();
        var fHeight = $(Name).height();
        $("#HiddenDialogHeightFlag").val('false');
        $(msgalert).fadeOut('slow', function () {
            //resize dailog when show error message
            $(".ms").remove();
            if ($('#MainContent_radAssoc').is(':checked')) {
                setDefaultSize();
            }
            else {
                resizeModal(-fHeight);
            }

        });
    });
    //resize dailog when show error message
    var fHeight = $(Name).height();
    if (!alreadyIncreaseSize) {
        resizeModal(fHeight);
    }
}
//--------------------Error Message--------------------------------//

//----------Check auto complete siteId valid null or empty---------//
function validateLeaseAdminSite(name) {
    var data = $(name).val();
    if  ((data.length > 0) &&(data.length != 8)) {
        changeBGColorError(name);
        return false;
    }
    else {
        changeBGColorDefault(name);
        return true;
    }
}
function validateLeaseAdminSiteTextArea(siteID,name) {
    if ((siteID.length > 0) && (siteID.length != 8)) {
        changeBGColorError(name);
        return false;
    }
    else {
        changeBGColorDefault(name);
        return true;
    }
}
//----------Check auto complete siteId valid null or empty---------//

//--------------------Add.aspx-------------------------------------//
function validateCreateAssocLeaseAdmin() {
    var check = true;
    // Check only textbox now, create require at least Assoc SiteID
    var message = '';
    if (!validateLeaseAdminSite("#txtAssocSiteID")) {
        if ($('#MainContent_radNoAssoc').attr('checked') != 'checked') {
            check = false;
            message = messageValidateUIDOnly;
            document.getElementById('MainContent_ddlAssociatedLeaseApps').innerHTML = "";
            $('#MainContent_lblNotfound').val('Please select a Site ID first.');
            $('#MainContent_lblNotfound').css('display', 'block');
            $('#MainContent_ddlAssociatedLeaseApps').css('display', 'none');
        }
        document.getElementById('MainContent_ddlAssociatedLeaseApps').innerHTML = "";
        $('#MainContent_lblNotfound').val('Please select a Site ID first.');
        $('#MainContent_lblNotfound').css('display', 'block');
        $('#MainContent_ddlAssociatedLeaseApps').css('display', 'none');
    }

    if (check) {
        var siteID = $("#txtAssocSiteID").val();
        var checkSite = AjaxCallSiteIDHandler(siteID, "");
        if (checkSite[0]) {
            if ($('#MainContent_ddlAssociatedLeaseApps').css('display') != 'none') {
                if (($('#MainContent_ddlAssociatedLeaseApps').length == 0 || $('#MainContent_ddlAssociatedLeaseApps').val() == "")) {
                    message = "Please, Select Lease Application";
                    $('#MainContent_ddlAssociatedLeaseApps').css("background-color", "#FFBABA");
                    check = false;
                }
            }
            else {
                message = "Lease Application not found!";
                changeBGColorError("#txtAssocSiteID");
                check = false;
            }
        }
        else {
            message = 'Please verify a Site ID.';
            changeBGColorError("#txtAssocSiteID");
            check = false;
        }
    }

    if (!check) {
        showMessageAlertByName("error", message, "#message_alert");
        return false;
    }
    else {
        $("#MainContent_btnSubmitCreateAssoc").val("Processing...");
        $("#MainContent_btnSubmitCreateAssoc").attr("disabled", true);
        return true;
    }
}

function validateSiteIDALL(onClick,textSiteID) {
    var check = true;
    var lstSiteId = [];
    var lstDupSiteId = [];
    var tmpSiteID = "";
    var message = "";
    var dubSiteFlag = false;

    if ($("#MainContent_txtSiteIds").val().toString() != '') {
        var mySplitCommaResult = $("#MainContent_txtSiteIds").val().split(',');
        for (j = 0; j < mySplitCommaResult.length; j++) {
            var mySplitSpaceResult = mySplitCommaResult[j].split(' ');
            for (i = 0; i < mySplitSpaceResult.length; i++) {
                if (mySplitSpaceResult[i] != '') {

                    if (!validateLeaseAdminSiteTextArea(mySplitSpaceResult[i], "#MainContent_txtSiteIds")) {
                        check = false;
                        message = "Please enter a valid Site ID.<br/>";
                        break;
                    }
                    else {
                        if ($.inArray(mySplitSpaceResult[i].toUpperCase(), lstSiteId) < 0) {
                            // Add check isExist siteID
                            if ($("Li[rel='" + mySplitSpaceResult[i].toUpperCase() + "']").length == 0) {
                                tmpSiteID += mySplitSpaceResult[i].toUpperCase() + "|";
                                lstSiteId.push(mySplitSpaceResult[i].toUpperCase());
                            }
                            else {
                                if ($.inArray(mySplitSpaceResult[i].toUpperCase(), lstDupSiteId) == -1) {
                                    lstDupSiteId.push(mySplitSpaceResult[i].toUpperCase());
                                    message += mySplitSpaceResult[i].toUpperCase() + " ";
                                    dubSiteFlag = true;
                                    check = false;
                                }
                            }
                        }
                        else {
                            message = "Site has already been added!";
                            changeBGColorError("#MainContent_txtSiteIds");
                            check = false;
                        }
                    }
                }
            }
        }

        if (dubSiteFlag) {
            message = "Site ID " + message + "was entered more than once.";
            changeBGColorError("#MainContent_txtSiteIds");
        }

        if (check) {
            var ajaxResult = AjaxCallSiteIDHandler(tmpSiteID, message);

            check = ajaxResult[0];
            message = ajaxResult[1];
        }

        if (!check) {
            if (!onClick) {
                showMessageAlertByName("error", message, "#message_alert");
            }
            return message;
        }
    }
    else {
        changeBGColorDefault("#MainContent_txtSiteIds");
    }
}

function AjaxCallSiteIDHandler(tmpSiteID, message) {
    var check = false;
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "/Handlers/SiteIDHandler.axd",
        async: false,
        data: {
            term: tmpSiteID
        },
        error: function (xhr, status, error) {
            //alert("AJAX Error!=" + error);
            message = "Site Id not found!";
            check = false;
        },
        success: function (data) {
            if (data != "[]") {
                message = "Site Id";
                var notFoundData = jQuery.parseJSON(data);
                for (x = 0; x < notFoundData.length; x++) {
                    message += " " + notFoundData[x];
                    if (x == notFoundData.length - 1) {
                        message += " ";
                    }
                    else {
                        message += ",";
                    }
                }
                //message = "Site Id not found!";
                message += " not found!";
                changeBGColorError("#MainContent_txtSiteIds");
                check = false;

                if (!check) {
                    if (!onClick) {
                        showMessageAlertByName("error", message, "#message_alert");
                    }
                }
            }
        }
    });

    if (message == "") {
        return [true, ""];
    }
    else {
        return [false, message];
    }
}

function validateCreateLeaseAdmin() {
    var check = true;
    // Check only list selected site id 
    var lstSiteId = [];

    
    if (!validateLeaseAdminSite("#txtSiteID")) {
        check = false;
        message = messageValidateUIDOnly;
    }

    if ($('#MainContent_radNoAssoc').attr('checked') == 'checked') {
        $('#siteIdList').find('span').each(function () {
            lstSiteId.push($(this).html());
        });

        // check textBox site ID
        if ($("#txtSiteID").val() != "" && $("#txtSiteID").val() != undefined && check) {
            if ($("Li[rel='" + $("#txtSiteID").val().toUpperCase() + "']").length == 0) {
                if (appendSiteSelected($("#txtSiteID").val().toUpperCase())) {
                    lstSiteId.push($("#txtSiteID").val().toUpperCase())
                    $("#txtSiteID").val('');
                }
            }
        }

        if ($("#MainContent_txtSiteIds").val().toString() != '') {
            var mySplitCommaResult = $("#MainContent_txtSiteIds").val().split(',');
            for (j = 0; j < mySplitCommaResult.length; j++) {
                var mySplitSpaceResult = mySplitCommaResult[j].split(' ');
                for (i = 0; i < mySplitSpaceResult.length; i++) {
                    if (mySplitSpaceResult[i] != '') {
                        if ($.inArray(mySplitSpaceResult[i].toUpperCase(), lstSiteId) < 0 && mySplitSpaceResult[i].toUpperCase() != $("#txtSiteID").val().toUpperCase()) {
                            lstSiteId.push(mySplitSpaceResult[i].toUpperCase());
                        }
                        else {
                            message = "Site has already been added!";
                            check = false;
                        }
                    }
                }
            }
        }
    }
    var textAreaValidateResult = validateSiteIDALL(true, $("#txtSiteID").val());

    if (textAreaValidateResult != undefined) {
        check = false;
        message = textAreaValidateResult;
    }

    if (!check) {
        showMessageAlertByName("error", message, "#message_alert");
        return false;
    }
    else {

        if ($("#txtSiteID").val() != "") {
            message = "Please verify a Site ID.";
            showMessageAlertByName("error", message, "#message_alert");
            changeBGColorError("#txtSiteID");
            return false;
        }
        else if (lstSiteId.length == 0 && $('#MainContent_radNoAssoc').attr('checked') != 'checked')
        {
            message = "Please select a Site ID first.";
            showMessageAlertByName("error", message, "#message_alert");
            changeBGColorError("#txtSiteID");
            return false;
        }
        else {
            $('#Hidden_selected_site_ids').val(JSON.stringify(lstSiteId));
            $("#MainContent_btnSubmitCreateApps").val("Processing...");
            $("#MainContent_btnSubmitCreateApps").attr("disabled", true);
            return true;
        }
       
    }
}

function CheckDuplicateSiteId(siteId) {
    var result = false;
    var fHeight = $("#message_alert").height();
    $("#message_alert").html("");
    $('#MainContent_ListSiteId').find('ol').children().each(function () {
        if ($(this).find('span').html() == siteId.toUpperCase()) {
            showMessageAlertByName("error", "Site has already been added!", "#message_alert");
            result = true;
        }
    });
    return result;
}

//--------------------Add.aspx-------------------------------------//

//--------------------Edit.aspx-----------------------------------//

function CheckBeforeSaveLeaseAdmin() {
    var check = true;
    var msgError = '';
    if (($("#MainContent_txtExecDocs_ExecSendtoCust").length && $("#MainContent_txtExecDocs_ExecSendtoCust").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_ExecSendtoCust")) |
        ($("#MainContent_txtExecDocs_BackFromCustDate").length && $("#MainContent_txtExecDocs_BackFromCustDate").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_BackFromCustDate")) |
        ($("#MainContent_txtExecDocs_SignedByCustDate").length && $("#MainContent_txtExecDocs_SignedByCustDate").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_SignedByCustDate")) |
        ($("#MainContent_txtExecDocs_ToFSCDate").length && $("#MainContent_txtExecDocs_ToFSCDate").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_ToFSCDate")) |
        ($("#MainContent_txtSDP_CompletedDate").length && $("#MainContent_txtSDP_CompletedDate").val().toString() != "" && !isValidDate("#MainContent_txtSDP_CompletedDate")) |
        ($("#MainContent_txtExecDocs_RecvedatFSCDate").length && $("#MainContent_txtExecDocs_RecvedatFSCDate").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_RecvedatFSCDate")) |
        ($("#MainContent_txtExecDocs_LeaseExecDate").length && $("#MainContent_txtExecDocs_LeaseExecDate").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_LeaseExecDate")) |
        ($("#MainContent_txtExecDocs_CommencementForecastDate").length && $("#MainContent_txtExecDocs_CommencementForecastDate").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_CommencementForecastDate")) |
        ($("#MainContent_txtExecDocs_CarrierSignature").length && $("#MainContent_txtExecDocs_CarrierSignature").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_CarrierSignature")) |
        ($("#MainContent_txtExecDocs_SendSignator").length && $("#MainContent_txtExecDocs_SendSignator").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_SendSignator")) |
        ($("#MainContent_txtExecDocs_BackSignator").length && $("#MainContent_txtExecDocs_BackSignator").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_BackSignator")) |
        ($("#MainContent_txtExecDocs_OrgSentUPS").length && $("#MainContent_txtExecDocs_OrgSentUPS").val().toString() != "" && !isValidDate("#MainContent_txtExecDocs_OrgSentUPS")) |
        ($("#MainContent_txtFinance_TerminateDate").length && $("#MainContent_txtFinance_TerminateDate").val().toString() != "" && !isValidDate("#MainContent_txtFinance_TerminateDate")) |
        ($("#MainContent_txtComDocs_NTPtoCust").length && $("#MainContent_txtComDocs_NTPtoCust").val().toString() != "" && !isValidDate("#MainContent_txtComDocs_NTPtoCust")) |
        ($("#MainContent_txtComDocs_CommencementDate").length && $("#MainContent_txtComDocs_CommencementDate").val().toString() != "" && !isValidDate("#MainContent_txtComDocs_CommencementDate")) |
        ($("#MainContent_txtComDocs_RecvFSCDate").length && $("#MainContent_txtComDocs_RecvFSCDate").val().toString() != "" && !isValidDate("#MainContent_txtComDocs_RecvFSCDate")) |
        ($("#MainContent_txtComDocs_RegCommencementLetterDate").length && $("#MainContent_txtComDocs_RegCommencementLetterDate").val().toString() != "" && !isValidDate("#MainContent_txtComDocs_RegCommencementLetterDate"))) {
        msgError += messageValidateDateTime;
        check = false;
    }

    if (!check) {
        $("#message_alert").html("");
        showMessageAlert("error", messageValidateUIDOnly);
        return false;
    }
    else {
        window.onbeforeunload = function () { };

        $("#MainContent_btnSubmitLeaseAdmin_Top").val("Processing...");
        $("#MainContent_btnSubmitLeaseAdmin_Bottom").val("Processing...");
        $("#MainContent_btnSubmitLeaseAdmin_Top").attr("disabled", true);
        $("#MainContent_btnSubmitLeaseAdmin_Bottom").attr("disabled", true);
        return true;
    }
}

//--------------------Edit.aspx-----------------------------------//

