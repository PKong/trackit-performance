﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="TrackiT Docs | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.DMS.Default"  %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/dms") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/dms") %>

    <script type="text/javascript">
        // Global Var
        currentFlexiGridID = '<%= this.fgDMS.ClientID %>';
        //example collumn fix ['Site ID','Site Name','Actions']
        var fixColumn = ['Actions'];
        
        // Doc Ready
        $(function () {
            // Init DMS Controls
            InitDMSFlexiRelatedControls("#h1", "#sBasePath", "#hidDMSBasePath", "");
            SetCDragLocation();
        });
        //-->

        // FG Column Change
        function fgColumnChange(cid, visible) {
            DynamicColumnChange(currentFlexiGridID, cid, visible);
        }
        //-->
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="message_alert" class="full-col"></div>
 
    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 id="h1" style="float:left; padding-top:0px; padding-right:30px;">TrackiT Docs</h1>
	        <div class="page-options-nav">
	            <span class="dms-specify">Specify Site ID:</span> <asp:TextBox ID="txtBasePath" runat="server"
                                        ClientIDMode="Static" Width="100px" style="text-transform: uppercase;" />
                <a id="aGoToDir" class="fancy-button" href="javascript:void(0)" onclick="GoToDir();">Go</a>
                
                <input type="button" id="modalBtnExternal-create-folder" class="ui-button ui-widget ui-state-default ui-corner-all" value="Create Folder" style="margin-left:16px;"  />
                <input type="button" id="modalBtnExternalDms" class="ui-button ui-widget ui-state-default ui-corner-all" value="Upload Document" />
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->
    
    <div class="cb"></div>
    
    <asp:Panel ID="pnlNoAccess" runat="server">
        <p style="text-align: center;">
            You do not have sufficient privileges to access the TrackiT Docs section.
            <br />
            Please contact the administrator to obtain privileges.
        </p>
    </asp:Panel>

    <asp:Panel ID="pnlDMS" runat="server">
       
        <asp:HiddenField ID="hidDMSBasePath" runat="server" Value="" ClientIDMode="Static"/>
        <asp:HiddenField ID="hidPreviousMarker" runat="server" Value="" />
        <asp:HiddenField ID="hidCurrentMarker" runat="server" Value="" />
        <asp:HiddenField ID="hiddenCurrentLocationCount" runat="server" Value="0" />

        <div class="dms-folder-bar-cont">
            Current Path:&nbsp;<span id="sBasePath"><a href="javascript:void(0);" onclick="ChangeDirectory('/');">Home</a></span>
        </div>
        
        <!-- DMS Grid -->
        <div style="overflow:visible; margin-bottom: 20px; position:relative;">
            
            <fx:Flexigrid ID="fgDMS"
                Width="990"
                ResultsPerPage="20"
		        ShowPager="true"
                Resizable="false"
                ShowToggleButton="false"
                ShowTableToggleButton="false"
                SearchEnabled="false"
                UseCustomTheme="true"
                CssClass="tmobile"
                WrapCellText="true"
                DoNotIncludeJQuery="true"
                OnClientBeforeSendData="fgDMSBeforeSendData"
                OnClientDataLoad="fgDMSDataLoad"
                OnClientRowClick="fgDMSRowClick"
                OnClientNoDataLoad="fgDMSNoData"
                OnClientColumnToggle="fgColumnChange"
		        HandlerUrl="~/DMSFlexiGrid.axd"
                runat="server">
		        <Columns>
			        <fx:FlexiColumn Code="Option" Text="Option" Width="25"/>
			        <fx:FlexiColumn Code="FileName" Text="Name" Width="197"/>
                    <fx:FlexiColumn Code="Folder" Text="Folder" Width="110" IsVisible="false"  />
                    <fx:FlexiColumn Code="Metadata.Classification" Text="Classification" Width="160"/>
                    <fx:FlexiColumn Code="Metadata.Keywords" Text="Keywords" Width="120" IsVisible="False" />
                    <fx:FlexiColumn Code="Metadata.Description" Text="Description" Width="165"/>                        
                    <fx:FlexiColumn Code="ItemObject.Size" Text="Size" Width="70" />
                    <fx:FlexiColumn Code="Metadata.UploadedBy" Text="Uploaded By" Width="110"  />
                    <fx:FlexiColumn Code="Revision" Text="Rev" Width="30"  />
                    <fx:FlexiColumn Code="Metadata.UploadDate" Text="Upload Date" Width="90" />
                    <fx:FlexiColumn Code="Metadata.isCheckedOut" Text="Lock" Width="32" />
                    <fx:FlexiColumn Code="actions" Text="Actions" Width="90" AllowSort="False" />
		        </Columns>
	        </fx:Flexigrid>

        </div>
        <!-- /DMS Grid -->

    </asp:Panel>
    
    <!-- Modal & Hidden -->
    <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternal-create-folder" class="modal-container" style="overflow:hidden;"></div>
    <div id="modal-message"></div>
    <asp:HiddenField id="hidDocumentDownload" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField id="hidLocationHash" ClientIDMode="Static" runat="server"/>
    <asp:HiddenField id="hidIsllDocumentFolder" ClientIDMode="Static" runat="server"/>
    <!-- /Modal & Hidden -->

</asp:Content>
