﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Remove File | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="RemoveDocument.aspx.cs" Inherits="TrackIT2.DMS.RemoveDocument" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Styles -->
    <%: Styles.Render("~/Styles/dms_remove_document") %>

    <!-- Scripts -->
    <%: Scripts.Render("~/Scripts/dms_remove_document") %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="margin: 0 auto 0 auto;">
        <div class="form_element">
            <div class="widget-content-window no-options" style="height:auto">
                <div id="divNoDocData" runat="server" clientidmode="Static" class="no-chart" style="height:200px; text-align:center;">
                <br />
                <br />
                <br />
                    <div class="infoNoData">No records exist for the field specified.</div>
                <br />
                <br />
                <br />
                </div>
                <div id="documentDiv" runat="server"  style="width:100%; margin:0 auto; padding-top:20px; height:320px; overflow:auto;">   
                     <asp:UpdatePanel id="documentUpdatePanel" updatemode="Always" runat="server">
                        <ContentTemplate>
                            <h4 id="divDocHeader" runat="server" style="width:100%;">Existing Document Links</h4>
                            <br />
                            <asp:Repeater ID="documentRepeater" runat="server">
                                <ItemTemplate>
                                    <div id="divDocumentInfo" runat="server" class="RemoveDocumentDataDiv">
                                        <asp:CheckBox ID="docCheckBox" runat="server"  />
                                        <asp:Label id="lblDocFileName" runat="server"  />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div id="RemoveNewSaDocDiv" runat="server" class="RemoveNewSaDoc" style="width:100%;">
                        
                    </div>
                    <br />
                <div id="removeDocDiv" runat="server">

                    <asp:Button ID="btnStepBack"
                        Text="&laquo; Go Back"
                        ClientIDMode="Static"
                        CssClass="BtnRemoveStepBackDocumentFooter"
                        OnClientClick="javascript:return false;"
                        runat="server" />

                    <asp:Button ID="removeDocBtn" ClientIDMode="Static" runat="server" Text="Remove"
                    OnClientClick="javascript:return OnDeleteDocumentClick(this.name);"
                    OnClick="RemoveDocBtnClick"
                    CssClass="BtnRemoveDocument" />

                </div>
                <asp:HiddenField ID="hidden_CurrentDoc" runat="server" />
            </div> 
            </div>
        </div>
    </div>
</asp:Content>