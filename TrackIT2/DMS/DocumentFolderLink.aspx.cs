﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.DMS
{
    public partial class DocumentFolderLink : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Stop Caching in IE
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            //Stop Caching in Firefox
            Response.Cache.SetNoStore();

            if (Request.QueryString[0] != "")
            {
                lblUploadingFolderLocation.Text =  Request.QueryString[0];
                String sitePath = Request.QueryString[0].Replace("/","") +"/";
                var objectSet = AmazonDMS.GetObjects(sitePath, null, isGetAll: true);
                List<AWSObjectItem> queryItems = new List<AWSObjectItem>(); ;
                queryItems = objectSet.S3Objects.ToList();

                if (queryItems.Count != 0)
                {
                    hiddSiteFolderExist.Value = "true";
                }
                else
                {
                    hiddSiteFolderExist.Value = "false";
                }
            }
        }
    }
}