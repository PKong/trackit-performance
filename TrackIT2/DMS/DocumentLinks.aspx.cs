﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;
using System.Globalization;

namespace TrackIT2.DMS
{
    public partial class DocumentLinks : System.Web.UI.Page
    {
        // Var
        private string _basePath = "";
        //>

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Stop Caching in IE
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            //Stop Caching in Firefox
            Response.Cache.SetNoStore();
            DmsLinkDocumentStructure document  = null;
            if (!Page.IsPostBack)
            {
                // Hidden Base Path
                if (!string.IsNullOrEmpty(Request.QueryString["basePath"]))
                {
                    _basePath = Request.QueryString["basePath"];

                    if (_basePath.Length > 1)
                    {
                        if (_basePath.StartsWith("/")) _basePath = _basePath.Substring(1, _basePath.Length -1);
                        if (_basePath.EndsWith("/")) _basePath = _basePath.Substring(0, _basePath.Length - 1);   
                    }else
                    {
                        _basePath = "";
                    }
                    hidBasePath.Value = _basePath;
                }

                //display info
                if (!string.IsNullOrEmpty(Request.QueryString["field"]) && !string.IsNullOrEmpty(Request.QueryString["typeID"]))
                {
                    document = DMSDocumentLinkHelper.DocumentField(Request.QueryString["field"]);
                    docType.Text = document.LabelType;
                    docFiled.Text = document.LabelField;
                    hidType.Value = document.Type.ToString();

                    if (document.Type == DMSDocumentLinkHelper.eDocumentType.lease_applications || document.Type == DMSDocumentLinkHelper.eDocumentType.tower_modification_general)
                    {
                        if (document.Field == DMSDocumentLinkHelper.eDocumentField.LE_Comments)
                        {
                            hidTypeID.Value = Request.QueryString["typeID"].ToString();
                        }
                        else
                        {
                            hidTypeID.Value = Request.QueryString["ID"].ToString();
                        }
                    }
                    else
                    {
                        hidTypeID.Value = Request.QueryString["typeID"].ToString();
                    }

                    // get auto-rename data
                    if (document.AutoRename)
                    {
                        hidAutoRename.Value = "true";
                        if (!string.IsNullOrEmpty(hidTypeID.Value))
                        {
                            int tmpId = int.Parse(Request.QueryString["ID"]);
                            
                            // GetTenant
                            customer customerObj = DMSDocumentLinkHelper.GetTenant(tmpId, document.Type);
                            if (customerObj != null)
                            {
                                hidNewName.Value = customerObj.customer_name;
                            }
                            else
                            {
                                hidNewName.Value = string.Empty;
                            }
                            //-->

                            // Get Description
                            hidNewNameDescription.Value = document.RenameDescription;
                            //-->

                            switch (document.RenameType)
                            {
                                // Get Application
                                case DMSDocumentLinkHelper.eDocumentRenameType.ApplicationType:
                                case DMSDocumentLinkHelper.eDocumentRenameType.SlaOrAmend:

                                    var currentApp = DMSDocumentLinkHelper.GetApplicationType(tmpId, document.Type);

                                    if (document.RenameType == DMSDocumentLinkHelper.eDocumentRenameType.SlaOrAmend)
                                    {
                                        if (currentApp != DMSDocumentLinkHelper.UNKNOW_TYPE)
                                        {
                                            if (currentApp == "Initial")
                                            {
                                                currentApp = "SLA";
                                            }
                                            else
                                            {
                                                currentApp = "Amend";
                                            }
                                        }
                                    }

                                    hidNewNameAppType.Value = currentApp;
                                    break;
                                //-->

                                default:
                                    // do nothing
                                    break;
                            }
                        }
                    }

                    hidField.Value = Request.QueryString["field"].ToString();
                    hidFolder.Value = document.Location.HasValue ? document.Location.Value.ToString() : "";
                   
                    header.Visible = true;
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["field"]) && !string.IsNullOrEmpty(Request.QueryString["typeID"]))
            {
                document = DMSDocumentLinkHelper.DocumentField(Request.QueryString["field"]);

                ClientScript.RegisterStartupScript(typeof(Page), "setupDocLinkModal", "setupDocLinkModal('" + checkExistDocument() + "');", true);
            }
        }
        //-->

        private bool checkExistDocument()
        {
            int iID;
            bool isValidId = Int32.TryParse(hidTypeID.Value, out iID);
            if (isValidId)
            {
                if (DMSDocumentLinkHelper.GetDocumentLink(Convert.ToInt32(hidTypeID.Value), DMSDocumentLinkHelper.DocumentField(hidField.Value)).Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (DMSDocumentLinkHelper.GetDocumentLink(hidTypeID.Value).Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    //>
}
