﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using TrackIT2.BLL;
using System.Text;

namespace TrackIT2.DMS
{
    public partial class RemoveDocument : System.Web.UI.Page
    {

        string docType = "";
        int id = 0;
        string field = "";
        string key = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                hidden_CurrentDoc.Value = "";
                btnStepBack.Visible = true;

                if (Request.QueryString["type"] != null)
                {
                    docType = Request.QueryString["type"];
                    string siteID = "";
                    bool isValidId = Int32.TryParse(Request.QueryString["typeID"], out id);
                    if (!isValidId)
                    {
                        siteID = Request.QueryString["typeID"];
                    }
                    field = Request.QueryString["field"];
                    key = Server.UrlDecode(Request.QueryString["key"]);

                    if (id != 0)
                    {
                        BindDocuments(id, DMSDocumentLinkHelper.DocumentField(field));
                    }
                    else
                    {
                        BindDocuments(siteID, DMSDocumentLinkHelper.DocumentField(field));
                    }

                    PageUtility.RegisterClientsideStartupScript(Page, "EnableAndSetLink", "EnableAndSetLink()");
                }
            }
        }

        protected void BindDocuments(String id, DmsLinkDocumentStructure document)
        {
            List<Dms_Document_Site> docData = new List<Dms_Document_Site>();
            if (id != "")
            {
                docData = DMSDocumentLinkHelper.GetDocumentLink(id);

                if (docData.Count > 0)
                {
                    btnStepBack.CssClass = "BtnRemoveStepBackDocumentHaveDataFooter";
                    divNoDocData.Visible = false;
                    divDocHeader.Attributes.CssStyle["display"] = "block";
                    RemoveNewSaDocDiv.Attributes.CssStyle["display"] = "none";
                    documentRepeater.DataSource = docData;
                    documentRepeater.ItemDataBound += new RepeaterItemEventHandler(documentRepeater_ItemDataBound);
                    documentRepeater.DataBind();
                }
            }
        }

        protected void BindDocuments( int id, DmsLinkDocumentStructure document)
        {
            List<Dms_Document> docData = new List<Dms_Document>();
            if (id != 0)
            {
                docData = DMSDocumentLinkHelper.GetDocumentLink(id, document);

                if (docData.Count > 0)
                {
                    btnStepBack.CssClass = "BtnRemoveStepBackDocumentHaveDataFooter";
                    divNoDocData.Visible = false;
                    divDocHeader.Attributes.CssStyle["display"] = "block";
                    RemoveNewSaDocDiv.Attributes.CssStyle["display"] = "none";
                    documentRepeater.DataSource = docData;
                    documentRepeater.ItemDataBound += new RepeaterItemEventHandler(documentRepeater_ItemDataBound);
                    documentRepeater.DataBind();
                }
            }
        }

        protected void RemoveDocBtnClick(object sender, EventArgs e)
        {
            string result = "";
            string siteID = "";
            docType = Request.QueryString["type"];
            int id = 0;
            bool isValidId = Int32.TryParse(Request.QueryString["typeID"], out id);
            if (!isValidId)
            {
                siteID = Request.QueryString["typeID"];
            }

            DmsLinkDocumentStructure document = DMSDocumentLinkHelper.DocumentField(Request.QueryString["field"]);
            if (id != 0)
            {
                foreach (RepeaterItem i in documentRepeater.Items)
                {
                    CheckBox chk = (CheckBox)i.FindControl("docCheckBox");
                    if (chk.Checked)
                    {
                        result = DMSDocumentLinkHelper.RemoveDocumentLink(Convert.ToInt32(Request.QueryString["typeID"]), document, chk.InputAttributes["ref_key"]);

                        if (result != "")
                        {
                            break;
                        }
                    }
                }

                List<Dms_Document> documents = DMSDocumentLinkHelper.GetDocumentLink(Convert.ToInt32(Request.QueryString["typeID"]), document);
                //insert template data in case no documents link
                if (documents.Count == 0)
                {
                    Dms_Document doc = new Dms_Document();
                    doc.ID = Convert.ToInt32(Request.QueryString["typeID"]);
                    doc.ref_field = Request.QueryString["field"];
                    documents.Add(doc);
                }
                hidden_CurrentDoc.Value = Newtonsoft.Json.JsonConvert.SerializeObject(documents);
            }
            else if (siteID != "")
            {
                foreach (RepeaterItem i in documentRepeater.Items)
                {
                    CheckBox chk = (CheckBox)i.FindControl("docCheckBox");
                    if (chk.Checked)
                    {
                        result = DMSDocumentLinkHelper.RemoveDocumentLink(siteID, Request.QueryString["field"], chk.InputAttributes["ref_key"]);

                        if (result != "")
                        {
                            break;
                        }
                    }
                }

                List<Dms_Document_Site> documents = DMSDocumentLinkHelper.GetDocumentLink(siteID);
                //insert template data in case no documents link
                if (documents.Count == 0)
                {
                    Dms_Document_Site doc = new Dms_Document_Site();
                    doc.ID = siteID;
                    doc.ref_field = Request.QueryString["field"];
                    documents.Add(doc);
                }
                hidden_CurrentDoc.Value = Newtonsoft.Json.JsonConvert.SerializeObject(documents);
            }
            if (result == "")
            {
                String script = "closeRemoveExistingDialog(\"{0}\",\"{1}\",\"{2}\");";
                StringBuilder sb = new StringBuilder();
                sb.Append(String.Format(script, docType, Request.QueryString["field"], document.RelatedField.HasValue));
                if (document.RelatedField.HasValue)
                {
                    DmsLinkDocumentStructure related_doc = DMSDocumentLinkHelper.DocumentField(document.RelatedField.ToString());
                    sb.Append(String.Format(script, related_doc.Type.ToString(),  related_doc.Field.ToString(),  false));
                }
                PageUtility.RegisterClientsideStartupScript(Page, "CloseDocumentDialogExisting", sb.ToString());
            }
            else
            {
                PageUtility.RegisterClientsideStartupScript(Page, "CloseRemoveDocumentDialogAfterError", "closeDialogAfterError(\"" + result + "\",\"" + docType + "\",\"" + id + "\",\"" + field + "\")");
            }
        }

        protected void documentRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (e.Item.DataItem.GetType().Name == "Dms_Document")
                {
                    Dms_Document docData = (Dms_Document)e.Item.DataItem;
                    int x = 0;
                    if (docData != null)
                    {
                        string key = Server.UrlDecode(docData.ref_key);
                        key = Server.UrlDecode(key);
                        string tmpName = key.Split('/')[key.Split('/').Length - 1];
                        string name = "";

                        if (tmpName.Split('.').Length > 1)
                        {
                            for (int y = 0; y < tmpName.Split('.').Length; y++)
                            {
                                if (y == tmpName.Split('.').Length - 1)
                                {
                                    name += ".";
                                }
                                name += tmpName.Split('.')[y];
                            }
                        }
                        else
                        {
                            name = tmpName;
                        }

                        CheckBox docCheckBox = (CheckBox)e.Item.FindControl("docCheckBox");
                        Label lblDocFileName = (Label)e.Item.FindControl("lblDocFileName");
                        docCheckBox.Text = name;
                        docCheckBox.InputAttributes["ref_key"] = docData.ref_key;
                        docCheckBox.InputAttributes["ref_field"] = docData.ref_field;
                        docCheckBox.InputAttributes["typeID"] = docData.ID.ToString();
                        docCheckBox.InputAttributes["type"] = docData.type;
                        docCheckBox.InputAttributes["removeChk"] = "removeChk_" + x;
                        x++;
                    }
                }
                else if (e.Item.DataItem.GetType().Name == "Dms_Document_Site")
                {
                    Dms_Document_Site docData = (Dms_Document_Site)e.Item.DataItem;
                    int x = 0;
                    if (docData != null)
                    {
                        string key = Server.UrlDecode(docData.ref_key);
                        key = Server.UrlDecode(key);
                        string tmpName = key.Split('/')[key.Split('/').Length - 1];
                        string name = "";

                        if (tmpName.Split('.').Length > 1)
                        {
                            for (int y = 0; y < tmpName.Split('.').Length; y++)
                            {
                                if (y == tmpName.Split('.').Length - 1)
                                {
                                    name += ".";
                                }
                                name += tmpName.Split('.')[y];
                            }
                        }
                        else
                        {
                            name = tmpName;
                        }

                        CheckBox docCheckBox = (CheckBox)e.Item.FindControl("docCheckBox");
                        Label lblDocFileName = (Label)e.Item.FindControl("lblDocFileName");
                        docCheckBox.Text = name;
                        docCheckBox.InputAttributes["ref_key"] = docData.ref_key;
                        docCheckBox.InputAttributes["ref_field"] = docData.ref_field;
                        docCheckBox.InputAttributes["typeID"] = docData.ID.ToString();
                        docCheckBox.InputAttributes["type"] = docData.type;
                        docCheckBox.InputAttributes["removeChk"] = "removeChk_" + x;
                        x++;
                    }
                }
            }
        }

    }
}