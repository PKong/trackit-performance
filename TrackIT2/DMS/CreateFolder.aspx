﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Upload File | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="CreateFolder.aspx.cs" Inherits="TrackIT2.DMS.CreateFolder" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/dms_create_folder") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/dms_create_folder") %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="margin: 0 auto 0 auto;">
    
        <div class="form_element">
            
            <h4>Folder Location</h4>
            <asp:Label ID="lblCurrentFolder" Text="Current folder:" style="width: auto;" runat="server" />&nbsp;
            <asp:Label ID="lblCurrentFolderLocation" runat="server" Text="/" style="width: auto;" />
            <br />

            <div id="detailHeader" runat="server">
                <h4>Folder Details</h4>
            </div>
       
                <div id="divFolderName" runat="server">
                    <label>Folder Name</label>
                    <asp:TextBox ID="txtFolderName" ClientIDMode="Static" runat="server" style="width: 240px;" />
                </div>

                <div id="localtion_tree" class="location_tree" runat="server" visible="false">
			        <h4>Move Location</h4>
                    <br />
                        <asp:Label ID="lblMoveTo" Text="Move to:" style="width: auto;" runat="server" />&nbsp;
                        <asp:Label ID="lblMovingFolderLocation" ClientIDMode="Static" runat="server" Text="/" style="width: auto;" />
                    <br />
                    <asp:Panel ID="TreePanel" runat="server" CssClass="TreePanelAttribute" ScrollBars="Auto">
    			        <div id="jqueryFileTree" class="jqueryFileTree" >
                        </div>
                    </asp:Panel>
		        </div>

                <br />

                <div style="float: right;display: block;">
                    
                    <div id="spinner-cont" style="display: block;float: left;width: 30px;height: 50px;"></div>

                    <asp:Button ID="btnCreateFolder"
                        Text="Create"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return validateCreateFolder();"
                        OnClick="BtnCreateFolderClick"
                        ClientIDMode="Static"
                        runat="server" />
                        
                    <asp:Button ID="btnSaveFolder"
                        Text="Save"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return validateCreateFolder();"
                        OnClick="BtnSaveFolderClick"
                        ClientIDMode="Static"
                        Visible="False"
                        runat="server" />

                    <asp:Button ID="btnMoveFolder"
                        Text="Move"
                        CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all right"
                        OnClientClick="return validateMoveFolder();"
                        OnClick="BtnMoveFolderClick"
                        ClientIDMode="Static"
                        Visible="False"
                        runat="server" />
                </div>
                    
                <asp:HiddenField ID="hidBasePath" runat="server" Value="" ClientIDMode="Static"/>
                <asp:HiddenField ID="hidTmpPath" runat="server" Value="" ClientIDMode="Static"/>
        </div>

    </div>
</asp:Content>
