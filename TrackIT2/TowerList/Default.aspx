﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Tower List | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.TowerList.Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <!-- Styles -->
   <%: Styles.Render("~/Styles/tower_list") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/tower_list")%>

   <script type="text/javascript" language="javascript">
       // Global Var
       searchFilterFields = "txtSiteID,txtSiteName";
       currentFlexiGridID = '<%= fgTowerList.ClientID %>';

       // Doc Ready
       $(function () {
           setupTowerList();
           SetupFilter();

           // Setup List Create Modal
           setupTowerListCreateModal("Create New Tower List");

           if (window.location.href.indexOf("forceSearch=true") > 0) {
               GetFilterParam();
               reloadDataGrid();
           }
       });
       //-->

       
    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">

        <div id="create-option" style=" width: 85px; margin-top:44px; float:left;">
            <a id="modalBtnExternal" class="fancy-button" style="padding: 0.53em 2.5em;" href="javascript:void(0)">+</a>
        </div>

        <div id="towerList-option" style="margin-left:80px;">
            
            <div id="trackit-option" style=" width: 250px; float:left;">
                <span>TrackiT</span>
                <hr class="dotHr" />
                 <div id="tabs1">
                    <input type="radio" name="trackiT" id="trackitOn" class="filter-checkbox" /><label for="trackitOn">Show On</label>
                    <input type="radio" name="trackiT" id="trackitOff" class="filter-checkbox"/><label for="trackitOff">Show Off</label>
                </div>
            </div>

            <div id="map-option" style="width: 250px; float:left;">
                <span>Website Map</span>
                <hr class="dotHr" />
                 <div id="tabs2">
                    <input type="radio" name="map" id="mapOn" class="filter-checkbox"/><label for="mapOn">Show On</label>
                    <input type="radio" name="map" id="mapOff" class="filter-checkbox"/><label for="mapOff">Show Off</label>
                </div>
            </div>

            <div class="search-container" style="float:right; margin-top:25px;">
                <span>Search</span>
                <br />
                <input id="txtSearch" type="text" value="" style="padding:0 0 0 6px;width:196px;height:22px;margin:0;font-size:12px;" />
            </div>
	    </div>
    </div>

	<div class="cb"></div>
    <!-- /Page Content Header -->

    <!-- Tower List Grid -->
    <div style="overflow:false; margin-bottom: 20px; min-height:580px;">

        <fx:Flexigrid ID="fgTowerList"
            AutoLoadData="False"
            Width="990"
            ResultsPerPage="20"
			   ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile fgTower"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			   HandlerUrl="~/TowerList.axd"
            runat="server">
		    <Columns>
			    <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="80" />
                <fx:FlexiColumn Code="site_name" Text="Site Name" Width="232" />
                <fx:FlexiColumn Code="site_class_desc" Text="Site Class Desc" Width="120" />
                <fx:FlexiColumn Code="market_name" Text="Market Name" Width="180" />
                <fx:FlexiColumn Code="site_status_desc" Text="Site Status Desc" Width="120" />
                <fx:FlexiColumn Code="trackit_override_list_status.list_status" Text="TrackiT" Width="120" />
                <fx:FlexiColumn Code="map_override_list_status.list_status" Text="Website Map" Width="120" />
		    </Columns>
	    </fx:Flexigrid>

        <br />
        <div>
            <div id="modal-init-page">
            <table class="modal-init-page-table">
	            <tbody><tr>
		            <td style="" >
	                    Welcome to the Tower List Management tool.
                        <br />
                        Please enter a Site or click a filter above.
	                </td>
	            </tr>
            </tbody></table>
            </div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgTowerList')" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenLastFieldToCheckLoaded" runat="server" ClientIDMode="Static" Value="" />
        <div class="cb"></div>

    </div>
    <!-- /Site Data Packages Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->

</asp:Content>
