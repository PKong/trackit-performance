﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Tower List | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TrackIT2.TowerList.Add" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>
<%@ Register TagPrefix="TrackIT2" TagName="TmoAppIDFieldAutoComplete" src="~/Controls/TmoAppIDFieldAutoComplete.ascx" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

   <!-- Styles -->
   <%: Styles.Render("~/Styles/tower_list_add")%>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/tower_list_add")%>  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="width:710px;margin: 0 auto 0 auto;">
        <div class="left-col form_element">
            <div id="divGeneralInfomation" runat="server">
                <h4>General Infomation</h4>  
                
                <label ID="lblSiteID" runat="server">Site ID</label>
                <asp:TextBox ID="txtSiteID" runat="server"></asp:TextBox><br />

                <label ID="Label1" runat="server">Site Name</label>
                <asp:TextBox ID="txtSiteName" runat="server"></asp:TextBox><br />
                
                <label ID="Label2" runat="server">Region</label>
                <asp:DropDownList ID="ddlRegionName" runat="server" CssClass="select-field"></asp:DropDownList><br />

                <label ID="Label3" runat="server">Market Name</label>
                <asp:DropDownList ID="ddlMarketName" runat="server" CssClass="select-field"></asp:DropDownList><br />

            </div>
           
        </div>
        <div class="right-col form_element">
            <div id="divStructureDetails" runat="server">
                <h4>Structure Details</h4>  
                <label ID="Label4" runat="server">Site Class Desc</label>
                <asp:DropDownList ID="ddlSiteClassDesc" runat="server" CssClass="select-field"></asp:DropDownList><br />

                <label ID="Label5" runat="server">Height</label>
                <asp:TextBox ID="txtHeight" runat="server"></asp:TextBox><br />

                <label ID="Label9" runat="server">Site Status Desc</label>
                <asp:DropDownList ID="ddlSiteStatusDesc" runat="server" CssClass="select-field"></asp:DropDownList><br />

            </div>
            
        </div>
        <div id="divTowerAddress" runat="server" style="width:100%; margin-top: 150px;" >
            <h4 style="margin-top: 19px;">Tower Address</h4>  
            <div class="left-col form_element">
                
                <label ID="Label8" runat="server">Address</label>
                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox><br />

                <label ID="Label12" runat="server">City</label>
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox><br />

                <label ID="Label6" runat="server">County</label>
                <asp:TextBox ID="txtCounty" runat="server"></asp:TextBox><br />
                
                <label ID="Label7" runat="server">State</label>
                <asp:DropDownList ID="ddlState" runat="server" CssClass="select-field"></asp:DropDownList><br />

                <label ID="Label11" runat="server">Zip</label>
                <asp:TextBox ID="txtZip" runat="server"></asp:TextBox><br />

            </div>
            
            <div class="right-col form_element">

                <label ID="Label13" runat="server">Latitude</label>
                <asp:TextBox ID="txtLatitude" runat="server"></asp:TextBox><br />
                
                <label ID="Label14" runat="server">Longitude</label>
                <asp:TextBox ID="txtLongitude" runat="server"></asp:TextBox><br />
                
            </div>

        </div>
        <asp:Button ID="btnSubmitCreateTowerList" runat="server" CssClass="TowerListCreateButton buttonLASubmit ui-button ui-widget ui-state-default ui-corner-all" UseSubmitBehavior="false" Text="Create" OnClick="btnAdd_Click" OnClientClick="if (!validateCreateTowerList()) { return false;}" />                

    </div>
    <asp:HiddenField ID="HiddenDialogHeightFlag" ClientIDMode="Static" runat="server" />
</asp:Content>
