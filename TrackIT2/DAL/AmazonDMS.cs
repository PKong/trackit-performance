﻿using System.Globalization;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Amazon.S3.Transfer;
using TrackIT2.BLL;

namespace TrackIT2.DAL
{
    /// <summary>
    /// AWSObjectItemType
    /// </summary>
    public enum AWSObjectItemType
    {
        File,
        Folder
    }
    //>

    /// <summary>
    /// AWSResultSet (Class)
    /// </summary>
    public class AWSResultSet
    {
        public bool IsPartialResult { get; set; }
        public string NextMarker { get; set; }
        public string PreviousMarker { get; set; }
        public int CurrentPage { get; set; }
        public List<AWSObjectItem> S3Objects;
    }
    // /AWSResultSet (Class) >
   
    /// <summary>
    /// AWSObjectItem (Class)
    /// </summary>
    public class AWSObjectItem
    {
        // Var
        private const String HTML_IMAGE = "<img src=\"{0}\" {1}/>";
        private const String PATH_FOLDER_IMG = "/images/icons/file-types/folder-horizontal.png";
        private const String PATH_FOLDER_OPEN_IMG = "/images/icons/file-types/folder-horizontal-open.png";
        private const string HOME = "Home";
        //>

        // Properties
        public string Key { get; set; }
        public AWSObjectItemType Type { get; set; }
        public string StrippedPath { get; set; }
        public string FileName { get; set; }
        public S3Object ItemObject {get; set;}
        public AWSObjectMetadata Metadata {get; set;}
        public int Revision { get; set; }
        public string Folder { get; set; }
        //
        public string FlexiPath
        {
            get
            {
                string titleVal = FileName.Replace("/", "");
                if (string.IsNullOrEmpty(titleVal)) titleVal = HOME;
                string title = String.Format("title=\"{0}\"", titleVal);
                string folder = String.Format(HTML_IMAGE, PATH_FOLDER_IMG, title);
                string displayPath = FileName;

                if (displayPath.Equals("/"))
                {
                    displayPath = HOME;
                    folder = String.Format(HTML_IMAGE, PATH_FOLDER_OPEN_IMG, title);
                }
                else if (displayPath[displayPath.Length - 1] == '/')
                {
                    displayPath = displayPath.Remove((displayPath.Length - 1), 1);   
                }

                return String.Format("<a href=\"javascript:void(0);\" onclick=\"ChangeDirectory('{0}');showActionsLoadingSpinner(this, true);\" style=\"text-decoration:none;display:block;\">{2}<br />{1}</a>", Key, displayPath, folder, title);
            }
        }
        //>
    }
    // /AWSObjectItem (Class) >
   
    /// <summary>
    /// AWSObjectMetadata (Class)
    /// </summary>
    public class AWSObjectMetadata
    {
        public int Revision { get; set; }
        public string FileType { get; set; }
        public List<string> Classification { get; set; }
        public string Title { get; set; }      
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string UploadedBy { get; set; }
        public DateTime UploadDate { get; set; }
        public bool IsCheckedOut { get; set; }
        public string CheckedOutBy { get; set; }
        public DateTime CheckOutDate { get; set; }
    }
    // /AWSObjectMetadata (Class) >
   
    /// <summary>
    /// AmazonDMS (Class)
    /// </summary>
    public static class AmazonDMS
    {
        // Var
        public static AmazonS3 S3Client;
        static readonly string BucketName = ConfigurationManager.AppSettings["AWSDefaultBucket"];
        private const int CLASSIFICATION_LIMIT = 3;
        public static readonly string ErrorCodeNoSuchKey = "NoSuchKey";

        // Properties
        public static List<string> CurrentBucketObjectNames { get; set; }

        /// <summary>
        /// Get Objects
        /// </summary>
        /// <param name="basePath"></param>
        /// <param name="marker"></param>
        /// <param name="prevMarker"></param>
        /// <param name="page"></param>
        /// <param name="isGetAll"></param>
        /// <param name="getMetadata"></param>
        /// <returns></returns>
        public static AWSResultSet GetObjects(string basePath, string marker, string prevMarker = null, int page = 1, bool isGetAll = false, bool getMetadata = false)
        {
            AWSResultSet results = new AWSResultSet();
            List<AWSObjectItem> s3Objects = new List<AWSObjectItem>();
            AWSObjectItem item = new AWSObjectItem();
            List<S3Object> listS3Objects = new List<S3Object>();

            using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                ListObjectsRequest request = new ListObjectsRequest();

                // For "root" requests, we don't append the trailing
                // slash to the base path.
                if ((basePath != "") && (!basePath.EndsWith("/")))
                {
                    basePath += "/";
                }

                request.BucketName = BucketName;
                request.Delimiter = "/";
                request.Prefix = basePath;
                if (!isGetAll)
                {
                    request.MaxKeys = 20;
                }


                if (!string.IsNullOrEmpty(marker))
                {
                    request.Marker = marker;
                }

                ListObjectsResponse result = new ListObjectsResponse();
                do
                {
                    try
                    {
                        result = S3Client.ListObjects(request);
                        listS3Objects.AddRange(result.S3Objects);

                        results.CurrentPage = page;


                        // If the previous marker specified does not contain a filname,
                        // ignore it. Previous marker will still be used even if the
                        // result set is not truncated (at the end of the list).
                        if (!string.IsNullOrEmpty(prevMarker) && prevMarker != marker)
                        {
                            results.PreviousMarker = prevMarker;
                        }
                        else
                        {
                            results.PreviousMarker = null;
                        }

                        // The CommonPrefixes property contains any sub folders there
                        // may be. However, these objects are simple strings, and we
                        // want to treat them as S3Objects for our final list.
                        foreach (string name in result.CommonPrefixes)
                        {
                            item = new AWSObjectItem();
                            item.Key = name;
                            item.Type = AWSObjectItemType.Folder;

                            string strippedPath = name.Substring(0, name.Length - 1);
                            string fileName = "";

                            // Since files are stored with their full name /a/b/c/file.txt, 
                            // strip off any leading details of the file for readability.
                            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(basePath))
                            {
                                fileName = name.Replace(basePath, null);

                                // Remove leading / at beginning of name for readability (if it exists)
                                if (fileName[0] == '/')
                                {
                                    fileName = fileName.Remove(0, 1);
                                }
                            }
                            else
                            {
                                fileName = name;
                            }

                            item.StrippedPath = strippedPath;
                            item.FileName = fileName;

                            S3Object s3Folder = new S3Object();
                            s3Folder.Key = name;
                            s3Folder.Size = 0;
                            item.ItemObject = s3Folder;

                            item.Metadata = new AWSObjectMetadata();

                            s3Objects.Add(item);
                        }


                        if (result.IsTruncated)
                        {
                            request.Marker = result.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }

                        if (!isGetAll)
                        {
                            break;
                        }
                    }
                    catch
                    {
                        request.Marker = result.NextMarker;
                    }
                } while (request != null);


                //using (ListObjectsResponse response = S3Client.ListObjects(request))
                //{
                    // Finally add any objects, except for the object
                    // matching the basePath.
                    foreach (S3Object s3File in listS3Objects)
                    {
                        if (s3File.Key != basePath)
                        {
                            item = new AWSObjectItem();
                            item.Key = s3File.Key;
                            item.Type = AWSObjectItemType.File;
                            item.ItemObject = s3File;

                            string strippedPath = s3File.Key.Substring(0, s3File.Key.Length - 1);
                            string fileName = "";

                            // Since files are stored with their full name /a/b/c/file.txt, 
                            // strip off any leading details of the file for readability.
                            if (!string.IsNullOrEmpty(s3File.Key) && !string.IsNullOrEmpty(basePath))
                            {
                                fileName = s3File.Key.Replace(basePath, null);

                                // Remove leading / at beginning of name for readability (if it exists)
                                if (fileName[0] == '/')
                                {
                                    fileName = fileName.Remove(0, 1);
                                }
                            }
                            else
                            {
                                fileName = s3File.Key;
                            }

                            item.StrippedPath = strippedPath;
                            item.FileName = fileName;

                            // Get META?
                            if (getMetadata) item.Metadata = GetObjectMetadata(s3File.Key);

                            s3Objects.Add(item);
                        }                  
                    }
                    results.S3Objects = s3Objects;
                //}
            }

            return results;
        }
        //-->
        /// <summary>
        /// Get Object by Key
        /// </summary>
        /// <param name="keyName"></param>
        public static AWSResultSet GetObjectByKey(string keyName)
        {
            using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                AWSResultSet results = new AWSResultSet();
                ListObjectsRequest request = new ListObjectsRequest();
                ListObjectsResponse result = new ListObjectsResponse();
                List<S3Object> listS3Objects = new List<S3Object>();
                List<AWSObjectItem> s3Objects = new List<AWSObjectItem>();
                AWSObjectItem item = new AWSObjectItem();

                // For "root" requests, we don't append the trailing
                // slash to the base path.
                request.BucketName = BucketName;
                request.Delimiter = "/";
                request.Prefix = keyName;
                result = S3Client.ListObjects(request);
                listS3Objects.AddRange(result.S3Objects);
                foreach (S3Object s3File in listS3Objects)
                {
                    item = new AWSObjectItem();
                    item.Key = s3File.Key;
                    item.Type = AWSObjectItemType.File;
                    item.ItemObject = s3File;

                    string strippedPath = s3File.Key.Substring(0, s3File.Key.Length - 1);
                    string[] fileName = s3File.Key.Split('/');

                    item.StrippedPath = strippedPath;
                    item.FileName = fileName[fileName.Length - 1];

                    // Get META?
                    item.Metadata = GetObjectMetadata(s3File.Key);

                    s3Objects.Add(item);
                }
                results.S3Objects = s3Objects;
                return results;
            }
        }
        /// <summary>
        /// Download Object
        /// </summary>
        /// <param name="keyName"></param>
        public static void DownloadObject(string keyName)
        {
            string[] keySplit = keyName.Split('/');
            string fileName = keySplit[keySplit.Length - 1];
            string dest = Path.Combine(HttpRuntime.CodegenDir, fileName);
            using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
            GetObjectRequest request = new GetObjectRequest().WithBucketName(BucketName).WithKey(keyName);

            using (GetObjectResponse response = S3Client.GetObject(request))
            {
                response.WriteResponseStreamToFile(dest, false);                              
            }

            string contenttype = String.Empty;

            //Add fucntion check download revision file will remove revsion from extension Kantorn J. 2012-09-18
            if (fileName.Split('.').Length > 1)
            {
                string fileRev = fileName.Split('.')[fileName.Split('.').Length - 1];
                int intRev;
                string buffer_filename = string.Empty;
                if (int.TryParse(fileRev, out intRev))
                {
                    for (int i = 0; i < (fileName.Split('.').Length - 1); i++)
                    {
                        if (i > 0)
                        {
                            buffer_filename += ".";
                        }
                        buffer_filename += fileName.Split('.')[i];
                    }
                    fileName = buffer_filename;
                }
                else
                {
                    contenttype = GetContentType(fileRev);
                }
            }


            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AppendHeader("pragma","no-cache");
            if (HttpContext.Current.Request.Browser.Type.Contains("IE"))
            {
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=\"" + downloadFilenameEncoding(fileName) + "\";");
            }
            else
            {
                HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=\"" + fileName + "\";");
            }
            //HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=\"" + HttpUtility.UrlEncode(fileName).Replace('+',' ') + "\";");
            HttpContext.Current.Response.ContentType = contenttype;
            HttpContext.Current.Response.TransmitFile(dest);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
            
            // Clean up temporary file.
            File.Delete(dest);
            }
        }
        //-->

        /// <summary>
        /// Download Object as Stream
        /// </summary>
        /// <param name="keyName"></param>
        public static byte[] DownloadObjectStream(string keyName)
        {
            byte[] buffer = null;
            string[] keySplit = keyName.Split('/');
            string fileName = keySplit[keySplit.Length - 1];
            string dest = Path.Combine(HttpRuntime.CodegenDir, fileName);
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    GetObjectRequest request = new GetObjectRequest().WithBucketName(BucketName).WithKey(keyName);

                    using (GetObjectResponse response = S3Client.GetObject(request))
                    {
                        response.WriteResponseStreamToFile(dest, false);
                        buffer = File.ReadAllBytes(dest);
                        File.Delete(dest);
                    }

                }
            }
            catch (Exception )
            { 
            
            }
            return buffer;
        }
        //-->
        private static String downloadFilenameEncoding(String fileName)
        {
            //return  fileName.Replace(";", "%3b").Replace(" ", "%20").Replace("#", "%23").Replace(".", "%2E").Replace("(","%28").Replace(")","%29");
            //String result = HttpUtility.UrlEncode(fileName, HttpContext.Current.Response.ContentEncoding).Replace(".", "%2E").Replace("(", "%28").Replace(")", "%29");

            //Chenck case filename has dot but not for splite extension then use urlencode
            String extension = Path.GetExtension(fileName);
            String result = HttpUtility.UrlEncode(Path.GetFileNameWithoutExtension(fileName)).Replace(".", "%2E").Replace("(", "%28").Replace(")", "%29").Replace("+", "%20").Replace("#", "%23");
            if (extension.Replace(".","").Length > 3)
            {
                result += HttpUtility.UrlEncode(extension).Replace(".", "%2E").Replace("(", "%28").Replace(")", "%29").Replace("+", "%20").Replace("#", "%23");
            }
            else
            {
                result +=  extension;
            }

            return result;
             
        }

        /// <summary>
        /// Get Content Type
        /// </summary>
        /// <param name="keyName"></param>
        private static String GetContentType(String fileType)
        {
            String contentType = String.Empty;
            switch(fileType)
            {
                case "xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case "xlsx" :
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "pdf" :
                    contentType = "application/pdf";
                    break;

                case "doc" :
                    contentType = "application/msword";
                    break;

                case "docx" :
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;

                case "ppt" :
                    contentType = "application/vnd.ms-powerpoint";
                    break;

                case "pptx" :
                    contentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    break;
                case "rft" :
                    contentType = "application/rtf";
                    break;
                case "zip":
                    contentType = "application/zip";
                    break;
                case "mp3":
                    contentType = "audio/mpeg";
                    break;
                case "bmp":
                    contentType = "image/bmp";
                    break;
                case "gif":
                    contentType = "image/gif";
                    break;
                case "jpg":
                    contentType = "image/jpg";
                    break;
                case "jpeg":
                    contentType = "image/jpeg";
                    break;
                case "png":
                    contentType = "image/png";
                    break;
                case "tiff":
                    contentType = "image/tiff";
                    break;
                case "tif":
                    contentType = "image/tiff";
                    break;
                case "txt":
                    contentType = "image/txt";
                    break;
                default:
                    contentType = "application/octet-stream";
                    break;
            
            }
            return contentType;
        }
        //-->

        /// <summary>
        /// Get Object Metadata
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public static AWSObjectMetadata GetObjectMetadata(string keyName)
        {
            AWSObjectMetadata results = new AWSObjectMetadata();                         
            GetObjectMetadataRequest request = new GetObjectMetadataRequest().WithBucketName(BucketName).WithKey(keyName);

            using (GetObjectMetadataResponse metaResponse = S3Client.GetObjectMetadata(request))
            {
            if (metaResponse.Metadata != null)
            {
                // Classification
                results.Classification = new List<string>();
                for (int i = 1; i <= CLASSIFICATION_LIMIT; i++)
                {
                    string c = i.ToString(CultureInfo.InvariantCulture);
                    if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-classification-" + c))
                    {
                        results.Classification.Add(metaResponse.Metadata.Get("x-amz-meta-classification-" + c));
                    }
                }
               
                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-revision"))
                {
                    results.Revision = int.Parse(metaResponse.Metadata.Get("x-amz-meta-revision"));
                }               

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-filetype"))
                {
                    results.FileType = metaResponse.Metadata.Get("x-amz-meta-filetype");
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-title"))
                {
                    results.Title = metaResponse.Metadata.Get("x-amz-meta-title");
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-description"))
                {
                    results.Description = metaResponse.Metadata.Get("x-amz-meta-description");
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-keywords"))
                {
                    results.Keywords = metaResponse.Metadata.Get("x-amz-meta-keywords");
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-uploadedby"))
                {
                    results.UploadedBy = metaResponse.Metadata.Get("x-amz-meta-uploadedby");
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-uploaddate"))
                {
                    results.UploadDate = DateTime.Parse(metaResponse.Metadata.Get("x-amz-meta-uploaddate"));
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-ischeckedout"))
                {
                    results.IsCheckedOut = bool.Parse(metaResponse.Metadata.Get("x-amz-meta-ischeckedout"));

                }
                else
                {
                    results.IsCheckedOut = false;
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-checkedoutby"))
                {
                    results.CheckedOutBy = metaResponse.Metadata.Get("x-amz-meta-checkedoutby");
                }

                if (metaResponse.Metadata.AllKeys.Contains("x-amz-meta-checkoutdate"))
                {
                    results.CheckOutDate = DateTime.Parse(metaResponse.Metadata.Get("x-amz-meta-checkoutdate"));
                }
            }
            else
            {
                results.IsCheckedOut = false;
            }
            }

            return results;
        }
        //-->

        /// <summary>
        /// Check In / Out Object
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="action"></param>
        /// <param name="userId"></param>
        /// <param name="classification"></param>
        /// <param name="fileType"></param>
        /// <param name="description"></param>
        /// <param name="keywords"></param>
        /// <param name="uploadedBy"></param>
        /// <param name="uploadDate"></param>
        public static void CheckInOutObject(string keyName, string action, string userId, List<string> classification,
                                        string fileType, string description, string keywords, string uploadedBy, string uploadDate)
        {
            // Set the check out status and the check out user based on the 
            // action passed in.
            string isCheckedOut = (action == "CheckOut") ? "true" : "false";
            userId = (action == "CheckOut") ? userId : null;
        
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    // Simple metadata updates are accomplished using a copy request.
                    CopyObjectRequest copyRequest = new CopyObjectRequest();

                    copyRequest.WithDirective(S3MetadataDirective.REPLACE)
                                .WithSourceBucket(BucketName)
                                .WithSourceKey(keyName)
                                .WithDestinationBucket(BucketName)
                                .WithDestinationKey(keyName)
                                .WithMetaData("ischeckedout", isCheckedOut)
                                .WithMetaData("checkedoutby", userId)
                                .WithMetaData("checkoutdate", DateTime.Now.ToString(CultureInfo.InvariantCulture))
                                .WithMetaData("filetype", fileType)
                                .WithMetaData("description", description)
                                .WithMetaData("keywords", keywords)
                                .WithMetaData("uploadedby", uploadedBy)
                                .WithMetaData("uploaddate", uploadDate);

                    // Classification
                    if (classification.Any())
                    {
                        int iLimit = classification.Count < CLASSIFICATION_LIMIT
                                         ? classification.Count
                                         : CLASSIFICATION_LIMIT;
                        for (int i = 0; i < iLimit; i++)
                        {
                            copyRequest.WithMetaData("classification-" + (i + 1).ToString(CultureInfo.InvariantCulture), classification[i]);
                        }
                    }

                    CopyObjectResponse copyResponse = S3Client.CopyObject(copyRequest);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }

                throw;
            }
        }
        //-->

        /// <summary>
        /// Delete Objects Recursively
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="objectIsFolder"></param>
        /// <remarks>Recursive delete required as Amazon S3 does not remove child obects automatically.</remarks>
        public static void DeleteObjectsRecursively(string keyName, bool objectIsFolder = false)
        {
            // Folder?
            if (objectIsFolder)
            {
                // Prepare Key
                var keyGetObjects = keyName.LastIndexOf('/') == keyName.Length - 1
                                        ? keyName.Substring(0, keyName.Length - 1)
                                        : keyName;

                // Check if have child objects in current folder
                var currentFolderObjects = GetObjects(keyGetObjects, null, isGetAll: true);
                if (currentFolderObjects.S3Objects.Count > 0)
                {
                    // Iterate Objects
                    foreach (var s3Item in currentFolderObjects.S3Objects)
                    {
                        if (s3Item.Type == AWSObjectItemType.File)
                        {
                            // File Type
                            //-----------
                            // Delete
                            DeleteObject(s3Item.Key);
                        }else
                        {
                            // Folder Type
                            //-----------
                            DeleteObjectsRecursively(s3Item.Key, true);
                        }
                    }
                }
            }
            //>

            // Delete Object
            DeleteObject(keyName);
        }
        //-->

        /// <summary>
        /// Delete Object
        /// </summary>
        /// <param name="keyName"></param>
        public static void DeleteObject(string keyName)
        {
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    DeleteObjectRequest deleteRequest = new DeleteObjectRequest();

                    deleteRequest.WithBucketName(BucketName)
                                .WithKey(keyName);

                    DeleteObjectResponse deleteResponse = S3Client.DeleteObject(deleteRequest);

                    //delete data from link document 
                    string result =  TrackIT2.BLL.DMSDocumentLinkHelper.RemoveDocument(HttpUtility.UrlEncode(keyName));
                    if (result != String.Empty)
                    {
                        throw new ArgumentException("Remove link document error");
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }
                throw;
            }
        }
        //-->

        /// <summary>
        /// Update Object
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="classification"></param>
        /// <param name="fileType"></param>
        /// <param name="description"></param>
        /// <param name="keywords"></param>
        /// <param name="uploadedBy"></param>
        /// <param name="uploadDate"></param>
        public static void UpdateObject(string keyName, List<string> classification,
                                        string fileType, string description, string keywords, string uploadedBy, string uploadDate)
        {
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    // Simple metadata updates are accomplished using a copy request.
                    CopyObjectRequest copyRequest = new CopyObjectRequest();

                    copyRequest.WithDirective(S3MetadataDirective.REPLACE)
                                .WithSourceBucket(BucketName)
                                .WithSourceKey(keyName)
                                .WithDestinationBucket(BucketName)
                                .WithDestinationKey(keyName)
                                .WithMetaData("filetype", fileType)
                                .WithMetaData("description", description)
                                .WithMetaData("keywords", keywords)
                                .WithMetaData("uploadedby", uploadedBy)
                                .WithMetaData("uploaddate", uploadDate);

                    // Classification
                    if (classification.Any())
                    {
                        int iLimit = classification.Count < CLASSIFICATION_LIMIT
                                         ? classification.Count
                                         : CLASSIFICATION_LIMIT;
                        for (int i = 0; i < iLimit; i++)
                        {
                            copyRequest.WithMetaData("classification-" + (i + 1).ToString(CultureInfo.InvariantCulture), classification[i]);
                        }
                    }

                    CopyObjectResponse copyResponse = S3Client.CopyObject(copyRequest);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }

                throw;
            }
        }
        //-->

        /// <summary>
        /// Rename Object
        /// </summary>
        /// <param name="keyCurrent"></param>
        /// <param name="keyNew"></param>
        /// <param name="uploadedBy"></param>
        /// <param name="uploadDate"></param>
        public static void RenameObject(string keyCurrent, string keyNew, string uploadedBy, string uploadDate)
        {
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    // Renaming an object consists of performing a copy, followed
                    // by a delete request.
                    CopyObjectRequest copyRequest = new CopyObjectRequest();

                    copyRequest.WithDirective(S3MetadataDirective.COPY)
                                .WithSourceBucket(BucketName)
                                .WithSourceKey(keyCurrent)
                                .WithDestinationBucket(BucketName)
                                .WithDestinationKey(keyNew)
                                .WithMetaData("uploadedby", uploadedBy)
                                .WithMetaData("uploaddate", uploadDate);

                    CopyObjectResponse copyResponse = S3Client.CopyObject(copyRequest);

                    DeleteObjectRequest deleteRequest = new DeleteObjectRequest();

                    deleteRequest.WithBucketName(BucketName)
                                .WithKey(keyCurrent);

                    DeleteObjectResponse deleteResponse = S3Client.DeleteObject(deleteRequest);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }

                throw;
            }
        }
        //-->

        /// <summary>
        /// Add Object
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="key"></param>
        /// <param name="classification"></param>
        /// <param name="fileType"></param>
        /// <param name="description"></param>
        /// <param name="uploadedBy"></param>
        /// <param name="keywords"></param>
        public static void AddObject(string filePath, string key, List<string> classification, string fileType, string description, string uploadedBy, string keywords)
        {
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    System.Net.ServicePointManager.DefaultConnectionLimit = 100;
                    TransferUtilityConfig config = new TransferUtilityConfig();
                    config.DefaultTimeout = 3600000;

                    using (TransferUtility fileTransferUtility = new TransferUtility(S3Client, config))
                    {
                        TransferUtilityUploadRequest fileTransferUtilityRequest =
                            new TransferUtilityUploadRequest()
                                .WithBucketName(BucketName)
                                .WithKey(key)
                                .WithFilePath(filePath)
                                .WithMetadata("filetype", fileType)
                                .WithMetadata("description", description)
                                .WithMetadata("ischeckedout", "false")
                                .WithMetadata("uploadedby", uploadedBy)
                                .WithMetadata("uploaddate", DateTime.Now.ToString(CultureInfo.InvariantCulture))
                                .WithMetadata("keywords", keywords)
                                .WithTimeout(3600000);


                        if (classification.Any())
                        {
                            int iLimit = classification.Count < CLASSIFICATION_LIMIT
                                             ? classification.Count
                                             : CLASSIFICATION_LIMIT;
                            for (int i = 0; i < iLimit; i++)
                            {
                                fileTransferUtilityRequest.WithMetadata("classification-" + (i + 1).ToString(CultureInfo.InvariantCulture), classification[i]);
                            }
                        }

                        fileTransferUtility.Upload(fileTransferUtilityRequest);

                    }

                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }

                throw;
            }
        }
        //-->

        /// <summary>
        /// Create New Folder
        /// </summary>
        /// <param name="folderPath">string</param>
        public static void CreateNewFolder(string folderPath)
        {

            string folderKey = folderPath;
            if (!folderKey.EndsWith("/")) folderKey += "/";

            try
            {
                // Create Folder (we have to upload a temporary image)
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    PutObjectRequest uploadRequest = new PutObjectRequest();

                    uploadRequest.WithBucketName(BucketName)
                        .WithKey(folderKey)
                        .WithContentBody(string.Empty);

                    S3Response uploadResponse = S3Client.PutObject(uploadRequest);
                }
                //>
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }

                throw;
            }
        }
        //-->

        /// <summary>
        /// Rename Folder
        /// </summary>
        /// <param name="folderPathCurrent"></param>
        /// <param name="folderPathNew"></param>
        /// <remarks>Rename folder and copy child files / folders rescursivley, as Amazon S3 does not move child objects automatically.</remarks>
        public static void RenameFolder(string folderPathCurrent, string folderPathNew)
        {
            // Create New Folder first
            if (!folderPathNew.EndsWith("/")) folderPathNew += "/";
            CreateNewFolder(folderPathNew);

            // Check if have child objects in current folder
            var folderObjects = GetObjects(folderPathCurrent, null, isGetAll: true);
            if(folderObjects.S3Objects.Count > 0)
            {
                // Move Child Objects
                MoveFileObjectsToFolderRecursive(folderObjects.S3Objects, folderPathNew);
            }

            // Delete Old Folder
            if (!folderPathCurrent.EndsWith("/")) folderPathCurrent += "/";
            DeleteObjectsRecursively(folderPathCurrent, true);
        }
        //-->

        /// <summary>
        /// Move File Objects To Folder Recursive
        /// </summary>
        /// <param name="s3FileObjects"></param>
        /// <param name="folderPathDestination"></param>
        /// <remarks>Recursive move required as Amazon S3 does not move child obects automatically.</remarks>
        public static void MoveFileObjectsToFolderRecursive(List<AWSObjectItem> s3FileObjects, string folderPathDestination)
        {

            foreach (var s3Item in s3FileObjects)
            {
                // Type of Object?
                if (s3Item.Type == AWSObjectItemType.File)
                {
                    // FILE
                    //---------------
                    string keyNameDestination = folderPathDestination + s3Item.FileName;
                    string fileType = "", keywords = "", description = "", uploadedBy = "", uploadDate = "";
                    List<string> classification = new List<string>();

                    // Get Object META Data
                    AWSObjectMetadata meta;
                    using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                    {
                        // Get Object META
                        meta = GetObjectMetadata(s3Item.Key);

                        if (meta != null)
                        {
                            // Classification
                            if (meta.Classification != null && meta.Classification.Any())
                                classification = meta.Classification;

                            // File Type
                            if (!string.IsNullOrEmpty(meta.FileType))
                                fileType = meta.FileType;

                            // Description
                            if (!string.IsNullOrEmpty(meta.Description))
                                description = meta.Description;

                            // Keywords
                            if (!string.IsNullOrEmpty(meta.Keywords))
                                keywords = meta.Keywords;

                            // Uploaded By
                            if (!string.IsNullOrEmpty(meta.UploadedBy))
                                uploadedBy = meta.UploadedBy;

                            // Upload Date
                            DateTime uploadDateDelayLoad;
                            if (!DateTime.TryParse(meta.UploadDate.ToString(CultureInfo.InvariantCulture), out uploadDateDelayLoad)) uploadDateDelayLoad = DateTime.UtcNow;
                            uploadDate = uploadDateDelayLoad == DateTime.MinValue ? "" : uploadDateDelayLoad.ToString(CultureInfo.InvariantCulture);
                        }
                    }
                    //>

                    // Copy Object
                    CopyObject(s3Item.Key, keyNameDestination, classification, fileType, description, keywords, uploadedBy, uploadDate);
                }
                else
                {
                    // FOLDER
                    //---------------

                    // Create Folder in new location
                    string folderPathNew = folderPathDestination + s3Item.FileName;
                    CreateNewFolder(folderPathNew);

                    // Check if have child objects in current folder
                    var folderObjects = GetObjects(s3Item.Key, null, isGetAll: true);
                    if (folderObjects.S3Objects.Count > 0)
                    {
                        // Move Child Objects
                        MoveFileObjectsToFolderRecursive(folderObjects.S3Objects, folderPathNew);
                    }
                }
            }
        }
        //-->

        /// <summary>
        /// Copy Object
        /// </summary>
        /// <param name="keyNameSource"></param>
        /// <param name="keyNameDestination"></param>
        /// <param name="classification"></param>
        /// <param name="fileType"></param>
        /// <param name="description"></param>
        /// <param name="keywords"></param>
        /// <param name="uploadedBy"></param>
        /// <param name="uploadDate"></param>
        public static void CopyObject(string keyNameSource, string keyNameDestination, List<string> classification,
                                        string fileType, string description, string keywords, string uploadedBy, string uploadDate)
        {
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    // Simple metadata updates are accomplished using a copy request.
                    CopyObjectRequest copyRequest = new CopyObjectRequest();

                    copyRequest.WithDirective(S3MetadataDirective.REPLACE)
                                .WithSourceBucket(BucketName)
                                .WithSourceKey(keyNameSource)
                                .WithDestinationBucket(BucketName)
                                .WithDestinationKey(keyNameDestination)
                                .WithMetaData("filetype", fileType)
                                .WithMetaData("description", description)
                                .WithMetaData("keywords", keywords)
                                .WithMetaData("uploadedby", uploadedBy)
                                .WithMetaData("uploaddate", uploadDate);

                    // Classification
                    if (classification.Any())
                    {
                        int iLimit = classification.Count < CLASSIFICATION_LIMIT
                                         ? classification.Count
                                         : CLASSIFICATION_LIMIT;
                        for (int i = 0; i < iLimit; i++)
                        {
                            copyRequest.WithMetaData("classification-" + (i + 1).ToString(CultureInfo.InvariantCulture), classification[i]);
                        }
                    }

                    CopyObjectResponse copyResponse = S3Client.CopyObject(copyRequest);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new ArgumentException("Invalid Access or Security Key");
                }

                throw;
            }
        }
        //-->

        /// <summary>
        /// Check Object Exist
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public static bool CheckObjectExist(string keyName)
        {
            GetObjectResponse results = new GetObjectResponse();
            bool isExist = false;
            try
            {
                using (S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
                {
                    ListObjectsRequest request = new ListObjectsRequest();
                    request.BucketName = BucketName;
                    request.WithPrefix(keyName + "/");
                    request.MaxKeys = 1;

                    using (ListObjectsResponse response = S3Client.ListObjects(request))
                    {
                        return (response.S3Objects.Count > 0);
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (!amazonS3Exception.ErrorCode.Equals(ErrorCodeNoSuchKey)))
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(amazonS3Exception);
                }
            }

            return isExist;
        }
        //-->

    }   
}