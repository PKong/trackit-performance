﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Tower Modifications | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.TowerModifications._Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/tower_modification") %>
   
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/tower_modification") %>

    <script type="text/javascript" language="javascript">
        // Global Var
        //searchFilterFields = "txtSiteID,ddlCustomer,ddlTowerModType,ddlTowerModStatus,ddlTowerModPM,ddlCurrentPhase,txtCurrentPacketRcvdFrom,txtCurrentPacketRcvdTo,txtProjectCompleteFrom,txtProjectCompleteTo";
        searchFilterFields = "txtSiteID,lsbCustomer,lsbTowerModType,lsbTowerModStatus,lsbTowerModPM,lsbCurrentPhase,txtCurrentPacketRcvdFrom,txtCurrentPacketRcvdTo,txtProjectCompleteFrom,txtProjectCompleteTo";
        currentFlexiGridID = '<%= this.fgModifications.ClientID %>';

        // ADV Search Var
        var GETdatas = new Array();
        var blnHaveGET = false;
        //ADV Search + Global Search Added by Nam
        var blnExec = false;

        // Doc Ready
        $(function ()
        {

            // Get query string.(query string, out arrayGET data)
            blnHaveGET = GrabGETData(window.location.search.substring(1), GETdatas);
            // Load Advance Search
            if (typeof $("#ddlCriteria") != "undefined") {
                ADVSearchInit($("#ddlCriteria"), "/TowerModifications");
            }
            if (typeof GETdatas['filter'] == "string") {
                blnExec = LoadAdvanceSearch(GETdatas['filter'], searchFilterFields);
                blnExec = blnExec | CheckGSFilter(GETdatas['filter']);
            }
            else {
                //Added 2012/02/28 for checking global search
                if (typeof GETdatas['gs'] == "string") {
                    var txtGS = GETdatas['gs'];
                    if (txtGS != undefined && txtGS != null) {
                        setCurrentGS(txtGS);
                        blnExec = true;
                    }
                }
            }
            //End Load advance search
            if (blnExec) {
                applySearchFilter(false);
            }

            if (GETdatas['filter'] == '') {
                $("#hiddenFilterParam").val('true');
                blnExec = false
            }

            // Set the modal size
            setModalSize(490, 450, 330);

            // Setup List Create Modal
            setupListCreateModal("Create New Tower Mod");

            // Setup Search Filter
            setupSearchFilter();

            // Setup Saved Filter Modal 
            setupSavedFilterModal(SavedFilterConstants.buttonCreateNew, SavedFilterConstants.listViews.towerMods.name, SavedFilterConstants.listViews.towerMods.url);

        });
        //-->

        // Flexigrid
        //
        // FG Before Send Data
        function fgBeforeSendData(data)
        {

            // Show - Loading Spinner
            $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

            // disable export button
            $("#MainContent_exportButton").val("Processing...");
            $("#MainContent_exportButton").attr("disabled", true);

            // Global Search
            var gs = $.urlParam(GlobalSearch.searchQueryString);
            //
            addGlobal:
            if (!String(gs).isNullOrEmpty())
            {
                for (i = 0; i < arrayOfFilters.length; i++)
                {
                    if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
                }
                addFilterPostParam(GlobalSearch.searchQueryString, gs);
            }
            //>

            // Set Default filter
            removeFilterPostParam(ConstGlobal.DEFAULT.returnPage);
            addFilterPostParam(ConstGlobal.DEFAULT.returnPage, savedFiltersQueryString);

            // Search Filter Data
            sendSearchFilterDataToHandler(data);
        }
        //
        // FG Data Load
        function fgDataLoad(sender, args)
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Enable export button
            $("#MainContent_exportButton").val("Export to Excel");
            $('#MainContent_exportButton').removeAttr("disabled");
        }
        //
        // FG Row Click
        function fgRowClick(sender, args)
        {

            return false;
        }
        //
        // FG Row Render
        function fgRowRender(tdCell, data, dataIdx)
        {

            var cVal = new String(data.cell[dataIdx]);

            // Type?
            switch (Number(dataIdx))
            {

                // Date 
                case 6:
                    if (cVal.isNullOrEmpty())
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;

                // Date 
                case 7:
                    if (cVal.isNullOrEmpty())
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;

                // Rogue Equipment   
                case 8:
                    sHtml = getColumnImageIcon(ColumnTypes.ROGUE_EQUIP, cVal, null);
                    tdCell.innerHTML = sHtml;
                    break;

                // Standard String 
                default:
                    if (cVal.isNullOrEmpty())
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;
            }
            //>
        }
        //
        // FG No Data
        function fgNoData()
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Disable export button
            $("#MainContent_exportButton").val("Export to Excel");
        }
        //-->

        // Window Load
        $(window).load(function () {

            // Apply Search Filter?
            if (blnExec) {

                // Wait For Last Field To Load
                var waitForLastFieldToLoadLimit = 0;
                var waitForLastFieldToLoad = setInterval(function () {
                    if (($('#hiddenLastFieldToCheckLoaded').val().length === 0) || (waitForLastFieldToLoadLimit === 50)) {
                        clearInterval(waitForLastFieldToLoad);

                        // Apply filters and reload the DataGrid
                        applySearchFilter(true);
                    }
                    waitForLastFieldToLoadLimit++;
                }, 100);
                //>

            } else {
                // Normal DataGrid Load
                if ($('.filter-tag').length > 0 || $("#hiddenFilterParam").val() == "true") {
                    reloadDataGrid();
                }
                else {
                    // Skip Normal load and show filter
                    $('.filter-box').slideToggle();
                    $("#MainContent_exportButton").attr("disabled", true);
                }
            }
        });
        //-->
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 id="h1" style="float:left; padding-top:3px; padding-right:30px;">Tower Modifications</h1>
	        <div class="page-options-nav">
                <a class="fancy-button clear-filter-button" href="javascript:void(0)">Clear Filters</a>
                <a class="fancy-button filter-button arrow-down" href="javascript:void(0)">Filter Results<span class="arrow-down-icon"></span></a> 
                <a id="modalBtnExternalSaveFilter" class="fancy-button save-filter-button" href="javascript:void(0)">Save Filter</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
		        <a id="modalBtnExternal" class="fancy-button" href="javascript:void(0)">Create New Tower Mod</a>
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->

    <!-- Search Filter Box -->
    <div class="filter-box">
	    <div style="padding:20px">
            <div class="filter-button-close"><a class="fancy-button close-filter" style="padding:2px 6px 2px 6px;" href="javascript:void(0)">x</a></div>
                <asp:Panel ID="panelFilterBox" runat="server">
                    <div class="left-col">

                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                            LabelFieldClientID="lblSiteID"
                            LabelFieldValue="Site ID"
                            LabelFieldCssClass="label"
                            TextFieldClientID="txtSiteID"
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188"
                            ScriptKeyName="ScriptAutoCompleteSiteID"
                            DataSourceUrl="SiteIdAutoComplete.axd"
                            ClientIDMode="Static"
                            runat="server" />
		                <br />

                        <label>Customer</label>
                        <%-- Change to multiple select by Katorn J. date 2012-09-03 --%>
                        <%-- <asp:DropDownList ID="ddlCustomer" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="Customer 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="Customer 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        
                        <div id="divCustomer" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCustomer" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
		
		                <label>Tower Mod Type</label>
                        <%-- Change to multiple select by Katorn J. date 2012-09-03 --%>
                        <%-- <asp:DropDownList ID="ddlTowerModType" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="Tower Mod Type 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="Tower Mod Type 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divTowerModType" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbTowerModType" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
		
		                <label>Tower Mod Status</label>
                        <%-- Change to multiple select by Katorn J. date 2012-09-03 --%>
                         <%-- <asp:DropDownList ID="ddlTowerModStatus" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="Tower Mod Status 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="Tower Mod Status 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divTowerModStatus" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbTowerModStatus" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>Tower Mod PM</label>
                        <%-- Change to multiple select by Katorn J. date 2012-09-03 --%>
                         <%-- <asp:DropDownList ID="ddlTowerModPM" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divTowerModPM" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbTowerModPM" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />		               
                    </div>

		            <div class="right-col">
                       <label>Current Phase</label>
                        <%-- Change to multiple select by Katorn J. date 2012-09-03 --%>
                         <%-- <asp:DropDownList ID="ddlCurrentPhase" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="Current Phase 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="Current Phase 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divCurrentPhase" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCurrentPhase" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

		                <label>Current Packet Rcvd</label>
		                <ul class="field-list">
                          <li>
                             <asp:TextBox ID="txtCurrentPacketRcvdFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valCurrentPackedRcvdFrom" runat="server"
                                                   ControlToValidate="txtCurrentPacketRcvdFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
		                    <li>&nbsp;to&nbsp;</li>
		                    <li>
                             <asp:TextBox ID="txtCurrentPacketRcvdTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valCurrentPacketRcvdTo" runat="server"
                                                   ControlToValidate="txtCurrentPacketRcvdTo"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
                        </ul>
                        <br />
		
		                <label>Project Complete</label>
		                <ul class="field-list">
                          <li>
                             <asp:TextBox ID="txtProjectCompleteFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valProjectCompleteFrom" runat="server"
                                                   ControlToValidate="txtProjectCompleteFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
		                    <li>&nbsp;to&nbsp;</li>
		                    <li>
                             <asp:TextBox ID="txtProjectCompleteTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valProjectCompleteTo" runat="server"
                                                   ControlToValidate="txtProjectCompleteTo"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
                        </ul>
                    </div>
                    <div class="cb"></div>
                    <!--Advance search -->
                    <div id="pnlAdvanceCriteria"></div>

                    <div class="cb"></div>
                    <asp:Label ID="lblAdvanceCriteria" CssClass="critesria-title" runat="server" >Add More Filter Criteria >></asp:Label>
                    <asp:DropDownList ID="ddlCriteria" CssClass="criteria-select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    <button id="blnAddAdvanceCriteria" class="fancy-button" type="button" onclick="OnAddingCriteria(null,null,null);" runat="server" >Add Criteria</button>
                    
                    <div class="cb"></div>
                    <div style="padding:20px 0 15px 0; overflow:visible;">
                        <div style="width:auto;float:left;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                        <div style="width:auto;float:right;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                    </div>
                </asp:Panel>
	    </div>
    </div>
    <!-- /Search Filter Box -->

    <div class="cb"></div>

    <!-- Filter Tags -->
    <div id="filterTagsContainer" class="filter-tags-container not-displayed"  runat="server"></div>
    <!-- /Filter Tags -->

    <div class="cb"></div>

    <!-- Tower Modifications Grid -->
    <div style="overflow:auto; margin-bottom: 20px; min-height:580px;">

        <fx:Flexigrid ID="fgModifications"
            AutoLoadData="False"
            Width="990"
            ResultsPerPage="20"
			ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			HandlerUrl="~/TowerModList.axd"
            runat="server">
		    <Columns>
			    <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="70" />
                <fx:FlexiColumn Code="customer.customer_name" Text="Customer" Width="130"  />
                <fx:FlexiColumn Code="tower_mod_statuses.tower_mod_status_name" Text="Tower Mod Status" Width="150" />
                <fx:FlexiColumn Code="tower_mod_types.tower_mod_type" Text="Tower Mod Type" Width="100" />
                <fx:FlexiColumn Code="tower_mod_pos.tower_mod_phase_types.phase_name" Text="Current Phase" Width="165" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="tower_mod_pm_user.last_name" Text="Tower Mod PM" Width="100" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="tower_mod_pos.ebid_req_rcvd_date" Text="Packet Rcvd" Width="80" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="project_complete_date" Text="Complete" Width="80" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site.rogue_equipment_yn" Text="RE" Width="32" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="id" Text="Actions" Width="60" AllowSort="false"
                                Format="<a class='fancy-button' href='/TowerModifications/Edit.aspx?id={0}'>Edit</a>" />
		    </Columns>
	    </fx:Flexigrid>
        <br />
        <div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgModifications')" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenLastFieldToCheckLoaded" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilterParam" runat="server" ClientIDMode="Static" Value="" />
        <div class="cb"></div>

    </div>
    <!-- /Tower Modifications Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->

</asp:Content>
