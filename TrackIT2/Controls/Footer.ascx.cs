﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.Objects;

namespace TrackIT2.Controls
{
    /// <summary>
    /// Footer
    /// </summary>
    public partial class Footer : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.SetupMenuItems();
            }
        }

        /// <summary>
        /// Setup Menu Items
        /// </summary>
        protected void SetupMenuItems()
        {
            hypApplicationsInitial.NavigateUrl = "~/LeaseApplications/?" + Globals.QS_PROJECT_CATEGORY + "=" + ProjectCategory.Initial.ToString();
            hypApplicationsModification.NavigateUrl = "~/LeaseApplications/?" + Globals.QS_PROJECT_CATEGORY + "=" + ProjectCategory.Mod.ToString();
            hypApplicationsTMOPremod.NavigateUrl = "~/LeaseApplications/?" + Globals.QS_PROJECT_CATEGORY + "=" + ProjectCategory.TMOPremod.ToString();

            // Issue Tracker
            hypIssueTracker.NavigateUrl = "~/IssueTracker/?filter=lsbStatus|1";
        }
    }
}