﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageSectionHeader.ascx.cs" Inherits="TrackIT2.Controls.PageSectionHeader" %>
<%@ Register TagPrefix="TrackIT2" TagName="Search" src="~/Controls/Search.ascx" %>
        
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/page_section_header") %>
        
        <!-- Page / Section Header -->
        <div class="section header">
            <div class="wrapper">
                <asp:HyperLink ID="logo" Target="" CssClass="logo" NavigateUrl="~/"  runat="server">T-Mobile Towers</asp:HyperLink>
                <ul class="nav">
                    <li><asp:HyperLink ID="hypDashboard" NavigateUrl="~/" runat="server">Dashboard</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypApplications" NavigateUrl="~/LeaseApplications/" runat="server">Applications</asp:HyperLink>
                            <ul class="sub_menu">
                                <li><asp:HyperLink ID="hypApplicationsAll" NavigateUrl="~/LeaseApplications/" runat="server">All Applications</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypApplicationsInitial" runat="server">Initial</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypApplicationsModification" runat="server">Modification</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypApplicationsTMOPremod" runat="server">TMO Premod</asp:HyperLink></li>
                            </ul>
                    </li>
                    <li><asp:HyperLink ID="hypWorkflows" runat="server" NavigateUrl="~/Sites/">Workflows</asp:HyperLink>
                            <ul class="sub_menu">
                                <li><asp:HyperLink ID="hypSites" NavigateUrl="~/Sites/" runat="server">Sites</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypSiteDataPackages" NavigateUrl="~/SiteDataPackages/" runat="server">Site Data Package</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypTowerMod" NavigateUrl="~/TowerModifications/" runat="server">Tower Mod</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypIssueTracker" NavigateUrl="~/IssueTracker/" runat="server">Issue Tracker</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypLeaseAdminTracker" NavigateUrl="~/LeaseAdminTracker/" runat="server">Lease Admin Tracker</asp:HyperLink></li>
                                <li><asp:HyperLink ID="hypDMS" NavigateUrl="~/DMS/" runat="server">TrackiT Docs</asp:HyperLink></li>
                            </ul>
                    </li>
                    <li><asp:HyperLink ID="hypReports" NavigateUrl="~/Reports" runat="server">Reports</asp:HyperLink></li>
                    <li style="position:relative;">
                        <TrackIT2:Search id="search" runat="server" />
                    </li>
                </ul>
                <div class="cb"></div>
            </div>
        </div>
        <!-- /Page / Section Header -->