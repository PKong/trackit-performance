﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNav.ascx.cs" Inherits="TrackIT2.Controls.TopNav" %>
        <!-- Top Nav -->
        <div class="section top-nav">
            <div class="wrapper">
                <asp:LoginView ID="HeadLoginView" runat="server" EnableViewState="false">
                    <AnonymousTemplate>
                       <a href="~/Account/Login.aspx" class="btnTopNav" id="HeadLoginStatus" runat="server">Log In</a>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        Welcome: <span class="bold"><asp:Label ID="lblDisplayName" CssClass="btnTopNav" runat="server" Text="[First Last]" /></span>
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="hypProfile" CssClass="btnTopNav" runat="server" NavigateUrl="~/Account/Profile.aspx">Profile</asp:HyperLink>
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="hypImportApplication" CssClass="btnTopNav" runat="server" NavigateUrl="javascript:void(0);" >Application Import </asp:HyperLink>  
                        <asp:Label runat="server" ID="lblSeparater" Text="&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;" ></asp:Label>
                        <asp:HyperLink ID="hypTowerList" CssClass="btnTopNav" runat="server" NavigateUrl="~/TowerList" Visible="false" >Tower List </asp:HyperLink>  
                        <asp:Label runat="server" ID="lblTowerListSpacer" Text="&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;" Visible="false" ></asp:Label>
                        <asp:HyperLink ID="hypAdmin" CssClass="btnTopNav" runat="server" NavigateUrl="~/Admin">Admin Menu</asp:HyperLink>                        
                        <asp:Label ID="lblAdminSpacer" runat="server" Text="&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;"></asp:Label>
                        <asp:HyperLink ID="hypLogout" CssClass="btnTopNav" runat="server" NavigateUrl="~/Account/Logout.aspx">Log Out</asp:HyperLink>
                    </LoggedInTemplate>
                </asp:LoginView>
                <div style="float: left;">
                   <asp:Label ID="lblImpersonateRole" runat="server" Text="Impersonate Role:&nbsp;"></asp:Label>
                   <asp:DropDownList ID="ddlImpersonateRole" runat="server" ClientIDMode="Static" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlImpersonateRole_SelectedIndexChanged">
                      <asp:ListItem Text="None" Value="None" Selected="True"></asp:ListItem>
                   </asp:DropDownList>
                </div>
            </div>
        </div>
        <!-- /Top Nav -->