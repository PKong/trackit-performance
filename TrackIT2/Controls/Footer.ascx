﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="TrackIT2.Controls.Footer" %>
        <!-- Footer -->
        <div class="section footer">
            <div class="wrapper">
                <ul class="footer-nav">
                    <li class="title"><asp:HyperLink ID="hypDashboard" runat="server" NavigateUrl="~/">Dashboard</asp:HyperLink></li>
                    <li class="title"><asp:HyperLink ID="hypApplications" runat="server" NavigateUrl="~/LeaseApplications/">Applications</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypApplicationsAll" runat="server" NavigateUrl="~/LeaseApplications/">All Applications</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypApplicationsInitial" runat="server">Initial</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypApplicationsModification" runat="server">Modification</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypApplicationsTMOPremod" runat="server">TMO Premod</asp:HyperLink></li>
                </ul>
                <ul class="footer-nav">
                    <li class="title"><asp:HyperLink ID="hypWorkflows" runat="server" NavigateUrl="~/Sites/">Workflows</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypSites" runat="server" NavigateUrl="~/Sites/">Sites</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypSiteDataPackages" runat="server" NavigateUrl="~/SiteDataPackages/">Site Data Package</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypTowerMod" runat="server" NavigateUrl="~/TowerModifications/">Tower Mod</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypIssueTracker" NavigateUrl="~/IssueTracker/" runat="server">Issue Tracker</asp:HyperLink></li>
                    <li><asp:HyperLink ID="hypLeaseAdminTracker" NavigateUrl="~/LeaseAdminTracker/" runat="server">Lease Admin Tracker</asp:HyperLink></li>
                </ul>
                <ul class="footer-nav">
                    <li class="title"><asp:HyperLink ID="hypReports" NavigateUrl="~/Reports/" runat="server">Reports</asp:HyperLink></li>
                    <li class="title"><asp:HyperLink ID="hypAccount" NavigateUrl="~/Account/Profile.aspx" runat="server">Account</asp:HyperLink></li>
                    <li class="title"><asp:HyperLink ID="hypSendError" NavigateUrl="mailto:support@t-mobiletowers.com?subject=TrackiT2 Error" Target="_blank" runat="server">Send Error</asp:HyperLink></li>
                    <li class="title"><asp:HyperLink ID="hypSuggestImprovement" NavigateUrl="mailto:support@t-mobiletowers.com?subject=TrackiT2 Suggestion" Target="_blank" runat="server">Suggest Improvement</asp:HyperLink></li>
                </ul>
            </div>
        </div>
        <!-- /Footer -->