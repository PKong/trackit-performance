﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;
using TrackIT2.BLL;

namespace TrackIT2.Controls
{
    public partial class PageSectionHeader : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.SetupMenuItems();
            }
        }

        /// <summary>
        /// Setup Menu Items
        /// </summary>
        protected void SetupMenuItems()
        {
            hypApplicationsInitial.NavigateUrl = "~/LeaseApplications/?filter=lsbLeaseAppType|7";
            hypApplicationsModification.NavigateUrl = "~/LeaseApplications/?filter=lsbLeaseAppType|8|lsbLeaseAppType|9|" +
                "lsbLeaseAppType|10|lsbLeaseAppType|11|lsbLeaseAppType|12|lsbLeaseAppType|13|lsbLeaseAppType|17|lsbLeaseAppType|18|lsbLeaseAppType|19|lsbLeaseAppType|20|lsbLeaseAppType|21";
            hypApplicationsTMOPremod.NavigateUrl = "~/LeaseApplications/?filter=lsbActivityType|7";

            // Issue Tracker
            hypIssueTracker.NavigateUrl = "~/IssueTracker/?filter=lsbStatus|1";
        }
    }
}