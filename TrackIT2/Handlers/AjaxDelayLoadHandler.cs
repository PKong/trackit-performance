﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.BLL;
using System.Web.SessionState;

namespace TrackIT2.Handlers
{
    public class AjaxDelayLoadHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public enum AjaxDelayLoadType
        {
            WorkFlow,
            Comment,
            SA,
            SA_Comment
        }

        /// <summary>
        /// IsReusable
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }
        //>

        /// <summary>
        /// ProcessRequest
        /// </summary>
        /// <param name="context"></param>
        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public void ProcessRequest(HttpContext context)
        {
            string returnResult = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["type"]))
                {
                    string type = HttpContext.Current.Request.QueryString["type"];
                    if (type == AjaxDelayLoadType.WorkFlow.ToString())
                    {
                        #region WorkFlow
                        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["siteid"]))
                        {
                            // Param
                            String siteId = HttpContext.Current.Request.QueryString["siteid"];
                            returnResult = AjaxDelayLoad.GetSiteWorkflowsList(siteId);
                        }
                        else if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["leaseAppId"]))
                        {
                            // Param
                            String stringLeaseAppId = HttpContext.Current.Request.QueryString["leaseAppId"];
                            int leaseAppId = 0;
                            if (int.TryParse(stringLeaseAppId, out leaseAppId))
                            {
                                returnResult = AjaxDelayLoad.GetApplicationWorkflowsForLA(leaseAppId);
                            }
                            else
                            {
                                throw new Exception("leaseAppId must be int.");
                            }
                        }
                        #endregion WorkFlow
                    }
                    else if (type == AjaxDelayLoadType.Comment.ToString())
                    {
                        #region Comment

                        #endregion Comment
                    }
                    else if (type == AjaxDelayLoadType.SA.ToString())
                    {
                        #region SA

                        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["leaseAppID"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["saId"]))
                        {
                            String stringLeaseAppId = HttpContext.Current.Request.QueryString["leaseAppID"];
                            String stringSaId = HttpContext.Current.Request.QueryString["saId"];
                            int leaseAppId = 0;
                            int saId = 0;
                            if (int.TryParse(stringLeaseAppId, out leaseAppId) && int.TryParse(stringSaId, out saId))
                            {
                                AjaxDelayLoad.GetJsonSA(false, out returnResult, leaseAppId, saId);
                            }
                            else
                            {
                                throw new Exception("leaseAppID or saId must be int.");
                            }
                        }

                        #endregion SA
                    }
                    else if (type == AjaxDelayLoadType.SA_Comment.ToString())
                    {
                        #region SA

                        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["leaseAppID"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["saId"]))
                        {
                            String stringLeaseAppId = HttpContext.Current.Request.QueryString["leaseAppID"];
                            String stringSaId = HttpContext.Current.Request.QueryString["saId"];
                            int leaseAppId = 0;
                            int saId = 0;
                            if (int.TryParse(stringLeaseAppId, out leaseAppId) && int.TryParse(stringSaId, out saId))
                            {
                                AjaxDelayLoad.GetJsonSAComment(false, out returnResult, leaseAppId, saId);
                            }
                            else
                            {
                                throw new Exception("leaseAppID or saId must be int.");
                            }
                        }

                        #endregion SA
                    }
                }
                //else do nothing
                context.Response.ContentType = "text";
                context.Response.Write(returnResult);
            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                context.Response.Write("");
            }
        }
        //-->

        #endregion
    }
}