﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using FlexigridASPNET;
using System.Collections.Specialized;
using System.Linq;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// SiteIdAutoCompleteHandler - returns the data for Site ID Auto-complete Text Fields
    /// </summary>
    public class TMOAppIdAutoCompleteHandler : IHttpHandler
    {

        #region IHttpHandler Members

        /// <summary>
        /// IsReusable
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }
        //>

        /// <summary>
        /// ProcessRequest
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                EquipmentEntities enEquip = new EquipmentEntities();
                IList<string> result = new List<string>();

                //check select site textBox
                if(context.Request.QueryString["selectSite"]=="")
                {
                    //create, load all data
                    var sites = enEquip.t_Application.Where("it.TMO_AppID LIKE '" + context.Request.QueryString["term"] + "%'").OrderBy("it.TMO_AppID");
                    
                    // Convert Result
                    foreach (var s in sites)
                    {
                        result.Add(s.TMO_AppID);
                    }
                }
                else
                {
                    //edit, load data start with selectSite
                    var AllSites = enEquip.t_Application.Where("it.TMO_AppID LIKE '" + context.Request.QueryString["selectSite"] + "%'").OrderBy("it.TMO_AppID");
                    string type = context.Request.QueryString["term"];
                    var sites = from s in AllSites
                                where s.TMO_AppID.Contains(type)
                                select s;

                    // Convert Result
                    foreach (var s in sites)
                    {
                        result.Add(s.TMO_AppID);
                    }
                }

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string ids = javaScriptSerializer.Serialize(result);
                context.Response.ContentType = "text/html";
                context.Response.Write(ids);
            }
            catch
            {
                context.Response.Write("");
            }
        }
        //--
        #endregion
    }
}