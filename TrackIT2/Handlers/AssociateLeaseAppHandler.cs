﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;

namespace TrackIT2.Handlers
{
    public class AssociateLeaseAppHandler : IHttpHandler
    {
        #region IHttpHandler Members

        /// <summary>
        /// IsReusable
        /// </summary>
        public bool IsReusable
        {
            get { return true; }
        }
        //>

        /// <summary>
        /// ProcessRequest
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                String siteId = context.Request.QueryString["siteid"];
                List<AssociatedLeaseAppInfo> returnValue = new List<AssociatedLeaseAppInfo>();
                List<customer> AlreadyDetachList = new List<customer>();

                using (CPTTEntities ce = new CPTTEntities())
                {
                    if (siteId != null)
                    {
                        returnValue = AssociatedLeaseApp.GetAssociatedLeaseApp(siteId, ce);
                        foreach (AssociatedLeaseAppInfo lease in returnValue)
                        {
                            if (lease.Customer != null)
                            {
                                var cus = from c in AlreadyDetachList where c.Equals(lease.Customer) select c;
                                if (!cus.Any())
                                {
                                    ce.Detach(lease.Customer);
                                    AlreadyDetachList.Add(lease.Customer);
                                }
                            }
                        }
                    }
                }

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string ids = javaScriptSerializer.Serialize(returnValue);
                context.Response.ContentType = "json";
                context.Response.Write(ids);
            }
            catch
            {
                context.Response.Write("");
            }
        }
        //--

        #endregion
    }
}