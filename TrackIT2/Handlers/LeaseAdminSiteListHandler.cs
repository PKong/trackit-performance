﻿using System;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;
using System.Text;

namespace TrackIT2.Handlers
{
    public class LeaseAdminSiteListHandler : BaseFlexiHandler
    {
        public static bool _subFilterHasNoResults = false;
        public static string GOOGLE_LINK = "<a target='_blank' href='{0}' >{1}</a>";
        public static string sAddressFormat = "{0}{1}<br />{2},{3},{4}";
        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {

            //
            CPTTEntities ce = new CPTTEntities();
            IQueryable<lease_admin_records> iqLeaseAdmin;
            List<site> lstSite = new List<site>();
            lease_admin_records LeaseAdminData = null;
            System.Data.Objects.DataClasses.EntityCollection<site> sitedata = null;
            int skipItems = (Page - 1) * this.ItemsPerPage;
            string sortName = !String.IsNullOrWhiteSpace(this.SortName) ? this.SortName : "site.site_uid";

            // Search Filter Values
            NameValueCollection form = HttpContext.Current.Request.Form;
            int startIndex = 6;
            StringBuilder sb = new StringBuilder();
            //
            // Get Filters
            if (form.Count > startIndex)
            {
                for (int i = startIndex; i < form.Count; i++)
                {
                    // Check not Global Search param
                    if (form.GetKey(i) != Globals.QS_GLOBAL_SEARCH)
                    {
                        // Build Where clause
                        String itemKey = form.GetKey(i);
                        String itemValue = form.Get(i);
                        if (itemKey != DefaultFilterSession.SessionName && itemKey != DefaultFilterSession.ApplyClick)
                        {
                            buildSearchFilterWhereClause(sb, itemKey, Utility.PrepareSqlSearchString(itemValue));
                        }
                    }
                }
            }
            //>

            // Get Working Set
            if (sb.Length > 0)
            {
                iqLeaseAdmin = ce.lease_admin_records.Where(sb.ToString());
            }
            else
            {
                iqLeaseAdmin = ce.lease_admin_records;
            }
            //>

            if (iqLeaseAdmin.Count() > 0)
            {
                LeaseAdminData = iqLeaseAdmin.ToList<lease_admin_records>()[0];
            }

            if (LeaseAdminData != null)
            {
                if (LeaseAdminData.sites != null)
                {
                    sitedata = LeaseAdminData.sites;
                }
            }

            // Items Count
            if (sitedata != null)
            {
                if (sitedata.Count() > 1)
                {
                    this.VitualItemsCount = sitedata.Count();
                    // Sort Order
                    if (this.SortDirection == GridSortOrder.Asc)
                    {
                        var a = Utility.OrderBy(sitedata.AsQueryable(), sortName);
                        lstSite = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                    }
                    else
                    {
                        var b = Utility.OrderByDescending(sitedata.AsQueryable(), sortName);
                        lstSite = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                    }
                }
                //>
            }

            // Convert Result
            var result = new List<KeyValuePair<string, object[]>>();
            foreach (var site in lstSite)
            {
                string saddress = string.Format(sAddressFormat, Utility.PrepareString(site.address.ToString())
                    , Utility.PrepareString(site.city)
                    , Utility.PrepareString(site.state)
                    , Utility.PrepareString(site.zip)
                    , Utility.PrepareString(site.county));
                
                string SiteLatitude = String.Empty;
                string DegreeLatitude = String.Empty;
                string SiteLongitude = String.Empty;
                string DegreeLongtitude = String.Empty;
                string LatLongString = String.Empty;
                string LatLongURL = String.Empty;

                SiteLatitude = site.site_latitude.ToString();
                DegreeLatitude = Utility.ConvertLatLongCoorToDegree(site.site_latitude).ToString();
                SiteLongitude = site.site_longitude.ToString();
                DegreeLongtitude = Utility.ConvertLatLongCoorToDegree(site.site_longitude).ToString();

                LatLongString = DegreeLatitude + ",&nbsp;" + DegreeLongtitude + "<br />" + SiteLatitude + ",&nbsp;" + SiteLongitude;
                LatLongURL = Utility.GetGooleMapLatLong(SiteLatitude, SiteLongitude).ToString();

                result.Add(new KeyValuePair<string, object[]>(site.site_uid.ToString(), 
                               new object[] { site.site_uid, 
                                              site.site_name, 
                                              site.market_name,
                                              saddress, 
                                              String.Format(GOOGLE_LINK,LatLongURL,LatLongString),
                                              Site.HasTowerModifications(site.site_uid).ToString(), 
                                              site.rogue_equipment_yn.ToString(), 
                                              site.site_uid.ToString() }));
            }
            return result;

        }
        //-->

        /// <summary>
        /// Build Search Filter Where Clause
        /// </summary>
        /// <returns></returns>
        public static void buildSearchFilterWhereClause(StringBuilder sb, String itemKey, String itemValue)
        {
            if (sb.Length > 0)
            {
                sb.Append(" AND ");
            }

            var sbType = new StringBuilder();
            switch (itemKey)
            {
                // ID
                case SearchFilterControls.ID.Name :
                    sb.AppendFormat(SearchFilterControls.ID.Criterea, itemValue);
                    break;
            }
        }
        //-->

        /// <summary>
        /// Search Filter Controls
        /// </summary>
        public struct SearchFilterControls
        {
            /// <summary>
            /// Lease Admin ID
            /// </summary>
            public struct ID
            {
                public const String Name = "ID";
                public const String Criterea = "(it.id = {0})";
            }
        }
        //-->
    }
}