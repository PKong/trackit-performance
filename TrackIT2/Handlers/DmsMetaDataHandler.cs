﻿using System;
using System.Collections.Generic;
using TrackIT2.DAL;

namespace TrackIT2.Handlers
{
    public class DmsMetaDataHandler
    {
        #region Attribute
            //Meta Data
            private List<KeyValuePair<string, object[]>>  _result = new List<KeyValuePair<string, object[]>>();
            private AWSObjectItem _objKey;
            private bool _bLoadAction = false;
            private string _rowClass = "";
            private string _basepath = "";
            //Backgrouynnd worker
            private System.ComponentModel.BackgroundWorker _objWorker = new System.ComponentModel.BackgroundWorker();
            //Self event
            public delegate void OnComplete_Handler(string basepath,AWSObjectItem aws,string rowIdAsClass);
            public event OnComplete_Handler OnComplete;
        #endregion

        #region Construction 
            public DmsMetaDataHandler(String basepath,AWSObjectItem key, bool LoadAction, string rowClass)
            {
                this._basepath = basepath;
                this._objKey = key;
                this._bLoadAction = LoadAction;
                this._rowClass = rowClass;
                this._objWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(_objWorker_DoWork);
                this._objWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_objWorker_RunWorkerCompleted);

                this._objWorker.RunWorkerAsync();
            }
        #endregion

        #region BackgroundWorkerEvet

            private void _objWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
                Complete(this._basepath, this._objKey, this._rowClass);
            }
            private void _objWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
            {
                //Add retry to retrieve metadata in 5 times
                bool bIsComplete = false;
                int retry = 0;
                while (!bIsComplete)
                {
                    try
                    {
                        if (retry >= 5)
                        {
                            bIsComplete = true;
                        }
                        this._objKey.Metadata = new AWSObjectMetadata();
                        this._objKey.Metadata = DmsFlexiGridHandler.LoadThreadMetaData(this._objKey.Key);
                        if (this._objKey.Metadata.UploadDate == null)
                        {
                            this._objKey.Metadata.UploadDate = new DateTime();
                        }
                        if (this._objKey.Metadata.Classification == null)
                        {
                            this._objKey.Metadata.Classification = new List<string>();
                        }
                        bIsComplete = true;
                    }
                    catch 
                    {
                        retry++;
                    }
                }
            }
        #endregion

        #region RaiseEvent
            protected virtual void Complete(string basepath,AWSObjectItem aws, string rowClass)
            {
                OnComplete(_basepath,aws, rowClass);
            }
        #endregion
    }

    public class DmsApplicationDataHandler
    {
        #region Attribute
        //Meta Data
        private List<KeyValuePair<string, object[]>> _result = new List<KeyValuePair<string, object[]>>();
        private AWSResultSet _objResult;
        private String _objKey;
        private bool _bLoadAction = false;
        private string _rowClass = "";
        //Backgrouynnd worker
        private System.ComponentModel.BackgroundWorker _objWorker = new System.ComponentModel.BackgroundWorker();
        //Self event
        public delegate void OnComplete_Handler(AWSResultSet aws, string rowIdAsClass);
        public event OnComplete_Handler OnComplete;
        #endregion

        #region Construction
        public DmsApplicationDataHandler(String key, bool LoadAction, string rowClass)
        {
            this._objKey = key;
            this._rowClass = rowClass;
            this._objWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(_objWorker_DoWork);
            this._objWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(_objWorker_RunWorkerCompleted);

            this._objWorker.RunWorkerAsync();
        }
        #endregion

        #region BackgroundWorkerEvet

        private void _objWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Complete(this._objResult, this._objKey);
        }
        private void _objWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            //Add retry to retrieve metadata in 5 times
            bool bIsComplete = false;
            int retry = 0;
            while (!bIsComplete)
            {
                try
                {
                    //get _objResult from key
                    this._objResult  = AmazonDMS.GetObjectByKey(this._objKey);
                    bIsComplete = true;
                }
                catch
                {
                    retry++;
                }
            }
        }
        #endregion

        #region RaiseEvent
        protected virtual void Complete(AWSResultSet aws, string key)
        {
            OnComplete(aws, key);
        }
        #endregion
    }


}
