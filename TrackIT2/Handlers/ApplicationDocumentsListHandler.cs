﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Collections;

namespace TrackIT2.Handlers
{
    public class ApplicationDocumentsListHandler : BaseFlexiHandler
    {
        // Var
        private const string HTML_JS_LINK = "<a href=\"javascript:void(0);\" onclick=\"{0}\" style=\"text-decoration:none;\">{1}</a>";
        private const string HTML_IMAGE = "<img src=\"{0}\" {1}/>";
        private const string HTML_IMAGE_ICON = "<img width=\"{2}\" height=\"{3}\" src=\"{0}\" {1}/>";
        private const string HTML_SELECT_ACTIONS = "<select class=\"sel-file-actions\" style=\"width:auto;\" disabled=\"disabled\"><option>- Select -</option><option value=\"{0}\">Download</option><option value=\"{0}\">Edit</option><option value=\"{0}\">Move</option><option value=\"{0}\">Delete</option></select>";
        private const string HTML_SELECT_ACTIONS_FOLDER = "<select class=\"sel-folder-actions\" style=\"width:auto;\"><option>- Select -</option><option value=\"{0}\">Edit</option><option value=\"{0}\">Move</option><option value=\"{0}\">Delete</option></select>";
        private const string HTML_SELECT_ACTIONS_NO_DELETE = "<select class=\"sel-file-actions\" style=\"width:auto;\" disabled=\"disabled\"><option>- Select -</option><option value=\"{0}\">Download</option><option value=\"{0}\">Edit</option><option value=\"{0}\">Move</option></select>";
        private const string HTML_SELECT_ACTIONS_REVISION = "<select class=\"sel-file-actions\" style=\"width:auto;\" ><option>- Select -</option><option value=\"{0}\">Download</option></select>";
        private const string PATH_LOCK_IMG = "/images/icons/Lock.png";
        private const string PATH_UNLOCK_IMG = "/images/icons/Unlock.png";
        private const string PATH_FILE_TYPE_IMG_ROOT = "/images/icons/file-types/32px/";
        private const string ACTIONS_UNAVAILABLE = "<em>Actions Unavailable</em>";
        private const string PATH_EXPAND_IMG = "/images/icons/details_open.png";
        private const string PATH_COLLAPSE_IMG = "/images/icons/details_close.png";
        //>
        private static bool _userHasAdministrationPrivileges;
        private DmsApplicationDataHandler metaHand;
        //>
        public static List<KeyValuePair<string, object[]>> result;

        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {

            // Flexigrid params
            result = new List<KeyValuePair<string, object[]>>();
            const int START_INDEX = 6;
            string basePath = "";
            bool bIsSortData = false;
            _userHasAdministrationPrivileges = Security.UserIsInRole("TrackiT Docs Admin");
            Hashtable hashAllRevisonItem = new Hashtable();
            GridSortOrder? customSorting = null;

            if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
            {
                hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
            }

            // Item Load META Request?
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["requestType"]) && !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["key"])
                && HttpContext.Current.Request.QueryString["requestType"] == "loadApplicationDocument")
            {
                // Key
                string key = HttpContext.Current.Request.QueryString["key"];
                Hashtable list = (Hashtable)HttpContext.Current.Session["hashStaticItems"];
                // Load Actions?
                bool loadActions = !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["loadActions"]) && HttpContext.Current.Request.QueryString["loadActions"] == "true";

                // Load revision?
                bool loadRevision = !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["revison"]) && HttpContext.Current.Request.QueryString["revison"] == "true";

                ApplicationDocumentsItem res = null;
                if (list.ContainsKey(key))
                {
                    res = (ApplicationDocumentsItem)list[key];
                }
                else
                {
                    hashAllRevisonItem = (Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                    AWSResultSet  aws = AmazonDMS.GetObjectByKey(key);
                    if (aws.S3Objects.Count > 0)
                    {
                        AWSObjectItem item = DmsFlexiGridHandler.GetCurrentVersionFile(aws.S3Objects, ref hashAllRevisonItem)[0];
                        list.Add(key, new ApplicationDocumentsItem(key.Split('/')[key.Split('/').Length - 1], key, aws.S3Objects.Count().ToString(),
                                                                  item, aws.S3Objects));
                    }
                }
                string option = string.Empty;
                string revision = "";
                string fileSize = string.Empty;
                if ((res != null) && (res.ObjectItem != null))
                {
                    revision = res.ListObjectItem.Count().ToString();
                    if (res.ListObjectItem.Count() > 1)
                    {
                        string optionImage = string.Format(HTML_IMAGE_ICON, PATH_EXPAND_IMG, "title=\"Click to see revision\" class=\"expand-row\" id=\"img" + HttpUtility.UrlEncode(key) + "\"", "16", "16");
                        option = string.Format(HTML_JS_LINK, "ExpandRevision(this,'" + HttpUtility.UrlEncode(key.Replace(" ", "|")) + "','" + basePath + "');", optionImage + "<br />");
                    }
                    fileSize = res.ObjectItem.ItemObject.Size.ToFileSize();
                    return LoadItemMeta(key, result, option,
                                          fileSize, revision, loadRevision);
                }
                // Load and return META
                else
                {
                     result.Add(new KeyValuePair<string, object[]>(key, new object[]
                                                                                    {
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        String.Empty,
                                                                                        option,
                                                                                        fileSize,
                                                                                        revision
                                                                                    }));
                     return result;
                }
            }

            HttpContext.Current.Session["hashAllRevisonItem"] = new Hashtable(); ;
            //>
            DMSDocumentLinkHelper.eListDocumentType? ListDocumenType = null;
            int LeaseApplicationId = -1;

            // Amazon Result Set
            new AWSResultSet();

            // Get Form Data.
            NameValueCollection form = HttpContext.Current.Request.Form;
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                // Get Hash Form Value
                string hashFormVal = "";
                if ((form.Get("hash") != null) && (form.Get("hash") != "")) hashFormVal = form.Get("hash");
                bIsSortData = string.IsNullOrEmpty(form.Get("sortdata")) ? false : true;
            }
            //>
            // Check Base Path
            if (form.Count > START_INDEX)
            {
                for (int i = START_INDEX; i < form.Count; i++)
                {

                    // Folder Change.
                    if (form.GetKey(i) == "fd")
                    {
                        if (form.Get(i) != null && form.Get(i) != "")
                        {
                            basePath = HttpUtility.UrlDecode(form.Get(i));
                            basePath = basePath.Substring(0, basePath.Length - 1);
                        }
                    }

                    // Delete File
                    if (form.GetKey(i) == "del")
                    {
                        if (form.Get(i) != null && form.Get(i) != "")
                        {
                            string keyName = HttpUtility.UrlDecode(form.Get(i));
                            bool isFolder = !keyName.Contains(".");
                            //Delete
                            AmazonDMS.DeleteObjectsRecursively(keyName, isFolder);

                            //remove revision file
                            if (!isFolder)
                            {
                                if (HttpContext.Current.Session["hashAllRevisonItem"] != null)
                                {
                                    hashAllRevisonItem = (System.Collections.Hashtable)HttpContext.Current.Session["hashAllRevisonItem"];
                                }
                                else
                                {
                                    hashAllRevisonItem = new System.Collections.Hashtable();
                                }
                                foreach (System.Collections.DictionaryEntry entry in hashAllRevisonItem)
                                {
                                    if (((AWSObjectItem)entry.Key).Key == keyName)
                                    {
                                        List<AWSObjectItem> lstRev = (List<AWSObjectItem>)entry.Value;
                                        foreach (AWSObjectItem s3Item in lstRev)
                                        {
                                            AmazonDMS.DeleteObjectsRecursively(s3Item.Key, false);

                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Get Lease Application Document Type
                    if (form.GetKey(i) == "listDocumentType")
                    {
                        ListDocumenType = (DMSDocumentLinkHelper.eListDocumentType)Enum.Parse(typeof(DMSDocumentLinkHelper.eListDocumentType), form.Get(i));
                    }
                    //Get Lease Application Document Type
                    if (form.GetKey(i) == "leaseAppId")
                    {
                        int.TryParse(form.Get(i), out LeaseApplicationId);
                    }
                }
            }
            //>

            AWSResultSet objectSet = new AWSResultSet();
            // Refresh data source
            Hashtable hashStaticItems = new Hashtable();
            //retrieve DMS data from database
            if (customSorting == null) customSorting = this.SortDirection;
            if (!bIsSortData && ListDocumenType.HasValue)
            {
                List<ApplicationDocumentsItem> listResult = new List<ApplicationDocumentsItem>();
                List<string> listKey = DMSDocumentLinkHelper.GetListDocumentByApp(LeaseApplicationId, ListDocumenType);
                int row = 0;

                if (HttpContext.Current.Session["hashStaticItems"] == null)
                {
                    HttpContext.Current.Session["hashStaticItems"] = hashStaticItems;
                }
                foreach (string key in listKey)
                {
                    metaHand = new DmsApplicationDataHandler(key, true, row.ToString());
                    metaHand.OnComplete += new DmsApplicationDataHandler.OnComplete_Handler(_OnRunComplete);

                    listResult.Add(new ApplicationDocumentsItem(key.Split('/')[key.Split('/').Length - 1], key,
                                                                  "", null,null));
                                                            
                    row++;
                }

                VitualItemsCount = listKey.Count();

                string sortName = "FileName";
                int skipItems = (Page - 1) * ItemsPerPage;
                if (customSorting == GridSortOrder.Asc)
                {
                    var a = Utility.OrderBy(listResult.AsQueryable(), sortName);
                    listResult = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                }
                else
                {
                    var b = Utility.OrderByDescending(listResult.AsQueryable(), sortName);
                    listResult = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                }

                var listDmsFileType = FileUtility.GetListDmsFileTypes();

                // Iterate & Build Results..
                foreach (ApplicationDocumentsItem s3item in listResult)
                {
                    // Build Result
                    string title = string.Format("title=\"{0}\"", s3item.FileName);
                    FileUtility.FileType objFileType = FileUtility.GetFileType(s3item.FileName, listDmsFileType);
                    string sFileImage = string.Format(HTML_IMAGE_ICON, PATH_FILE_TYPE_IMG_ROOT + objFileType.FileIcon, title, "24", "24");
                    string fileName = string.Format(HTML_JS_LINK, "OnDownloadClick('" + HttpUtility.UrlEncode(s3item.key) + "');", sFileImage + "<br />" + s3item.FileName);
                    string actionHtml = string.Format(_userHasAdministrationPrivileges ? HTML_SELECT_ACTIONS : HTML_SELECT_ACTIONS_NO_DELETE, HttpUtility.UrlEncode(s3item.key));

                    result.Add(new KeyValuePair<string, object[]>(s3item.key, new object[] { 
                                "",fileName,"",""
                                ,"","","","","","",actionHtml,""}));
                }

                return result;
            }
            else
            {
                List<AWSObjectItem> listResult = new List<AWSObjectItem>();
                List<string> listOption = new List<string>();
                if (HttpContext.Current.Session["hashStaticItems"] != null)
                {
                    hashStaticItems = (Hashtable)HttpContext.Current.Session["hashStaticItems"];
                }

                VitualItemsCount = hashStaticItems.Count;
                foreach (DictionaryEntry item in hashStaticItems)
                {
                    ApplicationDocumentsItem objAppDoc = (ApplicationDocumentsItem)item.Value;

                    // Build Result
                    string optionImage = "";
                    string option = "";

                    if (objAppDoc.ListObjectItem.Count > 1)
                    {
                        optionImage = string.Format(HTML_IMAGE_ICON, PATH_EXPAND_IMG, "title=\"Click to see revision\" class=\"expand-row\" id=\"img" + HttpUtility.UrlEncode(objAppDoc.key) + "\"", "16", "16");
                        option = string.Format(HTML_JS_LINK, "ExpandRevision(this,'" + HttpUtility.UrlEncode(objAppDoc.key.Replace(" ", "|")) + "','" + basePath + "');", optionImage + "<br />");
                    }
                    listResult.Add(objAppDoc.ObjectItem);
                    listOption.Add(option);
                }

                string sortName = (!(String.IsNullOrWhiteSpace(this.SortName)) && (this.SortName != "null")) ? this.SortName : "FileName";
                 //Add condition search when thread load complete by Kantorn J. 2012-08-24
                int skipItems = (Page - 1) * ItemsPerPage;
                if (((sortName == "FileName") || (sortName == "ItemObject.Size") || (sortName == "Metadata.Description")
                    || (sortName == "Metadata.UploadedBy") || (sortName == "Metadata.UploadDate") ) ||
                    (sortName == "FileName") || (sortName == "ItemObject.Size") || (sortName == "Revision"))
                {
                    IQueryable<AWSObjectItem> items = listResult.Select(x => x).AsQueryable<AWSObjectItem>();
                    if (customSorting == GridSortOrder.Asc)
                    {
                        var a = Utility.OrderBy(items, sortName);
                        listResult = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                    }
                    else
                    {
                        var b = Utility.OrderByDescending(items, sortName);
                        listResult = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                    }                  
                }
                else
                {
                    listResult = listResult.Skip(skipItems).Take(this.ItemsPerPage).ToList();
                }

                // Iterate & Build Results..
                int rowIndex = 0;
                foreach (AWSObjectItem s3Item in listResult)
                {
                    string option = listOption[rowIndex];
                    string fileName = s3Item.FileName;

                    // Format File Name fpr FlexiGrid
                    FileUtility.FileType objFileType = FileUtility.GetFileType(fileName);
                    string title = string.Format("title=\"{0}\"", fileName);
                    string sFileImage = string.Format(HTML_IMAGE_ICON, PATH_FILE_TYPE_IMG_ROOT + objFileType.FileIcon, title, "24", "24");
                    fileName = string.Format(HTML_JS_LINK, "OnDownloadClick('" + HttpUtility.UrlEncode(s3Item.Key) + "');", sFileImage + "<br />" + fileName);

                    // Action HTML
                    string actionHtml = string.Format(_userHasAdministrationPrivileges ? HTML_SELECT_ACTIONS : HTML_SELECT_ACTIONS_NO_DELETE, HttpUtility.UrlEncode(s3Item.Key));

                    // META Data
                    
                    string classification = "";
                    string keywords = "";
                    string description = "";
                    string uploadedBy = "";
                    string uploadDate = "";
                    string checkedInOutButton = "";

                    if (s3Item.Metadata != null)
                    {
                        // Classification
                        if (s3Item.Metadata.Classification != null && s3Item.Metadata.Classification.Any())
                        {
                            if (s3Item.Metadata.Classification != null && s3Item.Metadata.Classification.Any())
                            {
                                bool blfirstClassification = true;
                                foreach (var classificationItem in s3Item.Metadata.Classification)
                                {
                                    if (!blfirstClassification) classification += ", ";
                                    classification += classificationItem;
                                    blfirstClassification = false;
                                }
                            }
                        }
                        if (s3Item.Metadata.Keywords != null)
                        {
                            keywords = s3Item.Metadata.Keywords;
                        }
                        if (s3Item.Metadata.Description != null)
                        {
                            description = s3Item.Metadata.Description;
                        }
                        if (s3Item.Metadata.UploadedBy != null)
                        {
                            uploadedBy = s3Item.Metadata.UploadedBy;
                        }
                        if (s3Item.Metadata.UploadDate != null && s3Item.Metadata.UploadDate.Year != 1)
                        {
                            uploadDate = s3Item.Metadata.UploadDate.ToLocalTime().ToString(CultureInfo.InvariantCulture);
                        }
                        if (s3Item.Metadata.IsCheckedOut)
                        {
                            actionHtml = ACTIONS_UNAVAILABLE;

                            if (HttpContext.Current.User.Identity.Name == s3Item.Metadata.CheckedOutBy ||
                                HttpContext.Current.User.IsInRole("Administrator"))
                            {
                                checkedInOutButton = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(s3Item.Key) + "|false" + "', this)",
                                                        string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                        string.Format("title=\"{0}\"", "Checked out by " + s3Item.Metadata.CheckedOutBy + " on " + s3Item.Metadata.CheckOutDate.ToLocalTime()))
                                                        );
                            }
                            else
                            {
                                checkedInOutButton = string.Format(HTML_JS_LINK, "void(0)",
                                                        string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                        string.Format("title=\"{0}\"", "[Check in not allow.]Checked out by " + s3Item.Metadata.CheckedOutBy + " on " + s3Item.Metadata.CheckOutDate.ToLocalTime()))
                                                        );
                            }
                        }
                        else
                        {
                            checkedInOutButton = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(s3Item.Key) + "|true" + "', this)",
                                                        string.Format(HTML_IMAGE, PATH_UNLOCK_IMG, "title=\"Check Out File\"")
                                                        );
                        }
                    }
                    //>

                    //get max revision by Kantorn J. 2012-08-15
                    string revision = "1";
                    if (hashAllRevisonItem.ContainsKey(s3Item))
                    {
                        revision = s3Item.Revision.ToString();
                    }

                    // Build Result
                    result.Add(new KeyValuePair<string, object[]>(s3Item.Key, new object[] 
                            { option,fileName, classification, 
                                keywords, description, s3Item.ItemObject.Size.ToFileSize(),
                                uploadedBy,revision, uploadDate, checkedInOutButton,actionHtml,title
                            }));
                    rowIndex++;
                }
                return result;
            
            }

        }
        //-->
        private static IEnumerable<KeyValuePair<string, object[]>> LoadItemMeta(string key, List<KeyValuePair<string, object[]>> result, 
                                                                                string option, string fileSize, string revision, bool loadRevision = false)
        {
            // Get Object META Data
            using (AmazonDMS.S3Client = Amazon.AWSClientFactory.CreateAmazonS3Client())
            {
                // Get Object META
                AWSObjectMetadata metaDelayLoad = AmazonDMS.GetObjectMetadata(key);
                // Get META
                if (metaDelayLoad != null)
                {

                    // Action HTML
                    string actionHtml = "";


                    //check action of revision or not? Kantorn J. 2012-08-17
                    if (loadRevision)
                    {
                        actionHtml = string.Format(HTML_SELECT_ACTIONS_REVISION, HttpUtility.UrlEncode(key));
                    }
                    else
                    {
                        actionHtml = string.Format(_userHasAdministrationPrivileges ? HTML_SELECT_ACTIONS : HTML_SELECT_ACTIONS_NO_DELETE, HttpUtility.UrlEncode(key));
                    }

                    // Var
                    string classificationDelayLoad = "",
                           fileTypeDelayLoad = "",
                           descriptionDelayLoad = "",
                           keywordsDelayLoad = "",
                           uploadedByDelayLoad = "",
                           checkedInOutButtonDelayLoad = "";

                    string buffer_filename = string.Empty;

                    if (key.Split('/').Length > 1)
                    {
                        buffer_filename = key.Split('/')[key.Split('/').Length - 1];
                    }
                    else
                    {
                        buffer_filename = key;
                    }
                    string title = string.Format("\"{0}\"", buffer_filename);

                    DateTime uploadDateDelayLoad;
                    bool actionsUnavailable = false;

                    // Classification
                    if (metaDelayLoad.Classification != null && metaDelayLoad.Classification.Any())
                    {
                        bool blfirstClassification = true;
                        foreach (var classificationItem in metaDelayLoad.Classification)
                        {
                            if (!blfirstClassification) classificationDelayLoad += ", ";
                            classificationDelayLoad += classificationItem;
                            blfirstClassification = false;
                        }
                    }

                    // File Type
                    if (!string.IsNullOrEmpty(metaDelayLoad.FileType))
                        fileTypeDelayLoad = metaDelayLoad.FileType;

                    // Description
                    if (!string.IsNullOrEmpty(metaDelayLoad.Description))
                        descriptionDelayLoad = metaDelayLoad.Description;

                    // Keywords
                    if (!string.IsNullOrEmpty(metaDelayLoad.Keywords))
                        keywordsDelayLoad = metaDelayLoad.Keywords;

                    // Uploaded By
                    if (!string.IsNullOrEmpty(metaDelayLoad.UploadedBy))
                        uploadedByDelayLoad = metaDelayLoad.UploadedBy;

                    // Upload Date
                    if (!DateTime.TryParse(metaDelayLoad.UploadDate.ToString(CultureInfo.InvariantCulture), out uploadDateDelayLoad)) uploadDateDelayLoad = DateTime.UtcNow;
                    string uploadDateDelayLoadAsString = uploadDateDelayLoad == DateTime.MinValue ? "" : uploadDateDelayLoad.ToString(CultureInfo.InvariantCulture);

                    // Checked In / Out?
                    if (metaDelayLoad.IsCheckedOut)
                    {
                        actionsUnavailable = true;

                        if (HttpContext.Current.User.Identity.Name == metaDelayLoad.CheckedOutBy ||
                            HttpContext.Current.User.IsInRole("Administrator"))
                        {
                            checkedInOutButtonDelayLoad = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(key) + "|false" + "', this)",
                                                    string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                    string.Format("title=\"{0}\"", "Checked out by " + metaDelayLoad.CheckedOutBy + " on " + metaDelayLoad.CheckOutDate.ToLocalTime()))
                                                    );
                        }
                        else
                        {
                            checkedInOutButtonDelayLoad = string.Format(HTML_JS_LINK, "void(0)",
                                                    string.Format(HTML_IMAGE, PATH_LOCK_IMG,
                                                    string.Format("title=\"{0}\"", "[Check in not allow.]Checked out by " + metaDelayLoad.CheckedOutBy + " on " + metaDelayLoad.CheckOutDate.ToLocalTime()))
                                                    );
                        }
                    }
                    else
                    {
                        checkedInOutButtonDelayLoad = string.Format(HTML_JS_LINK, "OnCheckInOutClick('" + HttpUtility.UrlEncode(key) + "|true" + "', this)",
                                                    string.Format(HTML_IMAGE, PATH_UNLOCK_IMG, "title=\"Check Out File\"")
                                                    );
                    }
                    //>

                    ///Set action for revision file
                    string actionRevHtml = HTML_SELECT_ACTIONS_REVISION;
                    // Build & Return Result
                    result.Add(new KeyValuePair<string, object[]>(key, new object[]
                                                                                    {
                                                                                        classificationDelayLoad,
                                                                                        fileTypeDelayLoad,
                                                                                        keywordsDelayLoad,
                                                                                        descriptionDelayLoad,
                                                                                        uploadedByDelayLoad,
                                                                                        uploadDateDelayLoadAsString,
                                                                                        checkedInOutButtonDelayLoad,
                                                                                        actionsUnavailable,
                                                                                        actionHtml,
                                                                                        actionRevHtml,
                                                                                        title,
                                                                                        option,
                                                                                        fileSize,
                                                                                        revision
                                                                                    }));
                }
            }
            return result;
        }

        protected void _OnRunComplete(AWSResultSet aws, string key)
        {
            Hashtable list;
            if (HttpContext.Current.Session["hashStaticItems"] != null)
            {
                list = (Hashtable)HttpContext.Current.Session["hashStaticItems"];
            }
            else
            {
                list = new System.Collections.Hashtable();
            }
            AWSObjectItem item = null;
            Hashtable hashRevisonItem = new Hashtable();
            if (aws.S3Objects.Count > 0)
            {
                List<AWSObjectItem> items = DmsFlexiGridHandler.GetCurrentVersionFile(aws.S3Objects, ref hashRevisonItem);
                if(items.Count() > 0)
                {
                    item = items[0];
                }
            }
            if (list.ContainsKey(key) && (item != null))
            {
                list.Remove(key);
            }
            if (!list.ContainsKey(key))
            {
                if (item != null)
                {
                    list.Add(key, new ApplicationDocumentsItem(key.Split('/')[key.Split('/').Length - 1], key, aws.S3Objects.Count().ToString(),
                                                              item, aws.S3Objects));
                }
                else
                {
                    list.Add(key, new ApplicationDocumentsItem(key.Split('/')[key.Split('/').Length - 1], key, "",
                                                              null, aws.S3Objects));
                }
            }
            HttpContext.Current.Session["hashStaticItems"] = list;
        }
    }

    public class ApplicationDocumentsItem
    {
        private string _fileName;
        public string FileName 
        { 
            get 
            {
                return _fileName;
            } 
            set
            {
                _fileName = value;
            }
        }

        private string _key;
        public string key 
        { 
            get 
            {
                return _key;
            } 
            set
            {
                _key = value;
            }
        }

        private string _Revision;
        public string Revision
        {
            get
            {
                return _Revision;
            }
            set
            {
                _Revision = value;
            }
        }

        private AWSObjectItem _ObjectItem;
        public AWSObjectItem ObjectItem
        {
            get
            {
                return _ObjectItem;
            }
            set
            {
                _ObjectItem = value;
            }
        }

        private List<AWSObjectItem> _ListObjectItem;
        public List<AWSObjectItem> ListObjectItem
        {
            get
            {
                return _ListObjectItem;
            }
            set
            {
                _ListObjectItem = value;
            }
        }


        public ApplicationDocumentsItem( string fileName,
                                        string key,
                                        string Revision,
                                        AWSObjectItem ObjectItem,
                                        List<AWSObjectItem> ListObjectItem)
        {
            _fileName = fileName;
            _key = key;
            _Revision = Revision;
            _ObjectItem = ObjectItem;
            _ListObjectItem = ListObjectItem;
        }
    }
}