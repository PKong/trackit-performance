﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    public class CountryAutoCompleteHandler :IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    String strQuery = context.Request.QueryString["term"];
                    IList<string> result = new List<string>();
                    var objCountries = (from c in ce.sites
                                        where c.county.StartsWith(strQuery)
                                        select c.county
                                  ).Distinct().OrderBy(l => l);
                    foreach (var country in objCountries)
                    {
                        result.Add(country);
                    }
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string names = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(names);
                }
                
            }
            catch
            {
                context.Response.Write("");
            }
        }

        #endregion
    }
}