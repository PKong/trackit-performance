﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// SiteIdAutoCompleteHandler - returns the data for Site ID Auto-complete Text Fields
    /// </summary>
    public class SiteIdAutoCompleteHandler : IHttpHandler
    {

        #region IHttpHandler Members

            /// <summary>
            /// IsReusable
            /// </summary>
            public bool IsReusable
            {
                get { return true; }
            }
            //>

            /// <summary>
            /// ProcessRequest
            /// </summary>
            /// <param name="context"></param>
            public void ProcessRequest(HttpContext context)
            {
                try
                {
                    CPTTEntities ce = new CPTTEntities();
                    var sites = ce.sites.Where("it.site_uid LIKE '" + context.Request.QueryString["term"] + "%'").OrderBy("it.site_uid");

                    // Convert Result
                    IList<string> result = new List<string>();
                    foreach (var s in sites)
                    {
                        result.Add(s.site_uid + " - " + s.site_name);
                    }

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string ids = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(ids);
                }
                catch
                {
                    context.Response.Write("");
                }
            }
            //--

        #endregion
    }
}
