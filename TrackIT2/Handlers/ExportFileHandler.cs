﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using TrackIT2.BLL;
using System.ComponentModel;

namespace TrackIT2.Handlers
{
    public class ExportFileHandler
    {
        #region Attribute
        // Meta Data
        List<ExportClass> loadDataResult;
        private NameValueCollection loadData;
        private NameValueCollection column;
        private NameValueCollection formData;
        ExportFile.eExportType etype;

        // Background worker
        private BackgroundWorker _objWorker = new BackgroundWorker();
        
        //Self event
        public delegate void Load_Data_OnComplete_Handler(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e);
        public event Load_Data_OnComplete_Handler Load_Data_OnComplete;
        #endregion

        #region Construction 
        public ExportFileHandler(TrackIT2.BLL.ExportFile.eExportType type, NameValueCollection loadData, NameValueCollection column, NameValueCollection formData = null)
        {
            this.loadData = loadData;
            this.column = column;
            this.etype = type;

            formData.Remove(DefaultFilterSession.SessionName);
            formData.Remove(DefaultFilterSession.ApplyClick);
            this.formData = formData;

            this._objWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(Load_Data_DoWork);
            this._objWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(Load_Data_RunWorkerCompleted);
            this._objWorker.RunWorkerAsync();
        }

        #endregion

        #region BackgroundWorkerEvet

        private void Load_Data_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Load_Data_Complete(sender, e);
        }

        private void Load_Data_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                ExportFile ef = new ExportFile();
                switch (etype)
                {
                    case ExportFile.eExportType.eLeaseApp:
                        loadDataResult = ef.LeaseAppGetFilteredData(loadData, column, sender, formData);
                        break;
                    case ExportFile.eExportType.eSite:
                        loadDataResult = ef.SiteGetFilteredData(loadData, column, sender, formData);
                        break;
                    case ExportFile.eExportType.eSiteDataPackage:
                        loadDataResult = ef.SiteDataPackageGetFilteredData(loadData, column, sender, formData);
                        break;
                    case ExportFile.eExportType.eTowerMod:
                        loadDataResult = ef.TowerModificationsGetFilteredData(loadData, column, sender, formData);
                        break;
                    case ExportFile.eExportType.eIssueTracker:
                        loadDataResult = ef.IssueTrackerGetFilteredData(loadData, column, sender, formData);
                        break;
                    case ExportFile.eExportType.eLeaseAdmin:
                        loadDataResult = ef.LeaseAdminGetFilteredData(loadData, column, sender, formData);
                        break; 
                    case ExportFile.eExportType.eTowerlist:
                        loadDataResult = ef.TowerListGetFilteredData(loadData, column, sender);
                        break;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region RaiseEvent
        protected virtual void Load_Data_Complete(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Load_Data_OnComplete(loadDataResult, loadData, column, e);
        }

        #endregion
    }
}