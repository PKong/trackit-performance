﻿using System;
using System.Web;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace TrackIT2.Handlers
{
    /// <summary>
    /// UsersAutoCompleteHandler - returns the data for Users Auto-complete Text Fields
    /// </summary>
    public class UsersAutoCompleteHandler : IHttpHandler
    {

        #region IHttpHandler Members

            /// <summary>
            /// IsReusable
            /// </summary>
            public bool IsReusable
            {
                get { return true; }
            }
            //>

            /// <summary>
            /// ProcessRequest
            /// </summary>
            /// <param name="context"></param>
            public void ProcessRequest(HttpContext context)
            {
                try
                {
                    CPTTEntities ce = new CPTTEntities();
                    var users = ce.users.Where("it.tmo_userid LIKE '" + context.Request.QueryString["term"] + "%'").OrderBy("it.tmo_userid");

                    // Convert Result
                    IList<string> result = new List<string>();
                    foreach (var u in users)
                    {
                        result.Add(u.tmo_userid);
                    }

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string tmo_users = javaScriptSerializer.Serialize(result);
                    context.Response.ContentType = "text/html";
                    context.Response.Write(tmo_users);
                }
                catch
                {
                    context.Response.Write("");
                }
            }
            //--

        #endregion
    }
}
