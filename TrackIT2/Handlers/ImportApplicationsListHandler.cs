﻿using System;
using System.Globalization;
using System.Web;
using System.Collections.Generic;
using FlexigridASPNET;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Linq;
using System.Collections.Specialized;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;

namespace TrackIT2.Handlers
{
    public class ImportApplicationsListHandler : BaseFlexiHandler
    {
        public static bool _subFilterHasNoResults = false;
        private const string JS_LINK = "<a href=\"javascript:void(0);\" onclick=\"{0}\" style=\"text-decoration:none;\">{1}</a>";
        public static string VIEW_APPLICATION = "parent.showViewAppDialog('{0}');";
        public static string IMPORT_APPLICATION = "parent.OnImportApplication('{0}','{1}','{2}','{3}','{4}');";
        public static string HIDE_APPLICATION = "OnHiddenApplication('{0}',this);";

        /// <summary>
        /// Gets the filtered data.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<KeyValuePair<string, object[]>> GetFilteredData()
        {
            //
            IQueryable<TrackitImports_Item> iqImportApplication;
            String search = String.Empty;
            List<TrackitImports_Item> lstImportApplication = new List<TrackitImports_Item>();
            int skipItems = (Page - 1) * this.ItemsPerPage;
            string sortName = (!String.IsNullOrWhiteSpace(this.SortName) && (this.SortName != "null" )) ? this.SortName : "CreateDate";

            var result = new List<KeyValuePair<string, object[]>>();
            TrackIT2.TrackitImportApplication.TrackiTImportServiceClient import = new TrackIT2.TrackitImportApplication.TrackiTImportServiceClient();

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["tmoid"]))
            {
                int userId = -1;
                HttpContext currContext = HttpContext.Current;
                if (currContext != null)
                {
                    if (currContext.User.Identity.IsAuthenticated)
                    {
                        if (currContext.Session["userId"] != null)
                        {
                            userId = (int)currContext.Session["userId"];
                        }
                        else
                        {
                            userId = Security.GetUserId(currContext.User.Identity.Name);
                        }
                    }
                }
                //get current user email
                user current_user = User.Search(userId);
                // Hide application?
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["tmoid"]) &&
                    !string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["hidden"]))
                {
                    String TMO_AppID = HttpContext.Current.Request.QueryString["tmoid"];
                    String Hidden_Status = HttpContext.Current.Request.QueryString["hidden"];
                    TrackitImportApplication.eTrackiTImportStatus estatus;
                    if (Hidden_Status == "Hide")
                    {
                        Hidden_Status = "Show";
                        estatus = TrackitImportApplication.eTrackiTImportStatus.hidden;
                    }
                    else
                    {
                        Hidden_Status = "Hide";
                        estatus = TrackitImportApplication.eTrackiTImportStatus.None;
                    }
                    // Check Item Out or In, Load and return META
                    import.UpsertImportApplication(HMACHelper.GenerateHMAC(), null, TMO_AppID, estatus, current_user.email);
                    import.Close();
                    result.Add(new KeyValuePair<string, object[]>(TMO_AppID,new object[] { Hidden_Status }));
                    return result;
                }
            }

            // Search Filter Values
            NameValueCollection form = HttpContext.Current.Request.Form;
            int startIndex = 6;
            StringBuilder sb = new StringBuilder();
            //
            // Get Filters
            if (form.Count > startIndex)
            {
                for (int i = startIndex; i < form.Count; i++)
                {
                    // Check not Global Search param
                    if (form.GetKey(i) != Globals.QS_GLOBAL_SEARCH)
                    {
                        // Build Where clause
                        String itemKey = form.GetKey(i);
                        search = form.Get(i).Trim().ToLower();

                    }
                }
            }
            //>

            // Get Working Set
            String jonString = import.GetImportApplicationList(HMACHelper.GenerateHMAC());
            import.Close();
            if (jonString.Length > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                lstImportApplication = serializer.Deserialize<List<TrackitImports_Item>>(jonString);
            }

            List<string> lstTMOAppId = new List<string>();
            using (CPTTEntities ce = new CPTTEntities())
            {
                lstTMOAppId = (from la in ce.lease_applications
                                       where la.TMO_AppID != null
                                       select la.TMO_AppID).ToList();
            }
            iqImportApplication = lstImportApplication.AsQueryable();

            //filter created application 
            iqImportApplication = iqImportApplication.Where(x => !lstTMOAppId.Contains(x.TMO_AppID));

            //Apply fiter
            if (search != String.Empty)
            {
                iqImportApplication = from x in iqImportApplication
                                      where x.SiteID.ToLower().Contains(search) ||
                                            x.SiteName.ToLower().Contains(search) ||
                                            x.SubmitterTitle.ToLower().Contains(search) ||
                                            x.LegalEntityName.ToLower().Contains(search) ||
                                            x.DBA.ToLower().Contains(search) ||
                                            x.Customer_contact.ToLower().Contains(search) ||
                                            x.Customer_contact_Name.ToLower().Contains(search) ||
                                            x.CarrierDevMgr_contact.ToLower().Contains(search) ||
                                            x.CarrierDevMgr_contact_Name.ToLower().Contains(search) ||
                                            x.Billing_contact.ToLower().Contains(search) ||
                                            x.Billing_contact_Name.ToLower().Contains(search) ||
                                            x.SiteAcquisition_contact.ToLower().Contains(search) ||
                                            x.SiteAcquisition_contact_Name.ToLower().Contains(search)
                                      select x;

            }

            this.VitualItemsCount = iqImportApplication.Count();
            if (this.SortDirection == GridSortOrder.Asc)
            {
                var a = Utility.OrderBy(iqImportApplication, sortName);
                lstImportApplication = a.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }
            else
            {
                var b = Utility.OrderByDescending(iqImportApplication, sortName);
                lstImportApplication = b.Skip(skipItems).Take(this.ItemsPerPage).ToList();
            }


            // Convert Result
            foreach (var item in lstImportApplication)
            {
                String TMO_AppID = item.TMO_AppID;
                String CreateDate = Utility.DateToString(item.CreateDate);
                String StartDate = Utility.DateToString(item.StartDate);
                String SiteID = item.SiteID;
                String ApplicationType = item.s_ApplicationType;

                String apptype = String.Empty;
                String ViewApp =  ConfigurationManager.AppSettings["TMobileURL"] + "public/{0}_ViewApp.aspx?tmoid={1}";
                if (item.s_ApplicationType == "INIT")
                {
                    apptype = "Initial";
                    ViewApp = String.Format(ViewApp, "InitApp", HttpUtility.UrlEncode(item.TMO_AppID.Trim()));
                }
                else
                {
                    apptype = "Mod";
                    ViewApp = String.Format(ViewApp, "ModApp", HttpUtility.UrlEncode(item.TMO_AppID.Trim()));
                }
                String View = String.Format(JS_LINK, String.Format(VIEW_APPLICATION, ViewApp),"View");
                //OnImportApplication (TMO_AppID,site,apptype,customer,dba);
                String Import = String.Format(JS_LINK, String.Format(IMPORT_APPLICATION, item.TMO_AppID.Trim(),
                                                                                         item.SiteID,
                                                                                         apptype,
                                                                                         System.Web.HttpUtility.UrlEncode(item.LegalEntityName),
                                                                                         System.Web.HttpUtility.UrlEncode(item.DBA)), "Import");

                String HideText = String.Empty;
                if (HttpContext.Current.User.IsInRole("Administrator"))
                {
                    if (item.s_status == "hidden")
                    {
                        HideText = "Show";
                    }
                    else
                    {
                        HideText = "Hide";
                    }
                    HideText = String.Format(JS_LINK, String.Format(HIDE_APPLICATION, item.TMO_AppID.Trim()), HideText);
                }
                result.Add(new KeyValuePair<string, object[]>(item.TMO_AppID,
                                new object[] { CreateDate, 
                                            System.Web.HttpUtility.HtmlEncode(item.DBA), 
                                            SiteID,
                                            ApplicationType, 
                                            View,
                                            Import,
                                            HideText}));
            }

            return result;

        }

        /// <summary>
        /// Build Search Filter Where Clause
        /// </summary>
        /// <returns></returns>
        public static void buildSearchFilterWhereClause(StringBuilder sb, String itemKey, String itemValue)
        {
            if (sb.Length > 0)
            {
                sb.Append(" AND ");
            }

            var sbType = new StringBuilder();
            switch (itemKey)
            {
                // ID
                case SearchFilterControls.ID.Name:
                    sb.AppendFormat(SearchFilterControls.ID.Criterea, itemValue);
                    break;
            }
        }
        //-->

        /// <summary>
        /// Search Filter Controls
        /// </summary>
        public struct SearchFilterControls
        {
            /// <summary>
            /// Lease Admin ID
            /// </summary>
            public struct ID
            {
                public const String Name = "ID";
                public const String Criterea = "(it.id = {0})";
            }
        }
        //-->

    }

   

    public class TrackitImports_Item
    { 
    
    public String TMO_AppID {get; set;}
    public String status { get; set; }
    public String s_status { get; set; }
    public String created_by {get; set;}
    public DateTime? created_at{get; set;}
    public DateTime? updated_at{get; set;}

    //Application data
    public DateTime? CreateDate {get; set;}
    public DateTime? StartDate {get; set;}
    public String SiteID { get; set; }
    public String SiteName { get; set; }
    public String ApplicationType { get; set; }
    public String s_ApplicationType { get; set; }

    //Filter Properties
    public String SubmitterTitle { get; set; }
    public String LegalEntityName { get; set; }
    public String DBA { get; set; }
    public String Customer_contact { get; set; }
    public String Customer_contact_Name { get; set; }
    public String CarrierDevMgr_contact { get; set; }
    public String CarrierDevMgr_contact_Name { get; set; }
    public String Billing_contact { get; set; }
    public String Billing_contact_Name { get; set; }
    public String SiteAcquisition_contact { get; set; }
    public String SiteAcquisition_contact_Name { get; set; }

    //API Properties
    public bool KeyHasValue { get; set; }
    public string Key { get; set; }
    public bool IsDirty { get; set; }
    public DBMaster DbMaster { get; set; }

    }

    public class DBMaster
    {
        public string KeyName { get; set; }
    }
}