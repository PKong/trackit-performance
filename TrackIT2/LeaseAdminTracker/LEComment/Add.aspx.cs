﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;

namespace TrackIT2.LeaseAdminTracker.LEComment
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int iLeaseAdminID;
            if (!Page.IsPostBack && Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iLeaseAdminID))
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    lease_admin_comments comment = new lease_admin_comments();
                    comment.lease_admin_record_id = iLeaseAdminID;
                    string comments = this.Context.Request.Form["comment"];
                    comment.comments = Utility.PrepareString(comments);
                    ce.lease_admin_comments.AddObject(comment);
                    ce.SaveChanges();
                    this.Context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(BindComment(iLeaseAdminID, ce)));
                }
            }
        }
        //Display comment bottom of pages
        private  List<LeaseAdminCommentData> BindComment(int iLeaseAdminID, CPTTEntities ceRef)
        {
            List<lease_admin_comments> lstComment = LeaseAdminComments.GetComments(iLeaseAdminID, ceRef).OrderBy(x => x.created_at).ToList<lease_admin_comments>();
            List<LeaseAdminCommentData> result = new List<LeaseAdminCommentData>();
            foreach (lease_admin_comments comm in lstComment)
            {
                LeaseAdminCommentData data = new LeaseAdminCommentData();
                data.comment_id = comm.id;
                data.comment = comm.comments;
                data.created_at = Utility.DateToString(comm.created_at);
                data.update_at = Utility.DateToString(comm.updated_at);
                data.creator_id = comm.creator_id;
                data.updater = comm.creator_user.last_name + " " + comm.creator_user.first_name;
                result.Add(data);
            }
            return result;
        }
    }
}