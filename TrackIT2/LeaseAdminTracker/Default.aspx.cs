﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.IO;
using TrackIT2.Handlers;
using System.ComponentModel;

namespace TrackIT2.LeaseAdminTracker
{
    /// <summary>
    /// Default - Class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
                PageUtility.SetupSearchResultFilters(this, panelFilterBox);
                this.SetupFlexiGrid();

                // Clear Session
                DefaultFilterSession.ClearSession(HttpContext.Current, DefaultFilterSession.FilterType.LeaseAdminFilter);
            }
        }
        //-->
        
        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        private void BindLookupLists()
        {
            CPTTEntities ce = new CPTTEntities();
            // Customer
            var customers = ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order"));
            Utility.BindDDLDataSource(this.lsbCustomer, customers, "id", "customer_name", null);

            var documentType = ce.lease_admin_document_types;
            var sortDocumentType = documentType.ToList();
            var dynamicComparerDocumentType = new DynamicComparer<lease_admin_document_types>();
            dynamicComparerDocumentType.SortOrder(x => x.document_type);
            sortDocumentType.Sort(dynamicComparerDocumentType);
            Utility.BindDDLDataSource(this.lsbDocumentType__Exec__, sortDocumentType, "id", "document_type", null);
            Utility.BindDDLDataSource(this.lsbDocumentType__Comm__, sortDocumentType, "id", "document_type", null);

            // Advance Search 
            BindAdvanceSearch();
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        private void SetupFlexiGrid()
        {
            fgLeaseAdmin.SortColumn = fgLeaseAdmin.Columns.Find(col => col.Code == "CustomerName");
            fgLeaseAdmin.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgLeaseAdmin.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgLeaseAdmin.NoItemMessage = "No Data";
        }

        // Export CSV File
        protected void ExportClick(object s, EventArgs e)
        {
            try
            {
                ExportFileHandler extHand;
                ExportFile ef = new ExportFile();
                var searchData = hiddenSearchValue.Value;
                NameValueCollection data = new NameValueCollection();

                //deserialrize filter data for export
                if (!string.IsNullOrEmpty(searchData))
                {
                    var jss = new JavaScriptSerializer();
                    var table = jss.Deserialize<dynamic>(searchData);

                    for (int x = 0; x < table.Length; x++)
                    {
                        data.Add(table[x]["name"], table[x]["value"]);
                    }
                }

                const int startIndex = 4;
                var parameters = new NameValueCollection();

                // If we have form values, extract them to build the object query.
                if (data.Count > startIndex)
                {
                   for (var i = startIndex; i < data.Count; i++)
                   {
                      var itemKey = data.GetKey(i);
                      var itemValue = data.Get(i);

                      parameters.Add(itemKey, itemValue);
                   }
                }

                //Use Thread for export a CSV file
                NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value);
                extHand = new ExportFileHandler(ExportFile.eExportType.eLeaseAdmin, data, columnValue, parameters);
                extHand.Load_Data_OnComplete += Load_Data_OnComplete;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }

        }
        //-->

        /// <summary>
        /// Event on Thread Export complete
        /// </summary>
        /// <param name="loadDataResult"></param>
        /// <param name="data"></param>
        /// <param name="column"></param>
        /// <param name="e"></param>
        private void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            try
            {
                //check thread error on export
                if (e.Error == null)
                {
                    ExportFile ef = new ExportFile();
                    MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eLeaseAdmin, Server.MapPath("/"));
                    if (mem != null)
                    {
                        string fileName = ExportFile.GenarateFileName(data["fileName"]);
                        DownloadFile(mem, fileName);
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Export report error.");
                    }
                }
                else
                {
                    //Write elamh log when thread export not complete
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e.Error);
                    Master.StatusBox.showStatusMessage("Export report error.");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
        //-->

        /// <summary>
        /// Download File
        /// </summary>
        /// <param name="mstream"></param>
        /// <param name="reportName"></param>
        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {
                        if (c == "fileDownloadToken")
                        {
                            var httpCookie = Response.Cookies["fileDownloadToken"];
                            if (httpCookie != null)
                                httpCookie.Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString(CultureInfo.InvariantCulture));
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
        //-->

        /// <summary>
        /// Bind Advance Search
        /// </summary>
        private void BindAdvanceSearch()
        {
            ddlCriteria.Items.Clear();
            AdvanceSearch.LeaseAdminFilter advSearch = new AdvanceSearch.LeaseAdminFilter();
            List<ListItem> criteria = new List<ListItem>();
            foreach (AdvanceSearch.CritereaInfo item in advSearch.LstCriterea)
            {
                criteria.Add(new ListItem(item.NameInDDL, item.GetDDLValue()));
            }
            var sortCriteria = criteria;
            sortCriteria.Sort((obj1, obj2) => new AlphanumComparatorFast().Compare(obj1.Text, obj2.Text));
            ddlCriteria.Items.AddRange(sortCriteria.Cast<ListItem>().ToArray());
        }
        //-->
    }
}