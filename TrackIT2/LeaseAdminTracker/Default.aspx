﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.LeaseAdminTracker.Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_admin") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_admin") %>   

    <script type="text/javascript" language="javascript">

        searchFilterFields = "txtSiteID,lsbCustomer,lsbLeasePhase,txtExecutionDateFrom,txtExecutionDateTo,txtCommencementDateFrom,txtCommencementDateTo,lsbDocumentType__Exec__,lsbDocumentType__Comm__,lsbRevenueShare";
        currentFlexiGridID = '<%= fgLeaseAdmin.ClientID %>';

        // ADV Search Var
        var GETdatas = new Array();
        var blnHaveGET = false;
        var blnExec = false;

        // Doc Ready
        $(function () {

            var sTitlePrefix = "";
            // Get query string.(query string, out arrayGET data)
            blnHaveGET = GrabGETData(window.location.search.substring(1), GETdatas);

            // Set the modal size
            setModalSize(700, 670, 400);

            // Setup List Create Modal
            setupListLeaseAdminCreateModal("Create New Lease Admin");

            // Get query string.(query string, out arrayGET data)
            blnHaveGET = GrabGETData(window.location.search.substring(1), GETdatas);

            // Load Advance Search
            if (typeof $("#ddlCriteria") != "undefined") {
                ADVSearchInit($("#ddlCriteria"), "/LeaseAdminTracker");
            }

            if (typeof GETdatas['filter'] == "string") {
                blnExec = LoadAdvanceSearch(GETdatas['filter'], searchFilterFields);
                blnExec = blnExec | CheckGSFilter(GETdatas['filter']);
            }
            else {
                //Added 2012/02/28 for checking global search
                if (typeof GETdatas['gs'] == "string") {
                    var txtGS = GETdatas['gs'];
                    if (txtGS != undefined && txtGS != null) {
                        setCurrentGS(txtGS);
                        blnExec = true;
                    }
                }
            }

            // Setup Search Filter
            setupSearchFilter();

            // Setup Saved Filter Modal 
            setupSavedFilterModal(SavedFilterConstants.buttonCreateNew, SavedFilterConstants.listViews.LeaseAdminTracker.name, SavedFilterConstants.listViews.LeaseAdminTracker.url);
        });
        //-->

        // Window Load
        $(window).load(function () {

            // Apply Search Filter?
            if (blnExec) {

                // Wait For Last Field To Load
                var waitForLastFieldToLoadLimit = 0;
                var waitForLastFieldToLoad = setInterval(function () {
                    if (($('#hiddenLastFieldToCheckLoaded').val().length === 0) || (waitForLastFieldToLoadLimit === 50)) {
                        clearInterval(waitForLastFieldToLoad);

                        // Apply filters and reload the DataGrid
                        applySearchFilter(true);
                    }
                    waitForLastFieldToLoadLimit++;
                }, 100);
                //>

            } else {
                // Normal DataGrid Load
                reloadDataGrid();
            }
        });
        //-->
    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 class="h1" style="float:left; padding-top:3px; padding-right:30px;">Lease Admin Tracker</h1>
	        <div class="page-options-nav">
                <a class="fancy-button clear-filter-button" href="javascript:void(0)">Clear Filter</a> 
                <a class="fancy-button filter-button arrow-down" href="javascript:void(0)">Filter Results<span class="arrow-down-icon"></span></a> 
                <a id="modalBtnExternalSaveFilter" class="fancy-button save-filter-button" href="javascript:void(0)">Save Filter</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
		        <a id="modalBtnExternal" class="fancy-button" href="javascript:void(0)">Create New Lease Admin</a>
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->

    <!-- Search Filter Box -->
    <div class="filter-box">
	    <div style="padding:20px">
            <div class="filter-button-close"><a class="fancy-button close-filter" style="padding:2px 6px 2px 6px;" href="javascript:void(0)">x</a></div>
              
                <asp:Panel ID="panelFilterBox" runat="server">
                    <div class="left-col">
                            <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                                LabelFieldClientID="lblSiteID"
                                LabelFieldValue="Site ID"
                                LabelFieldCssClass="label"
                                TextFieldClientID="txtSiteID"
                                TextFieldCssClass="input-autocomplete"
                                TextFieldWidth="188"
                                ScriptKeyName="ScriptAutoCompleteSiteID"
                                DataSourceUrl="SiteIdAutoComplete.axd"
                                ClientIDMode="Static"
                                runat="server" />
		                    <br />

                            <label style="display: inline-block; vertical-align:top;">Customer</label>
                            <div id="divCustomer" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                                <asp:ListBox ID="lsbCustomer" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                            </div>
                            <br />

                            <label>Lease Phase</label>
                                <div id="divLeasePhase" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                                    <asp:ListBox ID="lsbLeasePhase" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" >
                                        <asp:ListItem Text="Execution" Value="Execution"></asp:ListItem>
                                        <asp:ListItem Text="Commencement" Value="Commencement"></asp:ListItem>
                                    </asp:ListBox> 
                                </div>
                            <br />
                            
                            <label>Revenue Share</label>
                            <div id="RevenueShared" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                                <asp:ListBox ID="lsbRevenueShare" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" >
                                    <asp:ListItem Text="yes" Value="true"></asp:ListItem>
                                    <asp:ListItem Text="no" Value="false"></asp:ListItem>
                                    <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                                </asp:ListBox> 
                            </div>
		                    <br />
                    </div>
                    <div class="right-col" >
                        <label>Execution Date</label>
                            <ul class="field-list">
                                <li>
                                    <asp:TextBox ID="txtExecutionDateFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                                    <asp:CompareValidator ID="valExecutionDateFrom" runat="server"
                                                        ControlToValidate="txtExecutionDateFrom"
                                                        ClientIDMode="Static" 
                                                        CssClass="inline-error"
                                                        Display="Dynamic"
                                                        Type="Date"
                                                        Operator="DataTypeCheck"
                                                        ErrorMessage="*">
                                    </asp:CompareValidator>
                                </li>
	                            <li>&nbsp;to&nbsp;</li>
	                            <li>
                                    <asp:TextBox ID="txtExecutionDateTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                                    <asp:CompareValidator ID="valExecutionDateTo" runat="server"
                                                        ControlToValidate="txtExecutionDateTo" 
                                                        ClientIDMode="Static"
                                                        CssClass="inline-error"
                                                        Display="Dynamic"
                                                        Type="Date"
                                                        Operator="DataTypeCheck"
                                                        ErrorMessage="*">
                                    </asp:CompareValidator>
                                </li>
                            </ul>                                            
                            <br />                            
                        
                            <label>Document Type (Exec)</label>
                            <div id="div1" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                                <asp:ListBox ID="lsbDocumentType__Exec__" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                            </div>
		                    <br />

                            <label>Commencement Date</label>
                            <ul class="field-list">
                                <li>
                                    <asp:TextBox ID="txtCommencementDateFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                                    <asp:CompareValidator ID="valCommencementFrom" runat="server"
                                                        ControlToValidate="txtCommencementDateFrom"
                                                        ClientIDMode="Static" 
                                                        CssClass="inline-error"
                                                        Display="Dynamic"
                                                        Type="Date"
                                                        Operator="DataTypeCheck"
                                                        ErrorMessage="*">
                                    </asp:CompareValidator>
                                </li>
		                            <li>&nbsp;to&nbsp;</li>
		                            <li>
                                    <asp:TextBox ID="txtCommencementDateTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                                    <asp:CompareValidator ID="valCommencementTo" runat="server"
                                                        ControlToValidate="txtCommencementDateTo"
                                                        ClientIDMode="Static" 
                                                        CssClass="inline-error"
                                                        Display="Dynamic"
                                                        Type="Date"
                                                        Operator="DataTypeCheck"
                                                        ErrorMessage="*">
                                    </asp:CompareValidator>
                                </li>
                            </ul>
                        
                            <label>Document Type (Comm)</label>
                            <div id="divProjectManager" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                                <asp:ListBox ID="lsbDocumentType__Comm__" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                            </div>
		                    <br />
                    </div> 
                    <div class="cb"></div>
                     <!--Advance search -->
                    <div id="pnlAdvanceCriteria"></div>

                    <div class="cb"></div>
                    <asp:Label ID="lblAdvanceCriteria" CssClass="critesria-title" runat="server" >Add More Filter Criteria >></asp:Label>
                    <asp:DropDownList ID="ddlCriteria" CssClass="criteria-select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    <button id="blnAddAdvanceCriteria" class="fancy-button" type="button" onclick="OnAddingCriteria(null,null,null);" runat="server" >Add Criteria</button>
                    <div class="cb"></div>
		
                    <div style="padding:20px 0 15px 0; overflow:visible;">
                        <div style="width:auto;float:left;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                        <div style="width:auto;float:right;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                    </div>
                </asp:Panel>
	    </div>
    </div>
    <!-- /Search Filter Box -->

    <div class="cb"></div>

    <!-- Filter Tags -->
    <div id="filterTagsContainer" class="filter-tags-container not-displayed"  runat="server"></div>
    <!-- /Filter Tags -->

    <div class="cb"></div>

    <!-- Lease Admin Grid -->
    <div style="overflow:visible; margin-bottom: 20px; min-height:580px; position:relative;">

        <fx:Flexigrid ID="fgLeaseAdmin"
            AutoLoadData="False"
            Width="990"
            ResultsPerPage="20"
			ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			HandlerUrl="~/LeaseAdminTrackerList.axd"
            runat="server">
		    <Columns>
                <fx:FlexiColumn Code="CustomerName" Text="Customer" Width="108" OnRowRender="fgRowRender" />
			    <fx:FlexiColumn Code="SiteId" Text="Site ID" Width="75" />
                <fx:FlexiColumn Code="SiteName" Text="Site Name" Width="150" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="Phase" Text="Lease Phase" Width="108" OnRowRender ="fgRowRender" />
                <fx:FlexiColumn Code="LeaseExecutionDate" Text="Execution Date" Width="125" />                                
                <fx:FlexiColumn Code="CommencementDate" Text="Commencement Date" Width="125" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="RevShare" Text="Rev Share" Width="80" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="DocumentType" Text="Document Type (Exec)" Width="135" />
                <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                Format="<a class='fancy-button' href='/LeaseAdminTracker/Edit.aspx?id={0}'>Edit</a>" />
		    </Columns>
	    </fx:Flexigrid>
        
        <br />
        <div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgLeaseAdmin');" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenLastFieldToCheckLoaded" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilterParam" runat="server" ClientIDMode="Static" Value="" />
        <div class="cb"></div>

    </div>
    <!-- /Lease Admin Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->
</asp:Content>
