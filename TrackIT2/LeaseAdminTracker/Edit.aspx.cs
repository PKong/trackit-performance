﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Text;

namespace TrackIT2.LeaseAdminTracker
{
    public partial class Edit : System.Web.UI.Page
    {
        #region Attribute
        private const string TOTAL_SITE_FORMAT = "{0} Related Sites";
        private const string CUSTOMER_FORMAT = "Customer: {0}"; 
        #endregion 
        
        #region View State
        public lease_admin_records CurrentLeaseAdmin
        {
            get
            {
                return (lease_admin_records)this.ViewState["CurrentLeaseAdmin"];
            }
            set
            {
                this.ViewState["CurrentLeaseAdmin"] = value;
            }
        }
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Security.UserIsInRole("Administrator"))
            {
                btnLeaseAdminDelete.Visible = false;
            }
            if (!Page.IsPostBack)
            {
                pnlNoID.Visible = false;
                Utility.ClearSubmitValidator();
                int iLeaseAdminID;
                bool isValidId = Int32.TryParse(Request.QueryString["Id"], out iLeaseAdminID);
                divNoRelatedLeaseApps.Visible = true;
                if (isValidId)
                {
                    hidden_lease_admin_id.Value = iLeaseAdminID.ToString();
                    if (BLL.LeaseAdminRecord.Search(iLeaseAdminID) != null)
                    {
                        pnlNoID.Visible = false;
                        pnlSectionContent.Visible = true;
                        Page.Title = "Edit Lease Admin - " + iLeaseAdminID.ToString() + " | TrackiT2";
                        BindLookupLists();
                        BindApplication(iLeaseAdminID);
                        GetLinkDocument();
                    }
                    else
                    {
                        // Page render event will write the 404 response code. This part
                        // of the code will display the No Id panel and hide the edit
                        // panel.
                        pnlSectionContent.Visible = false;
                        pnlNoID.Visible = true;
                    }
                }
                else
                {
                    // Page render event will write the 404 response code. This part
                    // of the code will display the No Id panel and hide the edit
                    // panel.
                    pnlSectionContent.Visible = false;
                    pnlNoID.Visible = true;
                }

                GetSessionFilter();
            }
            else
            {
                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    if (!string.IsNullOrEmpty(keyName))
                    {
                        try
                        {
                            AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                        }
                        catch (Amazon.S3.AmazonS3Exception ex)
                        {
                            Master.StatusBox.showErrorMessage(ex.Message);
                        }
                    }
                }
            }
        }

        protected void btnSubmitLeaseAdmin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveLeaseAdmin();
            }
        }

        protected void btnSubmitDocStatus_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveLeaseAdmin();
            }
        }
        
        protected void btnLeaseAdminDelete_Click(object sender, EventArgs e)
        {
            int iLeaseAdminID;
            bool isValidId = false;
            divNoRelatedLeaseApps.Visible = true;
            if (hidden_lease_admin_id.Value != String.Empty)
            {
                isValidId = Int32.TryParse(hidden_lease_admin_id.Value, out iLeaseAdminID);
            }
            else
            {
                isValidId = Int32.TryParse(Request.QueryString["Id"],out iLeaseAdminID);
            }
            if (isValidId)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    string result = LeaseAdminRecord.Delete(iLeaseAdminID);
                    if (String.IsNullOrEmpty(result))
                    {
                        Master.StatusBox.showStatusMessage("Lease Admin Tracker Removed.");
                        ClientScript.RegisterStartupScript(GetType(), "LoadAndRedirect",
                            "<script type='text/javascript'>" +
                                                     "   window.parent.location.href = '../LeaseAdminTracker';" +
                                                     "</script>");
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted."); 
                    }
                }
            }
            else
            {
                Master.StatusBox.showStatusMessage("Error: some related records cannot be deleted; operation aborted.");
            }
        }

        
        #endregion

        #region Function 

        //Binding listbox
        private void BindLookupLists()
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                var context_lease_admin_document_types = ce.lease_admin_document_types.ToList();
                var tmp_context_lease_admin_document_types = new DynamicComparer<lease_admin_document_types>();
                tmp_context_lease_admin_document_types.SortOrder(x => x.document_type);
                context_lease_admin_document_types.Sort(tmp_context_lease_admin_document_types);
                Utility.BindDDLDataSource(this.ddlExecDocs_DocType, context_lease_admin_document_types, "id", "document_type", "- Select -");
                Utility.BindDDLDataSource(this.ddlComDocs_DocType, context_lease_admin_document_types, "id", "document_type", "- Select -");

                var context_date_statuses = ce.date_statuses.ToList();
                var tmp_context_date_statuses = new DynamicComparer<date_statuses>();
                tmp_context_date_statuses.SortOrder(x => x.date_status);
                context_date_statuses.Sort(tmp_context_date_statuses);
                Utility.BindDDLDataSource(this.ddlExecDocs_SenttoCustStatus, context_date_statuses, "id", "date_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlExecDocs_BackFromCustDate_ColLeft1, context_date_statuses, "id", "date_status", "- Select -");
                Utility.BindDDLDataSource(this.ddlExecDocs_LeaseExecDate_ColLeft1, context_date_statuses, "id", "date_status", "- Select -");
                
                List<UserInfo> employees = BLL.User.GetUserInfo();
                var sort_employees = employees.ToList();
                var DynamicComparer_employees = new DynamicComparer<UserInfo>();
                DynamicComparer_employees.SortOrder(x => x.FullName);
                sort_employees.Sort(DynamicComparer_employees);
                Utility.BindDDLDataSource(this.ddlComDocs_RecvBy, sort_employees, "ID", "FullName", "- Select -");

                Utility.BindDDLDataSource(this.ddlLOEHoldupReasonTypes_AmendmentExecution, context_date_statuses, "id", "date_status", "- Select -");
            }
        }

        //Display information site or sites list
        private void VisibleSiteListInformation(int SiteCount = 0)
        {
            lblSiteID.Visible = SiteCount <= 1;
            lblSiteName.Visible = SiteCount <= 1;
            lblCustomerName.Visible = SiteCount <= 1;

            if (SiteCount > 1)
            {
                tab_site_data.Style.Add("display", "none");
                div_site_list.Style.Add("display", "block");
                div_site.Style.Add("display", "none");
                spn_total_site.InnerText = String.Format(TOTAL_SITE_FORMAT, SiteCount.ToString());
                spn_total_site.Style.Add("width", "190px");
            }
            else
            {
                if (SiteCount == 0)
                {
                    spn_total_site.InnerText = "Lease Admin Tracker";
                    lblSiteLatLong.Text = "";
                }
                tab_site_data.Style.Add("display", "block");
                div_site_list.Style.Add("display", "none");
                div_site.Style.Add("display", "block");
            }
        }

        //Set Site information 
        private void BindApplication(int iLeaseAdminID)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                lease_admin_records objLeaseAdmin = LeaseAdminRecord.Search(iLeaseAdminID, ce) ;
                this.CurrentLeaseAdmin = objLeaseAdmin;
                List<site> objLeaseAdminSite = new List<site>();
                if (objLeaseAdmin.sites != null)
                {
                    objLeaseAdminSite = objLeaseAdmin.sites.ToList();
                }

                if (!objLeaseAdmin.lease_application_id.HasValue)
                {
                    if (objLeaseAdmin.customer != null)
                    {
                        this.lblCustomerLabel.Text = String.Format(CUSTOMER_FORMAT, "");
                        this.lblCustomer2.Text = objLeaseAdmin.customer.customer_name;
                    }
                }

                VisibleSiteListInformation(objLeaseAdminSite.Count());
                if (objLeaseAdminSite.Count() > 0)
                {
                    //Display sites list on Flexi Grid
                    if(objLeaseAdminSite.Count() > 1 )
                    {
                        var rowPerPageOptions = new[] { 10, 20, 30, 40, 50 };
                        const string NO_ITEM_MESSAGE = "No Data";
                        fgSites.SortColumn = fgSites.Columns.Find(col => col.Code == "site_uid");
                        fgSites.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
                        fgSites.RowPerPageOptions = rowPerPageOptions;
                        fgSites.NoItemMessage = NO_ITEM_MESSAGE;
                        if(objLeaseAdmin.customer != null)
                        {
                            lblCustomer.Text = String.Format(CUSTOMER_FORMAT,objLeaseAdmin.customer.customer_name);
                        }
                    }
                    //Display a site information 
                    else
                    {
                        site objSite = objLeaseAdminSite[0];

                        BindTowerImages(objSite.site_uid);
                        Utility.ControlValueSetter(lblSiteID, objSite.site_uid, (0.0).ToString());
                        if (!string.IsNullOrEmpty(objSite.site_uid))
                        {
                            lblSiteID.NavigateUrl= Globals.SITE_LINK_URL + objSite.site_uid;
                        }

                        lblSiteName.Text = Utility.PrepareString("/ " + objSite.site_name);
                        Utility.ControlValueSetter(lblSiteLatitude, objSite.site_latitude, (0.0).ToString());
                        Utility.ControlValueSetter(lblDegreeLatitude, Utility.ConvertLatLongCoorToDegree(objSite.site_latitude), (0.0).ToString());
                        Utility.ControlValueSetter(lblSiteLongitude, objSite.site_longitude, (0.0).ToString());
                        Utility.ControlValueSetter(lblDegreeLongtitude, Utility.ConvertLatLongCoorToDegree(objSite.site_longitude), (0.0).ToString());

                        // Add Google Maps link to Latitude and Logitude
                        lblSiteLatLong.NavigateUrl = Utility.GetGooleMapLatLong(lblSiteLatitude.Text, lblSiteLongitude.Text).ToString();
                        Utility.ControlValueSetter(lblType, objSite.site_class_desc);
                        Utility.ControlValueSetter(lblHeight, objSite.structure_ht, (0).ToString());
                        Utility.ControlValueSetter(lblRogueEquip, objSite.rogue_equipment_yn, "None");
                        Utility.ControlValueSetter(lblStatus, objSite.site_status_desc);
                        if (BLL.Site.IsNotOnAir(objSite.site_on_air_date, objSite.site_status_desc))
                        {
                            imgStructureDetails_Construction.ImageUrl = "/images/icons/Construction.png";
                            imgStructureDetails_Construction.Visible = true;
                        }

                        if (objSite.address != null && objSite.city != null && objSite.state != null && objSite.zip != null && objSite.county != null)
                        {
                            //Set Address label Link 0 = address,1 = city,2 = state,3 = zip,4 = country
                            string sAddressFormat = "{0}<br />{1}, {2}, {3}<br />{4}";
                            mybtn.Text = string.Format(sAddressFormat, Utility.PrepareString(objSite.address.ToString())
                                , Utility.PrepareString(objSite.city)
                                , Utility.PrepareString(objSite.state)
                                , Utility.PrepareString(objSite.zip)
                                , Utility.PrepareString(objSite.county));

                            // Set Google Maps Link
                            StringBuilder sbGoogleSearch = new StringBuilder();
                            sbGoogleSearch.Append(objSite.address.ToString());
                            if (!String.IsNullOrWhiteSpace(objSite.city)) sbGoogleSearch.Append(", " + objSite.city);
                            if (!String.IsNullOrWhiteSpace(objSite.state)) sbGoogleSearch.Append(", " + objSite.state);
                            if (!String.IsNullOrWhiteSpace(objSite.zip)) sbGoogleSearch.Append(", " + objSite.zip);
                            if (!String.IsNullOrWhiteSpace(objSite.county)) sbGoogleSearch.Append(", " + objSite.county);
                            mybtn.NavigateUrl = Utility.GetGoogleMapLink(sbGoogleSearch.ToString());
                        }
                        else
                        {
                            lblAddressNA.Text = "Site Address is not Available.";
                            lblAddressNA.Visible = true;
                            mybtn.Visible = false;
                        }
                    }
                }
                else
                {
                    BindTowerImages(String.Empty);
                }
                customer objCus = null;
                if (objLeaseAdmin.lease_applications != null)
                {
                    objCus = Customer.SearchWithinTransaction(objLeaseAdmin.lease_applications.customer_id, ce);
                    if (objCus != null)
                    {
                        Utility.ControlValueSetter(lblCustomerName, " / " + objCus.customer_name, " / (no customer found)");
                    }
                    else
                    {
                        Utility.ControlValueSetter(lblCustomerName, " / (no customer found)");
                    }
                }
                else
                {
                    if (objLeaseAdmin.customer != null)
                    {
                        Utility.ControlValueSetter(lblCustomerName, " / " + objLeaseAdmin.customer.customer_name);
                    }
                    else
                    {
                        Utility.ControlValueSetter(lblCustomerName, " / (no customer found)");
                    }
                }

                divNoRelatedLeaseApps.Visible = true;
                if (objLeaseAdmin.lease_application_id.HasValue)
                {
                    //Prepare data to bind with grid.
                    List<SDPRelatedLeaseApplication> lstResult = BLL.SiteDataPackage.GetRelatedLeaseApplication(objLeaseAdmin.lease_application_id.Value,null);
                    if (lstResult != null && lstResult.Count > 0)
                    {
                        //Binding data to grid.
                        grdMain.DataSource = lstResult;
                        grdMain.DataBind();
                        divNoRelatedLeaseApps.Visible = false;
                    }
                }
                BindLeaseAdminApplication(objLeaseAdmin, ce);
                BindComment(iLeaseAdminID,ce);
            }
        }

        private void BindLeaseAdminApplication(lease_admin_records objLeaseAdmin,CPTTEntities ce )
        {
            //Check associate with Leaseapp
            bool bIsAssociate = false;
            if (objLeaseAdmin.lease_application_id != null)
            {
                bIsAssociate = true;
            }

            //New controls Shared
            if (bIsAssociate)
            {
                hidden_leaseapp_id.Value = objLeaseAdmin.lease_application_id.ToString();
                lease_applications objLeaseApp = objLeaseAdmin.lease_applications;
                ProjectCategory category = LeaseApplication.GetProjectCategory(objLeaseApp);
                List<metadata> viewFields = MetaData.GetViewableFieldsByCategory(category);

                if (objLeaseApp == null)
                {
                    objLeaseApp = ce.lease_applications.Where(x => x.id == objLeaseAdmin.lease_application_id).ToList()[0];
                }

                //Executables Sent To Customer
                #region Executables Sent To Customer
                if (Security.IsDisplayGroupVisible("Executables Sent to Customer", category))
                {
                    // Lease docs sent to customer status and date function together.
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "leasedoc_sent_to_cust_date_status_id"))
                    {
                        //Check if associate with Lease Admin get from field exec_leasedoc_sent_to_cust_date else get field leasedoc_sent_to_cust_date                     
                        Utility.ControlValueSetter(this.ddlExecDocs_SenttoCustStatus, objLeaseApp.leasedoc_sent_to_cust_date_status_id);
                        Utility.ControlValueSetter(this.txtExecDocs_ExecSendtoCust, objLeaseApp.leasedoc_sent_to_cust_date);
                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leasedoc_sent_to_cust_date_status_id"))
                        {
                            this.ddlExecDocs_SenttoCustStatus.Enabled = false;
                            this.txtExecDocs_ExecSendtoCust.Enabled = false;
                        }
                    }
                    else
                    {
                        this.divExecutablesSenttoCustomer.Visible = false;
                    }
                }
                else
                {
                    divExecutablesSenttoCustomer.Style.Add("display", "none");
                }
                #endregion Executables Sent To Customer

                //Docs Back from Customer
                #region Docs Back from Customer
               
                if (Security.IsDisplayGroupVisible("Lease Back From Customer", category))
                {
                    //Signed by Customer
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "signed_by_cust_date"))
                    {
                        Utility.ControlValueSetter(this.txtExecDocs_SignedByCustDate, objLeaseApp.signed_by_cust_date);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "signed_by_cust_date"))
                        {
                            this.txtExecDocs_SignedByCustDate.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblLOEHoldupReasonTypes_SignedbyCustomer.Visible = false;
                        this.txtExecDocs_SignedByCustDate.Visible = false;
                    }

                    // Lease back from customer status and date function together.
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "lease_back_from_cust_date_status_id"))
                    {
                        Utility.ControlValueSetter(this.ddlExecDocs_BackFromCustDate_ColLeft1, objLeaseApp.lease_back_from_cust_date_status_id);
                        Utility.ControlValueSetter(this.txtExecDocs_BackFromCustDate, objLeaseApp.lease_back_from_cust_date);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lease_back_from_cust_date_status_id"))
                        {
                            this.ddlExecDocs_BackFromCustDate_ColLeft1.Enabled = false;
                            this.txtExecDocs_BackFromCustDate.Enabled = false;
                        }
                    }
                    else
                    {
                        this.divLOEHoldupReasonTypes_DocsBackfromCustomer.Visible = false;
                    }
                }
                else
                {
                    docs_back_from_customer_wrapper.Style.Add("display", "none");
                }
                #endregion Docs Back from Customer

                //Docs to FSC
                #region Docs to FSC
                if (Security.IsDisplayGroupVisible("Lease Execution", category))
                {
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "leasedoc_to_fsc_date"))
                    {
                        Utility.ControlValueSetter(this.txtExecDocs_ToFSCDate, objLeaseApp.leasedoc_to_fsc_date);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "leasedoc_to_fsc_date"))
                        {
                            this.txtExecDocs_ToFSCDate.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblLOEHoldupReasonTypes_DocstoFSC.Visible = false;
                        this.txtExecDocs_ToFSCDate.Visible = false;
                    }

                    // Lease Execution status and date function together.
                    //Lease Execution
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "lease_execution_date_status_id"))
                    {
                        Utility.ControlValueSetter(this.ddlExecDocs_LeaseExecDate_ColLeft1, objLeaseApp.lease_execution_date_status_id);
                        Utility.ControlValueSetter(this.txtExecDocs_LeaseExecDate, objLeaseApp.lease_execution_date);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "lease_execution_date_status_id"))
                        {
                            this.ddlExecDocs_LeaseExecDate_ColLeft1.Enabled = false;
                            this.txtExecDocs_LeaseExecDate.Enabled = false;
                        }
                    }
                    else
                    {
                        this.divLOEHoldupReasonTypes_LeaseExecution.Visible = false;
                    }
                }
                else
                {
                    lease_execution_wrapper.Style.Add("display", "none");
                }
                #endregion Docs to FSC

                //Amendment Execution
                #region Amendment Execution
                if (Security.IsDisplayGroupVisible("Amendment Execution", category))
                {
                    // Amendment execution status and date function together.   
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "amendment_execution_date_status_id"))
                    {
                        Utility.ControlValueSetter(this.ddlLOEHoldupReasonTypes_AmendmentExecution, objLeaseApp.amendment_execution_date_status_id);
                        Utility.ControlValueSetter(this.txtLOEHoldupReasonTypes_AmendmentExecution, objLeaseApp.amendment_execution_date);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "amendment_execution_date_status_id"))
                        {
                            this.ddlLOEHoldupReasonTypes_AmendmentExecution.Enabled = false;
                            this.txtLOEHoldupReasonTypes_AmendmentExecution.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblLOEHoldupReasonTypes_AmendmentExecution.Visible = false;
                        this.ddlLOEHoldupReasonTypes_AmendmentExecution.Visible = false;
                        this.txtLOEHoldupReasonTypes_AmendmentExecution.Visible = false;
                    }
                }
                else
                {
                    LA_amendment_execution_wrapper.Style.Add("display", "none");
                }
                #endregion Amendment Execution

                //Financial
                #region Financial
                if (Security.IsDisplayGroupVisible("Financial", category))
                {
                    //Rent Forecast Amount
                    #region Rent Forecast Amount
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "rent_forecast_amount"))
                    {
                        Utility.ControlValueSetter(this.txtFinanace_ForecastAmount, objLeaseApp.rent_forecast_amount);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "rent_forecast_amount"))
                        {
                            this.txtFinanace_ForecastAmount.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_RentForecastAmount.Visible = false;
                        this.txtFinanace_ForecastAmount.Visible = false;
                    }
                    #endregion Rent Forecast Amount

                    //Revenue Share
                    #region Revenue Share
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "revenue_share_yn"))
                    {
                        Utility.ControlValueSetter(this.ddlFinanace_RevShare, objLeaseApp.revenue_share_yn);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "revenue_share_yn"))
                        {
                            this.ddlFinanace_RevShare.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_RevenueShare.Visible = false;
                        this.ddlFinanace_RevShare.Visible = false;
                    }
                    #endregion Revenue Share

                    //Commencement Forecast
                    #region Commencement Forecast
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "commencement_forcast_date"))
                    {
                        Utility.ControlValueSetter(this.txtExecDocs_CommencementForecastDate, objLeaseApp.commencement_forcast_date);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "commencement_forcast_date"))
                        {
                            this.txtExecDocs_CommencementForecastDate.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_RevenueShare.Visible = false;
                        this.ddlFinanace_RevShare.Visible = false;
                    }
                    #endregion Commencement Forecast

                    //REM Lease Sequence
                    #region REM Lease Sequence
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "rem_lease_seq"))
                    {
                        Utility.ControlValueSetter(this.txtFinancial_REMLeaseSequence, objLeaseApp.rem_lease_seq);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "rem_lease_seq"))
                        {
                            this.txtFinancial_REMLeaseSequence.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_REMLeaseSequence.Visible = false;
                        this.txtFinancial_REMLeaseSequence.Visible = false;
                    }
                    #endregion REM Lease Sequence

                    //Cost Share Agreement
                    #region Cost Share Agreement
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "cost_share_agreement_amount"))
                    {
                        Utility.ControlValueSetter(this.txtFinanace_CostShare, objLeaseApp.cost_share_agreement_amount);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cost_share_agreement_amount"))
                        {
                            this.txtFinanace_CostShare.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_CostShareAgreement.Visible = false;
                        this.txtFinanace_CostShare.Visible = false;
                    }

                    //Utility.ControlValueSetter(this.ddlFinanace_CostShare, objLeaseApp.cost_share_agreement_yn);
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "cost_share_agreement_yn"))
                    {
                        Utility.ControlValueSetter(this.ddlFinanace_CostShare, objLeaseApp.cost_share_agreement_yn);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cost_share_agreement_yn"))
                        {
                            this.ddlFinanace_CostShare.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_CostShareAgreement.Visible = false;
                        this.ddlFinanace_CostShare.Visible = false;
                    }
                    #endregion Cost Share Agreement

                    //Cap Cost Recovery
                    #region Cap Cost Recovery
                    if (Security.IsFieldViewable(viewFields, "lease_applications", "cap_cost_amount"))
                    {
                        Utility.ControlValueSetter(this.txtFinanace_Capcost, objLeaseApp.cap_cost_amount);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cap_cost_amount"))
                        {
                            this.txtFinanace_Capcost.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_CapCostRecovery.Visible = false;
                        this.txtFinanace_Capcost.Visible = false;
                    }

                    if (Security.IsFieldViewable(viewFields, "lease_applications", "cap_cost_recovery_yn"))
                    {
                        Utility.ControlValueSetter(this.ddlFinanace_Capcost, objLeaseApp.cap_cost_recovery_yn);

                        if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "cap_cost_recovery_yn"))
                        {
                            this.ddlFinanace_Capcost.Enabled = false;
                        }
                    }
                    else
                    {
                        this.lblFinancial_CapCostRecovery.Visible = false;
                        this.txtFinanace_Capcost.Visible = false;
                    }
                    #endregion Cap Cost Recovery

                }
                else
                {
                    divFinancial.Style.Add("display", "none");
                    Financial_CommencementForecast_wrapper.Style.Add("display", "none");
                    left_col_wrapper.Style.Add("display", "none");
                }
                #endregion Financial

                //Termination Date
                #region Termination Date
                if (Security.IsFieldViewable(viewFields, "lease_applications", "termination_date"))
                {
                    Utility.ControlValueSetter(this.txtFinance_TerminateDate, objLeaseApp.termination_date);

                    if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "termination_date"))
                    {
                        this.txtFinance_TerminateDate.Enabled = false;
                    }
                }
                else
                {
                    this.lblLOEHoldupReasonTypes_TerminationDate.Visible = false;
                    this.txtFinance_TerminateDate.Visible = false;
                }
                #endregion Termination Date
            }
            else
            {
                //Executables Sent To Customer
                this.txtExecDocs_ExecSendtoCust.Text = Utility.DateToString(objLeaseAdmin.leasedoc_sent_to_cust_date);

                //Docs Back from Customer
                this.txtExecDocs_BackFromCustDate.Text = Utility.DateToString(objLeaseAdmin.lease_back_from_cust_date);
                Utility.ControlValueSetter(this.ddlExecDocs_SenttoCustStatus, objLeaseAdmin.leasedoc_sent_to_cust_date_status_id);

                //Document Type
                Utility.ControlValueSetter(ddlExecDocs_BackFromCustDate_ColLeft1, objLeaseAdmin.lease_back_from_cust_date_status_id);

                //Signed by Customer
                this.txtExecDocs_SignedByCustDate.Text = Utility.DateToString(objLeaseAdmin.signed_by_cust_date);

                //Docs to FSC
                this.txtExecDocs_ToFSCDate.Text = Utility.DateToString(objLeaseAdmin.leasedoc_to_fsc_date);

                //Lease Execution
                this.txtExecDocs_LeaseExecDate.Text = Utility.DateToString(objLeaseAdmin.lease_execution_date);
                Utility.ControlValueSetter(ddlExecDocs_LeaseExecDate_ColLeft1, objLeaseAdmin.lease_execution_date_status_id);

                //Amendment Execution
                LA_amendment_execution_wrapper.Visible = false;

                //Commencement Forecast
                this.txtExecDocs_CommencementForecastDate.Text = Utility.DateToString(objLeaseAdmin.commencement_forcast_date);

                //Rent Forecast Amount
                Utility.ControlValueSetter(this.txtFinanace_ForecastAmount, objLeaseAdmin.rent_forecast_amount);

                //Revenue Share
                Utility.ControlValueSetter(this.ddlFinanace_RevShare, objLeaseAdmin.revenue_share_yn);

                //REM Lease Sequence
                Utility.ControlValueSetter(this.txtFinancial_REMLeaseSequence, objLeaseAdmin.rem_lease_seq);

                //Cost Share Agreement
                Utility.ControlValueSetter(this.ddlFinanace_CostShare, objLeaseAdmin.cost_share_agreement_yn);
                Utility.ControlValueSetter(this.txtFinanace_CostShare, objLeaseAdmin.cost_share_agreement_amount);

                //Cap Cost Recovery
                Utility.ControlValueSetter(this.ddlFinanace_Capcost, objLeaseAdmin.cap_cost_recovery_yn);
                Utility.ControlValueSetter(this.txtFinanace_Capcost, objLeaseAdmin.cap_cost_amount);

                //Termination Date
                this.txtFinance_TerminateDate.Text = Utility.DateToString(objLeaseAdmin.termination_date);
            }

            //New controls
            Utility.ControlValueSetter(ddlExecDocs_DocType, objLeaseAdmin.lease_admin_execution_document_type_id);
            
            //Date Received at FSC
            this.txtExecDocs_RecvedatFSCDate.Text = Utility.DateToString(objLeaseAdmin.date_received_at_fsc);

            //Carrier Signature Notarized
            this.txtExecDocs_CarrierSignature.Text = Utility.DateToString(objLeaseAdmin.carrier_signature_notarized_date);
            Utility.ControlValueSetter(this.ddlExecDocs_CarrierSignature, objLeaseAdmin.carrier_signature_notarized_yn);

            //Sent to Signatory 
            this.txtExecDocs_SendSignator.Text = Utility.DateToString(objLeaseAdmin.sent_to_signatory_date);

            //Back from Signatory
            this.txtExecDocs_BackSignator.Text = Utility.DateToString(objLeaseAdmin.back_from_signatory_date);
            
            //Original Sent to UPS
            this.txtExecDocs_OrgSentUPS.Text = Utility.DateToString(objLeaseAdmin.original_sent_to_ups_date);

            //Doc Loaded to CST
            Utility.ControlValueSetter(ddlExecDocs_DocLoadCST, objLeaseAdmin.doc_loaded_to_cst_yn);

            //Amendment Rent Affecting
            Utility.ControlValueSetter(this.ddlFinance_AmendRentAffect, objLeaseAdmin.amendment_rent_affecting_yn);

            //One Time Fee
            Utility.ControlValueSetter(this.ddlFinance_OnTimeFee, objLeaseAdmin.one_time_fee_yn);

            Utility.ControlValueSetter(this.ddlFinanace_LoadCSTRecv, objLeaseAdmin.revshare_loaded_to_cst_receivable_yn);
            Utility.ControlValueSetter(this.ddlFinanace_LoadCSTPay, objLeaseAdmin.revshare_loaded_to_cst_payable_yn);
            //Executables Sent To Customer
            this.txtComDocs_NTPtoCust.Text = Utility.DateToString(objLeaseAdmin.commencement_ntp_to_customer_date);

            //Document Type
            Utility.ControlValueSetter(this.ddlComDocs_DocType, objLeaseAdmin.lease_admin_commencement_document_type_id);

            //Commencement Date
            this.txtComDocs_CommencementDate.Text = Utility.DateToString(objLeaseAdmin.commencement_date);

            //Date Received at FSC
            this.txtComDocs_RecvFSCDate.Text = Utility.DateToString(objLeaseAdmin.date_received_at_fsc_date);

            //Date Received By
            Utility.ControlValueSetter(this.ddlComDocs_RecvBy, objLeaseAdmin.document_received_by_emp_id);

            //Reg Commencement Letter Created
            this.txtComDocs_RegCommencementLetterDate.Text = Utility.DateToString(objLeaseAdmin.reg_commencement_letter_created_date);

            //REM Alert
            this.txtComDocs_RemAlert.Text = Utility.PrepareString(objLeaseAdmin.rem_alert_comments);
        }

        //Display comment bottom of pages
        private void BindComment(int iLeaseAdminID,CPTTEntities ceRef)
        {
            List<lease_admin_comments> lstComment = LeaseAdminComments.GetComments(iLeaseAdminID,ceRef).OrderBy(x=>x.created_at).ToList<lease_admin_comments>();
            List<LeaseAdminCommentData> result = new List<LeaseAdminCommentData>();
            foreach (lease_admin_comments comm in lstComment)
            {
                LeaseAdminCommentData data = new LeaseAdminCommentData();
                data.comment_id = comm.id;
                data.comment = comm.comments;
                data.created_at = Utility.DateToString(comm.created_at);
                data.update_at = Utility.DateToString(comm.updated_at);
                data.creator_id = comm.creator_id;
                data.updater = comm.creator_user.last_name + " " + comm.creator_user.first_name;
                result.Add(data);
            }
            this.hidden_lease_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            PageUtility.RegisterClientsideStartupScript(Page, "createLeaseAdminCommentUI", "createLeaseAdminCommentUI()");
        }

        protected void BindTowerImages(string siteId)
        {
            DMSHelper imageHelper = new DMSHelper();
            List<PhotoInfo> siteImages = new List<PhotoInfo>();

            siteImages = imageHelper.GetSitePhotos(siteId, false, string.Empty);
            Image2.ImageUrl = siteImages[0].photoURL;
            Image2.AlternateText = siteImages[0].photoID;
        }

        private void SaveLeaseAdmin()
        {
            int iLeaseAdminID;
            bool isValidId = Int32.TryParse(Request.QueryString["Id"], out iLeaseAdminID);
            if (isValidId)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    lease_admin_records objLeaseAdmin = LeaseAdminRecord.Search(iLeaseAdminID,ce);

                    if (objLeaseAdmin != null)
                    {
                        
                        //Chaeck associate with Leaseapp
                        bool bIsAssociate = false;
                        if (objLeaseAdmin.lease_application_id != null)
                        {
                            bIsAssociate = true;
                        }

                        //New controls Shared
                        if (bIsAssociate)
                        {
                            objLeaseAdmin.lease_applications.leasedoc_sent_to_cust_date = Utility.PrepareDate(this.txtExecDocs_ExecSendtoCust.Text);
                            objLeaseAdmin.lease_applications.leasedoc_sent_to_cust_date_status_id = Utility.PrepareInt(this.ddlExecDocs_SenttoCustStatus.SelectedValue);

                            objLeaseAdmin.lease_applications.lease_back_from_cust_date = Utility.PrepareDate(this.txtExecDocs_BackFromCustDate.Text);
                            objLeaseAdmin.lease_applications.lease_back_from_cust_date_status_id = Utility.PrepareInt(this.ddlExecDocs_BackFromCustDate_ColLeft1.SelectedValue);
                            objLeaseAdmin.lease_applications.signed_by_cust_date = Utility.PrepareDate(this.txtExecDocs_SignedByCustDate.Text);
                            objLeaseAdmin.lease_applications.leasedoc_to_fsc_date = Utility.PrepareDate(this.txtExecDocs_ToFSCDate.Text);

                            objLeaseAdmin.lease_applications.lease_execution_date = Utility.PrepareDate(this.txtExecDocs_LeaseExecDate.Text);
                            objLeaseAdmin.lease_applications.lease_execution_date_status_id = Utility.PrepareInt(this.ddlExecDocs_LeaseExecDate_ColLeft1.SelectedValue);

                            objLeaseAdmin.lease_applications.amendment_execution_date = Utility.PrepareDate(this.txtLOEHoldupReasonTypes_AmendmentExecution.Text);
                            objLeaseAdmin.lease_applications.amendment_execution_date_status_id = Utility.PrepareInt(this.ddlLOEHoldupReasonTypes_AmendmentExecution.SelectedValue);

                            objLeaseAdmin.lease_applications.commencement_forcast_date = Utility.PrepareDate(this.txtExecDocs_CommencementForecastDate.Text);

                            objLeaseAdmin.lease_applications.rent_forecast_amount = Utility.PrepareDecimal(this.txtFinanace_ForecastAmount.Text);
                            objLeaseAdmin.lease_applications.revenue_share_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_RevShare.SelectedValue));

                            objLeaseAdmin.lease_applications.rem_lease_seq = Utility.PrepareInt(this.txtFinancial_REMLeaseSequence.Text);
                            objLeaseAdmin.lease_applications.cost_share_agreement_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_CostShare.SelectedValue));
                            objLeaseAdmin.lease_applications.cost_share_agreement_amount = Utility.PrepareDecimal(this.txtFinanace_CostShare.Text);
                            objLeaseAdmin.lease_applications.cap_cost_recovery_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_Capcost.SelectedValue));
                            objLeaseAdmin.lease_applications.cap_cost_amount = Utility.PrepareDecimal(this.txtFinanace_Capcost.Text);
                            objLeaseAdmin.lease_applications.termination_date = Utility.PrepareDate(this.txtFinance_TerminateDate.Text);
                        }
                        else
                        {
                            objLeaseAdmin.leasedoc_sent_to_cust_date = Utility.PrepareDate(this.txtExecDocs_ExecSendtoCust.Text);
                            objLeaseAdmin.leasedoc_sent_to_cust_date_status_id = Utility.PrepareInt(this.ddlExecDocs_SenttoCustStatus.SelectedValue);

                            objLeaseAdmin.lease_back_from_cust_date = Utility.PrepareDate(this.txtExecDocs_BackFromCustDate.Text);
                            objLeaseAdmin.lease_back_from_cust_date_status_id = Utility.PrepareInt(this.ddlExecDocs_BackFromCustDate_ColLeft1.SelectedValue);
                            objLeaseAdmin.signed_by_cust_date = Utility.PrepareDate(this.txtExecDocs_SignedByCustDate.Text);
                            objLeaseAdmin.leasedoc_to_fsc_date = Utility.PrepareDate(this.txtExecDocs_ToFSCDate.Text);

                            objLeaseAdmin.lease_execution_date = Utility.PrepareDate(this.txtExecDocs_LeaseExecDate.Text);
                            objLeaseAdmin.lease_execution_date_status_id = Utility.PrepareInt(this.ddlExecDocs_LeaseExecDate_ColLeft1.SelectedValue);
                            objLeaseAdmin.commencement_forcast_date = Utility.PrepareDate(this.txtExecDocs_CommencementForecastDate.Text);

                            objLeaseAdmin.rent_forecast_amount = Utility.PrepareDecimal(this.txtFinanace_ForecastAmount.Text);
                            objLeaseAdmin.revenue_share_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_RevShare.SelectedValue));

                            objLeaseAdmin.rem_lease_seq = Utility.PrepareInt(this.txtFinancial_REMLeaseSequence.Text);
                            objLeaseAdmin.cost_share_agreement_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_CostShare.SelectedValue));
                            objLeaseAdmin.cost_share_agreement_amount = Utility.PrepareDecimal(this.txtFinanace_CostShare.Text);
                            objLeaseAdmin.cap_cost_recovery_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_Capcost.SelectedValue));
                            objLeaseAdmin.cap_cost_amount = Utility.PrepareDecimal(this.txtFinanace_Capcost.Text);
                            objLeaseAdmin.termination_date = Utility.PrepareDate(this.txtFinance_TerminateDate.Text);
                        }

                        objLeaseAdmin.lease_admin_execution_document_type_id = Utility.PrepareInt(this.ddlExecDocs_DocType.SelectedValue);
                        //Execution Document Dates
                        objLeaseAdmin.date_received_at_fsc = Utility.PrepareDate(this.txtExecDocs_RecvedatFSCDate.Text);
                        objLeaseAdmin.carrier_signature_notarized_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlExecDocs_CarrierSignature.SelectedValue));
                        objLeaseAdmin.carrier_signature_notarized_date = Utility.PrepareDate(this.txtExecDocs_CarrierSignature.Text);
                        objLeaseAdmin.sent_to_signatory_date = Utility.PrepareDate(this.txtExecDocs_SendSignator.Text);
                        objLeaseAdmin.back_from_signatory_date = Utility.PrepareDate(this.txtExecDocs_BackSignator.Text);
                        objLeaseAdmin.original_sent_to_ups_date = Utility.PrepareDate(this.txtExecDocs_OrgSentUPS.Text);
                        objLeaseAdmin.doc_loaded_to_cst_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlExecDocs_DocLoadCST.SelectedValue));

                        //Execution Financial
                        objLeaseAdmin.amendment_rent_affecting_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinance_AmendRentAffect.SelectedValue));
                        objLeaseAdmin.one_time_fee_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinance_OnTimeFee.SelectedValue));
                        objLeaseAdmin.revshare_loaded_to_cst_receivable_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_LoadCSTRecv.SelectedValue));
                        objLeaseAdmin.revshare_loaded_to_cst_payable_yn = Utility.ConvertYNToBoolean(Utility.PrepareString(this.ddlFinanace_LoadCSTPay.SelectedValue));
                        
                        //Commencement
                        objLeaseAdmin.commencement_ntp_to_customer_date = Utility.PrepareDate(this.txtComDocs_NTPtoCust.Text);
                        objLeaseAdmin.lease_admin_commencement_document_type_id = Utility.PrepareInt(this.ddlComDocs_DocType.SelectedValue);
                        objLeaseAdmin.commencement_date = Utility.PrepareDate(this.txtComDocs_CommencementDate.Text);
                        objLeaseAdmin.date_received_at_fsc_date = Utility.PrepareDate(this.txtComDocs_RecvFSCDate.Text);
                        objLeaseAdmin.document_received_by_emp_id = Utility.PrepareInt(this.ddlComDocs_RecvBy.SelectedValue);
                        objLeaseAdmin.reg_commencement_letter_created_date = Utility.PrepareDate(this.txtComDocs_RegCommencementLetterDate.Text);
                        objLeaseAdmin.rem_alert_comments = Utility.PrepareString(this.txtComDocs_RemAlert.Text);

                        ce.Connection.Open();
                        try
                        {
                            LeaseAdminRecord.Update(objLeaseAdmin, ce);
                            BindApplication(objLeaseAdmin.id);
                            Master.StatusBox.showStatusMessage("Lease Admin tracker Updated.");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            ce.Connection.Dispose();
                        }
                    }
                }
            }
        }

        #endregion

        #region Documnent Link
        private void GetLinkDocument()
        {
            lease_admin_records leaseAdmin = this.CurrentLeaseAdmin;

            if (!leaseAdmin.lease_application_id.HasValue)
            {
                List<Dms_Document> TowerModPacketSentDate = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LA_ExecutablesSenttoCustomer.ToString()));
                SetLinkDocument(leaseAdmin.id, DMSDocumentLinkHelper.eDocumentField.LA_ExecutablesSenttoCustomer, TowerModPacketSentDate);

                List<Dms_Document> LA_LOEHoldupReasonTypes_DocsBackfromCustomer = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LA_LOEHoldupReasonTypes_DocsBackfromCustomer.ToString()));
                SetLinkDocument(leaseAdmin.id, DMSDocumentLinkHelper.eDocumentField.LA_LOEHoldupReasonTypes_DocsBackfromCustomer, LA_LOEHoldupReasonTypes_DocsBackfromCustomer);

                List<Dms_Document> LA_LeaseExecution = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LA_LeaseExecution.ToString()));
                SetLinkDocument(leaseAdmin.id, DMSDocumentLinkHelper.eDocumentField.LA_LeaseExecution, LA_LeaseExecution);

                List<Dms_Document> LA_NTPtoCust = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LA_NTPtoCust.ToString()));
                SetLinkDocument(leaseAdmin.id, DMSDocumentLinkHelper.eDocumentField.LA_NTPtoCust, LA_NTPtoCust);

                List<Dms_Document> LA_RegCommencementLetterDate = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LA_RegCommencementLetterDate.ToString()));
                SetLinkDocument(leaseAdmin.id, DMSDocumentLinkHelper.eDocumentField.LA_RegCommencementLetterDate, LA_RegCommencementLetterDate);

                List<Dms_Document> LA_RentForecastAmount = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LA_RentForecastAmount.ToString()));
                SetLinkDocument(leaseAdmin.id, DMSDocumentLinkHelper.eDocumentField.LA_RentForecastAmount, LA_RentForecastAmount);
            }
            else
            {

                List<Dms_Document> ExecutablesSenttoCustomer = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer.ToString()));
                SetLinkDocument(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer, ExecutablesSenttoCustomer);

                List<Dms_Document> LOEHoldupReasonTypes_DocsBackfromCustomer = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer.ToString()));
                SetLinkDocument(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer, LOEHoldupReasonTypes_DocsBackfromCustomer);

                List<Dms_Document> LeaseExecution = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.LeaseExecution.ToString()));
                SetLinkDocument(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.eDocumentField.LeaseExecution, LeaseExecution);

                List<Dms_Document> NTPtoCust = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.NTPtoCust.ToString()));
                SetLinkDocument(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.eDocumentField.NTPtoCust, NTPtoCust);

                List<Dms_Document> RegCommencementLetterDate = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate.ToString()));
                SetLinkDocument(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate, RegCommencementLetterDate);

                List<Dms_Document> Financial_RentForecastAmount = DMSDocumentLinkHelper.GetDocumentLink(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount.ToString()));
                SetLinkDocument(leaseAdmin.lease_applications.id, DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount, Financial_RentForecastAmount);
                
            }
        }

        private void SetLinkDocument(int ID, DMSDocumentLinkHelper.eDocumentField field, List<Dms_Document> docData)
        {
            //insert template data in case no documents link
            if (docData.Count == 0)
            {
                Dms_Document doc = new Dms_Document();
                doc.ID = ID;
                doc.ref_field = field.ToString() ;
                docData.Add(doc);
            }

            switch (field)
            {
                case DMSDocumentLinkHelper.eDocumentField.LA_ExecutablesSenttoCustomer:
                case DMSDocumentLinkHelper.eDocumentField.ExecutablesSenttoCustomer:
                    this.hidden_document_LA_ExecutablesSenttoCustomer.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.LA_LOEHoldupReasonTypes_DocsBackfromCustomer:
                case DMSDocumentLinkHelper.eDocumentField.LOEHoldupReasonTypes_DocsBackfromCustomer:
                    this.hidden_document_LA_LOEHoldupReasonTypes_DocsBackfromCustomer.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.LA_LeaseExecution:
                case DMSDocumentLinkHelper.eDocumentField.LeaseExecution:
                    this.hidden_document_LA_LeaseExecution.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.LA_NTPtoCust:
                case DMSDocumentLinkHelper.eDocumentField.NTPtoCust:
                    this.hidden_document_LA_NTPtoCust.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.LA_RegCommencementLetterDate:
                case DMSDocumentLinkHelper.eDocumentField.RegCommencementLetterDate:
                    this.hidden_document_LA_RegCommencementLetterDate.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
                case DMSDocumentLinkHelper.eDocumentField.LA_RentForecastAmount:
                case DMSDocumentLinkHelper.eDocumentField.Financial_RentForecastAmount:
                    this.hidden_document_LA_RentForecastAmount.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
                    break;
            }
        }

        protected void DocumentDownload(object sender, EventArgs e)
        {
            LinkButton obj = (LinkButton)sender;
            string keyName = obj.Attributes["key"];
            if (!string.IsNullOrEmpty(keyName))
            {
                AmazonDMS.DownloadObject(keyName);
            }
        }
              
        #endregion

        private void GetSessionFilter()
        {
            // Check session filter
            DefaultFilterSession filterSession = new DefaultFilterSession();

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

                string hiddenFilter = DefaultFilterSession.GetFilterSession(filterSession, DefaultFilterSession.FilterType.LeaseAdminFilter);
                backToList.NavigateUrl += "?filter=" + hiddenFilter;
            }
        }
    }
}