﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="SavedFiltersManage.aspx.cs" Inherits="TrackIT2.Account.SavedFiltersManage" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/saved_filters_manage") %>

   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/saved_filters_manage") %>   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert"></div>

    <div class="create-container form_element" style="text-align:center;">
        <div id="savedFilterForm">
            <h4>Saved Filter Details</h4>
            <label>Filter Name:</label>
            <asp:TextBox id="txtFilterName" Width="300px" ClientIDMode="Static" runat="server" />

            <div class="cb"></div>

            <asp:Button ID="btnCreateSavedFilter" Text="Save" CssClass="buttonSubmit"
                        OnClientClick="return validateCreateNewSavedFilter();" runat="server" />
        </div>
        <div class="cb"></div>
        <div id="closeWindowDiv" style="margin:0 auto 0 auto; padding:20px 0 0 0;width:180px;display:none;overflow:auto;">
            <input type="button" class="buttonClose ui-button ui-widget ui-state-default ui-corner-all"
                        value="Close Window" onclick="parent.$('#modalExternalSavedFilterManage').dialog('close');" />
        </div>

    </div>

</asp:Content>
