﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TrackIT2.Account.Login" Title="T-Mobile Towers TrackiT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">   
   <title></title>
   
   <!-- Styles -->
   <%: Styles.Render("~/Styles/login") %>	

    <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/trackit")%>

   <script type="text/javascript" language="javascript">
    // Doc Ready
       $(function () {
           $("#LoginUser_UserName").focus();
       });
   </script>
</head>
<body>
	<div style="text-align: center;margin-bottom:0px;">
		<a href="/" title="Home">
			<img src="/images/logo.gif" alt="T-Mobile Towers" border="0" />
		</a>
		<h1 style="color:#6f7073;">TrackiT2</h1>
	</div>
	<form id="form1" runat="server">
	<asp:Panel ID="Panel6" runat="server" HorizontalAlign="Center">
		<asp:Label ID="lblMessage" runat="server" Text="You need to Sign In before continuing." CssClass="Label_Message"></asp:Label>
	</asp:Panel>
	<asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center" CssClass="Panel_Login">
		<asp:Panel ID="Panel2" runat="server" CssClass="Border_Login" HorizontalAlign="Left">
			<asp:Login ID="LoginUser" runat="server" 
                    EnableViewState="true"                     
                    FailureText="Invalid email or password."                    
                    OnAuthenticate="LoginUser_Authenticate"
                    OnLoggedIn="LoginUser_LoggedIn"
                    OnLoginError="LoginUser_LoginError" 
                    RenderOuterTable="false">
				<LayoutTemplate>
					<asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="Label_Normal">TMO User ID/Email</asp:Label>
					<br />
					<asp:Panel ID="Panel7" runat="server" Wrap="False">
						<asp:TextBox ID="UserName" runat="server" CssClass="TextBox_Login"></asp:TextBox>
					</asp:Panel>
					<asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="Label_Normal">Password</asp:Label>
					<asp:TextBox ID="Password" runat="server" CssClass="TextBox_Login" TextMode="Password"></asp:TextBox>
					<br />
					<asp:CheckBox ID="RememberMe" runat="server" />
					<asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="Label_Normal">Remember Me</asp:Label>
					<asp:Panel ID="Panel3" runat="server" HorizontalAlign="Right">
						<asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Sign In" ValidationGroup="LoginUserValidationGroup" CssClass="fancy-button" />
					</asp:Panel>
				</LayoutTemplate>
			</asp:Login>
		</asp:Panel>
		<asp:Panel ID="Panel5" runat="server" HorizontalAlign="Left" CssClass="Panel_Nav">
			<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Account/ResetPassword.aspx">Forgot password?</asp:HyperLink>
		</asp:Panel>
	</asp:Panel>
	</form>
</body>
</html>
