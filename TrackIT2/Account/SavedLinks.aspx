﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="SavedLinks.aspx.cs" Inherits="TrackIT2.Account.SavedLinks" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/saved_links") %>

    <style type="text/css">
        .page-body
        {
            margin: 5px;
            padding-bottom: 0;
        }  
        .wrapper
        {
            width: auto;
        }
        #statusBox
        {
            height: auto;
        }
    </style>
    <script type="text/javascript">
        function trickFillAdd()
        {
            $("#MainContent_gvLinks_txtAddTitle").val("title").css("color", "white");
            $("#MainContent_gvLinks_txtAddUrl").val("http://www.a.com").css("color", "white");

            $("input[id^='MainContent_gvLinks_txtEditTitle']").val("title").css("color", "white");
            $("input[id^='MainContent_gvLinks_txtEditUrl']").val("http://www.a.com").css("color", "white");
        }
        $(document).ready(function ()
        {
        /*
            $("#MainContent_gvLinks_txtAddTitle").val("");
            $("#MainContent_gvLinks_txtAddUrl").val("");

            $(".savedlinks_form").hide();
            */

            /*
            var total = $("#MainContent_gvLinks tbody").children("tr").length;
            if (total > 2)
            {
                var count = 1;
                // Insert Edit button to every row of Gridview, 'id' was define like a GridView row or EditIndex.
                $("#MainContent_gvLinks tbody tr td:first-child").each(function ()
                {
                    $(this).append(
                        '&nbsp;<a id="' + count + '" href="javascript:void(0)" class="gdvSavedLink_edit">Edit</a>'
                    );
                    count++;
                });
                $("#MainContent_gvLinks tbody tr:last-child").find("a.gdvSavedLink_edit").remove();
            }*/

            ///
            /// Additional Edit button for GridView, front-end
            ///
            /*
            $("a.gdvSavedLink_edit").live("click", function ()
            {
                // Hide GridView

                $("#MainContent_gvLinks").hide();

                // Show form

                $(".savedlinks_form").show();
                var old_title = $(this).parent().next().text();
                var old_url = $(this).parent().next().next().text();
                var old_description = $(this).parent().next().next().next().text();

                // Send Value to form

                $("#MainContent_txtSavedLink_title").val(old_title);
                $("#MainContent_txtSavedLink_url").val(old_url);
                $("#MainContent_txtSavedLink_description").val(old_description);

                // Also set editIndex to Hidden field

                var EditIndex = $(this).attr('id');
                $("#MainContent_hidden_editIndex").val(EditIndex);
                $("#MainContent_hidden_old_title").val(old_title);

            });
            */
            ///
            /// Cancel button, front-end
            ///
            /*
            $("a.gdvSavedLink_cancel").live("click", function ()
            {
                // Clear value in textbox form

                //                $("#MainContent_txtSavedLink_title").val("");
                //                $("#MainContent_txtSavedLink_url").val("");
                //                $("#MainContent_txtSavedLink_description").val("");
                $(".savedlinks_form").hide();

                // Show gridView, may be refresh or sth

                $("#MainContent_gvLinks").show();
            });
            */

        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--
   <h2>Saved Filters</h2>
   <h3>These links are displayed on your dashboard.</h3>
   -->

   <asp:GridView ID="gvLinks" runat="server" CssClass="widget-table"
                 AutoGenerateColumns="False" 
                 ShowFooter="true"
                 onrowdatabound="gvLinks_RowDataBound" 
                 onrowdeleting="gvLinks_RowDeleting" 
                 onrowediting="gvLinks_RowEditing" 
                 onrowcancelingedit="gvLinks_RowCancelingEdit" 
                 onrowupdating="gvLinks_RowUpdating" BorderStyle="None" 
        GridLines="None" CellSpacing="2"> 
            
      <EditRowStyle CssClass="GridViewEditRow" />
       <HeaderStyle BorderColor="White" BorderStyle="None" BorderWidth="0px" 
           ForeColor="#EC008C" />
      <PagerStyle HorizontalAlign="Right" />

      <Columns>
         <asp:TemplateField HeaderText="Options">
            <HeaderStyle BackColor="White" Width="20px"/>
            <ItemStyle Width="20px" />
            <FooterStyle HorizontalAlign="Center" Font-Size="9pt" />
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                    <asp:Label ID="lblOrigTitle" runat="server" Visible="false"></asp:Label>
                    <asp:LinkButton ID="lnbEdit" CommandName="Edit" runat="server" Text="Edit" CausesValidation="false"></asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="lnbDelete" runat="server" CommandName="Delete" Text="Delete" CausesValidation="false" OnClientClick="return confirm('Are you sure you want to delete this link?');"></asp:LinkButton>               
            </ItemTemplate>
                  
            <EditItemTemplate>
               <asp:Label ID="lblOrigTitle" runat="server" Visible="false"></asp:Label>
               <asp:LinkButton ID="lnbCancel" runat="server" CommandName="Cancel" Text="Cancel" OnClientClick="return trickFillAdd();"></asp:LinkButton>
               &nbsp;&nbsp;
               <asp:LinkButton ID="lnbUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
            </EditItemTemplate>
            
            <FooterTemplate>               
               <asp:LinkButton ID="lnbAddLink" runat="server" Text="Add" onclick="lnbAddLink_Click"></asp:LinkButton>
            </FooterTemplate>      
         </asp:TemplateField>
         
         <asp:TemplateField HeaderText="SavedLink" HeaderStyle-Width="170px">
            <ItemTemplate>
                <div class="savedlinks_form">
                    <div style="float:left;">
                        <span class="savedlink_head">Title: </span>
                        <asp:Label ID="lblSavedLink_title" runat="server" Text=""></asp:Label>
                    </div>
                    
                    <div style="float:left; margin-left: 5px;">
                        <span class="savedlink_head">URL: </span>
                        <asp:Label ID="lblSavedLink_url" runat="server" Text=""></asp:Label>
                    </div>

                    <div class="cb"></div>

                    <div style="float:left;">
                        <span class="savedlink_head">Description: </span>
                        <asp:Label ID="lblSavedLink_description" runat="server" Text=""></asp:Label>
                    </div>

                    <div class="cb"></div>
                </div>
            </ItemTemplate>

            <EditItemTemplate>
                <div class="savedlinks_form">
                    <div style="float: left;">
                        <span class="savedlink_head">Title: </span>
                        <asp:TextBox ID="txtEditTitle" runat="server" ToolTip="Enter title"></asp:TextBox>
                        <div class="cb"></div>
                        <asp:RequiredFieldValidator ID="valEditTitleRequired" runat="server"
                                               ControlToValidate="txtEditTitle"
                                               CssClass="errorMessage"
                                               Display="Dynamic"
                                               EnableClientScript="true"
                                               ErrorMessage="Please specify a title.">
                        </asp:RequiredFieldValidator>  
                    </div>

                    <div style="float:left; margin-left:5px;" >
                        <span class="savedlink_head">URL: </span>
                        <asp:TextBox ID="txtEditUrl" runat="server"></asp:TextBox>
                        <div class="cb"></div>
                        <asp:RequiredFieldValidator ID="valEditUrlRequired" runat="server"                              
                                                   ControlToValidate="txtEditUrl"
                                                   CssClass="errorMessage"
                                                   Display="Dynamic"
                                                   EnableClientScript="true"
                                                   ErrorMessage="Please specify a URL.">                                                 
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="valEditUrlRegex" runat="server"
                                                       ControlToValidate="txtEditUrl"
                                                       CssClass="errorMessage"
                                                       Display="Dynamic"
                                                       EnableClientScript="true"
                                                       ErrorMessage="Url is not in proper format." 
                                                       ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?">
                        </asp:RegularExpressionValidator>
                    </div>

                    <div class="cb"></div>

                    <div style="margin-top: 10px;">
                        <span class="savedlink_head">Description: </span>
                        <asp:TextBox ID="txtEditDescription" runat="server" Width="260px"></asp:TextBox>
                    </div>

                    <div class="cb"></div>
                </div>            
            </EditItemTemplate>

            <FooterTemplate>
                <div class="savedlinks_form">
                    <div style="float:left;">
                        <span class="savedlink_head">Title: </span>
                        <asp:TextBox ID="txtAddTitle" runat="server"></asp:TextBox>
                        <div class="cb"></div>
                        <asp:RequiredFieldValidator ID="valAddTitleRequired" runat="server"
                                                   ControlToValidate="txtAddTitle"
                                                   CssClass="errorMessage"
                                                   Display="Dynamic"
                                                   EnableClientScript="true"
                                                   ErrorMessage="Please specify a title.">
                        </asp:RequiredFieldValidator>   
                    </div> 
                       
                    <div style="float:left; margin-left: 5px;">
                        <span class="savedlink_head">URL: </span>
                        <asp:TextBox ID="txtAddUrl" runat="server"></asp:TextBox>
                        <div class="cb"></div>
                        <asp:RequiredFieldValidator ID="valAddUrlRequired" runat="server"                              
                                                   ControlToValidate="txtAddUrl"
                                                   CssClass="errorMessage"
                                                   Display="Dynamic"
                                                   EnableClientScript="true"
                                                   ErrorMessage="Please specify a URL.">                                                 
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="valAddUrlRegex" runat="server"
                                                       ControlToValidate="txtAddUrl"
                                                       CssClass="errorMessage"
                                                       Display="Dynamic"
                                                       EnableClientScript="true"
                                                       ErrorMessage="Url is not in proper format." 
                                                       ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?">
                        </asp:RegularExpressionValidator>
                    </div>

                    <div class="cb"></div>

                    <div style="margin-top: 10px;float:left;">
                        <span class="savedlink_head">Description: </span>
                        <asp:TextBox ID="txtAddDescription" Width="260px" runat="server"></asp:TextBox>
                    </div>

                    <div class="cb"></div>
                </div>      
            </FooterTemplate>

            <HeaderStyle BackColor="White" Width="170px"></HeaderStyle>
         </asp:TemplateField>

<%--         <asp:TemplateField HeaderText="Url" HeaderStyle-Width="170px">
            <EditItemTemplate>
            </EditItemTemplate>
            <FooterTemplate>               
               
            </FooterTemplate>

            <HeaderStyle Width="170px" BackColor="White"></HeaderStyle>
         </asp:TemplateField>

         <asp:TemplateField HeaderText="Description" HeaderStyle-Width="200px">
            <EditItemTemplate>
               
            </EditItemTemplate>

            <FooterTemplate>
               
            </FooterTemplate>

            <HeaderStyle Width="200px" BackColor="White"></HeaderStyle>
         </asp:TemplateField>--%>
      </Columns>
   </asp:GridView>

<%--    <div class="savedlinks_form">
        <h1>Edit SavedLink</h1>
        <div style="float: left;">
            <span style="width: 70px;">Title: </span>
            <asp:TextBox ID="txtSavedLink_title" MaxLength="100" runat="server"></asp:TextBox>
            <div class="cb"></div>
            <asp:RequiredFieldValidator ID="SavedLink_EditTitleRequired" runat="server"
                ControlToValidate="txtSavedLink_title"
                CssClass="errorMessage"
                Display="Dynamic"
                EnableClientScript="true"
                ErrorMessage="Please specify a title."
                >
            </asp:RequiredFieldValidator>
        </div>

        <div style="float:left;">
            <span style="width: 40px;">URL: </span>
            <asp:TextBox ID="txtSavedLink_url" MaxLength="100" runat="server"></asp:TextBox>
            <div class="cb"></div>
            <asp:RequiredFieldValidator ID="SavedLink_EditUrlRequired" runat="server"                              
                ControlToValidate="txtSavedLink_url"
                CssClass="errorMessage"
                Display="Dynamic"
                EnableClientScript="true"
                ErrorMessage="Please specify a URL.">                                                 
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="SavedLink_EditUrlRegex" runat="server"
                ControlToValidate="txtSavedLink_url"
                CssClass="errorMessage"
                Display="Dynamic"
                EnableClientScript="true"
                ErrorMessage="URL is not in proper format." 
                ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?">
            </asp:RegularExpressionValidator>
        </div>
        <br />
        <div class="cb"></div>
        <div style="margin-top: 10px;">
            <span style="width: 70px;">Description: </span>
            <asp:TextBox ID="txtSavedLink_description" MaxLength="100" runat="server"></asp:TextBox>

            <div class="cb"></div>
            <div class="savedlinks_edit_action">
                <asp:LinkButton ID="updateSavedLink" CssClass="gdvSavedLink_update" 
                    runat="server" onclick="updateSavedLink_Click" OnClientClick="return trickFillAdd();">Update</asp:LinkButton>
                &nbsp; 
                <asp:LinkButton ID="cancelSavedLink" CssClass="gdvSavedLink_cancel" 
                    runat="server" onclick="cancelSavedLink_Click" OnClientClick="return trickFillAdd();">Cancel</asp:LinkButton>
            </div>
        </div>

        <div class="cb"></div>
    </div>
    <asp:HiddenField ID="hidden_editIndex" runat="server" />
    <asp:HiddenField ID="hidden_old_title" runat="server" />
    <asp:HiddenField ID="hidden_url" runat="server" />
    <asp:HiddenField ID="hidden_description" runat="server" />--%>
</asp:Content>