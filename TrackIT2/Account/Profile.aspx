﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="User Profile" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="TrackIT2.Account.Profile" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">     
   <!-- Styles -->
   <%: Styles.Render("~/Styles/profile") %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Edit Profile</h2>
    <asp:Panel ID="pnlEditUser" runat="server">
        <p>
            <em>Note: Id and name fields are maintained by T-Mobile.
            <br />
            Please contact the administrator if you need these details changed.</em>
        </p>

        <p>&nbsp;</p>

        <p>
            <a href="SavedLinks.aspx">Manage Saved Links</a>
        </p>

        <div style="width: 600px; margin-top: 20px;">
            <h4>Profile</h4>
            <ul class="profile-list">
                <li><span>User Name:</span>
                    <asp:Label ID="lblUserName" runat="server" Text="[UserName]"></asp:Label>
                </li>
                <li><span>E-mail Address:</span>
                    <asp:Label ID="lblEmail" runat="server" Text="[Email]"></asp:Label>
                </li>
                <li><span>First Name:</span>
                    <asp:Label ID="lblFirstName" runat="server" Text="[First Name]"></asp:Label>
                </li>
                <li><span>Last Name:</span>
                    <asp:Label ID="lblLastName" runat="server" Text="[Last Name]"></asp:Label>
                </li>
                <li>&nbsp;</li>
            </ul>                  

            <h4>Change TrackiT Password</h4>
            <ul class="profile-list">
                <li>
                    <span>Current Password:</span>
                    <asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                </li>
                <li>
                    <span>New Password:</span>
                    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="valNewPassworddRegex" runat="server" 
                                                   ControlToValidate="txtNewPassword"
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   ErrorMessage="Password must be at least 6 characters."
                                                    ValidationExpression="^.{6,}$"/>
                                                    <%--Old validation ValidationExpression="^[a-zA-Z0-9]{6,}$"--%>
                </li>
                <li>
                    <span>Confirm Password:</span>
                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ID="valConfirmPasswordCompare" runat="server" 
                        ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtNewPassword" 
                        CssClass="inline-error" 
                        Display="Dynamic" 
                        ErrorMessage="Your passwords do not match!" />
                </li>
            </ul>

            <h4>Change Avatar</h4>
            <ul class="profile-list">
                <li>
                    <span>User Avatar:</span>
                    <asp:Image ID="imgAvatar" runat="server" Width="100" Height="100" />
                </li>
                <li>
                    <span>Specify Avatar Image: </span>
                    <asp:FileUpload ID="filAvatar" runat="server" />
                    <asp:RegularExpressionValidator ID="valAvatarRegex" runat="server" 
                        ControlToValidate="filAvatar"
                        Display="Dynamic" 
                        ErrorMessage="Please specify an image file in PNG or JPG format."
                        ValidationExpression="^.+\.((jpg)|(jpeg)|(png))$">
                    </asp:RegularExpressionValidator>
                    <br />
                    <p style="text-indent: 120px;">
                        <em>Images will be resized to 100 x 100 pixels. </em>
                    </p>
                    <li>&nbsp;</li>
                </li>
            </ul>

            <h4>Reporting Server Login</h4>
            <ul class="profile-list">
                <li>
                   <span>Login ID:</span>
                   <asp:TextBox ID="txtReportLogin" runat="server" Text=""></asp:TextBox>
                </li>
                <li>
                   <span>Password:</span>
                   <asp:TextBox ID="txtReportPassword" runat="server" TextMode="Password"></asp:TextBox>                   
                </li>
                <li>
                   <span>Confirm Password:</span>
                   <asp:TextBox ID="txtReportConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                   <asp:CompareValidator ID="valReportConfirmPasswordCompare" runat="server" 
                                         ControlToValidate="txtReportConfirmPassword"
                                         ControlToCompare="txtReportPassword" 
                                         CssClass="inline-error"
                                         Display="Dynamic" 
                                         ErrorMessage="Your passwords do not match!" />
                </li>
            </ul>

            <div class="cb" />

            <ul class="profile-list">
               <li>
                  <br /><br />
                  <asp:Button ID="btnUpdate" runat="server" Text="Save Changes" onclick="btnUpdate_Click" />
               </li>
            </ul>            
        </div>        
    </asp:Panel>
    <!-- OLD -->
    <asp:Panel ID="pnlNoId" runat="server">
        <p style="text-align: center">
            Please <a href="Login.aspx">login</a> in order to edit your profile.
        </p>
    </asp:Panel>
</asp:Content>
