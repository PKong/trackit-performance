﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.Account
{
   public partial class Profile : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (User.Identity.IsAuthenticated)
         {
            if (!Page.IsPostBack)
            {
               pnlNoId.Visible = false;
               pnlEditUser.Visible = true;
               BindUser();
            }
         }
         else
         {
            pnlEditUser.Visible = false;
            pnlNoId.Visible = true;
         }
      }

      protected void BindUser()
      {
         CPTTEntities ce = new CPTTEntities();
         UserProfile profile = UserProfile.GetUserProfile();
         user currUser;
         string avatarJpg;
         string avatarPng;
         string avatarUrl;                  

         currUser = ce.users
                      .Where(u => u.aspnet_Users.UserName.Equals(User.Identity.Name))
                      .First();

         lblUserName.Text = User.Identity.Name;
         lblEmail.Text = currUser.email;
         lblFirstName.Text = currUser.first_name;
         lblLastName.Text = currUser.last_name;

         ce.Detach(currUser);
         Session["currentUser"] = currUser;         
         
         if (!string.IsNullOrEmpty(profile.ReportLogin))
         {
            txtReportLogin.Text = profile.ReportLogin;
         }

         avatarPng = Server.MapPath("~/images/avatars/" + User.Identity.Name.ToLower() + ".png");
         avatarJpg = Server.MapPath("~/images/avatars/" + User.Identity.Name.ToLower() + ".jpg");

         if (File.Exists(avatarPng))
         {
            avatarUrl = "~/images/avatars/" + User.Identity.Name.ToLower() + ".png";
         }
         else if (File.Exists(avatarJpg))
         {
            avatarUrl = "~/images/avatars/" + User.Identity.Name.ToLower() + ".jpg";
         }
         else
         {
            avatarUrl = "~/images/avatars/default.png";
         }

         imgAvatar.ImageUrl = avatarUrl;
         
         // Dispose the resources used with the image so that we can have access 
         // to delete it if necessary. Image will still render on the page.
         imgAvatar.Dispose();         
      }

      protected void ResetForm()
      {
         txtCurrentPassword.Text = "";
         txtNewPassword.Text = "";
         txtConfirmPassword.Text = "";
         txtReportLogin.Text = "";
         txtReportPassword.Text = "";
         txtReportConfirmPassword.Text = "";
      }

      protected void btnUpdate_Click(object sender, EventArgs e)
      {        
         if (Page.IsValid)
         {
            if (!string.IsNullOrWhiteSpace(txtNewPassword.Text) || 
                !string.IsNullOrWhiteSpace(txtReportLogin.Text) || 
                !string.IsNullOrWhiteSpace(txtReportPassword.Text) ||
                filAvatar.HasFile)
            {
               bool changeSuccess = false;

               if (!string.IsNullOrWhiteSpace(txtNewPassword.Text))
               {
                  MembershipUser currUser = Membership.GetUser(User.Identity.Name);
                  changeSuccess = currUser.ChangePassword(txtCurrentPassword.Text.Trim(),
                                                          txtNewPassword.Text.Trim());

                  if (!changeSuccess)
                  {
                     Master.StatusBox.showErrorMessage
                                      ("Password not updated. Please make sure " + 
                                       "you have specified your original password.");

                     return;
                  }
               }

               if (!string.IsNullOrWhiteSpace(txtReportLogin.Text))
               {
                  try
                  {
                     UserProfile profile = UserProfile.GetUserProfile();
                     profile.ReportLogin = txtReportLogin.Text.Trim();
                     profile.Save();
                     changeSuccess = true;
                  }
                  catch (Exception)
                  {
                     Master.StatusBox.showErrorMessage
                                      ("Unable to update report login credentials." + 
                                       "Please try again");
                  }
                  
               }

               if (!string.IsNullOrWhiteSpace(txtReportPassword.Text))
               {
                  try
                  {
                     UserProfile profile = UserProfile.GetUserProfile();
                     profile.ReportPassword = txtReportPassword.Text.Trim();
                     profile.Save();
                     changeSuccess = true;
                  }
                  catch (Exception)
                  {
                     Master.StatusBox.showErrorMessage
                                      ("Unable to update report password credentials." +
                                       "Please try again");
                  }
                  
               }

               if (filAvatar.HasFile)
               {
                  string avatarPath;
                  string avatarPath2;
                  string fileName;
                  string fileExtension = "";

                  fileName = filAvatar.FileName;
                  fileExtension = fileName.Substring(fileName.LastIndexOf('.'));

                  avatarPath = Server.MapPath("~/images/avatars/" + User.Identity.Name.ToLower() + fileExtension);

                  if (File.Exists(avatarPath))
                  {
                     File.Delete(avatarPath);
                  }

                  filAvatar.SaveAs(avatarPath);
                  filAvatar.Dispose();

                  // Resize image to 100 x 100 for display and size issues.
                  System.Drawing.Bitmap avatar = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile(avatarPath);
                  System.Drawing.Image scaledAvatar = Utility.ResizeImage(avatar, 100, 100);

                  // Dispose of objects and remove any existing .jpg/.png files to have a clean avatar directory.
                  filAvatar.Dispose();
                  avatar.Dispose();

                  if (fileExtension == ".png")
                  {
                     avatarPath2 = Server.MapPath("~/images/avatars/" + User.Identity.Name.ToLower() + ".jpg");
                  }
                  else
                  {
                     avatarPath2 = Server.MapPath("~/images/avatars/" + User.Identity.Name.ToLower() + ".png");
                  }

                  File.Delete(avatarPath);
                  File.Delete(avatarPath2);

                  scaledAvatar.Save(avatarPath);                  

                  changeSuccess = true;                  
               }

               if (changeSuccess)
               {
                  ResetForm();
                  BindUser();
                  Master.StatusBox.showStatusMessage
                                   ("<p>User profile updated.</p>" +
                                    "<p>Note: It may take a moment for your " +
                                    "avatar to refresh if you uploaded one.</p>");
               }
               else
               {
                  Master.StatusBox.showErrorMessage
                                  ("<p>Profile update failed.</p>" +
                                   "<p>An error occurred updating your profile. " +
                                   "Please try again.</p>");
               }
            }
            else
            {
               Master.StatusBox.showErrorMessage
                                 ("<p>No information</p>" +
                                  "<p>Please specify a password, reporting credentials, or a" +
                                  "profile picture.</p>");
            }
         }         
      }
   }
}