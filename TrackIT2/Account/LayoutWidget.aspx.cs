﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Text;
using System.Collections.Specialized;
using TrackIT2.Objects.JSONWrapper;

namespace TrackIT2.Account
{
    public partial class LayoutWidget : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.Count > 0) {

                /// <summary>
                /// The JSON data contains 2 attributes - 'layout' and 'data'
                /// </summary>
                /// 
                NameValueCollection data = Request.Form;
                if (!String.IsNullOrEmpty(Request.Form["result"])) {
                    SaveWidgetToProfile(Request.Form["result"].ToString());
                    
                    pageResponseContent(Globals.SUCCESS);
                }
            }
        }

        protected void SaveWidgetToProfile(string sJSONLayout)
        {
            UserProfile profile = UserProfile.GetUserProfile();
            DashboardJSONHelper objJSONHelper = this.Session[Globals.SESSION_JSON_HELPER] as DashboardJSONHelper;
            if (objJSONHelper != null)
            {
                if (objJSONHelper.SetMyWidgetWithJSON(sJSONLayout))
                {
                    profile.UserWidgets = objJSONHelper.GetMyWidgets();
                    profile.Save();

                    //Saving helper back to Session.
                    this.Session[Globals.SESSION_JSON_HELPER] = objJSONHelper;
                }
            }

        }

        /// <summary>
        /// Page Response Content
        /// </summary>
        /// <param name="content"></param>
        protected void pageResponseContent(string message)
        {
            Response.Write(message);
            Response.Flush();
            Response.End();
        }
        //-->
    }
}