﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.Objects;

namespace TrackIT2.Account
{
   public partial class SavedLinks : System.Web.UI.Page
   {
       protected enum eColSaveLink
       {
           Action = 0,
           Info = 1
       }
      protected void Page_Load(object sender, EventArgs e)
      {
          // Hide MasterPage Components, 20 Dec 2011

          Master.FindControl("topNav").Visible = false;
          Master.FindControl("pageSectionHeader").Visible = false;
          Master.FindControl("footer").Visible = false;

          // /Hide

          if (!Page.IsPostBack)
          {
              BindSavedLinks();
          }
      }

      protected void BindSavedLinks()
      {
         UserProfile profile = UserProfile.GetUserProfile();
         TrackIT2.Objects.SavedLinks links = profile.SavedLinks;

         // If no links exist, create an empty one so the GridView footer
         // is visible.
         if (links.LinkItems == null || links.LinkItems.Count == 0)
         {
            links.LinkItems = new List<SavedLink>();
            links.LinkItems.Add(new SavedLink("", "", ""));
         }

         gvLinks.DataSource = links.LinkItems;
         gvLinks.DataBind();
      }

      protected void gvLinks_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Title
          * 2 - Url
          * 3 - Description
          */
         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            SavedLink linkItem = (SavedLink)e.Row.DataItem;                       

            if (e.Row.RowIndex == gvLinks.EditIndex)
            {
               Label lblOrigTitle = (Label)e.Row.Cells[(int)eColSaveLink.Action].FindControl("lblOrigTitle");
               LinkButton updateButton = (LinkButton)e.Row.Cells[(int)eColSaveLink.Action].FindControl("lnbUpdate");
               TextBox txtEditTitle = (TextBox)e.Row.Cells[(int)eColSaveLink.Info].FindControl("txtEditTitle");
               TextBox txtEditUrl = (TextBox)e.Row.Cells[(int)eColSaveLink.Info].FindControl("txtEditUrl");
               TextBox txtEditDescription = (TextBox)e.Row.Cells[(int)eColSaveLink.Info].FindControl("txtEditDescription");

               lblOrigTitle.Text = linkItem.title;
               txtEditTitle.Text = linkItem.title;
               txtEditUrl.Text = linkItem.url;
               txtEditDescription.Text = linkItem.description;
               
            }
            else
            {
                Label lblOrigTitle = (Label)e.Row.Cells[(int)eColSaveLink.Action].FindControl("lblOrigTitle");
                LinkButton lnbEdit = (LinkButton)e.Row.Cells[(int)eColSaveLink.Action].FindControl("lnbEdit");
                LinkButton lnbDelete = (LinkButton)e.Row.Cells[(int)eColSaveLink.Action].FindControl("lnbDelete");

                Label lblSavedLink_title = (Label)e.Row.Cells[(int)eColSaveLink.Info].FindControl("lblSavedLink_title");
                Label lblSavedLink_url = (Label)e.Row.Cells[(int)eColSaveLink.Info].FindControl("lblSavedLink_url");
                Label lblSavedLink_description = (Label)e.Row.Cells[(int)eColSaveLink.Info].FindControl("lblSavedLink_description");
               // If we have an empty title, it is our placeholder, remove the
               // edit/delete links for that row. Also remove the row if we're
               // editing a row
               if (String.IsNullOrEmpty(linkItem.title) || gvLinks.EditIndex > -1)
               {
                  lnbEdit.Visible = false;
                  lnbDelete.Visible = false;
               }

               lblOrigTitle.Text = linkItem.title;
               lblSavedLink_title.Text = linkItem.title;
               lblSavedLink_url.Text = linkItem.url;
               lblSavedLink_description.Text = linkItem.description;
            }
         }
      }

      protected void lnbAddLink_Click(object sender, EventArgs e)
      {
         if (Page.IsValid)
         {
            UserProfile profile = UserProfile.GetUserProfile();
            TrackIT2.Objects.SavedLinks links = profile.SavedLinks;
            SavedLink newLink = new SavedLink();
            TextBox txtAddTitle = (TextBox)gvLinks.FooterRow.FindControl("txtAddTitle");
            TextBox txtAddUrl = (TextBox)gvLinks.FooterRow.FindControl("txtAddUrl");
            TextBox txtAddDescription = (TextBox)gvLinks.FooterRow.FindControl("txtAddDescription");
            bool titleAlreadyExists = false;

            if (links.LinkItems == null || links.LinkItems.Count == 0)
            {
               links.LinkItems = new List<SavedLink>();               
            }

            newLink.title = txtAddTitle.Text;
            newLink.url = txtAddUrl.Text;

            if (!string.IsNullOrEmpty(txtAddDescription.Text))
            {
               newLink.description = txtAddDescription.Text;
            }

            foreach (SavedLink currLink in links.LinkItems)
            {
               if (currLink.title == newLink.title)
               {
                  titleAlreadyExists = true;
                  break;
               }
            }

            if (!titleAlreadyExists)
            {
               links.LinkItems.Add(newLink);

               profile.SavedLinks = links;
               profile.Save();

               BindSavedLinks();
               Master.StatusBox.showStatusMessage("Link added.");
            }
            else
            {
               Master.StatusBox.showErrorMessage("Error: Link already exists with title specified.");
            }            
         }           
      }

      protected void gvLinks_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
      {
         gvLinks.EditIndex = -1;
         gvLinks.ShowFooter = true;
         BindSavedLinks();
         Master.StatusBox.showStatusMessage("<p>Edit Cancelled.</p>");
      }

      protected void gvLinks_RowDeleting(object sender, GridViewDeleteEventArgs e)
      {
         UserProfile profile = UserProfile.GetUserProfile();
         TrackIT2.Objects.SavedLinks links = profile.SavedLinks;
         Label lblOrigTitle;
         string titleToDelete;

         lblOrigTitle = (Label)gvLinks.Rows[e.RowIndex].FindControl("lblOrigTitle");
         titleToDelete = lblOrigTitle.Text;
         
         foreach (SavedLink currLink in links.LinkItems)
         {
            if (currLink.title == titleToDelete)
            {
               links.LinkItems.Remove(currLink);
               break;
            }
         }

         profile.SavedLinks = links;
         profile.Save();

         BindSavedLinks();

         Master.StatusBox.showStatusMessage("Link updated.");
      }

      protected void gvLinks_RowEditing(object sender, GridViewEditEventArgs e)
      {
         if (gvLinks.EditIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Please cancel the current edit before continuing.");
            e.Cancel = true;
         }
         else
         {
            gvLinks.EditIndex = e.NewEditIndex;
            gvLinks.ShowFooter = false;
            BindSavedLinks();
         }

      }

      protected void gvLinks_RowUpdating(object sender, GridViewUpdateEventArgs e)
      {
         if (Page.IsValid)
         {
            UserProfile profile = UserProfile.GetUserProfile();
            TrackIT2.Objects.SavedLinks links = profile.SavedLinks;
            Label lblOrigTitle;
            TextBox txtEditTitle;
            TextBox txtEditUrl;
            TextBox txtEditDescription;
            bool titleAlreadyExists = false;

            lblOrigTitle = (Label)gvLinks.Rows[gvLinks.EditIndex].FindControl("lblOrigTitle");
            txtEditTitle = (TextBox)gvLinks.Rows[gvLinks.EditIndex].FindControl("txtEditTitle");
            txtEditUrl = (TextBox)gvLinks.Rows[gvLinks.EditIndex].FindControl("txtEditUrl");
            txtEditDescription = (TextBox)gvLinks.Rows[gvLinks.EditIndex].FindControl("txtEditDescription");

            if (lblOrigTitle.Text == txtEditTitle.Text)
            {
               foreach (SavedLink currLink in links.LinkItems)
               {
                  if (currLink.title == lblOrigTitle.Text)
                  {
                     currLink.url = txtEditUrl.Text;

                     if (!string.IsNullOrEmpty(txtEditDescription.Text))
                     {
                        currLink.description = txtEditDescription.Text;
                     }
                     break;
                  }
               }

               profile.SavedLinks = links;
               profile.Save();

               gvLinks.EditIndex = -1;
               BindSavedLinks();

               Master.StatusBox.showStatusMessage("Link updated.");
            }
            else
            {
               foreach (SavedLink currLink in links.LinkItems)
               {
                  if (currLink.title == txtEditTitle.Text)
                  {
                     titleAlreadyExists = true;
                     break;
                  }
               }

               if (!titleAlreadyExists)
               {
                  foreach (SavedLink currLink in links.LinkItems)
                  {
                     if (currLink.title == lblOrigTitle.Text)
                     {
                        currLink.title = txtEditTitle.Text;
                        currLink.url = txtEditUrl.Text;

                        if (!string.IsNullOrEmpty(txtEditDescription.Text))
                        {
                           currLink.description = txtEditDescription.Text;
                        }

                        break;
                     }
                  }

                  profile.SavedLinks = links;
                  profile.Save();

                  gvLinks.EditIndex = -1;
                  BindSavedLinks();
                  Master.StatusBox.showStatusMessage("Link updated.");
               }
               else
               {
                  Master.StatusBox.showErrorMessage("Error: Link already exists with title specified.");
               }
            }
         }
      }

      //protected void updateSavedLink_Click(object sender, EventArgs e)
      //{
      //    //Master.StatusBox.showStatusMessage("click updated.");

      //    UserProfile profile = UserProfile.GetUserProfile();
      //    TrackIT2.Objects.SavedLinks links = profile.SavedLinks;
      //    string lblOrigTitle;
      //    TextBox txtEditTitle;
      //    TextBox txtEditUrl;
      //    TextBox txtEditDescription;
      //    bool titleAlreadyExists = false;

      //    lblOrigTitle = hidden_old_title.Value;
      //    txtEditTitle = txtSavedLink_title;
      //    txtEditUrl = txtSavedLink_url;
      //    txtEditDescription = txtSavedLink_description;

      //    if (lblOrigTitle == txtEditTitle.Text)
      //    {
      //        foreach (SavedLink currLink in links.LinkItems)
      //        {
      //            if (currLink.title == lblOrigTitle)
      //            {
      //                currLink.url = txtEditUrl.Text;

      //                if (!string.IsNullOrEmpty(txtEditDescription.Text))
      //                {
      //                    currLink.description = txtEditDescription.Text;
      //                }
      //                break;
      //            }
      //        }

      //        profile.SavedLinks = links;
      //        profile.Save();

      //        gvLinks.EditIndex = -1;
      //        BindSavedLinks();

      //        Master.StatusBox.showStatusMessage("Link updated.");
      //    }
      //    else
      //    {
      //        foreach (SavedLink currLink in links.LinkItems)
      //        {
      //            if (currLink.title == txtEditTitle.Text)
      //            {
      //                titleAlreadyExists = true;
      //                break;
      //            }
      //        }

      //        if (!titleAlreadyExists)
      //        {
      //            foreach (SavedLink currLink in links.LinkItems)
      //            {
      //                if (currLink.title == lblOrigTitle)
      //                {
      //                    currLink.title = txtEditTitle.Text;
      //                    currLink.url = txtEditUrl.Text;

      //                    if (!string.IsNullOrEmpty(txtEditDescription.Text))
      //                    {
      //                        currLink.description = txtEditDescription.Text;
      //                    }

      //                    break;
      //                }
      //            }

      //            profile.SavedLinks = links;
      //            profile.Save();

      //            gvLinks.EditIndex = -1;
      //            BindSavedLinks();
      //            Master.StatusBox.showStatusMessage("Link updated.");
      //        }
      //        else
      //        {
      //            Master.StatusBox.showErrorMessage("Error: Link already exists with title specified.");
      //        }
      //    }
      //}

      //protected void cancelSavedLink_Click(object sender, EventArgs e)
      //{
      //    gvLinks.EditIndex = -1;
      //    gvLinks.ShowFooter = true;
      //    BindSavedLinks();
      //    Master.StatusBox.showStatusMessage("<p>Edit Cancelled.</p>");
      //}
  
   }
}