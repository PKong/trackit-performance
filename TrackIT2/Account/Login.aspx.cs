﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;

namespace TrackIT2.Account
{
   public partial class Login : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         // If the user is already logged in, we'll have the authentication 
         // cookie for them and we can simply forward them on to the proper 
         // destination.
         if (User.Identity.IsAuthenticated)
         {
            // We do need to track the user's Id and name in the session
            // for display purposes. Check to make sure these items are
            // there before continuing.
            if (Session["userId"] == null)
            {
               using (CPTTEntities ce = new CPTTEntities())
               {
                  user userInfo;

                  var currUser = from u in ce.users
                                 join au in ce.aspnet_Users on u.aspnet_user_id equals au.UserId
                                 where au.UserName == User.Identity.Name
                                 select u;

                  userInfo = currUser.First();

                  Session["userId"] = userInfo.id;
                  Session["userDisplayName"] = userInfo.first_name + " " + userInfo.last_name;
               }               
            }

            // If user is only in the TrackiT Docs User or TrackiT Docs Admin role, redirect them
            // to the DMS page
            if (Roles.GetRolesForUser(User.Identity.Name).Count() == 1 &&
               (User.IsInRole("TrackiT Docs User") || User.IsInRole("TrackiT Docs Administrator")))
            {
               Response.Redirect("~/DMS/Default.aspx");
            }
            
            // User may have a ReturnUrl parameter in which to redirect to.
            if (Request["ReturnUrl"] != null)
            {
               Response.Redirect(Request["ReturnUrl"], true);
            }
            else
            {
               // If the user is authenticated, there is no need to go back to
               // the login page, although the site may be trying to redirect 
               // the user there.
               if (Request.Url.ToString().Contains("Login.aspx"))
               {
                  Response.Redirect("~/Default.aspx", true);
               }
               else
               {
                  Response.Redirect(Request.Url.ToString(), true);
               }
            }
         }
      }

      protected void LoginUser_LoginError(object sender, EventArgs e)
	   {
		   this.lblMessage.Text = this.LoginUser.FailureText;
	   }

      protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
      {
         // Since the user can log in with either their TMO Id or their e-mail
         // address, we intercept the login process to find the username if an
         // e-mail address is specified.
         string userName;

         if (LoginUser.UserName.Contains("@"))
         {
            userName = Membership.GetUserNameByEmail(LoginUser.UserName);
         }
         else
         {
            userName = LoginUser.UserName;
         }

         LoginUser.UserName = userName;

         if (Membership.ValidateUser(userName, LoginUser.Password))
         {
            e.Authenticated = true;
         
            // Save user's ID and name in the session for tracking purposes.
            CPTTEntities ce = new CPTTEntities();
            user userInfo;

            var currUser = from u in ce.users
            join au in ce.aspnet_Users on u.aspnet_user_id equals au.UserId
            where au.UserName == LoginUser.UserName
            select u;

            userInfo = currUser.First();

            Session["userId"] = userInfo.id;
            Session["userDisplayName"] = userInfo.first_name + " " + userInfo.last_name;
         }
         else
         {
            e.Authenticated = false;
         }            
      }

      protected void LoginUser_LoggedIn(object sender, EventArgs e)
      {
         string homePage;
         string[] userRoles = Roles.GetRolesForUser(LoginUser.UserName);

         // The user hasn't had their credentials stored in the User object
         // yet, but since we have their login name, we can retrieve their
         // roles and set their home page accordingly.
         //
         // Users that are solely in the TrackiT Docs User or TrackiT Docs Admin role are 
         // redirected to the DMS page.
         if (
             userRoles.Count() == 1 &&
             (userRoles[0] == "TrackiT Docs User" || userRoles[0] == "TrackiT Docs Administrator")
            )
         {
            homePage = "~/DMS/Default.aspx";
         }
         else
         {
            homePage = "~/Default.aspx";
         }
         
         FormsAuthentication.SetAuthCookie(LoginUser.UserName, LoginUser.RememberMeSet);
         Response.Redirect(homePage);
         
         //FormsAuthentication.RedirectFromLoginPage(LoginUser.UserName, LoginUser.RememberMeSet);
      }
   }
}


