﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="SavedFilters.aspx.cs" Inherits="TrackIT2.Account.SavedFilters" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2>Saved Filters</h2>

   <p>
      <b>Filter 1: </b>
      <asp:TextBox ID="txtSavedFilter1" runat="server"></asp:TextBox>
      <br />
      <b>Filter 2: </b>
      <asp:TextBox ID="txtSavedFilter2" runat="server"></asp:TextBox>
      <br />
      <b>Filter 3: </b>
      <asp:TextBox ID="txtSavedFilter3" runat="server"></asp:TextBox>
   </p>

   <p>&nbsp;</p>

   <p>
      <asp:Button ID="btnUpdate" runat="server" Text="Update Profile" onclick="btnUpdate_Click" />
   </p>

   <p>&nbsp;</p>

   <p>
      <a href="Profile.aspx">Back to Profile</a>
   </p>
</asp:Content>