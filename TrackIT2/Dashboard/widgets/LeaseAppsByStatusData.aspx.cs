﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using TrackIT2.DAL;

namespace TrackIT2.Dashboard.widgets
{
   public partial class LeaseAppsByStatusData : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         Response.Cache.SetCacheability(HttpCacheability.NoCache);
         
         // Create a new XmlTextWriter instance that writes directly to the
         // output stream.
         Response.ContentType = "text/xml";
         XmlTextWriter writer = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);
         List<leaseapp_statuses> statusNames = new List<leaseapp_statuses>();
         int appCount;
         
         using (CPTTEntities ce = new CPTTEntities())
         {

             //Getting all status except : Installed,Dead,Withdrawn,Terminated,Assigned and Inactive.
             var queryLeaseAppStatus = from st in ce.leaseapp_statuses
                                       where st.id == 1 || st.id == 3 || st.id == 7
                                       select st;
             if (queryLeaseAppStatus.ToList().Count > 0)
             {
                 statusNames.AddRange(queryLeaseAppStatus.ToList<leaseapp_statuses>());
             }

            writer.WriteStartDocument();

            // Creating the <chart> element
            writer.WriteStartElement("chart");

            // Creating the <categories> element
            writer.WriteStartElement("categories");

            foreach (leaseapp_statuses currStatus in statusNames)
            {               
               writer.WriteElementString("item", currStatus.lease_application_status);
            }

            // Creating the data elements
            foreach (leaseapp_statuses currStatus in statusNames)
            {
               appCount = ce.lease_applications
                            .Where(la => la.leaseapp_status_id.Value.Equals(currStatus.id))
                            .Count(); 
               
               writer.WriteStartElement("series");
               writer.WriteElementString("name", currStatus.lease_application_status);
               writer.WriteStartElement("data");
               writer.WriteElementString("point", appCount.ToString());
               writer.WriteEndElement();
               writer.WriteEndElement();
            }

            // Close <chart> element and document
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();         
         }
      }
   }
}