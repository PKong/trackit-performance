﻿using Newtonsoft.Json.Linq;
using System.Linq;
using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using TrackIT2.BLL;
using TrackIT2.Objects;

namespace TrackIT2.Dashboard.widgets
{
    public partial class MySavedFiltersListViewDataProvider : System.Web.UI.Page
    {
        // Var
        private int resultsPerPage = 20;
        private int resultsCurrentPage = 1;
        private int resultsTotalPages = 0;
        //
        private int repeaterRowCurrent = 0;
        private string[] repeaterColumnNames;
        private string[] repeaterColumnIndexes;
        private string repeaterDataHandler;
        private string repeaterDetailUrl;
        private const string paddingParams = "param1=null&param2=null&param3=null&param4=null";
        //>

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
           Response.Cache.SetCacheability(HttpCacheability.NoCache); 
           
           // Check Form Collection
            if (Request.Form.Count > 0)
            {
                if ((!String.IsNullOrEmpty(Request.Form["title"])) && (!String.IsNullOrEmpty(Request.Form["currentListViewName"])))
                {

                    string currentListViewName = Request.Form["currentListViewName"];
                    string filterFilters = null;
                    string title = HttpUtility.UrlDecode(Request.Form["title"]);
                    resultsCurrentPage = int.Parse(Request.Form["page"]);
                    string postData = paddingParams + "&page=" + resultsCurrentPage.ToString() + "&rp=" + resultsPerPage.ToString() +
                        "&" + Globals.QS_SORT_DIRECTION + "=" + Globals.SORT_DIRECTION_VALUE_ASC;

                    // Get the custom filters
                    UserProfile profile = UserProfile.GetUserProfile();
                    SavedFilters filters = profile.SavedFilters;
                    if (profile.SavedFilters.SavedFilterItems != null)
                    {
                       foreach (SavedFilter filter in profile.SavedFilters.SavedFilterItems)
                       {
                          if (filter.title.Equals(title))
                          {
                             filterFilters = filter.filter;
                             String[] arrayFilters = HttpUtility.UrlDecode(filterFilters).Split('|');
                             for (int i = 0; i < arrayFilters.Length; i += 2)
                             {
                                // Var
                                String fieldID = arrayFilters[i];
                                String field = fieldID.Substring(3, fieldID.Length - 3);
                                String fieldValue = arrayFilters[i + 1];

                                postData += "&" + field + "=" + fieldValue;
                             }

                             currentListViewName = filter.listView;
                             break;
                          }
                       }
                    }
                    //-->

                    if (System.Text.RegularExpressions.Regex.IsMatch(title, ".*Comment"))
                    {
                        #region Comment
                        var tmpCurrent = currentListViewName.Split('|');
                        bool isAssignFlag = false;
                        if (currentListViewName != "null|false")
                        {
                            if (tmpCurrent.Length > 1)
                            {
                                currentListViewName = tmpCurrent[0];
                                isAssignFlag = bool.Parse(tmpCurrent[1]);
                            }
                        }
                        else
                        {
                            foreach (var saveWidget in profile.UserWidgets.MyWidgetsResult.result.data)
                            {
                                if (saveWidget.title.Contains("Comment"))
                                {
                                    if (saveWidget.title.Contains("Lease"))
                                    {
                                        currentListViewName = "LeaseApplicationsComment";
                                        isAssignFlag = bool.Parse(saveWidget.editurl);
                                        break;
                                    }
                                    else if (saveWidget.title.Contains("Structural"))
                                    {
                                        currentListViewName = "StructuralAnalysisComment";
                                        isAssignFlag = bool.Parse(saveWidget.editurl);
                                        break;
                                    }
                                    else if (saveWidget.title.Contains("Tower"))
                                    {
                                        currentListViewName = "TowerModificationComment";
                                        isAssignFlag = bool.Parse(saveWidget.editurl);
                                        break;
                                    }
                                }
                            }
                        }

                        switch(currentListViewName)
                        {
                            case "LeaseApplicationsComment":
                                if (isAssignFlag)
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + SavedFilterListViews.LeaseApplicationsComment.DetailUrl+"&flag=true");
                                }
                                else
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + SavedFilterListViews.LeaseApplicationsComment.DetailUrl);
                                }
                                break;
                            case "StructuralAnalysisComment":
                                if (isAssignFlag)
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + SavedFilterListViews.StructuralAnalysisComment.DetailUrl + "&flag=true");
                                }
                                else
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + SavedFilterListViews.StructuralAnalysisComment.DetailUrl);
                                }
                                break;
                            case "TowerModificationComment":
                                if (isAssignFlag)
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + SavedFilterListViews.TowerModificationComment.DetailUrl + "&flag=true");
                                }
                                else
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + SavedFilterListViews.TowerModificationComment.DetailUrl);
                                }
                                break;
                            case "AllRecentComment":
                            default:
                                if (isAssignFlag)
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + "dashboard/widgets/RecentComments.aspx?type=al&flag=true");
                                }
                                else
                                {
                                    Response.Redirect(Utility.BaseSiteUrl + "dashboard/widgets/RecentComments.aspx?type=al");
                                }
                                
                                break;
                        }
                        #endregion Comment
                    }
                    else if (System.Text.RegularExpressions.Regex.IsMatch(title, ".*Document"))
                    {
                        #region Document
                        var tmpCurrent = currentListViewName.Split('|');
                        bool isAssignFlag = false;
                        if (currentListViewName != "null")
                        {
                            if (tmpCurrent.Length > 1)
                            {
                                currentListViewName = tmpCurrent[0];
                                isAssignFlag = bool.Parse(tmpCurrent[1]);
                            }
                        }
                        else
                        {
                            foreach (var saveWidget in profile.UserWidgets.MyWidgetsResult.result.data)
                            {
                                if (saveWidget.title.Contains("Document"))
                                {
                                    if (saveWidget.title.Contains("Lease"))
                                    {
                                        currentListViewName = "LeaseApplicationsMissingDocument";
                                        break;
                                    }
                                    else if (saveWidget.title.Contains("Site"))
                                    {
                                        currentListViewName = "SiteDataPackageMissingDocument";
                                        break;
                                    }
                                    else if (saveWidget.title.Contains("Tower"))
                                    {
                                        currentListViewName = "TowerModificationMissingDocument";
                                        break;
                                    }
                                    else
                                    {
                                        currentListViewName = "AllMissingDocument";
                                        break;
                                    }
                                }
                            }
                        }

                        switch (currentListViewName)
                        {
                            case "LeaseApplicationsMissingDocument":
                                postData += "&type=" + SavedFilterListViews.LeaseApplicationsMissingDocument.Name;
                                break;
                            case "SiteDataPackageMissingDocument":
                                postData += "&type=" + SavedFilterListViews.SiteDataPackageMissingDocument.Name;
                                break;
                            case "TowerModificationMissingDocument":
                                postData += "&type=" + SavedFilterListViews.TowerModificationMissingDocument.Name;
                                break;
                            case "AllMissingDocument":
                            default:
                                currentListViewName = "AllMissingDocument";
                                postData += "&type=all";
                                break;

                        }

                        if (setupRepeater(currentListViewName))
                        {
                            // load My Saved Filters Data
                            loadMySavedFiltersData(repeaterDataHandler, postData);
                            // Save Nav Links State
                            SaveNavLinksState();
                        }
                        #endregion Document
                    }
                    else if (setupRepeater(currentListViewName))
                    {
                        // load My Saved Filters Data
                        loadMySavedFiltersData(repeaterDataHandler, postData);
                        // Save Nav Links State
                        SaveNavLinksState();
                    }
                }
            }
        }
        //-->

        /// <summary>
        /// Save Nav Links State
        /// </summary>
        protected void SaveNavLinksState()
        {
            hfWidgetNavState.InnerText = resultsPerPage.ToString() + "|" + resultsCurrentPage.ToString() + "|" + resultsTotalPages.ToString();
        }
        //-->

        /// <summary>
        /// setup Repeater
        /// </summary>
        /// <param name="currentListViewName"></param>
        /// <returns></returns>
        protected bool setupRepeater(string currentListViewName)
        {
            bool isValid = false;

            switch (currentListViewName)
            {
                // Lease Apps
                case SavedFilterListViews.LeaseApps.Name:
                    repeaterDataHandler = SavedFilterListViews.LeaseApps.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.LeaseApps.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.LeaseApps.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.LeaseApps.DetailUrl;
                    isValid = true;
                    break;

                // Sites
                case SavedFilterListViews.Sites.Name:
                    repeaterDataHandler = SavedFilterListViews.Sites.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.Sites.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.Sites.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.Sites.DetailUrl;
                    isValid = true;
                    break;

                // TowerMod
                case SavedFilterListViews.TowerMod.Name:
                    repeaterDataHandler = SavedFilterListViews.TowerMod.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.TowerMod.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.TowerMod.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.TowerMod.DetailUrl;
                    isValid = true;
                    break;

                // SDP
                case SavedFilterListViews.SDP.Name:
                    repeaterDataHandler = SavedFilterListViews.SDP.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.SDP.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.SDP.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.SDP.DetailUrl;
                    isValid = true;
                    break;
                //comment
                case "LeaseApplicationsComment":
                    repeaterDetailUrl = "le";
                    repeaterDataHandler = SavedFilterListViews.LeaseApplicationsComment.DataHandler;
                    isValid = true;
                    break;
                case "TowerModificationComment":
                    repeaterDetailUrl = "to";
                    repeaterDataHandler = SavedFilterListViews.TowerModificationComment.DataHandler;
                    isValid = true;
                    break;

                case "StructuralAnalysisComment":
                    repeaterDetailUrl = "to";
                    repeaterDataHandler = SavedFilterListViews.StructuralAnalysisComment.DataHandler;
                    isValid = true;
                    break;
                // Document
                case "AllMissingDocument":
                    repeaterDetailUrl = "leaseApp";
                    repeaterDataHandler = SavedFilterListViews.AllMissingDocument.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.AllMissingDocument.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.AllMissingDocument.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.LeaseApplicationsMissingDocument.DetailUrl;

                    isValid = true;
                    break;
                case "LeaseApplicationsMissingDocument":
                    repeaterDetailUrl = "leaseApp";
                    repeaterDataHandler = SavedFilterListViews.LeaseApplicationsMissingDocument.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.LeaseApplicationsMissingDocument.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.LeaseApplicationsMissingDocument.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.LeaseApplicationsMissingDocument.DetailUrl;

                    isValid = true;
                    break;
                case "TowerModificationMissingDocument":
                    repeaterDetailUrl = "towerMod";
                    repeaterDataHandler = SavedFilterListViews.TowerModificationMissingDocument.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.TowerModificationMissingDocument.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.TowerModificationMissingDocument.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.TowerModificationMissingDocument.DetailUrl;
                    isValid = true;
                    break;

                case "SiteDataPackageMissingDocument":
                    repeaterDetailUrl = "sdp";
                    repeaterDataHandler = SavedFilterListViews.SiteDataPackageMissingDocument.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.SiteDataPackageMissingDocument.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.SiteDataPackageMissingDocument.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.SiteDataPackageMissingDocument.DetailUrl;
                    isValid = true;
                    break;

                case SavedFilterListViews.LeaseAdmin.Name:
                    repeaterDataHandler = SavedFilterListViews.LeaseAdmin.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.LeaseAdmin.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.LeaseAdmin.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.LeaseAdmin.DetailUrl;
                    isValid = true;
                    break;

                case SavedFilterListViews.Issue.Name:
                    repeaterDataHandler = SavedFilterListViews.Issue.DataHandler;
                    repeaterColumnNames = getStringArrayFromString(SavedFilterListViews.Issue.ColumnNames);
                    repeaterColumnIndexes = getStringArrayFromString(SavedFilterListViews.Issue.ColumnIndexes);
                    repeaterDetailUrl = SavedFilterListViews.Issue.DetailUrl;
                    isValid = true;
                    break;
            }

            return isValid;
        }

        // Get Column Names From String
        private string[] getStringArrayFromString(string columnNames)
        {
            return columnNames.Split('|');
        }
        //-->

        /// <summary>
        /// Load My Saved Filters Data
        /// </summary>
        /// <param name="postData"></param>
        protected void loadMySavedFiltersData(string dataHandlerName, string postData)
        {
            try
            {
                // Get the Data from Handler
                HttpWebResponse httpWebResponse = Utility.WebRequestPostOrGet(HttpContext.Current, dataHandlerName, Globals.CONTENT_TYPE_FORM, postData, Globals.REQUEST_METHOD_POST);
                string jsonResultString = Utility.GetDataFromWebResponse(httpWebResponse);
                JToken token = JObject.Parse(jsonResultString);

                resultsTotalPages = (int)token["total"];
                JArray obj = (JArray)token["rows"];

                // Bind Repeater
                if (obj.Count > 0)
                {
                    rptListView.DataSource = obj.ToList();
                }
                else
                {
                    rptListView.DataSource = null;
                }

                rptListView.DataBind();
            }
            catch (Exception ex)
            {
                // Error!
                pageResponseContent(Globals.ERROR + " " + ex.ToString());
            }
        }
        //-->

        /// <summary>
        /// rptListView OnDataBinding
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptListView_OnDataBinding(object sender, EventArgs e)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(sw);
            Literal lt = (Literal)(sender);

            string id = Eval("id").ToString();
            JArray cell = (JArray)Eval("cell");

            // Initial Row - Build Table Header
            if (repeaterRowCurrent == 0)
            {
                writer.WriteBeginTag("thead");
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.WriteLine();
                writer.Indent++;

                writer.WriteBeginTag("tr");
                writer.Write(HtmlTextWriter.TagRightChar);
                writer.WriteLine();
                writer.Indent++;

                foreach (string title in repeaterColumnNames)
                {
                    writer.WriteBeginTag("td");
                    writer.Write(HtmlTextWriter.TagRightChar);
                    writer.Write(title);
                    writer.WriteEndTag("td");
                }

                writer.WriteLine();
                writer.Indent--;
                writer.WriteEndTag("tr");
                writer.WriteLine();

                writer.Indent--;
                writer.WriteEndTag("thead");
            }
            //>

            // Normal Table Row
            writer.WriteBeginTag("tr");
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
            writer.Indent++;

            bool firstCol = true;
            
            foreach (string index in repeaterColumnIndexes)
            {
                writer.WriteBeginTag("td");
                writer.Write(HtmlTextWriter.TagRightChar);
                if (firstCol && repeaterDataHandler != "MissingDocumentsList.axd")
                {
                    string link = cell[int.Parse(index)].ToString();
                    if (link.IndexOf("javascript") > 0)
                    {
                        cell[int.Parse(index)] = link.Replace("<a href='javascript:void(0)' >", "").Replace("</a>", "");
                    }

                    writer.WriteBeginTag("a");
                    writer.WriteAttribute("href", String.Format(repeaterDetailUrl, id));
                    if (repeaterDataHandler == "IssueTrackerList.axd")
                    {
                        writer.WriteAttribute("title", "View details for TMIT-" + cell[int.Parse(index)].ToString());
                    }
                    else
                    {
                        writer.WriteAttribute("title", "View details for " + cell[int.Parse(index)].ToString());
                    }
                    writer.Write(HtmlTextWriter.TagRightChar);
                }
                //writer.Write(cell[int.Parse(index)].ToString());

                if (firstCol)
                {
                    string cellData = cell[int.Parse(index)].ToString();

                    if (repeaterDataHandler == "IssueTrackerList.axd")
                    {
                        cellData = "TMIT-" + cellData;
                    }
                    writer.Write(cellData);

                    if (repeaterDataHandler != "MissingDocumentsList.axd")
                    {
                        writer.WriteEndTag("a");
                    }
                    firstCol = false;
                }
                else
                {
                    if (repeaterDataHandler == "MissingDocumentsList.axd" && index == "1")
                    {
                        string link = cell[int.Parse(index)].ToString();
                        if (link.IndexOf("javascript") > 0)
                        {
                            cell[int.Parse(index)] = link.Replace("<a href='javascript:void(0)' >", "").Replace("</a>", "");
                        }

                        string recordId = cell[2].ToString();
                        switch (cell[int.Parse(index)].ToString())
                        {
                            case "Lease Application":
                            case "Structural Analysis":
                                repeaterDetailUrl = "/LeaseApplications/Edit.aspx?id={0}";
                                break;
                            case "Tower Modification":
                                repeaterDetailUrl = "/TowerModifications/Edit.aspx?id={0}";
                                break;
                            case "Site Data Package":
                                repeaterDetailUrl = "/SiteDataPackages/Edit.aspx?id={0}";
                                break;

                        }

                        writer.WriteBeginTag("a");
                        writer.WriteAttribute("href", String.Format(repeaterDetailUrl, recordId));
                        writer.WriteAttribute("title", "View details for " + cell[int.Parse(index)].ToString());
                        writer.Write(HtmlTextWriter.TagRightChar);
                        string cellData = cell[int.Parse(index)].ToString();
                        writer.Write(cellData);
                        writer.WriteEndTag("a");
                    }
                    else
                    {
                        writer.Write(cell[int.Parse(index)].ToString());
                    }
                }
                writer.WriteEndTag("td");
            }

            writer.WriteLine();
            writer.Indent--;
            writer.WriteEndTag("tr");
            writer.WriteLine();
            //>

            repeaterRowCurrent++;
            lt.Text = sw.ToString();
        }
        //-->

        /// <summary>
        /// Saved Filter List Views
        /// </summary>
        public struct SavedFilterListViews
        {
            public struct LeaseApps
            {
                public const String Name = "Lease Applications";
                public const String DataHandler = "LeaseAppList.axd";
                public const String ColumnNames = "SiteID|Site Name|Customer";
                public const String ColumnIndexes = "0|1|2";
                public const String DetailUrl = "/LeaseApplications/Edit.aspx?id={0}";
            }

            public struct Sites
            {
                public const String Name = "Sites";
                public const String DataHandler = "SitesList.axd";
                public const String ColumnNames = "SiteID|Site Name|Market Name";
                public const String ColumnIndexes = "0|1|2";
                public const String DetailUrl = "/Sites/SiteDashboard.aspx?id={0}";
            }

            public struct TowerMod
            {
                public const String Name = "Tower Modifications";
                public const String DataHandler = "TowerModList.axd";
                public const String ColumnNames = "SiteID|Customer|Tower Mod Status";
                public const String ColumnIndexes = "0|1|2";
                public const String DetailUrl = "/TowerModifications/Edit.aspx?id={0}";
            }

            public struct SDP
            {
                public const String Name = "Site Data Packages";
                public const String DataHandler = "SdpList.axd";
                public const String ColumnNames = "SiteID|Site Region Name|Site On Air Date";
                public const String ColumnIndexes = "0|1|3";
                public const String DetailUrl = "/SiteDataPackages/Edit.aspx?id={0}";
            }

            public struct Issue
            {
                public const String Name = "Issue Tracker";
                public const String DataHandler = "IssueTrackerList.axd";
                public const String ColumnNames = "Issue ID|Site Name|Status";
                public const String ColumnIndexes = "0|2|6";
                public const String DetailUrl = "/IssueTracker/Edit.aspx?id={0}";

            }

            #region Comment
            public struct LeaseApplicationsComment
            {
                public const String Name = "LeaseApplicationsComment";
                public const String DataHandler = "RecentComments.aspx";
                public const String ColumnNames = "";
                public const String ColumnIndexes = "";
                public const String DetailUrl = "dashboard/widgets/RecentComments.aspx?type=le";
            }

            public struct TowerModificationComment
            {
                public const String Name = "TowerModificationComment";
                public const String DataHandler = "RecentComments.aspx";
                public const String ColumnNames = "";
                public const String ColumnIndexes = "";
                public const String DetailUrl = "dashboard/widgets/RecentComments.aspx?type=to";
            }

            public struct StructuralAnalysisComment
            {
                public const String Name = "StructuralAnalysisComment";
                public const String DataHandler = "RecentComments.aspx";
                public const String ColumnNames = "";
                public const String ColumnIndexes = "";
                public const String DetailUrl = "dashboard/widgets/RecentComments.aspx?type=sa";
            }
            #endregion Comment

            #region Document

            public struct AllMissingDocument
            {
                public const String Name = "AllMissingDocument";
                public const String DataHandler = "MissingDocumentsList.axd";
                public const String ColumnNames = "Site ID|Record Type|Field Name";
                public const String ColumnIndexes = "0|1|3";
                public const String DetailUrl = "/Edit.aspx?id={0}";
            }
            public struct LeaseApplicationsMissingDocument
            {
                public const String Name = "LeaseApplicationsMissingDocument";
                public const String DataHandler = "MissingDocumentsList.axd";
                public const String ColumnNames = "Site ID|Record Type|Field Name";
                public const String ColumnIndexes = "0|1|3";
                public const String DetailUrl = "/LeaseApplications/Edit.aspx?id={0}";
            }

            public struct TowerModificationMissingDocument
            {
                public const String Name = "TowerModificationMissingDocument";
                public const String DataHandler = "MissingDocumentsList.axd";
                public const String ColumnNames = "Site ID|Record Type|Field Name";
                public const String ColumnIndexes = "0|1|3";
                public const String DetailUrl = "/TowerModifications/Edit.aspx?id={0}";
            }

            public struct SiteDataPackageMissingDocument
            {
                public const String Name = "SiteDataPackageMissingDocument";
                public const String DataHandler = "MissingDocumentsList.axd";
                public const String ColumnNames = "Site ID|Record Type|Field Name";
                public const String ColumnIndexes = "0|1|3";
                public const String DetailUrl = "/SiteDataPackages/Edit.aspx?id={0}";
            }
            #endregion Document

            public struct LeaseAdmin
            {
                public const String Name = "Lease Admin Tracker";
                public const String DataHandler = "LeaseAdminTrackerList.axd";
                public const String ColumnNames = "SiteID|Site Name|Customer";
                public const String ColumnIndexes = "1|2|0";
                public const String DetailUrl = "/LeaseAdminTracker/Edit.aspx?id={0}";
            }
        }
        //-->

        /// <summary>
        /// Page Response Content
        /// </summary>
        /// <param name="content"></param>
        protected void pageResponseContent(string message)
        {
            Response.Write(message);
            Response.Flush();
            Response.End();
        }
        //-->
    }
}