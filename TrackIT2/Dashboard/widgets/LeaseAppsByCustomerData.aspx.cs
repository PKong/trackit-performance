﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using TrackIT2.DAL;

namespace TrackIT2.Dashboard.widgets
{
   public partial class LeaseAppsByCustomerData : System.Web.UI.Page
   {
      
       //New page load with top 5 customers who have most active leaseapplication.
       protected void Page_Load(object sender, EventArgs e)
       {
          Response.Cache.SetCacheability(HttpCacheability.NoCache); 
          
          // Create a new XmlTextWriter instance that writes directly to the
           // output stream.
           Response.ContentType = "text/xml";
           XmlTextWriter writer = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);

           using (CPTTEntities ce = new CPTTEntities())
           {
               //Select top5 from lease_applications group by customer_id orderby Count(customer_id) DESC
               var queryCustomer = (from la in ce.lease_applications
                                    group la by la.customer_id into g
                                    orderby g.Count() descending
                                    select new CustomCustomer
                                    {
                                        Name = g.FirstOrDefault().customer.customer_name,
                                        Count = g.Count()
                                    }).Take(5);
                                   
                                   
               writer.WriteStartDocument();

               // Creating the <chart> element
               writer.WriteStartElement("chart");

               // Creating the <categories> element
               writer.WriteStartElement("categories");

               foreach (CustomCustomer item in queryCustomer)
               {
                   writer.WriteElementString("item", item.Name);
               }
               // Creating the data elements
               foreach (CustomCustomer item in queryCustomer)
               {
                   writer.WriteStartElement("series");
                   writer.WriteElementString("name", item.Name);
                   writer.WriteStartElement("data");
                   writer.WriteElementString("point", item.AppCount);
                   writer.WriteEndElement();
                   writer.WriteEndElement();
               }
               // Close <chart> element and document
               writer.WriteEndElement();
               writer.WriteEndDocument();
               writer.Close();
           }
       }
   }
    
    //Custom wrapper class for Customer.
   public class CustomCustomer
   {
       public CustomCustomer()
       {

       }
       public String Name { get; set; }
       public int Count { get; set; }
       public String AppCount
       {
           get { return Count.ToString(); }
       }
   }
}