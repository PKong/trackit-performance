﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public class ApplicationFieldAccess
   {
      public static void Add(string type, string name, string tableName, 
                             string fieldName, bool canView, bool canEdit)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();
         application_field_access access = new application_field_access();

         access.type = type;
         access.name = name;
         access.table_name = tableName;
         access.field_name = fieldName;
         access.can_view = canView;
         access.can_edit = canEdit;

         ce.application_field_access.AddObject(access);
         ce.SaveChanges();
      }

      public static void Add(application_field_access access)
      {
         // TODO: Add additional security/business logic in here as needed.
         using (CPTTEntities ce = new CPTTEntities())
         {
            HttpContext context = HttpContext.Current;

            ce.application_field_access.AddObject(access);
            ce.SaveChanges();
         }
      }

      public static void Update(application_field_access access, string type, 
                                string name, string tableName, string fieldName, 
                                bool canView, bool canEdit)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         ce.application_field_access.Attach(access);
         access.type = type;
         access.name = name;
         access.table_name = tableName;
         access.field_name = fieldName;
         access.can_view = canView;
         access.can_edit = canEdit;

         // Exceptions are thrown up to the calling method to handle. Elmah will log
         // unhandled execptions.
         try
         {
            ce.SaveChanges();
         }
         catch (OptimisticConcurrencyException)
         {
            throw;
         }
         catch (EntitySqlException)
         {
            throw;
         }
      }

      public static void Delete(int id)
      {
         // TODO: Add additional security/business logic in here as needed.
         CPTTEntities ce = new CPTTEntities();

         application_field_access access;

         access = ce.application_field_access.Where(u => u.id.Equals(id)).First();
         ce.DeleteObject(access);
         ce.SaveChanges();
      }
   }
}