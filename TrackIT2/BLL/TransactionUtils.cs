﻿using System;
using System.Transactions;

namespace TrackIT2.BLL
{
   public static class TransactionUtils
   {
      public static TransactionScope CreateTransactionScope()
      {
         var transactionOptions = new TransactionOptions();
         transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
         transactionOptions.Timeout = new TimeSpan(0);

         return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
      }
   }
}