﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class LOEHoldupReasons
    {
        public static List<loe_holdup_reasons> GetLOE(int? lease_app_id)
        {
            List<loe_holdup_reasons> listRet = new List<loe_holdup_reasons>();
            if (lease_app_id.HasValue)
            {
                using (CPTTEntities context = new CPTTEntities())
                {
                    var query = from loe in context.loe_holdup_reasons
                                where loe.lease_application_id == lease_app_id.Value
                                select loe;
                    listRet.AddRange(query.ToList<loe_holdup_reasons>());
                }
            }
            return listRet;
        }

        public static string Add(List<loe_holdup_reasons> objLOE, int? lease_app_id, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

           CPTTEntities ce;
           if (ceRef == null)
           {
               //using new entity object
               ce = new CPTTEntities();
           }
           else
           {
               //using current entity object
               ce = ceRef;
               isRootTransaction = false;
           }
            try
            {
               using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
               {
                  if (isRootTransaction)
                  {
                     // Manually open connection to prevent EF from auto closing
                     // connection and causing issues with future queries.
                     ce.Connection.Open();
                  }
                  
                  results = DeleteAllWithinTransaction(lease_app_id, ce);

                  // A null result indicates success
                  if (string.IsNullOrEmpty(results))
                  {
                     foreach (loe_holdup_reasons item in objLOE)
                     {
                        ce.loe_holdup_reasons.AddObject(item);
                     }

                     if (isRootTransaction)
                     {
                        // We tentatively save the changes so that we can finish 
                        // processing the transaction.
                        ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                        trans.Complete();     // Transaction complete
                     }

                     isSuccess = true;                     
                  }
                  else
                  {
                     // If delete all fails. We will have the result to pass
                     // to the calling method and the transaction will be 
                     // aborted.
                     isSuccess = false;
                  }                    
               }
            }              
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

               isSuccess = false;
               results = "An error occurred adding the record. Please try again.";
            }
            finally
            {
               if (isRootTransaction)
               {
                  ce.Connection.Dispose();
               }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
               ce.AcceptAllChanges();
               ce.Dispose();
            }

            return results;
        }

        /// <summary>
        ///    Adds LOE holdup reason objects within an existing context.
        ///    This is needed when doing updates as part of a full lease
        ///    application update. It is assumed that the call to the method is
        ///    within a transaction block outside this method. This method will
        ///    not make a call to trans.complete or ce.SaveChanges since it will
        ///    be the responsibility of the calling method to make the final
        ///    save and allow for a full rollback if an error occurs.
        /// </summary>
        /// <param name="objLOE">LOE Holdup Reason list to add.</param>
        /// <param name="lease_app_id">Lease Application Id to add to.</param>
        /// <param name="ceRef">Entity Context Reference.</param>
        /// <returns>String indicating status up process.</returns>
        public static string AddWithinTransaction(List<loe_holdup_reasons> objLOE, int? lease_app_id, CPTTEntities ceRef)
        {
           String results = null;

           try
           {              
               results = DeleteAllWithinTransaction(lease_app_id, ceRef);

               // A null result indicates success
               if (string.IsNullOrEmpty(results))
               {
                  foreach (loe_holdup_reasons item in objLOE)
                  {
                     ceRef.loe_holdup_reasons.AddObject(item);
                  }              
               }
               else
               {
                  // If delete all fails. We will have the result to pass
                  // to the calling method and the transaction will be 
                  // aborted.
                  results = "Error removing LOE holdup reason records.";
               }
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
              results = "An error occurred adding the record. Please try again.";
           }

           return results;
        }

        public static string deleteAll(int? lease_app_id, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

           CPTTEntities ce;
           if (ceRef == null)
           {
               //using new entity object
               ce = new CPTTEntities();
           }
           else
           {
               //using current entity object
               ce = ceRef;
               isRootTransaction = false;
           }
            try
            {
               using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
               {
                  if (isRootTransaction)
                  {
                     // Manually open connection to prevent EF from auto closing
                     // connection and causing issues with future queries.
                     ce.Connection.Open();
                  }

                     var query = from loe in ce.loe_holdup_reasons
                                 where loe.lease_application_id == lease_app_id.Value
                                 select loe;
                     if (query.ToList().Count > 0)
                     {
                        foreach (loe_holdup_reasons item in query)
                        {
                           ce.DeleteObject(item);
                        }

                        if (isRootTransaction)
                        {
                           // We tentatively save the changes so that we can finish 
                           // processing the transaction.
                           ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                           trans.Complete();     // Transaction complete
                        }
                     }

                     isSuccess = true;
               }
            }              
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

               isSuccess = false;
               results = "An error occurred removing LOE holdup reason records. Please try again.";
            }
            finally
            {
               if (isRootTransaction)
               {
                  ce.Connection.Dispose();
               }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
               ce.AcceptAllChanges();
               ce.Dispose();
            }

         return results;
        }

       /// <summary>
       ///    Removes LOE holdup reason objects within an existing context.
       ///    This is needed when doing updates as part of a full lease
       ///    application update. It is assumed that the call to the method is
       ///    within a transaction block outside this method. This method will
       ///    not make a call to trans.complete or ce.SaveChanges since it will
       ///    be the responsibility of the calling method to make the final
       ///    save and allow for a full rollback if an error occurs.
       /// </summary>
       /// <param name="lease_app_id">Lease application Id to reference.</param>
       /// <param name="ceRef">Entity Context Reference.</param>
       /// <returns></returns> 
       public static string DeleteAllWithinTransaction(int? lease_app_id, CPTTEntities ceRef)
        {
           String results = null;

           try
           {
               var query = from loe in ceRef.loe_holdup_reasons
                           where loe.lease_application_id == lease_app_id.Value
                           select loe;
               if (query.ToList().Count > 0)
               {
                  foreach (loe_holdup_reasons item in query)
                  {
                     ceRef.DeleteObject(item);
                  }
               }
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
              results = "An error occurred removing LOE holdup reason records. Please try again.";
           }

           return results;
        }
    }
}