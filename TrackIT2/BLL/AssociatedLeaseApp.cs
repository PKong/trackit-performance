﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Transactions;
using System.Data;

namespace TrackIT2.BLL
{
    public static class AssociatedLeaseApp
    {
        public static List<AssociatedLeaseAppInfo> GetAssociatedLeaseApp(string site_uid, CPTTEntities ceRef = null)
        {
            bool isSuccess = false;
            bool isRootTransaction = true;
            List<AssociatedLeaseAppInfo> lstAssociatedLA = new List<AssociatedLeaseAppInfo>();

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            // Start transaction for get all comments.
            try
            {
                if (isRootTransaction)
                {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();
                }


                var result = (from sdp in ce.lease_applications
                              where sdp.site_uid.Equals(site_uid)
                              select new AssociatedLeaseAppInfo
                              {
                                  id = sdp.id,
                                  SiteUID = sdp.site_uid,
                                  Customer = sdp.customer,
                                  ReceivedDate = sdp.leaseapp_rcvd_date,
                                  Type = sdp.leaseapp_types.lease_application_type
                              });
                if (result.ToList().Count > 0)
                {
                    lstAssociatedLA.AddRange(result.ToList<AssociatedLeaseAppInfo>());
                }
                isSuccess = true;
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
                isSuccess = false;
                lstAssociatedLA = null;
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                isSuccess = false;
                lstAssociatedLA = null;
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return lstAssociatedLA;
        }
    }

    public class AssociatedLeaseAppInfo
    {
        public int id { get; set; }
        public string SiteUID { get; set; }
        public customer Customer { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string Type { get; set; }

        public string ReceivedDateString
        {
            get
            {
                string ret = Utility.DateToString(ReceivedDate);
                return ret;
            }
        }

        public string Descriptions
        {
            get
            {
                /*
                    change from
                    ID - Customer Date 
                    to
                    ID - Customer - Type Date 
                    ref:TRACKIT-659
                */
                string sCusName = "";
                if (Customer != null) sCusName = Customer.customer_name;
                string sType = "";
                if (Type != null) sType = " " + LeaseApplication.GetLeaseAppTypeAliases(Type);
                return SiteUID + " - " + sCusName + " -"+ sType +" " + ReceivedDateString;
            }
        }
    }
}