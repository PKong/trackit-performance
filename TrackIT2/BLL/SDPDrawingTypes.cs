﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public static class SDPDrawingTypes
   {
      public static List<sdp_tower_drawings> GetAllTowerDrawing(int sdp_id)
      {
         List<sdp_tower_drawings> lstReturn = new List<sdp_tower_drawings>();
         using (CPTTEntities ce = new CPTTEntities())
         {
            var query = from td in ce.sdp_tower_drawings
                        where td.site_data_package_id == sdp_id
                        select td;

            if (query.ToList().Count > 0)
            {
               lstReturn = query.ToList<sdp_tower_drawings>();
            }
         }
         return lstReturn;
      }

      public static string Add(List<sdp_tower_drawings> objTD, int sdp_id)
      {
         string results = null;
         bool isSuccess = false;

         // Start transaction for delete.
         using (CPTTEntities ce = new CPTTEntities())
         {
            try
            {
               using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
               {
                  // Manually open connection to prevent EF from auto closing
                  // connection and causing issues with future queries.
                  ce.Connection.Open();

                  foreach (sdp_tower_drawings item in objTD)
                  {
                     item.created_at = DateTime.Now;
                     item.updated_at = DateTime.Now;
                     ce.sdp_tower_drawings.AddObject(item);
                  }

                  ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                  isSuccess = true;
                  trans.Complete();     // Transaction complete
               }
            }
            catch (Exception ex)
            {
               //Error occurs
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

               isSuccess = false;
               results = "An error occurred adding a tower drawing.";
            }
            finally
            {
               ce.Connection.Dispose();
            }

            // Finally accept all changes from the transaction.
            if (isSuccess)
            {
               ce.AcceptAllChanges();
            }
         }

         return results;
      }

      public static string AddWithinTransaction(List<sdp_tower_drawings> objTD, int sdp_id, CPTTEntities ceRef)
      {
         string results = null;

         try
         {
            foreach (sdp_tower_drawings item in objTD)
            {
               item.created_at = DateTime.Now;
               item.updated_at = DateTime.Now;
               ceRef.sdp_tower_drawings.AddObject(item);
            }
         }
         catch (Exception ex)
         {
            //Error occurs
            // Log error in ELMAH for any diagnostic needs.
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

            results = "An error occurred adding a tower drawing.";
         }

         return results;
      }

      public static string deleteAll(int sdp_id)
      {
         string results = null;
         bool isSuccess = false;

         // Start transaction for delete.
         using (CPTTEntities ce = new CPTTEntities())
         {
            try
            {
               using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
               {
                  // Manually open connection to prevent EF from auto closing
                  // connection and causing issues with future queries.
                  ce.Connection.Open();

                  var query = from td in ce.sdp_tower_drawings
                              where td.site_data_package_id == sdp_id
                              select td;

                  if (query.ToList().Count > 0)
                  {
                     foreach (sdp_tower_drawings item in query)
                     {
                        ce.DeleteObject(item);
                     }

                     ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                  }

                  isSuccess = true;
                  trans.Complete();     // Transaction complete
               }
            }
            catch (Exception ex)                 
            {
               // Error occurs
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

               isSuccess = false;
               results = "An error occurred removing tower drawings.";
            }
            finally
            {
               ce.Connection.Dispose();
            }

            // Finally accept all changes from the transaction.
            if (isSuccess)
            {
               ce.AcceptAllChanges();
            }
         }
         return results;
      }

      public static string deleteAllWithinTransaction(int sdp_id, CPTTEntities ceRef)
      {
         string results = null;

         try
         {
            var query = from td in ceRef.sdp_tower_drawings
                        where td.site_data_package_id == sdp_id
                        select td;

            if (query.ToList().Count > 0)
            {
               foreach (sdp_tower_drawings item in query)
               {
                  ceRef.DeleteObject(item);
               }
            }
         }
         catch (Exception ex)
         {
            // Error occurs
            // Log error in ELMAH for any diagnostic needs.
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            results = "An error occurred removing tower drawings.";
         }

         return results;
      }
   }
}