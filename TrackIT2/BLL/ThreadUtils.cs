﻿namespace TrackIT2.BLL
{
   public static class ThreadUtils
   {
      public static void HandleThreadDone(ref System.Threading.EventWaitHandle thread, int numberOfAllThread, ref System.Collections.Generic.List<bool> IsAllComplete)
      {
         IsAllComplete.Add(true);

         if (IsAllComplete.Count != numberOfAllThread) return;

         thread.Set();
         IsAllComplete.Clear();
      }
   }
}