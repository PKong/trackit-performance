﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Web.SessionState;

namespace TrackIT2.BLL
{
    public class DefaultFilterSession
    {
        public static string SessionName = "DefaultFilterSession";
        public static string ApplyClick = "ApplyClick";

        public enum FilterType
        {
            LeaseAppFilter,
            SitesFilter,
            SdpFilter,
            TowerModFilter,
            LeaseAdminFilter,
            IssueFilter
        }

        public string LeaseAppFilter { get; set; }
        public string SitesFilter { get; set; }
        public string SdpFilter { get; set; }
        public string TowerModFilter { get; set; }
        public string LeaseAdminFilter { get; set; }
        public string IssueFilterString { get; set; }

        public DefaultFilterSession()
        { }

        public static void SetFilterSession(ref DefaultFilterSession filterSession, string filterString, FilterType filterType)
        {
            switch (filterType)
            {
                case FilterType.LeaseAppFilter:
                    filterSession.LeaseAppFilter = filterString;
                    break;
                case FilterType.SitesFilter:
                    filterSession.SitesFilter = filterString;
                    break;
                case FilterType.SdpFilter:
                    filterSession.SdpFilter = filterString;
                    break;
                case FilterType.TowerModFilter:
                    filterSession.TowerModFilter = filterString;
                    break;
                case FilterType.LeaseAdminFilter:
                    filterSession.LeaseAdminFilter = filterString;
                    break;
                case FilterType.IssueFilter:
                    filterSession.IssueFilterString = filterString;
                    break;
                default:
                    // do nothing
                    break;
            }
        }

        public static string GetFilterSession(DefaultFilterSession filterSession, FilterType filterType)
        {
            string returnFilterString = string.Empty;
            switch (filterType)
            {
                case FilterType.LeaseAppFilter:
                    returnFilterString = filterSession.LeaseAppFilter;
                    break;
                case FilterType.SitesFilter:
                    returnFilterString = filterSession.SitesFilter;
                    break;
                case FilterType.SdpFilter:
                    returnFilterString = filterSession.SdpFilter;
                    break;
                case FilterType.TowerModFilter:
                    returnFilterString = filterSession.TowerModFilter;
                    break;
                case FilterType.LeaseAdminFilter:
                    returnFilterString = filterSession.LeaseAdminFilter;
                    break;
                case FilterType.IssueFilter:
                    returnFilterString = filterSession.IssueFilterString;
                    break;
                default:
                    // do nothing
                    break;
            }
            return returnFilterString;
        }

        public static void GetOrSetFilterSession(HttpContext current, ref DefaultFilterSession filterSession, string filterString, FilterType filterType)
        {
            if (current.Session != null && current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

                if (!string.IsNullOrEmpty(current.Request.Form[DefaultFilterSession.ApplyClick]) || current.Request.Form[DefaultFilterSession.SessionName] != null)
                {
                    SetFilterSession(ref filterSession, filterString, filterType);
                }

                current.Session[DefaultFilterSession.SessionName] = filterSession;

            }
            else
            {
                filterSession = new DefaultFilterSession();
                SetFilterSession(ref filterSession, filterString, filterType);
                current.Session.Add(DefaultFilterSession.SessionName, filterSession);
            }
        }

        public static void ClearSession(HttpContext current, FilterType filterType)
        {
            DefaultFilterSession filterSession;
            if (current.Session != null && current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];
            }
            else
            {
                filterSession = new DefaultFilterSession();
            }

            SetFilterSession(ref filterSession, string.Empty, filterType);

        }

    }
}