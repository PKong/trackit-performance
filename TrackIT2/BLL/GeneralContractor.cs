﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
    public static class GeneralContractor
    {
        private static void Add(general_contractors gc)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                ce.general_contractors.AddObject(gc);
                ce.SaveChanges();
            }
        }
        public static int? GetIDByName(string name)
        {
            int? ret = null;
            using (CPTTEntities ce = new CPTTEntities())
            {
                var queryGC = from gc in ce.general_contractors
                              where gc.general_contractor_name.ToUpper().Equals(name.ToUpper())
                              select gc;
                general_contractors objGC;
                if (queryGC.ToList().Count > 0)
                {
                    objGC = queryGC.ToList<general_contractors>()[0];
                    ret = objGC.id;
                }
                else
                {
                    //Count = 0 need to add new entry
                    objGC = new general_contractors();
                    objGC.general_contractor_name = name;
                    Add(objGC);
                    if (objGC.id >= 0)
                    {
                        ret = objGC.id;
                    }
                }
            }
            return ret;
        }
    }
}