﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class AdvanceSearch
    {
        public const String QUERY_LIKE = " LIKE '%{0}%')";
        public const String QUERY_EQUAL = " = {0})";
        public const String QUERY_MORE_EQUAL_DATETIME = " >= DATETIME'{0}')";
        public const String QUERY_LESS_EQUAL_DATETIME = " <= DATETIME'{0}')";
        public const String QUERY_MORE_EQUAL_DECIMAL = " >= {0}M)";
        public const String QUERY_LESS_EQUAL_DECIMAL = " <= {0}M)";
        public const String QUERY_MORE_EQUAL_NUMERIC = " >= {0})";
        public const String QUERY_LESS_EQUAL_NUMERIC = " <= {0})";
        public const String QUERY_SA_EXIST = "EXISTS(SELECT VALUE sa FROM it.structural_analyses AS sa WHERE {0})";
        public const String QUERY_SDP_USER_EXIST = "EXISTS(SELECT VALUE u FROM [CPTTEntities].[users] AS u WHERE {0})";
        public const String QUERY_TM_PO_EXIST = "EXISTS(SELECT VALUE po FROM it.tower_mod_pos AS po WHERE {0})";
        public const String QUERY_LA_REVISION_EXIST = "EXISTS(SELECT VALUE la FROM [CPTTEntities].[lease_applications] AS la " +
                                                "LEFT JOIN [CPTTEntities].[leaseapp_revisions] AS rv ON rv.lease_application_id = it.id " + 
                                                "WHERE {0})";

        
        private const string DATE_TIME_WITH_STATUS = "apn";
        private const string DB_DECISION_TYPES = "decision_types";
        private const string DB_STRUCT_DECISION_TYPES = "struct_decision_types";
        private const string DB_LANDLORD_CONSENT_TYPES = "landlord_consent_type";
        private const string DB_COHOST_TENANTS = "cohost_tenants";
        private const string DB_YN = "yn";
        private const string DB_USERS = "users";
        private const string DB_SDP_DOCS_STATUS = "sdp_doc_status";
        private const string DB_SDP_ABS_VENDOR = "abstract_vendor";
        private const string DB_CUSTOMERS = "customers";
        private const string DB_TM_STATUS = "tower_mod_statuses";
        private const string DB_TM_TYPES = "tower_mod_types";
        private const string DB_TM_POS_TYPES = "tower_mod_phase_types";
        private const string DB_SA_TYPES = "structural_analysis_types";
        private const string DB_SA_FEE_PAYOR_TYPES = "structural_analysis_fee_payor_types";
        private const string DB_SA_SAC_PRIORITY = "sac_priorities";
        private const string DB_PO_RELEASE_STATUSES = "po_release_statuses";
        private const string DB_SA_VENDORS = "sa_vendors";
        private const string DB_SA_SAC_DESC = "sac_description";
        private const string DB_DATE_STATUS = "date_statuses";

        #region Abstract + Template class
        public abstract class SearchFilter
        {
            public List<CritereaInfo> LstCriterea;
            public Hashtable HashCriterea;

            protected abstract void InitializeData();

            //Store Query String and add to list.
            protected void AddCriterea(String sNameInDDL, String sType, String sName, String sDBName = null, String sDDLDBName = null, String sExtension = null)
            {
                LstCriterea.Add(new CritereaInfo(sNameInDDL, sType, sName, sDDLDBName));
                if (sType == "dat" || sType == "dst")
                {
                    String sFromDB = "(" + sDBName + QUERY_MORE_EQUAL_DATETIME;
                    String sToDB = "(" + sDBName + QUERY_LESS_EQUAL_DATETIME;
                    
                    if (sExtension != null)
                    {
                        sFromDB = String.Format(sExtension, sFromDB);
                        sToDB = String.Format(sExtension, sToDB);
                    }
                    HashCriterea.Add(sName + "From", sFromDB + "|dat");
                    HashCriterea.Add(sName + "To", sToDB + "|dat");
                    if (sType == "dst")
                    {
                        HashCriterea.Add(sName + "Status", "(" + sDDLDBName + QUERY_EQUAL + "|ddl");
                    }
                }
                else if (sType == "dec")
                {
                    String sFromDB = "(" + sDBName + QUERY_MORE_EQUAL_DECIMAL;
                    String sToDB = "(" + sDBName + QUERY_LESS_EQUAL_DECIMAL;
                    if (sExtension != null)
                    {
                        sFromDB = String.Format(sExtension, sFromDB);
                        sToDB = String.Format(sExtension, sToDB);
                    }
                    HashCriterea.Add(sName + "From", sFromDB + "|dec");
                    HashCriterea.Add(sName + "To", sToDB + "|dec");
                }
                else if (sType == "num")
                {
                    String sFromDB = "(" + sDBName + QUERY_MORE_EQUAL_NUMERIC;
                    String sToDB = "(" + sDBName + QUERY_LESS_EQUAL_NUMERIC;
                    if (sExtension != null)
                    {
                        sFromDB = String.Format(sExtension, sFromDB);
                        sToDB = String.Format(sExtension, sToDB);
                    }
                    HashCriterea.Add(sName + "From", sFromDB + "|num");
                    HashCriterea.Add(sName + "To", sToDB + "|num");
                }
                else if (sType == "txt" || sType == "aut")
                {
                    sDBName = "(" + sDBName + QUERY_LIKE;
                    if (sExtension != null)
                    {
                        sDBName = String.Format(sExtension, sDBName);
                    }
                    HashCriterea.Add(sName,  sDBName + "|txt");
                }
                else if (sType == "ddl")
                {
                    sDBName = "(" + sDBName + QUERY_EQUAL;
                    
                    if (sExtension != null)
                    {
                        sDBName = String.Format(sExtension, sDBName);
                    }
                    HashCriterea.Add(sName, sDBName + "|ddl");
                }
                else if (sType == "dds" || sType == "sat")
                {
                    String sFromDB = "(";
                    String sToDB = "(";

                    for (int x = 0; x < sDBName.Split('|').Length; x++)
                    {
                        sFromDB += sDBName.Split('|')[x] + QUERY_MORE_EQUAL_DATETIME;
                        sToDB += sDBName.Split('|')[x] + QUERY_LESS_EQUAL_DATETIME;
                        if (x < sDBName.Split('|').Length -1)
                        {
                            sFromDB += " or (";
                            sToDB += " or (";
                        }
                    }

                    sFromDB = "(" + sFromDB + ")";
                    sToDB = "(" + sToDB + ")";

                    if (sExtension != null)
                    {
                        sFromDB = String.Format(sExtension, sFromDB);
                        sToDB = String.Format(sExtension, sToDB);
                    }

                    HashCriterea.Add(sName + "From", sFromDB + "|dat");
                    HashCriterea.Add(sName + "To", sToDB + "|dat");
                    if (sType == "dst" || sType == "dds")
                    {
                        HashCriterea.Add(sName + "Status", "(" + sDDLDBName + QUERY_EQUAL + "|ddl");
                    }
                }
                else if (sType == "sdl")
                {
                    String tmpDbname = "(";
                    for (int x = 0; x < sDBName.Split('|').Length; x++)
                    {
                        tmpDbname += sDBName.Split('|')[x] + QUERY_EQUAL;
                        if (x < sDBName.Split('|').Length - 1)
                        {
                            tmpDbname += " or (";
                        }
                    }

                    sDBName = "(" + tmpDbname + ")";
                    
                    if (sExtension != null)
                    {
                        sDBName = String.Format(sExtension, tmpDbname);
                    }

                    HashCriterea.Add(sName, sDBName + "|ddl");
                }
                else if (sType == "sec")
                {
                    String sFromDB = "(";
                    String sToDB = "(";

                    for (int x = 0; x < sDBName.Split('|').Length; x++)
                    {
                        sFromDB += sDBName.Split('|')[x] + QUERY_MORE_EQUAL_DECIMAL;
                        sToDB += sDBName.Split('|')[x] + QUERY_LESS_EQUAL_DECIMAL;
                        if (x < sDBName.Split('|').Length - 1)
                        {
                            sFromDB += " or (";
                            sToDB += " or (";
                        }
                    }

                    sFromDB = "(" + sFromDB + ")";
                    sToDB = "(" + sToDB + ")";

                    if (sExtension != null)
                    {
                        sFromDB = String.Format(sExtension, sFromDB);
                        sToDB = String.Format(sExtension, sToDB);
                    }

                    HashCriterea.Add(sName + "From", sFromDB + "|dec");
                    HashCriterea.Add(sName + "To", sToDB + "|dec");
                }
                else if (sType == "sxt")
                {
                    String tmpDbname = "(";

                    for (int x = 0; x < sDBName.Split('|').Length; x++)
                    {
                        tmpDbname += sDBName.Split('|')[x] + QUERY_LIKE;
                        if (x < sDBName.Split('|').Length - 1)
                        {
                            tmpDbname += " or (";
                        }
                    }

                    sDBName = "(" + tmpDbname + ")";

                     if (sExtension != null)
                    {
                        sDBName = String.Format(sExtension, sDBName);
                    }

                    HashCriterea.Add(sName, sDBName + "|txt");
                }
                else if (sType == "snm")
                {
                    String sFromDB = "(";
                    String sToDB = "(";

                    for (int x = 0; x < sDBName.Split('|').Length; x++)
                    {
                        sFromDB += sDBName.Split('|')[x] + QUERY_MORE_EQUAL_NUMERIC;
                        sToDB += sDBName.Split('|')[x] + QUERY_LESS_EQUAL_NUMERIC;
                        if (x < sDBName.Split('|').Length - 1)
                        {
                            sFromDB += " or (";
                            sToDB += " or (";
                        }
                    }

                    sFromDB = "(" + sFromDB + ")";
                    sToDB = "(" + sToDB + ")";

                    if (sExtension != null)
                    {
                        sFromDB = String.Format(sExtension, sFromDB);
                        sToDB = String.Format(sExtension, sToDB);
                    }
                    HashCriterea.Add(sName + "From", sFromDB + "|num");
                    HashCriterea.Add(sName + "To", sToDB + "|num");
                }
            }
            //Get Query String by name.
            public String CritereaByName(String sName, String value)
            {
                String ret = "";
                if (HashCriterea[sName] != null)
                {
                    String[] sCriterea = HashCriterea[sName].ToString().Split('|');

                    if (sCriterea[1] == "dat")
                    {
                        ret = String.Format(sCriterea[0], Convert.ToDateTime(DateTime.ParseExact(value, Globals.SHORT_DATE_FORMAT, null).ToString()).ToString(Globals.ENTITY_SQL_DATE_FORMAT));
                    }
                    else
                    {
                        String criterea = sCriterea[0];
                        if (sCriterea[1] == "ddl" && value == "null")
                        {
                            criterea = criterea.Replace("=", "is");
                        }
                        ret = String.Format(criterea, value);
                    }

                }
                return ret;
            }

            //Get DB String By name.
            public String GetDDLDatabaseString(String sName)
            {
                String sSubName = sName.Substring(3);
                String ret = null;
                var query = from lst in LstCriterea
                            where lst.Name == sSubName
                            select lst;
                if (query.ToList().Count > 0)
                {
                    CritereaInfo obj = query.ToList()[0] as CritereaInfo;
                    if (obj != null)
                    {
                        ret = obj.DDLDB;
                    }
                }
                return ret;
            }
        }
        //Template Class
        public class CritereaInfo
        {
            public String NameInDDL;
            public String Type;
            public String Name;
            public String DDLDB;

            public CritereaInfo(String NameInDDL, String Type, String Name, String DDLDB = null)
            {
                this.NameInDDL = NameInDDL;
                this.Type = Type;
                this.Name = Name;
                this.DDLDB = DDLDB;
            }
            public String GetDDLValue()
            {
                return this.Type + "," + this.Name;
            }
        }
        //Template Class for non-table object
        public class NonTableObject
        {
            public int? Ivalue { get; set; }
            public string Svalue { get; set; }
            public string html { get; set; }
            public NonTableObject(int? value, string html)
            {
                this.Ivalue = value;
                this.html = html;
            }
            public NonTableObject(string value, string html, Boolean blnIsCheck)
            {
                this.Svalue = value;
                this.html = html;
            }
        }
        #endregion

        #region AJAX Function
        //Return json data for ddl items.
        public static string GetDDLDBJSON(SearchFilter objADVSearch, string sRequest)
        {
            string ret = "";
            using (CPTTEntities ce = new CPTTEntities())
            {

                if (sRequest == DATE_TIME_WITH_STATUS)
                {
                    var query = from date in ce.date_statuses
                                orderby date.date_status
                                select new
                                {
                                    value = date.id,
                                    html = date.date_status
                                };
                    ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                }
                else
                {
                    String sDBName = objADVSearch.GetDDLDatabaseString(sRequest);
                    if (sDBName != null)
                    {
                        if (sDBName == DB_DECISION_TYPES)
                        {
                            var query = from d in ce.decision_types
                                        orderby d.decision_type
                                        select new
                                        {
                                            value = d.id,
                                            html = d.decision_type
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_STRUCT_DECISION_TYPES)
                        {
                            var query = from s in ce.struct_decision_types
                                        orderby s.struct_decision_type
                                        select new
                                        {
                                            value = s.id,
                                            html = s.struct_decision_type
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_LANDLORD_CONSENT_TYPES)
                        {
                            var query = from l in ce.landlord_consent_types
                                        orderby l.landlord_consent_type
                                        select new
                                        {
                                            value = l.id,
                                            html = l.landlord_consent_type
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_COHOST_TENANTS)
                        {
                            var query = from c in ce.cohost_tenants
                                        orderby c.sort_order, c.cohost_tenant
                                        select new
                                        {
                                            value = c.id,
                                            html = c.cohost_tenant
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_YN)
                        {
                            List<NonTableObject> lstObj = new List<NonTableObject>();
                            lstObj.Add(new NonTableObject("true", "Yes", true));
                            lstObj.Add(new NonTableObject("false", "No", true));
                            lstObj.Add(new NonTableObject(null, "N/A", true));

                            var query = from yn in lstObj
                                        select new
                                        {
                                            value = yn.Svalue,
                                            html = yn.html
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_USERS)
                        {
                            List<UserInfo> lstUser = new List<UserInfo>();
                            lstUser = BLL.User.GetUserInfo();
                            var query = from u in lstUser
                                        select new
                                        {
                                            value = u.ID,
                                            html = u.FullName
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_SDP_DOCS_STATUS)
                        {
                            var query = from s in ce.sdp_doc_statuses
                                        orderby s.sdp_doc_status
                                        select new
                                        {
                                            value = s.id,
                                            html = s.sdp_doc_status
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_SDP_ABS_VENDOR)
                        {
                            var query = from s in ce.abstract_vendors
                                        orderby s.abstract_vendor
                                        select new
                                        {
                                            value = s.id,
                                            html = s.abstract_vendor
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_CUSTOMERS)
                        {
                            var query = from c in ce.customers
                                        orderby c.sort_order, c.customer_name
                                        select new
                                        {
                                            value = c.id,
                                            html = c.customer_name
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_TM_STATUS)
                        {
                            var query = from s in ce.tower_mod_statuses
                                        orderby s.tower_mod_status_name
                                        select new
                                        {
                                            value = s.id,
                                            html = s.tower_mod_status_name
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_TM_TYPES)
                        {
                            var query = from s in ce.tower_mod_types
                                        orderby s.tower_mod_type
                                        select new
                                        {
                                            value = s.id,
                                            html = s.tower_mod_type
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_TM_POS_TYPES)
                        {
                            var query = from po in ce.tower_mod_phase_types
                                        orderby po.phase_name
                                        select new
                                        {
                                            value = po.id,
                                            html = po.phase_name
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_SA_TYPES)
                        {
                            var query = from sa in ce.structural_analysis_types
                                        orderby sa.sort_order, sa.structural_analysis_type
                                        select new
                                        {
                                            value = sa.id,
                                            html = sa.structural_analysis_type
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_SA_FEE_PAYOR_TYPES)
                        {
                            var query = from sa in ce.structural_analysis_fee_payor_types
                                        orderby sa.sa_fee_payor_desc
                                        select new
                                        {
                                            value = sa.id,
                                            html = sa.sa_fee_payor_desc
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_SA_SAC_PRIORITY)
                        {
                            var query = from sa in ce.sac_priorities
                                        orderby sa.priority
                                        select new
                                        {
                                            value = sa.id,
                                            html = sa.priority
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_PO_RELEASE_STATUSES)
                        {
                            var query = from sa in ce.po_release_statuses
                                        orderby sa.po_release_status
                                        select new
                                        {
                                            value = sa.id,
                                            html = sa.po_release_status
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_SA_VENDORS)
                        {
                            var query = from v in ce.sa_vendors
                                        where v.is_preferred == true
                                        orderby v.sa_vendor
                                        select new
                                        {
                                            value = v.id,
                                            html = v.sa_vendor
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if(sDBName == DB_SA_SAC_DESC)
                        {
                            var query = from desc in ce.sac_descriptions
                                        where desc.is_preferred == true
                                        orderby desc.description
                                        select new
                                        {
                                            value = desc.id,
                                            html = desc.description
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        else if (sDBName == DB_DATE_STATUS)
                        {
                            var query = from date in ce.date_statuses
                                        orderby date.date_status
                                        select new
                                        {
                                            value = date.id,
                                            html = date.date_status
                                        };
                            ret = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        }
                        //TODO : ADD MORE DROP DOWN LIST DATABASE OBJECT HERE.
                    }
                }
            }
            return ret;
        }
        #endregion

        #region Filter classes
        public class LeaseApplicationFilter : SearchFilter
        {
            public LeaseApplicationFilter()
            {
                LstCriterea = new List<CritereaInfo>();
                HashCriterea = new Hashtable();
                InitializeData();
            }
            protected override void InitializeData()
            {
                //TODO : Manually add the additional criterea here.
                //Customer Site UID
                AddCriterea("Customer Site UID", "aut", "CustomerSiteUID", "it.customer_site_uid");
                //site address
                AddCriterea("Street Address", "txt", "StreetAddress", "it.site.address");
                AddCriterea("City", "aut", "City", "it.site.city");
                AddCriterea("State", "aut", "State", "it.site.state");
                AddCriterea("County", "aut", "County", "it.site.county");

                //Preliminary Tab
                AddCriterea("SDP to Customer", "dat", "SDPToCustomer", "it.sdp_to_customer_date");
                AddCriterea("Prelim Decision Date", "dat", "PrelimDecisionDate", "it.prelim_approval_date");
                AddCriterea("Prelim Decision", "ddl", "PrelimDecision", "it.prelim_decision_id", "decision_types");
                AddCriterea("Prelim Site Walk", "dst", "PrelimSiteWalk", "it.prelim_sitewalk_date", "it.prelim_sitewalk_date_status_id");
                AddCriterea("Landlord Consent Type", "ddl", "LandlordConsentType", "it.landlord_consent_type_id", "landlord_consent_type");
                AddCriterea("Landlord Consent Type Note", "txt", "LandlordConsentTypeNote", "it.landlord_consent_type_notes");
                //--> END Preliminary Tab

                //Document Review Tab
                AddCriterea("Cohost Tenant", "ddl", "CohostTenant", "it.cohost_tenant_id", "cohost_tenants");
                AddCriterea("LE Ordered", "dst", "LEOrdered", "it.leaseexhbt_ordered_date", "it.leaseexhbt_ordered_date_status_id");
                AddCriterea("LE Received", "dat", "LEReceived", "it.leaseexhbt_rcvd_date");
                AddCriterea("LE Decision Date", "dat", "LEDecisionDate", "it.leaseexhbt_decision_date");
                AddCriterea("LE Decision", "ddl", "LEDecision", "it.leaseexhbt_decision_id", "decision_types");
                AddCriterea("Landlord Notice", "dat", "LandlordNotice", "it.landlord_notice_sent_date");
                AddCriterea("Landlord Consent Requested", "dat", "LandlordConsentRequested", "it.landlord_consent_sent_date");
                AddCriterea("Landlord Consent Received", "dat", "LandlordConsentReceived", "it.landlord_consent_rcvd_date");
                AddCriterea("CD Received", "dat", "CDReceived", "it.cd_rcvd_date");
                AddCriterea("CD Decision Date", "dat", "CDDecisionDate", "it.cd_decision_date");
                AddCriterea("CD Decision", "ddl", "CDDecision", "it.cd_decision_id", "decision_types");
                //--> END Document Review Tab

                //Lease Tab
                AddCriterea("Executable Sent To Customer", "dat", "ExecutableSentToCustomer", "it.leasedoc_sent_to_cust_date");
                AddCriterea("LOE Sent", "dst", "LOESent", "it.leasedoc_sent_to_cust_date", "it.leasedoc_sent_to_cust_date_status_id");
                //TODO : Add LOE Holdup reason types.
                AddCriterea("LOE Holdup Comments", "txt", "LOEHoldupComments", "it.loe_holdup_comments");
                AddCriterea("Docs Back From Customer", "dst", "DocsBackFromCustomer", "it.lease_back_from_cust_date");
                AddCriterea("Signed By Customer", "dat", "SignedByCustomer", "it.signed_by_cust_date");
                AddCriterea("Docs To FSC", "dat", "DocsToFSC", "it.leasedoc_to_fsc_date");
                AddCriterea("Lease Exec", "dst", "LeaseExec", "it.lease_execution_date", "it.lease_execution_date_status_id");
                AddCriterea("Amend Exec", "dst", "AmendExec", "it.amendment_execution_date", "it.amendment_execution_date_status_id");
                AddCriterea("Rent Forecast Amount", "dec", "RentForecastAmount", "it.rent_forecast_amount");
                AddCriterea("Revenue Share", "ddl", "RevenueShare", "it.revenue_share_yn", "yn");
                AddCriterea("Commencement Forecast", "dat", "CommencementForecast", "it.commencement_forcast_date");
                AddCriterea("REM Lease Sequence", "num", "REMLeaseSequence", "it.rem_lease_seq");
                AddCriterea("Cost Share Agreement", "ddl", "CostShareAgreement", "it.cost_share_agreement_yn", "yn");
                AddCriterea("Cost Share Agreement Amount", "dec", "CostShareAgreementAmount", "it.cost_share_agreement_amount");
                AddCriterea("Cap Cost Recovery", "ddl", "CapCostRecovery", "it.cap_cost_recovery_yn", "yn");
                AddCriterea("Cap Cost Recovery Amount", "dec", "CapCostRecoveryAmount", "it.cap_cost_amount");
                AddCriterea("Bulk PO Available", "ddl", "BulkPOAvailable", "it.bulk_po_availible_yn", "yn");
                AddCriterea("Termination Date", "dat", "TerminationDate", "it.termination_date");
                //--> END Lease Tab

                //Construction Tab
                AddCriterea("Precon Site Walk", "dst", "PreconSiteWalk", "it.preconstr_sitewalk_date", "it.preconstr_sitewalk_date_status_id");
                AddCriterea("NTP Issued", "dst", "NTPIssued", "it.ntp_issued_date", "it.ntp_issued_date_status_id");
                AddCriterea("NTP Verified", "dat", "NTPVerified", "it.ntp_verified_date");
                AddCriterea("Early Inst Discovered", "dat", "EarlyInstDiscovered", "it.early_installation_discovered_date");
                AddCriterea("Violation Letter Sent", "dat", "ViolationLetterSent", "it.early_installation_violation_letter_sent_date");
                AddCriterea("Violation Invoice Created By Finops", "dat", "ViolationInvoiceCreatedByFinops", "it.early_installation_violation_invoice_created_by_finops_date");
                AddCriterea("Violation Invoice Number", "num", "ViolationInvoiceNumber", "it.early_installation_violation_invoice_number");
                AddCriterea("Violation Fee Received", "dat", "ViolationFeeReceived", "it.early_installation_violation_fee_rcvd_date");
                AddCriterea("Extension Notice Received", "dat", "ExtensionNoticeReceived", "it.extension_notice_rcvd_date");
                AddCriterea("Extension Notice Fee Received", "dat", "ExtensionNoticeFeeReceived", "it.extension_notice_fee_rcvd_date");
                AddCriterea("Extension Notice Invoice Created By Finops", "dat", "ExtensionNoticeInvoiceCreatedByFinops", "it.extension_notice_invoice_created_by_finops_date");
                AddCriterea("Extension Notice Fee Invoice Number", "num", "ExtensionNoticeFeeInvoiceNumber", "it.extension_notice_fee_invoice_number");
                AddCriterea("Equipment Removed", "dat", "EquipmentRemoved", "it.equipment_removed_date");
                AddCriterea("Failure To Remove Violation", "ddl", "FailureToRemoveViolation", "it.failure_to_remove_violation", "yn");
                AddCriterea("Failure To Remove Violation Letter Sent", "dat", "FailureToRemoveViolationLetterSent", "it.failure_to_remove_violation_letter_sent_date");
                AddCriterea("Failure To Remove Violation Letter Submitted To Customer", "dat", "FailureToRemoveViolationLetterSubmittedToCustomer", "it.failure_to_remove_violation_letter_submitted_to_cst_date");
                AddCriterea("Violation Equipment Removal Confirmed Submitted To Customer", "dat", "ViolationEquipmentRemovalConfirmedSubmittedToCustomer", "it.violation_equipment_removal_confirmed_submitted_to_cst_date");
                AddCriterea("Post Construct Site Walk", "dst", "PostConstructSiteWalk", "it.postconstr_sitewalk_date", "it.postconstr_sitewalk_date_status_id");
                AddCriterea("Post Construct Ordered", "dst", "PostConstructOrdered", "it.postconstr_ordered_date", "it.postconstr_ordered_date_status_id");
                AddCriterea("Construction Completed", "dst", "ConstructionCompleted", "it.constr_cmplt_date", "it.constr_cmplt_date_status_id");
                AddCriterea("Close Out Received", "dat", "CloseOutReceived", "it.all_closeout_docs_rcvd_date");
                AddCriterea("Close Out Doc Fee Required", "ddl", "CloseOutDocFeeRequired", "it.closeout_docs_fee_required", "yn");
                AddCriterea("Close Out Doc Notice Sent", "dat", "CloseOutDocNoticeSent", "it.closeout_docs_notice_sent_date");
                AddCriterea("Close Out Doc Fee Invoice Sent", "dat", "CloseOutDocFeeInvoiceSent", "it.closeout_docs_fee_invoice_sent_date");
                AddCriterea("Close Out Doc Fee Invoice Created By Finops", "dat", "CloseOutDocFeeInvoiceCreatedByFinops", "it.closeout_doc_fee_invoice_created_by_finops_date");
                AddCriterea("Close Out Doc Fee Invoice Number", "num", "CloseOutDocFeeInvoiceNumber", "it.closeout_docs_fee_invoice_number");
                AddCriterea("Close Out Doc Fee Received", "dat", "CloseOutDocFeeReceived", "it.closeout_docs_fee_rcvd_date");
                AddCriterea("Final Site Approval", "dst", "FinalSiteApproval", "it.final_site_apprvl_date", "it.final_site_apprvl_date_status_id");
                AddCriterea("As Built", "dat", "AsBuilt", "it.as_built_date");
                AddCriterea("Market Handoff", "dst", "MarketHandoff", "it.market_handoff_date", "it.market_handoff_date_status_id");

                //--> END Construction Tabv
 
                //SA Decision Date
                AddCriterea("SA Decision Date", "dat", "SADecisionDate", "it.struct_decision_date");
                //SA Decision struct_decision_type
                AddCriterea("SA Decision", "ddl", "SADecision", "it.struct_decision_id", "struct_decision_types");

                //SA Tab
                AddCriterea("SA Po Ordered", "dat", "SAPoOrdered", "sa.po_ordered_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Po Received", "dat", "SAPoReceived", "sa.po_received_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Po Amount", "dec", "SAPoAmount", "sa.po_amount", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Po Number", "txt", "SAPoNumber", "sa.po_number", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Type", "ddl", "SAType", "sa.structural_analysis_type_id", DB_SA_TYPES, QUERY_SA_EXIST);
                AddCriterea("SA Ordered", "dst", "SAOrdered", "sa.sa_ordered_date", "sa.sa_ordered_date_status_id", QUERY_SA_EXIST);
                AddCriterea("SA Received", "dst", "SAReceived", "sa.sa_received_date", "sa.sa_received_date_status_id", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Order Onhold", "dat", "SAOrderOnhold", "sa.sa_order_onhold_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Order Cancelled", "dat", "SAOrderCancelled", "sa.sa_order_cancelled_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Fee Received", "dat", "SAFeeReceived", "sa.sa_fee_received_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Fee Amount", "dec", "SAFeeAmount", "sa.sa_fee_amount", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Check Number", "txt", "SACheckNumber", "sa.sa_check_nbr", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Fee Payor Type", "ddl", "SAFeePayorType", "sa.structural_analysis_fee_payor_type_id", DB_SA_FEE_PAYOR_TYPES, sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Vendor", "ddl", "SAVendor", "sa.sa_vendor_id", DB_SA_VENDORS, QUERY_SA_EXIST);
                AddCriterea("SA Sac Name EMP", "ddl", "SASacNameEMP", "sa.sac_name_emp_id", DB_USERS, sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Saw To Customer", "dat", "SASawToCustomer", "sa.saw_to_customer_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Saw Approved", "dat", "SASawApproved", "sa.saw_approved_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Saw To SAC", "dat", "SASawToSAC", "sa.saw_to_sac_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Request", "dat", "SARequest", "sa.saw_request_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Sac Priority", "ddl", "SASacPriority", "sa.sac_priority_id", DB_SA_SAC_PRIORITY, QUERY_SA_EXIST);
                AddCriterea("SA Sac Description", "ddl", "SASacDescription", "sa.sac_description_id", DB_SA_SAC_DESC, QUERY_SA_EXIST);
                AddCriterea("SA Shopping Cart Number", "txt", "SAShoppingCartNumber", "sa.shopping_cart_number", sExtension: QUERY_SA_EXIST);
                //TODO: Blanket PO Month., Blanket PO Year.
                AddCriterea("SA Po Release Status", "ddl", "SAPoReleaseStatus", "sa.po_release_status_id", DB_PO_RELEASE_STATUSES, sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Po Release", "dat", "SAPoRelease", "sa.po_release_date", sExtension: QUERY_SA_EXIST);
                AddCriterea("SA Notes", "txt", "SANotes", "sa.po_notes", sExtension: QUERY_SA_EXIST);
                //AddCriterea("SA PS Received", "dat", "SAPsReceived", "sa.ps_received_date", sExtension: QUERY_SA_EXIST);

                //--> END SA Tab


                //LeaseApp Revision Tab
                AddCriterea("Revision Received", "dat", "RevisionReceived", "rv.revision_rcvd_date", sExtension: QUERY_LA_REVISION_EXIST);
                AddCriterea("Revision Fee Received", "dat", "RevisionFeeReceived", "rv.revision_fee_rcvd_date", sExtension: QUERY_LA_REVISION_EXIST);
                AddCriterea("Revision Fee Check PO Number", "txt", "RevisionFeeCheckPONumber", "rv.revision_fee_check_po_nbr", sExtension: QUERY_LA_REVISION_EXIST);
                AddCriterea("Revision Fee Amount", "dec", "RevisionFeeAmount", "rv.revision_fee_amount", sExtension: QUERY_LA_REVISION_EXIST);
                //--> END LA Revision Tab
            }

        }

        public class SiteDatePackageFilter : SearchFilter
        {
            public SiteDatePackageFilter()
            {
                LstCriterea = new List<CritereaInfo>();
                HashCriterea = new Hashtable();
                InitializeData();
            }
            protected override void InitializeData()
            {
                //General Data
                AddCriterea("SDP Request", "dat", "SDPRequest", "it.sdp_request_date");
                AddCriterea("SDP Due By", "dat", "SDPDueBy", "it.sdp_due_by_date");
                AddCriterea("SDP Due Date Notes", "txt", "SDPDueDateNotes", "it.sdp_due_date_notes");
                AddCriterea("First Sweep", "dat", "FirstSweep", "it.first_sweep_completed_date");
                AddCriterea("Notice Of Completion", "dat", "NoticeOfCompletion", "it.sdp_notice_of_completion_date");
                AddCriterea("SDP Complete", "dat", "SDPComplete", "it.sdp_completed_date");
                AddCriterea("First Market", "dat", "FirstMarket", "it.first_notice_to_market_date");
                AddCriterea("Second Market", "dat", "SecondMarket", "it.second_notice_to_market_date");
                AddCriterea("Third Market", "dat", "ThirdMarket", "it.third_notice_to_market_date");
                AddCriterea("Escalated By EMP", "ddl", "EscalatedByEMP", "it.escalated_by_emp_id", "users");
                AddCriterea("Escalation", "dat", "Escalation", "it.escalation_date");
                AddCriterea("Escalation Complete By EMP", "ddl", "EscalationCompleteByEMP", "it.escalation_completed_by_emp_id", "users");
                AddCriterea("Escalation Complete", "dat", "EscalationComplete", "it.escalation_completed_date");
                AddCriterea("Vendor Order Received", "dat", "VendorOrderReceived", "it.po_for_sa_ordered_date");
                AddCriterea("Vendor Order PO Received", "dat", "VendorOrderPOReceived", "it.po_for_sa_rcvd_date");
                AddCriterea("Cost Approval", "dat", "CostApproval", "it.hard_cost_approval_date");
                AddCriterea("Prime Lease Exec", "dat", "PrimeLeaseExec", "it.prime_lease_exec_date");
                AddCriterea("Permit Applied", "dat", "PermitApplied", "it.permit_applied_date");
                AddCriterea("Construction Start", "dat", "ConstructionStart", "it.construction_start_date");

                //Document Status Data
                AddCriterea("Tower Drawing Doc Status", "ddl", "TowerDrawingDocStatus", "it.tower_drawings_doc_status_id",DB_SDP_DOCS_STATUS);
                AddCriterea("Tower Tag Photo Doc Status", "ddl", "TowerTagPhotoDocStatus", "it.tower_tag_photo_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Structural Calcs Doc Status", "ddl", "StructuralCalcsDocStatus", "it.structural_calcs_doc_status_id",DB_SDP_DOCS_STATUS);
                AddCriterea("Structural Analysis Doc Status", "ddl", "StructuralAnalysisDocStatus", "it.structural_analysis_current_doc_status_id",DB_SDP_DOCS_STATUS);
                AddCriterea("Tower Erection Detail Doc Status", "ddl", "TowerErectionDetailDocStatus", "it.tower_erection_detail_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Foundation Design Doc Status", "ddl", "FoundationDesignDocStatus", "it.foundation_design_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Construction Drawing Doc Status", "ddl", "ConstructionDrawingDocStatus", "it.construction_drawings_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Geotech Report Doc Status", "ddl", "GeotechReportDocStatus", "it.geotech_report_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Building Permit Doc Status", "ddl", "BuildingPermitDocStatus", "it.building_permit_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Zoning Approval Doc Status", "ddl", "ZoningApprovalDocStatus", "it.zoning_approval_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Phase I Doc Status", "ddl", "PhaseIDocStatus", "it.phase_i_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Phase II Doc Status", "ddl", "PhaseIIDocStatus", "it.phase_ii_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Nepa Doc Status", "ddl", "NepaDocStatus", "it.nepa_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Shpo Doc Status", "ddl", "ShpoDocStatus", "it.shpo_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("GPS1a2c Doc Status", "ddl", "GPS1a2cDocStatus", "it.gps_1a2c_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Airspace Doc Status", "ddl", "AirspaceDocStatus", "it.airspace_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("FAA Study Determination Doc Status", "ddl", "FAAStudyDeterminationDocStatus", "it.faa_study_determination_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("FCC Doc Status", "ddl", "FCCDocStatus", "it.fcc_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("AM Certification Doc Status", "ddl", "AMCertificationDocStatus", "it.am_certification_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Towair Doc Status", "ddl", "TowairDocStatus", "it.towair_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Prime Lease Doc Status", "ddl", "PrimeLeaseDocStatus", "it.prime_lease_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Memorandum Doc Status", "ddl", "MemorandumDocStatus", "it.memorandum_of_lease_doc_status_id", DB_SDP_DOCS_STATUS);
                AddCriterea("Title Doc Status", "ddl", "TitleDocStatus", "it.title_doc_status_id", DB_SDP_DOCS_STATUS);

                //Abstract Data
                AddCriterea("ABS Employee Assigned", "ddl", "ABSEmployeeAssigned", "it.abstract_assigned_to_emp_id", DB_USERS);
                AddCriterea("ABS Assigned", "dat", "ABSAssigned", "it.abstract_assigned_date");
                AddCriterea("ABS Complete", "dat", "ABSComplete", "it.abstract_completed_date");
                AddCriterea("ABS Notes", "txt", "ABSNotes", "it.abstract_notes");
                AddCriterea("ABS Vendor", "ddl", "ABSVendor", "it.abstract_vendor_id", DB_SDP_ABS_VENDOR);

                AddCriterea("SDP Assigned To EMP", "ddl", "SDPAssignedToEMP", "it.sdp_assigned_to_emp_id", "users");
            }
        }

        public class TowerModificationFilter : SearchFilter
        {
            public TowerModificationFilter()
            {
                LstCriterea = new List<CritereaInfo>();
                HashCriterea = new Hashtable();
                InitializeData();
            }

            protected override void InitializeData()
            {
                //General Data.
                AddCriterea("TM Customer", "ddl", "TMCustomer", "it.customer_id", DB_CUSTOMERS);
                AddCriterea("TM Payor", "ddl", "TMPayor", "it.payor_id", DB_CUSTOMERS);
                AddCriterea("TM Stat", "ddl", "TMStat", "it.tower_mod_status_id", DB_TM_STATUS);
                AddCriterea("TM Type", "ddl", "TMType", "it.tower_mod_type_id", DB_TM_TYPES);
                AddCriterea("TM Height", "num", "TMHeight", "it.tower_height_proposed_ft");
                AddCriterea("TM Capacity", "num", "TMCapacity", "it.postmod_structural_capacity_pct");
                AddCriterea("Reg Notification", "ddl", "RegNotification", "it.regulatory_notification_yn", DB_YN);
                AddCriterea("Permit Carrier", "ddl", "PermitCarrier", "it.building_permit_from_carrier_yn", DB_YN);
                AddCriterea("Permit Received", "dst", "PermitReceived", "it.building_permit_rcvd_date", "it.building_permit_rcvd_date_status_id");
                AddCriterea("Top Out Notification Required", "ddl", "TopOutNotificationRequired", "it.top_out_notification_required_yn", DB_YN);
                AddCriterea("Materials Ordered", "dat", "MaterialsOrdered", "it.materials_ordered_date");
                AddCriterea("Materials Received", "dst", "MaterialReceived", "it.materials_rcvd_date", "it.materials_rcvd_date");
                AddCriterea("Work Start", "dst", "WorkStart", "it.work_start_date", "it.work_start_date_status_id");
                AddCriterea("Work Completion", "dst", "WorkCompletion", "it.work_completion_date", "it.work_completion_date_status_id");
                AddCriterea("Third Party Inspection", "dst", "ThirdPartyInspection", "it.third_party_inspection_date", "it.third_party_inspection_date_status_id");
                AddCriterea("TM Pm Emp", "ddl", "TMPmEmp", "it.tower_mod_pm_emp_id", DB_USERS);
                AddCriterea("Package Sent", "dat", "PackageSent", "it.tower_mod_packet_sent_date");

                //Tower Mod POs
                AddCriterea("PO Tower Mod Phase Type", "ddl", "POTowerModPhaseType", "po.tower_mod_phase_type_id", DB_TM_POS_TYPES, QUERY_TM_PO_EXIST);
                AddCriterea("PO Request Received", "dat", "PORequestReceived", "po.ebid_req_rcvd_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Packet Ready", "dat", "POPacketReady", "po.ebid_packet_ready_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Scheduled Published", "dat", "POScheduledPublished", "po.ebid_scheduled_published_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Scheduled Close", "dat", "POScheduledClose", "po.ebid_scheduled_close_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Maturity", "dat", "POMaturity", "po.ebid_maturity_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Ebid Comment", "txt", "POEbidComment", "po.ebid_comment", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Est Const Cost", "dec", "POEstConstCost", "po.estimated_construction_cost_amount", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Bid", "dec", "POBid", "po.bid_amount", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Markup", "dec", "POMarkup", "po.markup_amount", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Total From Customer", "dec", "POTotalFromCustomer", "po.total_from_customer_amount", sExtension: QUERY_TM_PO_EXIST);
                //TODO : Need to add "General Contractors Name"
                AddCriterea("PO Sent to Cs PM", "dat", "POSentToCsPM", "po.sent_to_cs_pm_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO To Gc Requested", "dat", "POToGcRequested", "po.po_to_gc_requested_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Shopping Cart Number", "txt", "POShoppingCartNumber", "po.shopping_cart_nbr", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO To Gc Received", "dat", "POToGcReceived", "po.po_to_gc_rcvd_date", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Number", "txt", "PONumber", "po.po_nbr", sExtension: QUERY_TM_PO_EXIST);
                AddCriterea("PO Tax", "dec", "POTax", "po.tax_amount", sExtension: QUERY_TM_PO_EXIST);


            }
        }

        public class LeaseAdminFilter : SearchFilter
        {
            public LeaseAdminFilter()
            {
                LstCriterea = new List<CritereaInfo>();
                HashCriterea = new Hashtable();
                InitializeData();
            }

            protected override void InitializeData()
            {
                AddCriterea("Lease Application ID", "txt", "LeaseApplicationID", "it.lease_application_id");
                AddCriterea("Executables Sent To Customer Date", "sat", "ExecutablesSentToCustomerDate", "(it.leasedoc_sent_to_cust_date|it.lease_applications.leasedoc_sent_to_cust_date)");
                AddCriterea("Executables Sent To Customer", "sdl", "ExecutablesSentToCustomer", "(it.lease_applications.leasedoc_sent_to_cust_date_status_id|it.leasedoc_sent_to_cust_date_status_id)", "date_statuses");
                AddCriterea("Docs Back From Customer Date", "sat", "DocsBackFromCustomerDate", "(it.lease_back_from_cust_date|it.lease_applications.lease_back_from_cust_date)", "it.lease_back_from_cust_date_status_id");
                AddCriterea("Docs Back From Customer", "sdl", "DocsBackFromCustomer", "(it.lease_back_from_cust_date_status_id|it.lease_applications.lease_back_from_cust_date_status_id)", "date_statuses");
                AddCriterea("Signed By Customer", "sat", "SignedByCustomer", "(it.signed_by_cust_date|it.lease_applications.signed_by_cust_date)");
                AddCriterea("Docs To FSC", "sat", "DocsToFSC", "(it.leasedoc_to_fsc_date|it.lease_applications.leasedoc_to_fsc_date)");
                AddCriterea("Date Received At FSC (Exec)", "dat", "DateReceivedAtFSC(Exec)", "it.date_received_at_fsc");
                AddCriterea("Lease Execution Status", "sdl", "LeaseExecutionStatus", "(it.lease_execution_date_status_id|it.lease_applications.lease_execution_date_status_id)", "date_statuses");
                AddCriterea("Commencement Forecast", "sat", "CommencementForecast", "(it.commencement_forcast_date|it.lease_applications.commencement_forcast_date)");
                AddCriterea("Carrier Signature Notarized Date", "dat", "CarrierSignatureNotarizedDate", "it.carrier_signature_notarized_date");
                AddCriterea("Carrier Signature Notarized", "ddl", "CarrierSignatureNotarized", "it.carrier_signature_notarized_yn", "yn");
                AddCriterea("Sent To Signatory", "dat", "SentToSignatory", "it.sent_to_signatory_date");
                AddCriterea("Back From Signatory", "dat", "BackFromSignatory", "it.back_from_signatory_date");
                AddCriterea("Original Sent To UPS", "dat", "OriginalSentToUPS", "it.original_sent_to_ups_date");
                AddCriterea("Doc Loaded to CST", "ddl", "DocLoadedToCST", "it.doc_loaded_to_cst_yn", "yn");
                AddCriterea("Amendment Rent Affecting", "ddl", "AmendmentRentAffecting", "it.amendment_rent_affecting_yn", "yn");
                AddCriterea("One Time Fee", "ddl", "OneTimeFee", "it.one_time_fee_yn", "yn");
                AddCriterea("Rent Forecast Amount", "sec", "RentForecastAmount", "(it.rent_forecast_amount|it.lease_applications.rent_forecast_amount)");
                AddCriterea("Revshare Loaded To CST Receivable", "ddl", "RevshareLoadedToCSTReceivable", "it.revshare_loaded_to_cst_receivable_yn", "yn");
                AddCriterea("Revshare Loaded To CST Payable", "ddl", "RevshareLoadedToCSTPayable", "it.revshare_loaded_to_cst_payable_yn", "yn");
                AddCriterea("REM Lease Sequence", "snm", "REMLeaseSequence", "(it.rem_lease_seq|it.lease_applications.rem_lease_seq)");
                AddCriterea("Cost Share Agreement", "sdl", "CostShareAgreement", "(it.cost_share_agreement_yn|it.lease_applications.cost_share_agreement_yn)", "yn");
                AddCriterea("Cost Share Agreement Amount", "sec", "CostShareAgreementAmount", "(it.cost_share_agreement_amount|it.lease_applications.cost_share_agreement_amount)");
                AddCriterea("Cap Cost Recovery", "sdl", "CapCostRecovery", "(it.cap_cost_recovery_yn|it.lease_applications.cap_cost_recovery_yn)", "yn");
                AddCriterea("Cap Cost Recovery Amount", "sec", "CapCostRecoveryAmount", "(it.cap_cost_amount|it.lease_applications.cap_cost_amount)");
                AddCriterea("Termination Date", "sat", "TerminationDate", "(it.termination_date|it.lease_applications.termination_date)");
                AddCriterea("Commencement NTP To Customer", "dat", "CommencementNTPToCustomer", "it.commencement_ntp_to_customer_date", "it.lease_admin_commencement_document_type_id");
                AddCriterea("Date Received At FSC (Comm)", "dat", "DateReceivedAtFSC(Comm)", "it.date_received_at_fsc_date");
                AddCriterea("Received By", "ddl", "ReceivedBy", "it.document_received_by_emp_id", "users");
                AddCriterea("Reg Commencement Letter Created", "dat", "RegCommencementLetterCreated", "it.reg_commencement_letter_created_date");
                AddCriterea("Saved To DMS (Exec)", "dat", "SavedToDMS(Exec)", "it.saved_to_dms_date");
                AddCriterea("Saved To DMS (Comm)", "dat", "SavedToDMS(Comm)", "it.date_saved_to_dms_date");
                AddCriterea("REM Alert", "txt", "REMAlert", "it.rem_alert_comments");
                AddCriterea("Created At", "dat", "CreatedAt", "it.created_at");
                AddCriterea("Updated At", "dat", "UpdatedAt", "it.updated_at");
                AddCriterea("Created By", "ddl", "CreatedBy", "it.creator_id", "users");
                AddCriterea("Updated By", "ddl", "UpdatedBy", "it.updater_id", "users");
            }
        }

        #endregion
    }
}