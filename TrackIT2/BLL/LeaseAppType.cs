﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class LeaseAppType
    {
        public static leaseapp_types Search(int? type_id)
        {

            leaseapp_types ret = null;
            if (type_id.HasValue)
            {
                using (CPTTEntities ce = new CPTTEntities())
                {

                    ret = ce.leaseapp_types.Where(c => c.id.Equals(type_id.Value)).First();

                }
            }
            return ret;

        }

        public static leaseapp_types SearchWithinTransaction(int? type_id, CPTTEntities ceRef)
        {

           leaseapp_types ret = null;
           if (type_id.HasValue)
           {
              ret = ceRef.leaseapp_types.Where(c => c.id.Equals(type_id.Value)).First();
           }
           return ret;

        }
    }
}