﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace TrackIT2.BLL
{
    /// <summary>
    /// Globals
    /// </summary>
    public static class Globals
    {
        public static string SHORT_DATE_FORMAT = ConfigurationManager.AppSettings["ShortDateFormat"];
        public static string SHORT_DATETIME_FORMAT = ConfigurationManager.AppSettings["ShortDateTimeFormat"];
        public static string ENTITY_SQL_DATE_FORMAT = ConfigurationManager.AppSettings["EntitySqlDateFormat"];
        public static string HOST_NAME = ConfigurationManager.AppSettings["HostName"];

        public const string H1_LABEL_STRING_FORMAT = " {0} / {1} / {2}";
        public const string QS_GLOBAL_SEARCH = "gs";
        public const string QS_PROJECT_CATEGORY = "pc";
        public const string QS_SORT_DIRECTION = "sortdi";
        public const string SORT_DIRECTION_VALUE_ASC = "asc";

        public const string SUCCESS = "success";
        public const string ERROR = "error";
        public const string EXISTS = "exists";

        public const string ERROR_HEADER = "<b>ERROR:</b><br/>";
        public const string ERROR_TXT_INVALID_UID = "Site ID not found, please enter a valid Site ID.";
        public const string ERROR_INVALID_LENGTH_FORMAT = "The input for <b>\"{0}\"</b> field has exceeded its maximum length <b>({1})</b>.<br/>";
        //TODO: Not finish yet.
        public const string SESSION_SUBMIT_VALIDATE = "SubmitValidator";
        public const string SESSION_JSON_HELPER = "JSONHelper";

        public const string REQUEST_METHOD_GET = "GET";
        public const string REQUEST_METHOD_POST = "POST";
        public const string CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
        public const string CONTENT_TYPE_JSON = "application/json; charset=utf-8";

        // Issue Tracker
        public const string ISSUE_TRACKER_ID_PREFIX = "TMIT-";
        public const string ISSUE_TRACKER_NO_SITE_NAME = "Various";

        // Site Url
        public const string SITE_LINK_URL = "/Sites/SiteDashboard.aspx?id=";

    }
}