﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
   public static class LeaseApplicationVersion
   {
      public static void Add(lease_application_versions version)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
            ce.lease_application_versions.AddObject(version);
            ce.SaveChanges();
         }
      }

      public static void Delete(int applicationId, int lockVersion)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
           lease_application_versions version;

         version = ce.lease_application_versions
                     .Where(ver => ver.lease_application_id.Equals(applicationId))
                     .Where(ver => ver.lock_version.Equals(lockVersion))
                     .First();

         ce.DeleteObject(version);
         ce.SaveChanges();
         }
      }

      public static void Delete(int id)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
            lease_application_versions version;

            version = ce.lease_application_versions
                        .Where(ver => ver.id.Equals(id))
                        .First();

            ce.DeleteObject(version);
            ce.SaveChanges();
         }
      }

      public static List<VersionLog> GenerateVersionLog(int id)
      {
         List<VersionLog> results = new List<VersionLog>();
         List<lease_application_versions> versions;
         List<user> userList;
         lease_applications currApplication;

         using (CPTTEntities ce = new CPTTEntities())
         {
            versions = ce.lease_application_versions
                      .Where(ver => ver.lease_application_id.Equals(id))
                      .OrderBy(Utility.GetIntField<lease_application_versions>("id"))
                      .ToList();

            userList = ce.users.ToList();

            if (versions.Count > 0)
            {
               // If any history records were found, add the current version of the 
               // lease application to properly track differences up to the most 
               // recent copy.
               currApplication = ce.lease_applications
                                 .Where(la => la.id.Equals(id))
                                 .First();

               versions.Add(LeaseApplication.ToLeaseApplicationVersion(currApplication));

               for (int i = 1; i < versions.Count; i++)
               {                  
                  lease_application_versions currVersion = versions.ElementAt(i);
                  lease_application_versions prevVersion = versions.ElementAt(i - 1);
                  user userInfo;

                  // Create a "point of creation" record to help establish the timeline of
                  // changes.
                  if (i == 1)
                  {
                     VersionLog initialLog = new VersionLog();
                     initialLog.oldValue = "-";
                     initialLog.newValue = "-";
                     initialLog.fieldChanged = "Record Created";

                     if (prevVersion.created_at.HasValue)
                     {
                        initialLog.updatedAt = prevVersion.created_at.Value;
                     }

                     var initialQuery = from ul in userList
                                        where ul.id == prevVersion.creator_id
                                        select ul;

                     userInfo = initialQuery.FirstOrDefault();

                     if (userInfo != null)
                     {
                        initialLog.updatedBy = userInfo.last_name + ", " +
                                               userInfo.first_name;
                        initialLog.UserAvatarImageUrl = BLL.User.GetUserAvatarImage(userInfo.tmo_userid);
                     }
                     else
                     {
                        initialLog.updatedBy = "";
                        initialLog.UserAvatarImageUrl = BLL.User.GetUserAvatarImage("");
                     }

                     results.Add(initialLog);
                  }

                  // Use reflection to get all modifiyable properties
                  var properties = from p in typeof(lease_application_versions).GetProperties()
                                   where p.CanRead &&
                                         p.CanWrite &&
                                         p.Name != "id" &&
                                         p.Name != "created_at" &&
                                         p.Name != "updated_at" &&
                                         p.Name != "updater_id" &&
                                         p.Name != "creator_id" &&
                                         p.Name != "lock_version" &&
                                         p.Name != "EntityKey"
                                   select p;

                  foreach (var property in properties)
                  {
                     // Ignore any values where the field changed ends in "_id" since
                     // the entity associations handle the foreign key lookups. There
                     // are a couple of columns that are exceptions to the rule.
                     if (!property.Name.EndsWith("_id") && !property.Name.EndsWith("Reference") &&
                         property.Name != "creator_user" && property.Name != "updater_user")
                     {
                        var currValue = property.GetValue(currVersion, null);
                        var prevValue = property.GetValue(prevVersion, null);

                        if (HasValueChanges(currValue, prevValue))
                        {
                           // Compare values based on object type.
                           VersionLog log = new VersionLog();

                           log.version = currVersion.lock_version;

                           if (currVersion.updater_id != null)
                           {
                              var query = from ul in userList
                                          where ul.id == currVersion.updater_id
                                          select ul;

                              userInfo = query.FirstOrDefault();

                              if (userInfo != null)
                              {
                                 log.updatedBy = userInfo.last_name + ", " +
                                                 userInfo.first_name;
                                 log.UserAvatarImageUrl = BLL.User.GetUserAvatarImage(userInfo.tmo_userid);
                              }
                              else
                              {
                                 log.updatedBy = "None";
                                 log.UserAvatarImageUrl = BLL.User.GetUserAvatarImage("");
                              }
                           }

                           if (currVersion.updated_at != null)
                           {
                              log.updatedAt = currVersion.updated_at.Value;
                           }

                           log.oldValue = GetFieldValue(prevValue);

                           // Get friendly name for column
                           string tempField = "";
                           List<metadata> columnInfo = ce.metadatas
                                                         .Where(md => md.table.Equals("lease_applications"))
                                                         .Where(md => md.column.Equals(property.Name))
                                                         .ToList();
                           if (columnInfo.Count == 1)
                           {
                              tempField = columnInfo[0].friendly_name;
                           }
                           else if (columnInfo.Count > 1)
                           {
                              foreach (metadata currItem in columnInfo)
                              {
                                 if (!String.IsNullOrEmpty(currItem.friendly_name))
                                 {
                                    tempField = currItem.friendly_name;
                                    break;
                                 }
                                 else
                                 {
                                    tempField = "No friendly name: " + property.Name;
                                 }
                              }
                           }
                           else
                           {
                              tempField = property.Name;
                           }

                           // If the current value is null, use the previous value in
                           // order to determine the field changed friendly name.
                           if (currValue != null)
                           {
                              log.fieldChanged = GetFieldChangedName(currValue, tempField);
                           }
                           else
                           {
                              log.fieldChanged = GetFieldChangedName(prevValue, tempField);
                           }

                           log.newValue = GetFieldValue(currValue);

                           results.Add(log);
                        }
                     }
                  }
               }
            }            
         }

         return results;
      }

      private static bool HasValueChanges(object value1, object value2)
      {
         bool results = false;
         object testObject;

         if (value1 == null && value2 == null)
         {
            results = false;
         }
         // If one value is null and the other is not, there is a change.
         else if ((value1 == null && value2 != null) || (value1 != null && value2 == null))
         {
            results = true;
         }
         // Otherwise we do a comparison based off of object type. Simple
         // object types can do a "ToString" comparison, others need to 
         // compare at the object level.
         else
         {           
            if (value1 != null)
            {
               testObject = value1;
            }
            else
            {
               testObject = value2;
            }
            switch (testObject.GetType().Name)
            {
               case "String":
               case "Int32":
               case "Boolean":
               case "Float":
               case "Decimal":
                  
                  if (value1.ToString() != value2.ToString())
                  {
                     results = true;
                  }

                  break;

               case "DateTime":

                  if (((DateTime)value1).Date != ((DateTime)value2).Date)
                  {
                     results = true;
                  }

                  break;

               default:
                  
                  if (value1 != value2)
                  {
                     results = true;
                  }

                  break;
            }
         }

         return results;
      }

      private static string GetFieldValue(object item)
      {
         string results = "";

         if (item != null)
         {          
            switch (item.GetType().Name)
            {
               // While the entity model and already retrieved the 
               // foreign key objects, we need to ignore those and
               // only process the ID or primitive values so that we 
               // can properly set the friendly column name.
               case "applicant":
                  results = ((applicant)item).legal_entity_name;
                  break;

               case "cohost_tenants":
                  results = ((cohost_tenants)item).cohost_tenant;
                  break;

               case "commencement_ltr_types":
                  results = ((commencement_ltr_types)item).commencement_ltr_type;
                  break;

               case "customer":
                  results = ((customer)item).customer_name;
                  break;

               case "DateTime":
                  results = ((DateTime)item).ToString("MM/dd/yyyy");
                  break;

               case "date_statuses":
                  results = ((date_statuses)item).date_status;
                  break;

               case "decision_types":
                  results = ((decision_types)item).decision_type;
                  break;

               case "document_preparation_types":
                  results = ((document_preparation_types)item).document_in_preparation_name;
                  break;

               case "insite4_lease_applications":
                  results = ((insite4_lease_applications)item).id.ToString();
                  break;

               case "landlord_consent_types":
                  results = ((landlord_consent_types)item).landlord_consent_type;
                  break;

               case "lease_applications":
                  results = ((lease_applications)item).id.ToString();
                  break;

               case "leaseapp_activities":
                  results = ((leaseapp_activities)item).lease_app_activity;
                  break;

               case "leaseapp_carrier_projects":
                  results = ((leaseapp_carrier_projects)item).carrier_project_name;
                  break;

               case "lease_doc_types":
                  results = ((lease_doc_types)item).lease_document_type;
                  break;

               case "leaseapp_statuses":
                  results = ((leaseapp_statuses)item).lease_application_status;
                  break;

               case "leaseapp_types":
                  results = ((leaseapp_types)item).lease_application_type;
                  break;

               case "ntp_document_types":
                  results = ((ntp_document_types)item).ntp_document_type;
                  break;

               case "risk_levels":
                  results = ((risk_levels)item).risk_level;
                  break;

               case "site":
                  results = ((site)item).site_uid + ": " + ((site)item).site_name;
                  break;

               case "struct_decision_types":
                  results = ((struct_decision_types)item).struct_decision_type;
                  break;

               case "user":
                  results = ((user)item).first_name + " " + ((user)item).last_name;
                  break;

               default:
                  results = item.ToString();
                  break;
            }
         }
         else
         {
            results = "-";
         }
                 
         return results;
      }

      private static string GetFieldChangedName(object item, string columnName)
      {
         string results = "";

         if (item != null)
         {
            switch (item.GetType().Name)
            {
               case "applicant":
                  results = "Applicant";
                  break;

               case "cohost_tenants":
                  results = "Cohost Tenant";
                  break;

               case "commencement_ltr_types":
                  results = "Commencement Letter Type";
                  break;

               case "customer":
                  results = "Customer";
                  break;

               case "date_statuses":

                  // There are multiple references from lease application version records 
                  // to date statuses, we set the field changed value based off the name
                  // column name of the association object.
                  switch (columnName)
                  {
                     case "amendment_execution_datestatuses":
                        results = "Amendment Execution Date Status";
                        break;

                     case "construction_complete_datestatuses":
                        results = "Construction Complete Date Status";
                        break;

                     case "final_site_approval_datestatuses":
                        results = "Final Site Approval Date Status";
                        break;

                     case "lease_back_from_customer_datestatuses":
                        results = "Lease Back From Customer Date Status";
                        break;

                     case "lease_execution_datestatuses":
                        results = "Lease Execution Date Status";
                        break;

                     case "leasedoc_sent_to_customer_datestatuses":
                        results = "Lease Doc Sent To Customer Date Status";
                        break;

                     case "lease_exhibit_ordered_datestatuses":
                        results = "Lease Exhibit Ordered Date Status";
                        break;

                     case "market_handoff_datestatuses":
                        results = "Market Handoff Date Status";
                        break;

                     case "ntp_issued_datestatuses":
                        results = "NTP Issued Date Status";
                        break;

                     case "post_construction_ordered_datestatuses":
                        results = "Post Construction Ordered Date Status";
                        break;

                     case "post_construction_sitewalk_datestatuses":
                        results = "Post Construction Sitewalk Date Status";
                        break;

                     case "pre_construction_sitewalk_datestatuses":
                        results = "Pre Construction Sitewalk Date Status";
                        break;

                     case "prelim_sitewalk_datestatuses":
                        results = "Preliminary Sitewalk Date Status";
                        break;

                     case "sdp_to_customer_datestatuses":
                        results = "SDP To Customer Date Status";
                        break;

                     default:
                        results = columnName + " Date Status";
                        break;
                  }

                  break;

               case "decision_types":

                  // There are multiple references from lease application version records 
                  // to decision records, we set the field changed value based off the name
                  // column name of the association object.
                  switch (columnName)
                  {
                     case "cd_decision_decisiontypes":
                        results = "CD Decision";
                        break;

                     case "lease_exhibit_decisiontypes":
                        results = "Lease Exhibit Decision";
                        break;

                     case "prelim_decision_decisiontypes":
                        results = "Preliminary Decision";
                        break;

                     case "struct_decision_decisiontypes":
                        results = "Structural Decision";
                        break;

                     default:
                        results = columnName + " Decision";
                        break;
                  }

                  break;

               case "document_preparation_types":
                  results = "Document Preparation Type";
                  break;

               case "insite4_lease_applications":
                  results = "Insite4 Lease Application Id";
                  break;

               case "landlord_consent_types":
                  results = "Landlord Consent Type";
                  break;

               case "lease_applications":
                  results = "Lease Applicatin ID";
                  break;

               case "leaseapp_activities":
                  results = "Lease Application Activity";
                  break;

               case "leaseapp_carrier_projects":
                  results = "Carrier Project";
                  break;

               case "lease_doc_types":
                  results = "Lease Doc Type";
                  break;

               case "leaseapp_statuses":
                  results = "Status";
                  break;

               case "leaseapp_types":
                  results = "Lease Application Type";
                  break;

               case "ntp_document_types":
                  results = "NTP Document Type";
                  break;

               case "risk_levels":
                  results = "Risk Level";
                  break;

               case "site":
                  results = "Site";
                  break;

               case "struct_decision_types":
                  results = "Structural Decision Type";
                  break;

               case "user":

                  // There are multiple references from lease application version records 
                  // to user records, we set the field changed value based off the name
                  // column name of the association object.
                  switch (columnName)
                  {
                     case "colocation_coordinator_user":
                        results = "Colocation Coordinator User";
                        break;

                     case "commencement_letter_created_user":
                        results = "Commencement Letter Created User";
                        break;

                     case "creator_user":
                        results = "Created By User";
                        break;

                     case "leasedoc_to_lease_library_user":
                        results = "Lease Doc to Lease Library User";
                        break;

                     case "leasedoc_cst_loaded_user":
                        results = "Lease Doc CST Loaded User";
                        break;

                     case "leasedoc_received_by_fsc_user":
                        results = "Lease Doc Received by FSC User";
                        break;

                     case "ntp_received_at_fsc_user":
                        results = "NTP Received at FSC User";
                        break;

                     case "rent_adjustment_completed_user":
                        results = "Rent Adjustment Completed User";
                        break;
                    
                     case "tmo_pm_user":
                        results = "TMO PM User";
                        break;

                     case "tmo_specialist_current_user":
                        results = "TMO Specialist Current User";
                        break;

                     case "tmo_specialist_initial_user":
                        results = "TMO Specialist Initial User";
                        break;

                     case "updater_user":
                        results = "Updated by User";
                        break;

                     case "am_emp":
                        results = "Account Manager User";
                        break;

                     default:
                        results = columnName + " User";
                        break;
                  }

                  break;

               default:
                  results = columnName;
                  break;
            }
         }
         else
         {
            results = columnName;
         }
        
         return results;
      }
   }
}