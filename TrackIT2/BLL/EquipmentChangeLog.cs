﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using System.Collections;

namespace TrackIT2.BLL
{
    public class EquipmentChangeLog
    {
        public static List<t_ChangeLog> GetEquipmentChangeLog(int ApplicationID)
        {
            List<t_ChangeLog> log = new List<t_ChangeLog>();
            try
            {
                using (EquipmentEntities enEquip = new EquipmentEntities())
                {
                    log = (from data in enEquip.t_ChangeLog
                           where data.ApplicationID == ApplicationID
                           orderby data.ChangeDate descending
                           select data).ToList<t_ChangeLog>();
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return log;
        }
    }
}