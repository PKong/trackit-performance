﻿using System;

namespace TrackIT2.BLL
{
    /// <summary>
    /// Extension Methods
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// To File Size
        /// </summary>
        /// <param name="l">long</param>
        /// <returns>string</returns>
        public static string ToFileSize(this long l)
        {
            return String.Format(new FileSizeFormatProvider(), "{0:fs}", l);
        }
        //-->
    }
}