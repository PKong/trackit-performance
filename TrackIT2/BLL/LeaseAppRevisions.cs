﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    public static class LeaseAppRevisions
    {
       private const String MsLastError = "";

       public static String GetLastError()
       {
          return MsLastError;
       }
       
       #region Transaction Methods

        public static leaseapp_revisions Search(int id)
        {
            leaseapp_revisions ret = null;

            using (var ce = new CPTTEntities())
            {
                ret = ce.leaseapp_revisions.First(c => c.id.Equals(id));
                if (ret != null) ce.Detach(ret);
            }

            return ret;
        }

        public static leaseapp_revisions SearchWithinTransaction(int id, CPTTEntities ceRef)
        {
           leaseapp_revisions ret = null;

           ret = ceRef.leaseapp_revisions.First(c => c.id.Equals(id));
           if (ret != null) ceRef.Detach(ret);

           return ret;
        }

        public static string Add(leaseapp_revisions objRV)
        {
           string results = null;

           // Start transaction for update.
           using (var ce = new CPTTEntities())
           {
              var isSuccess = false;
              try
              {
                 using (var trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                    ce.leaseapp_revisions.AddObject(objRV);
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                 }
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                 isSuccess = false;
                 results = "An error occurred creating the record. Please try again.";
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }

           return results;
        }

        /// <summary>
        ///    Adds revision objects within an existing context.
        ///    This is needed when doing updates as part of a full lease
        ///    application update. It is assumed that the call to the method is
        ///    within a transaction block outside this method. This method will
        ///    not make a call to trans.complete or ce.SaveChanges since it will
        ///    be the responsibility of the calling method to make the final
        ///    save and allow for a full rollback if an error occurs.
        /// </summary>
        /// <param name="objRV">Lease Application Revision object to add.</param>
        /// <param name="ceRef">Entity Context Reference.</param>
        /// <returns></returns>
        public static string AddWithinTransaction(leaseapp_revisions objRV, CPTTEntities ceRef)
        {
           string results = null;

            try
            {
               ceRef.leaseapp_revisions.AddObject(objRV);
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
               results = "An error occurred creating the record. Please try again.";
            }

           return results;
        }

        public static string Update(leaseapp_revisions objRV)
        {
           string results = null;

           // Start transaction for update.
           using (var ce = new CPTTEntities())
           {
              var isSuccess = false;
              try
              {
                 using (var trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();
                    
                    ce.AttachUpdated(objRV);
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                 }
              }
              catch (OptimisticConcurrencyException oex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                 isSuccess = false;
                 results = "The current record has been modifed since you have " +
                           "last retrieved it. Please reload the record and " +
                           "attempt to save it again.";
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                 isSuccess = false;
                 results = "An error occurred saving the record. Please try again.";
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }

           return results;
        }

       /// <summary>
       ///    Updates revision objects within an existing context.
       ///    This is needed when doing updates as part of a full lease
       ///    application update. It is assumed that the call to the method is
       ///    within a transaction block outside this method. This method will
       ///    not make a call to trans.complete or ce.SaveChanges since it will
       ///    be the responsibility of the calling method to make the final
       ///    save and allow for a full rollback if an error occurs.
       /// </summary>
       /// <param name="objRV">Lease Application Revision object to update.</param>
       /// <param name="ceRef">Entity Context Reference.</param>
       /// <returns></returns> 
       public static string UpdateWithinTransaction(leaseapp_revisions objRV, CPTTEntities ceRef)
        {
           string results = null;

            try
            {
               ceRef.AttachUpdated(objRV);
            }
            catch (OptimisticConcurrencyException oex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(oex);
               results = "The current record has been modifed since you have " +
                        "last retrieved it. Please reload the record and " +
                        "attempt to save it again.";
            }
            catch (Exception ex)
            {
               // Log error in ELMAH for any diagnostic needs.
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
               results = "An error occurred saving the record. Please try again.";
            }

           return results;
        }

        public static String Delete(int id, CPTTEntities ceRef = null)
        {
           String sResult = null;
           var isSuccess = false;
           var isRootTransaction = true;

           CPTTEntities ce;
           if (ceRef == null)
           {
              // Using new entity object
              ce = new CPTTEntities();
           }
           else
           {
              // Using current entity object
              ce = ceRef;
              isRootTransaction = false;
           }
           try
           {
              using (var trans = TransactionUtils.CreateTransactionScope())
              {
                 if (isRootTransaction)
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();
                 }

                 // Delete record
                 var queryRevision = from revision in ce.leaseapp_revisions
                                     where revision.id == id
                                     select revision;

                 if (queryRevision.Any())
                 {
                    var item = queryRevision.First();
                    ce.DeleteObject(item);
                 }
                 else
                 {
                    throw new NullReferenceException();
                 }

                 if (isRootTransaction)
                 {
                    // We tentatively save the changes so that we can finish 
                    // processing the transaction.
                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                 }

                 isSuccess = true;
                 trans.Complete();     //Transaction complete.
              }
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

              sResult = ex.ToString();
              isSuccess = false;
           }
           finally
           {
              if (isRootTransaction)
              {
                 ce.Connection.Dispose();
              }
           }

           // Finally accept all changes from the transaction.
           if (isSuccess && isRootTransaction)
           {
              ce.AcceptAllChanges();
              ce.Dispose();
           }

           return sResult;
        }

        /// <summary>
        ///    Updates revision objects within an existing context.
        ///    This is needed when doing updates as part of a full lease
        ///    application update. It is assumed that the call to the method is
        ///    within a transaction block outside this method. This method will
        ///    not make a call to trans.complete or ce.SaveChanges since it will
        ///    be the responsibility of the calling method to make the final
        ///    save and allow for a full rollback if an error occurs.
        /// </summary>
        /// <param name="id">Lease Application Revision to remove.</param>
        /// <param name="ceRef">Entity Context Reference.</param>
        /// <returns></returns>
        public static String DeleteWithinContext(int id, CPTTEntities ceRef)
        {
           String sResult = null;

           try
           {              
               // Delete record
               var queryRevision = from revision in ceRef.leaseapp_revisions
                                    where revision.id == id
                                    select revision;

               if (queryRevision.Any())
               {
                  var item = queryRevision.First();
                  ceRef.DeleteObject(item);
               }

               else
               {
                  throw new NullReferenceException();
               }
           }
           catch (Exception ex)
           {
              // Log error in ELMAH for any diagnostic needs.
              Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
              sResult = "Error removing lease application revision.";
           }

           return sResult;
        }
        #endregion


        public static int GetJSON(Boolean isNull, out String sJSON, int iAppID = 0)
        {
            var iRet = 0;
            var results = "";
            using (var ce = new CPTTEntities())
            {
                if (!isNull && iAppID != 0)
                {
                    var query = from rev in (
                                from revision in ce.leaseapp_revisions
                                where revision.lease_application_id == iAppID
                                select revision).ToList()
                                select new json_leaseapp_revisions
                                {
                                    ID = rev.id,
                                    LA_id = rev.lease_application_id,
                                    revision_rcvd_date = Utility.DateToString(rev.revision_rcvd_date),
                                    revision_fee_rcvd_date = Utility.DateToString(rev.revision_fee_rcvd_date),
                                    revision_fee_check_po_nbr = rev.revision_fee_check_po_nbr,
                                    revision_fee_amount = rev.revision_fee_amount,
                                    revision_updated_at = Utility.DateTimeToString(rev.updated_at)
                                };

                    if (query.Any())
                    {
                        results = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        iRet = query.Count();
                    }
                }
                else 
                {
                    var query = new
                                {
                                    ID = 0,
                                    LA_id = iAppID,
                                    revision_rcvd_date = (object)null,
                                    revision_fee_rcvd_date = (object)null,
                                    revision_fee_check_po_nbr = (object)null,
                                    revision_fee_amount = (object)null,
                                    revision_updated_at = (object)null
                                };

                    if (query != null)
                    {
                        results = Newtonsoft.Json.JsonConvert.SerializeObject(query);
                        iRet = 1;
                    }
                }
            }
            sJSON = results;
            return iRet;
        }

        public static List<LeaseAppRevisionsTemplate> ParseFromJson(String sJson)
        {
            var lstResult = new List<LeaseAppRevisionsTemplate>();

            if (!string.IsNullOrEmpty(sJson))
            {
                const string jsonResultFormat = "\"result\" : {0}";
                var sTestJson = "{" + String.Format(jsonResultFormat, sJson) + "}";
                var jsonResult = JsonConvert.DeserializeObject<LeaseAppRevisionResult>(sTestJson);
                lstResult = jsonResult.Result;
            }
            
            return lstResult;
        }
    }
    [Serializable]
    public class LeaseAppRevisionResult
    {
        public List<LeaseAppRevisionsTemplate> Result { get; set; }
    }
    public class LeaseAppRevisionsTemplate
    {
        public String ID {get;set;}
        public String LA_id {get;set;}

        private String m_revision_rcvd_date;
        public String revision_rcvd_date
        {
            get
            {
                return m_revision_rcvd_date;
            }
            set
            {
                CheckIsNull(value);
                m_revision_rcvd_date = value;
            }
        }
        private String m_revision_fee_rcvd_date;
        public String revision_fee_rcvd_date
        {
            get
            {
                return m_revision_fee_rcvd_date;
            }
            set
            {
                CheckIsNull(value);
                m_revision_fee_rcvd_date = value;
            }
        }
        private String m_revision_fee_check_po_nbr;
        public String revision_fee_check_po_nbr
        {
            get
            {
                return m_revision_fee_check_po_nbr;
            }
            set
            {
                CheckIsNull(value);
                m_revision_fee_check_po_nbr = value;
            }
        }
        private String m_revision_fee_amount;
        public String revision_fee_amount
        {
            get
            {
                return m_revision_fee_amount;
            }
            set
            {
                CheckIsNull(value);
                m_revision_fee_amount = value;
            }
        }

       private String m_revision_updated_at;
       public String revision_updated_at
       {
          get { return m_revision_updated_at; }
          set 
          { 
             CheckIsNull(value);
             m_revision_updated_at = value;
          }
       }

        private Boolean m_IsNull = true;
        private void CheckIsNull(String sValue)
        {
            if (sValue != null && sValue != "")
            {
                m_IsNull = false;
            }
        }
        public Boolean CheckIsNull()
        {
            return m_IsNull & (ID == "0");
        }
    }
    public class json_leaseapp_revisions
    { 
        public int? ID { get; set; }
        public int? LA_id { get; set; } 
        public string revision_rcvd_date { get; set; }
        public string revision_fee_rcvd_date {get; set;}
        public int? revision_fee_check_po_nbr { get; set; }
        public Decimal? revision_fee_amount { get; set; }
        public string revision_updated_at { get; set; }
    }
}