﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Linq.Expressions;

namespace TrackIT2.BLL
{
    public class AlphanumComparatorFast : IComparer
    {
        public int Compare(object x, object y)
        {
            string s1 = x as string;
            if (s1 == null)
            {
                return 0;
            }
            string s2 = y as string;
            if (s2 == null)
            {
                return 0;
            }

            int len1 = s1.Length;
            int len2 = s2.Length;
            int marker1 = 0;
            int marker2 = 0;

            while (marker1 < len1 && marker2 < len2)
            {
                char ch1 = s1[marker1];
                char ch2 = s2[marker2];

                char[] space1 = new char[len1];
                int loc1 = 0;
                char[] space2 = new char[len2];
                int loc2 = 0;

                do
                {
                    space1[loc1++] = ch1;
                    marker1++;

                    if (marker1 < len1)
                    {
                        ch1 = s1[marker1];
                    }
                    else
                    {
                        break;
                    }
                } while (char.IsDigit(ch1) == char.IsDigit(space1[0]));

                do
                {
                    space2[loc2++] = ch2;
                    marker2++;

                    if (marker2 < len2)
                    {
                        ch2 = s2[marker2];
                    }
                    else
                    {
                        break;
                    }
                } while (char.IsDigit(ch2) == char.IsDigit(space2[0]));

                string str1 = new string(space1);
                string str2 = new string(space2);

                int result;

                if (char.IsDigit(space1[0]) && char.IsDigit(space2[0]))
                {
                    int thisNumericChunk = int.Parse(str1);
                    int thatNumericChunk = int.Parse(str2);
                    result = thisNumericChunk.CompareTo(thatNumericChunk);
                }
                else
                {
                    result = str1.CompareTo(str2);
                }

                if (result != 0)
                {
                    return result;
                }
            }
            return len1 - len2;
        }
    }
    /// <summary>
    /// A comparer that enables the user to define a list of properties to
    /// compare by. This comparer will cause value type properties that are
    /// included in the sort order to be boxed when comparing.
    /// </summary>
    /// <typeparam name="T">The type this comparer acts on</typeparam>
    public class DynamicComparer<T> : IComparer<T>
    {
        Dictionary<string, Func<T, IComparable>> properties = new Dictionary<string, Func<T, IComparable>>();
        List<Func<T, IComparable>> sortOrder = new List<Func<T, IComparable>>();
        List<string> sortOrderNames = new List<string>();
        List<string> uncomparable = new List<string>();

        /// <summary>
        /// Create a DynamicComparer object.
        /// </summary>
        public DynamicComparer()
        {
            var ps = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            foreach (var p in ps)
            {
                if (!typeof(IComparable).IsAssignableFrom(p.PropertyType))
                {
                    uncomparable.Add(p.Name);
                    continue;
                }
                ParameterExpression pe = Expression.Parameter(typeof(T), "x");
                MemberExpression me = Expression.MakeMemberAccess(pe, p);
                Expression<Func<T, IComparable>> le;
                if (!p.PropertyType.IsValueType)
                {
                    le = Expression.Lambda<Func<T, IComparable>>(me, pe);
                }
                else
                {
                    UnaryExpression ue = Expression.Convert(me, typeof(IComparable));
                    le = Expression.Lambda<Func<T, IComparable>>(ue, pe);
                }
                var f = le.Compile();
                properties[p.Name] = (Func<T, IComparable>)f;
            }
        }

        string GetNameFromExpression(Expression<Func<T, IComparable>> x)
        {
            LambdaExpression le = (LambdaExpression)x;
            UnaryExpression ue = le.Body as UnaryExpression;
            MemberExpression me;
            if (ue != null)
                me = (MemberExpression)ue.Operand;
            else
                me = (MemberExpression)le.Body;
            return me.Member.Name;
        }

        /// <summary>
        /// Reset the sort order (unsorted).
        /// </summary>
        public void Reset()
        {
            sortOrder.Clear();
            sortOrderNames.Clear();
        }

        /// <summary>
        /// Specify the sort order as a sequence of strings representing property
        /// names. This is unsafe as no compile time checks are made to make sure
        /// the property names actually exist in the type being compared, or that
        /// they are of a comparable type (implementing IComparable).
        /// </summary>
        /// <param name="sortProperties">The property names to sort by</param>
        public void SortOrder(params string[] sortProperties)
        {
            string err = sortProperties.FirstOrDefault(x => uncomparable.Contains(x));
            if (err != null)
                throw new InvalidOperationException("Property '" + err + "' does not implement IComparable");
            err = sortProperties.FirstOrDefault(x => !properties.ContainsKey(x));
            if (err != null)
                throw new InvalidOperationException("Property '" + err + "' does not exist");
            sortOrder.Clear();
            sortOrderNames.Clear();
            sortOrder.AddRange(sortProperties.Select(x => properties[x]));
            sortOrderNames.AddRange(sortProperties);
        }

        /// <summary>
        /// Specify the sort order as a sequence of member access lambda expressions.
        /// This is type safe; a compiler error will ensue if the lambda expressions
        /// don't represent valid properties that implement IComparable. It also
        /// ensures that the properties are accessible to the caller.
        /// </summary>
        /// <param name="sortProperties">The properties to sort by</param>
        public void SortOrder(params Expression<Func<T, IComparable>>[] sortProperties)
        {
            sortOrder.Clear();
            sortOrderNames.Clear();
            sortOrder.AddRange(sortProperties.Select(x => x.Compile()));
            sortOrderNames.AddRange(sortProperties.Select(x => GetNameFromExpression(x)));
        }

        /// <summary>
        /// Returns a string representation of this object. The string representation
        /// includes the type argument, and the list of property names in the sort
        /// order list.
        /// </summary>
        /// <returns>A string representation of this object. The string representation
        /// includes the type argument, and the list of property names in the sort
        /// order list.</returns>
        public override string ToString()
        {
            var desc = string.Join(", ", sortOrderNames.ToArray());
            return "Comparing " + typeof(T).ToString() + " by " + desc;
        }

        #region IComparer<T> Members

        /// <summary>
        /// Compares two instances of the type argument.
        /// </summary>
        /// <param name="x">The first instance</param>
        /// <param name="y">The second instance</param>
        /// <returns>Returns 0 if the objects are equal, -1 if the first is less
        /// than the second, and 1 if the first is greater than the second.</returns>
        public int Compare(T x, T y)
        {
            foreach (var l in sortOrder)
            {
                int c = new AlphanumComparatorFast().Compare(l(x), (l(y)));
                if (c != 0) return c;
            }
            return 0;
        }

        #endregion
    }
}