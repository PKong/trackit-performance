﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using TrackIT2.DAL;                                      
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
   public static class Security
   {
      public static int GetUserId(string userName)
      {
         int results = 0;

         CPTTEntities ce = new CPTTEntities();
         var currUser = from u in ce.users
                        join au in ce.aspnet_Users on u.aspnet_user_id equals au.UserId
                        where au.UserName == userName
                        select u;

         if (currUser.First() != null)
         {
            results = ((user)currUser.First()).id;
         }
        
         return results;
      }

      public static bool UserIsImpersonating()
      {
         bool results = false;

         if (HttpContext.Current.Session["ImpersonatedRole"] == null)
         {
            results = false;
         }
         else
         {
            results = true;
         }

         return results;
      }

      public static string GetImpersonatedRole()
      {
         string results = "None";

         if (HttpContext.Current.Session["ImpersonatedRole"] != null)
         {
            results = (string) HttpContext.Current.Session["ImpersonatedRole"];
         }
         else
         {
            results = "None";
         }

         return results;
      }

      public static bool UserIsInRole(string roleName)
      {
         // Method takes into account administrators that may be impersonating
         // a role when evaluating the result.
         bool results = false;

         if (!UserIsImpersonating())
         {
            results = HttpContext.Current.User.IsInRole(roleName);
         }
         else
         {
            string impersonatedRole = GetImpersonatedRole();

            if (impersonatedRole == roleName)
            {
               results = true;
            }
            else
            {
               results = false;
            }
         }

         return results;
      }

      public static bool UserCanRemoveComment(string userName, int userId, int commentId, string commentSource)
      {
          bool results = false;
          CPTTEntities ce = new CPTTEntities();
          List<string> userRoles = new List<string>();

          if (UserIsImpersonating())
          {
              userRoles.Add(GetImpersonatedRole());
          }
          else
          {
              userRoles = new List<string>(Roles.GetRolesForUser(userName));
          }

          if (commentSource.ToUpper() == "SA")
          {
              structural_analysis_comments comment;
              comment = ce.structural_analysis_comments
                          .Where(sac => sac.id.Equals(commentId))
                          .FirstOrDefault();

              if (comment != null)
              {
                  if (userId == comment.creator_id ||
                      userRoles.Contains("Administrator"))
                  {
                      results = true;
                  }
              }
              else
              {
                  results = false;
              }
          }
          else if (commentSource.ToUpper() == "LE")
          {
              lease_admin_comments comment;
              comment = ce.lease_admin_comments
                          .Where(sac => sac.id.Equals(commentId))
                          .FirstOrDefault();

              if (comment != null)
              {
                  if (userId == comment.creator_id ||
                      userRoles.Contains("Administrator"))
                  {
                      results = true;
                  }
              }
              else
              {
                  results = false;
              }
          }
          else if (commentSource.ToUpper() == "TM")
          {
              tower_mod_comments comment;
              comment = ce.tower_mod_comments
                          .Where(sac => sac.id.Equals(commentId))
                          .FirstOrDefault();

              if (comment != null)
              {
                  if (userId == comment.creator_id ||
                      userRoles.Contains("Administrator"))
                  {
                      results = true;
                  }
              }
              else
              {
                  results = false;
              }
          }
          else if (commentSource.ToUpper() == "SDP")
          {
              sdp_comments comment;
              comment = ce.sdp_comments
                          .Where(sac => sac.id.Equals(commentId))
                          .FirstOrDefault();

              if (comment != null)
              {
                  if (userId == comment.creator_id ||
                      userRoles.Contains("Administrator"))
                  {
                      results = true;
                  }
              }
              else
              {
                  results = false;
              }
          }
          else
          {
              leaseapp_comments comment;
              comment = ce.leaseapp_comments
                          .Where(lac => lac.id.Equals(commentId))
                          .FirstOrDefault();

              if (comment != null)
              {
                  if (userId == comment.creator_id ||
                      userRoles.Contains("Administrator"))
                  {
                      results = true;
                  }
              }
              else
              {
                  results = false;
              }
          }

          return results;
      }

      public static List<string> GetUserRoles(string userName, int userId)
      {
          List<string> userRoles = new List<string>();

          if (UserIsImpersonating())
          {
              userRoles.Add(GetImpersonatedRole());
          }
          else
          {
              userRoles = new List<string>(Roles.GetRolesForUser(userName));
          }

          return userRoles;
      }

      public static bool UserCanRemoveComment(List<string> userRoles, int userId, int commentId, string commentSource, List<AllComments> lstComment)
      {
          bool results = false;
          var item = (from comment in lstComment
                         where comment.CommentID.Equals(commentId)
                         && comment.Source == commentSource
                      select comment).FirstOrDefault();
          if (item != null)
          {
              if (userId == item.CreatorID ||
                  userRoles.Contains("Administrator"))
              {
                  results = true;
              }
          }
          else
          {
              results = false;
          }
          return results;
      }

      public static bool UserCanViewField(string userName, string tableName, string fieldName, List<string> userRoles, List<application_field_access> application_field_access)
      {
          bool results = true;

          // Security functions in a blacklist layout. By default any user can
          // view a record, unless we find a record in the table. Then we check 
          // the access record for specific cases. A User may also be in multiple 
          // roles and if any of them allow view access, we set our results 
          // accordingly. 

          // If the field access count (roles that are allowed access to the 
          // field) has at least 1 record with CanView set to true, we have at 
          // least one role with proper view access to the field, and we can grant 
          // the user access to the field.
          var access = from fieldAccess in application_field_access
                       where userRoles.Contains(fieldAccess.name)
                       where fieldAccess.table_name.ToLower().Equals(tableName.ToLower())
                       where fieldAccess.field_name.ToLower().Equals(fieldName.ToLower())
                       select fieldAccess;

          if (access.Count() == 0 || access.Count() < userRoles.Count())
          {
              results = true;
          }
          else
          {
              // For a final check, there may have been access records that overwrite
              // the default blacklist model and allow a user/role to view a field.
              // We do one final query against our access records for any rows with a
              // can_view value set to true and set our results accordingly.
              var viewCheck = from viewAccess in access
                              where viewAccess.can_view
                              select viewAccess;

              if (viewCheck.Count() > 0)
              {
                  results = true;
              }
              else
              {
                  results = false;
              }
          }

          return results;
      }

      public static bool UserCanEditField(string userName, string tableName, string fieldName, List<string> userRoles, List<application_field_access> application_field_access)
      {
          bool results = true;

          // Security functions in a blacklist layout. By default any user can
          // edit a record, unless we find a record in the table. Then we check 
          // the access record for specific cases. A User may also be in multiple 
          // roles and if any of them allow edit access, we set our results 
          // accordingly. 

          // If the field access count (roles that are allowed access to the 
          // field) has at least 1 record with CanEdit set to true, we have at 
          // least one role with proper view access to the field, and we can grant 
          // the user access to the field.
          var access = from fieldAccess in application_field_access
                       where userRoles.Contains(fieldAccess.name)
                       where fieldAccess.table_name.ToLower().Equals(tableName.ToLower())
                       where fieldAccess.field_name.ToLower().Equals(fieldName.ToLower())
                       select fieldAccess;

          if (access.Count() == 0 || access.Count() < userRoles.Count())
          {
              results = true;
          }
          else
          {
              // For a final check, there may have been access records that overwrite
              // the default blacklist model and allow a user/role to modify a field.
              // We do one final query against our access records for any rows with a
              // can_edit value set to true and set our results accordingly.
              var editCheck = from editAccess in access
                              where editAccess.can_edit
                              select editAccess;

              if (editCheck.Count() > 0)
              {
                  results = true;
              }
              else
              {
                  results = false;
              }
          }

          return results;
      }

      public static bool UserCanEditField(string userName, string tableName, string fieldName)
      {
         bool results = true;
         CPTTEntities ce = new CPTTEntities();
         List<string> userRoles = new List<string>();

         // If an administrator is impersonating a different user role, use that role
         // instead of the admin role.
         if (UserIsImpersonating())
         {
            userRoles.Add(GetImpersonatedRole());
         }
         else
         {
            userRoles = new List<string>(Roles.GetRolesForUser(userName));
         }

         // Security functions in a blacklist layout. By default any user can
         // edit a record, unless we find a record in the table. Then we check 
         // the access record for specific cases. A User may also be in multiple 
         // roles and if any of them allow edit access, we set our results 
         // accordingly. 

         // If the field access count (roles that are allowed access to the 
         // field) has at least 1 record with CanEdit set to true, we have at 
         // least one role with proper view access to the field, and we can grant 
         // the user access to the field.
         var access = from fieldAccess in ce.application_field_access
                      where userRoles.Contains(fieldAccess.name)
                      where fieldAccess.table_name.Equals(tableName)
                      where fieldAccess.field_name.Equals(fieldName)
                      select fieldAccess;

         if (access.Count() == 0 || access.Count() < userRoles.Count())
         {
            results = true;
         }
         else
         {
            // For a final check, there may have been access records that overwrite
            // the default blacklist model and allow a user/role to modify a field.
            // We do one final query against our access records for any rows with a
            // can_edit value set to true and set our results accordingly.
            var editCheck = from editAccess in access
                            where editAccess.can_edit
                            select editAccess;

            if (editCheck.Count() > 0)
            {
               results = true;
            }
            else
            {
               results = false;
            }
         }

         return results;
      }

      public static bool UserCanViewField(string userName, string tableName, string fieldName)
      {
         bool results = true;
         CPTTEntities ce = new CPTTEntities();
         List<string> userRoles = new List<string>();

         // If an administrator is impersonating a different user role, use that role
         // instead of the admin role.
         if (UserIsImpersonating())
         {
            userRoles.Add(GetImpersonatedRole());
         }
         else
         {
            userRoles = new List<string>(Roles.GetRolesForUser(userName));
         }

         // Security functions in a blacklist layout. By default any user can
         // view a record, unless we find a record in the table. Then we check 
         // the access record for specific cases. A User may also be in multiple 
         // roles and if any of them allow view access, we set our results 
         // accordingly. 

         // If the field access count (roles that are allowed access to the 
         // field) has at least 1 record with CanView set to true, we have at 
         // least one role with proper view access to the field, and we can grant 
         // the user access to the field.
         var access = from fieldAccess in ce.application_field_access
                      where userRoles.Contains(fieldAccess.name)
                      where fieldAccess.table_name.Equals(tableName)
                      where fieldAccess.field_name.Equals(fieldName)
                      select fieldAccess;

         if (access.Count() == 0 || access.Count() < userRoles.Count())
         {
            results = true;
         }
         else
         {
            // For a final check, there may have been access records that overwrite
            // the default blacklist model and allow a user/role to view a field.
            // We do one final query against our access records for any rows with a
            // can_view value set to true and set our results accordingly.
            var viewCheck = from viewAccess in access
                            where viewAccess.can_view
                            select viewAccess;

            if (viewCheck.Count() > 0)
            {
               results = true;
            }
            else
            {
               results = false;
            }
         }

         return results;
      }

      public static List<application_field_access> GetFieldAccessForUser(string userName)
      {
         List<application_field_access> results = new List<application_field_access>();
         CPTTEntities ce = new CPTTEntities();
         string[] userRoles;

         userRoles = Roles.GetRolesForUser(userName);

         // User may be in multiple roles and if any of them allow edit access, we
         // set our results accordingly.                
         var access = from fieldAccess in ce.application_field_access
                      where userRoles.Contains(fieldAccess.name)
                      select fieldAccess;

         results = access.ToList();

         return results;
      }

      public static bool IsFieldViewable(List<metadata> metaList, string tableName, string columnName)
      {
         bool results = false;

         var query = from meta in metaList
                     where meta.table.ToLower().Equals(tableName.ToLower())
                     where meta.column.ToLower().Equals(columnName.ToLower())
                     select meta;

         if (query.Count() > 0)
         {
            results = true;
         }
         else
         {
            results = false;
         }

         return results;
      }

      public static bool IsDisplayGroupVisible(string group, ProjectCategory category)
      {
         bool results = false;
         CPTTEntities ce = new CPTTEntities();
         List<metadata> groupFields = new List<metadata>();
         List<metadata> viewFields = MetaData.GetViewableFieldsByCategory(category);

         groupFields = ce.metadatas
                         .Where(m => m.metagroup.name.Equals(group))
                         .ToList();


         foreach (metadata currMeta in groupFields)
         {
            if (IsFieldViewable(viewFields, currMeta.table, currMeta.column))
            {
               results = true;
               break;
            }
         }

         return results;
      }

      public static bool IsDisplayGroupVisible(string group, ProjectCategory category, List<metadata> metadatasList, List<metadata> viewFields)
      {
          bool results = false;
          List<metadata> groupFields = new List<metadata>();

          groupFields = metadatasList
                          .Where(m => m.metagroup.name.ToLower().Equals(group.ToLower()))
                          .ToList();

          foreach (metadata currMeta in groupFields)
          {
              if (IsFieldViewable(viewFields, currMeta.table, currMeta.column))
              {
                  results = true;
                  break;
              }
          }

          return results;
      }

      public static bool UserCanAccessPage()
      {
         // Access to pages within TrackIT2 is allowed in most cases, except
         // for a couple unique scenarios.
         bool results = true;
         HttpContext context = HttpContext.Current;

         // The admin section is only allowed for administrators.
         if (context.Request.Path.ToLower() == "admin")
         {
            if (!context.User.IsInRole("Administrator"))
            {
               results = false;
            }
         }

         // Users that are solely in the TrackiT Docs User or TrackiT Docs Admin role are only 
         // allowed access to the DMS section and their account related pages.
         if (
             Roles.GetRolesForUser(context.User.Identity.Name).Count() == 1 &&
             (context.User.IsInRole("TrackiT Docs User") || context.User.IsInRole("TrackiT Docs Administrator"))
            )
         {
            if (
                context.Request.Path.ToLower() != "dms" &&
                context.Request.Path.ToLower() != "account"
               )
            {
               results = false;
            }  
         }

         // Similarly, users that are not in a DMS role do not have accesss to
         // the DMS pages.
         if (context.Request.Path.ToLower() == "dms" &&
             !context.User.IsInRole("TrackiT Docs User") &&
             !context.User.IsInRole("TrackiT Docs Admin") &&
             !context.User.IsInRole("Administrator")
            )
         {
            results = false;
         }

         return results;
      }

      public static List<application_field_access> GetApplicationFieldAccess()
      {
          CPTTEntities ce = new CPTTEntities();

          var application_field_access = ce.application_field_access.ToList();
          return application_field_access;
      }
   }
}