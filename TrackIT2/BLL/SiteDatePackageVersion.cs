﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Reflection;
using System.Transactions;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;

namespace TrackIT2.BLL
{
    public static class SiteDatePackageVersion
    {
        public static string Add(site_data_package_versions version)
        {
           String results = null;
           bool isSuccess = false;

           // Start transaction for add.
           using (CPTTEntities ce = new CPTTEntities())
           {
              try
              {
                 using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                    ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                 }
              }
              catch (Exception ex)
              {
                 // Log error in ELMAH for any diagnostic needs.
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                 isSuccess = false;
                 results = ex.ToString();
              }
              finally
              {
                 ce.Connection.Dispose();
              }

              // Finally accept all changes from the transaction.
              if (isSuccess)
              {
                 ce.AcceptAllChanges();
              }
           }

           return results;
        }

        public static string Delete(int sdpID, int lockVersion)
        {
            String results = null;
            bool isSuccess = false;

           // Start transaction for delete.
           using (CPTTEntities ce = new CPTTEntities())
           {
              try
              {
                 using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                 {
                    // Manually open connection to prevent EF from auto closing
                    // connection and causing issues with future queries.
                    ce.Connection.Open();

                     site_data_package_versions version;

                     version = ce.site_data_package_versions
                                 .Where(ver => ver.site_data_package_id.Equals(sdpID))
                                 .Where(ver => ver.lock_version.Equals(lockVersion))
                                 .First();

                     ce.DeleteObject(version);

                     ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                     isSuccess = true;
                     trans.Complete();     // Transaction complete
                  }
               }
               catch (Exception ex)
               {
                  // Error occurs
                  // Log error in ELMAH for any diagnostic needs.
                  Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                  isSuccess = false;
                  results = ex.ToString();
               }
               finally
               {
                  ce.Connection.Dispose();
               }

               // Finally accept all changes from the transaction.
               if (isSuccess)
               {
                  ce.AcceptAllChanges();
               }
            }

         return results;
        }

        public static List<VersionLog> GenerateVersionLog(int id)
        {
            List<VersionLog> results = new List<VersionLog>();

            CPTTEntities ce = new CPTTEntities();
            List<site_data_package_versions> versions;
            List<user> userList;
            site_data_packages currSDP;

            versions = ce.site_data_package_versions
                         .Where(ver => ver.site_data_package_id.Equals(id))
                         .OrderBy(Utility.GetIntField<site_data_package_versions>("id"))
                         .ToList();

            userList = ce.users.ToList();

            // Add the current version of the lease application to properly
            // track differences up to the most recent copy.
            currSDP = ce.site_data_packages
                              .Where(la => la.id.Equals(id))
                              .First();

            versions.Add(BLL.SiteDataPackage.ToSDPVersion(currSDP));

            for (int i = 1; i < versions.Count; i++)
            {
                site_data_package_versions currVersion = versions.ElementAt(i);
                site_data_package_versions prevVersion = versions.ElementAt(i - 1);
                user userInfo;

                // Use reflection to get all modifiyable properties
                var properties = from p in typeof(site_data_package_versions).GetProperties()
                                 where p.CanRead &&
                                       p.CanWrite &&
                                       p.Name != "id" &&
                                       p.Name != "created_at" &&
                                       p.Name != "updated_at" &&
                                       p.Name != "updater_id" &&
                                       p.Name != "creater_id" &&
                                       p.Name != "lock_version" &&
                                       p.Name != "EntityKey"
                                 select p;

                foreach (var property in properties)
                {
                    var currValue = property.GetValue(currVersion, null);
                    var prevValue = property.GetValue(prevVersion, null);

                    if (
                          currValue == null && prevValue != null ||
                          currValue != null && prevValue == null ||
                          (
                          currValue != null && prevValue != null &&
                          currValue.ToString() != prevValue.ToString()
                          )
                       )
                    {
                        VersionLog log = new VersionLog();

                        log.version = currVersion.lock_version;
                        log.fieldChanged = property.Name;

                        if (currVersion.updater_id != null)
                        {
                            var query = from ul in userList
                                        where ul.id == currVersion.updater_id
                                        select ul;

                            userInfo = query.FirstOrDefault();

                            if (userInfo != null)
                            {
                                log.updatedBy = userInfo.last_name + ", " +
                                                userInfo.first_name;
                            }
                            else
                            {
                                log.updatedBy = "None";
                            }
                        }

                        if (currVersion.updated_at != null)
                        {
                            log.updatedAt = currVersion.updated_at.Value;
                        }

                        if (prevValue != null)
                        {
                            log.oldValue = prevValue.ToString();
                        }

                        if (currValue != null)
                        {
                            log.newValue = currValue.ToString();
                        }

                        results.Add(log);
                    }
                }
            }

            return results;
        }
    }
}