﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Text;
using System.Transactions;
using System.Data;

namespace TrackIT2.BLL
{
    public class TowerListStatus
    {
        public enum TowerListUpdateAction
        {
            ADD,
            DELETE,
            UPDATE,
            NONE
        }

        public enum TowerListType
        {
            TrackiT = 0,
            Map = 1
        }

        public const string DELETE_ERROR = "This site cannot be removed because other records reference this site. Please delete these records first and then try removing the site again.";
        public const string GRID_FORMAT = "<div><ul class=\"tower-status-menu\">{0}</ul></div>";
        public const string DEFAULT_ON = "Default On";
        public const string DEFAULT_OFF = "Default Off";
        public const string ALWAYS_ON = "Always On";
        public const string ALWAYS_OFF = "Always Off";
        public const string NULL = "";
        public const string DISPLAY = "block";
        public const string NOT_DISPLAY = "none";

        public const string TOWER_STATUS_NULL = "<li siteid=\"{2}\" status=\"\" type=\"{1}\" style=\"display:{3};\" ><a href=\"javascript:void(0)\" newStatus=\"\" currentStatus=\"{0}\" type=\"{1}\" siteid=\"{2}\" class=\"statusLink\" style=\"display: block; height: 18px;\" ><img alt=\"\" src=\"/images/icons/Dash-Greyscale.png\" /></a></li>";
        public const string TOWER_STATUS_DEFAULT_ON = "<li siteid=\"{3}\" status=\"{0}\" type=\"{2}\" style=\"display:{4};\" ><a href=\"javascript:void(0)\" newStatus=\"{0}\" currentStatus=\"{1}\" type=\"{2}\" siteid=\"{3}\" class=\"statusLink\" style=\"display: block; height: 18px;\" >Default On <img class=\"tower-current-status-icon\" alt=\"Default On\" src=\"/images/icons/Apply_Greyscale.png\" /></a></li>";
        public const string TOWER_STATUS_DEFAULT_OFF = "<li siteid=\"{3}\" status=\"{0}\" type=\"{2}\" style=\"display:{4};\" ><a href=\"javascript:void(0)\" newStatus=\"{0}\" currentStatus=\"{1}\" type=\"{2}\" siteid=\"{3}\" class=\"statusLink\" style=\"display: block; height: 18px;\" >Default Off <img class=\"tower-current-status-icon\" alt=\"Default Off\" src=\"/images/icons/Delete_Greyscale.png\" /></a></li>";
        public const string TOWER_STATUS_AWAYS_ON = "<li siteid=\"{3}\" status=\"{0}\" type=\"{2}\" style=\"display:{4};\" ><a href=\"javascript:void(0)\" newStatus=\"{0}\" currentStatus=\"{1}\" type=\"{2}\" siteid=\"{3}\" class=\"statusLink\" style=\"display: block; height: 18px;\" >Always On <img class=\"tower-current-status-icon\" alt=\"Always On\" src=\"/images/icons/Apply.png\" /></a></li>";
        public const string TOWER_STATUS_AWAYS_OFF = "<li siteid=\"{3}\" status=\"{0}\" type=\"{2}\" style=\"display:{4};\" ><a href=\"javascript:void(0)\" newStatus=\"{0}\" currentStatus=\"{1}\" type=\"{2}\" siteid=\"{3}\" class=\"statusLink\" style=\"display: block; height: 18px;\" >Always Off <img class=\"tower-current-status-icon\" alt=\"Always Off\" src=\"/images/icons/Delete.png\" /></a></li>";

        public const string TOWER_STATUS_BUTTON = "<div class=\"tower-status-container\" style=\"position:relative;\"><a href=\"javascript:void(0)\" class=\"fancy-button-tower tower-status-button\">{0}</a>{1}</div>";

        public const string NULL_STRING = "<span current-text-{1}=\"{0}\"></span> <img  class=\"tower-current-status-icon\"current-img-{1}=\"{0}\"  alt=\"\" src=\"/images/icons/Dash-Greyscale.png\" />";
        public const string DEFAULT_ON_STRING = "<span current-text-{1}=\"{0}\">Default On</span> <img class=\"tower-current-status-icon\" current-img-{1}=\"{0}\" alt=\"Default On\" style=\"width:12px; height:12px\" src=\"/images/icons/Apply_Greyscale.png\" />";
        public const string DEFAULT_OFF_STRING = "<span current-text-{1}=\"{0}\">Default Off</span> <img  class=\"tower-current-status-icon\" current-img-{1}=\"{0}\" alt=\"Default Off\" style=\"width:12px; height:12px\" src=\"/images/icons/Delete_Greyscale.png\" />";
        public const string ALWAYS_ON_STRING = "<span current-text-{1}=\"{0}\">Always On</span> <img  class=\"tower-current-status-icon\" current-img-{1}=\"{0}\" alt=\"Always On\" style=\"width:12px; height:12px\" src=\"/images/icons/Apply.png\" />";
        public const string ALWAYSOFF_STRING = "<span current-text-{1}=\"{0}\">Always Off</span> <img  class=\"tower-current-status-icon\" current-img-{1}=\"{0}\" alt=\"Always Off\" style=\"width:12px; height:12px\" src=\"/images/icons/Delete.png\" />";

        static tower_list_statuses defaultOnStatus = GetTowerListStatus(DEFAULT_ON);
        static tower_list_statuses defaultOffStatus = GetTowerListStatus(DEFAULT_OFF);
        static tower_list_statuses alwaysOnStatus = GetTowerListStatus(ALWAYS_ON);
        static tower_list_statuses alwaysOffStatus = GetTowerListStatus(ALWAYS_OFF);

        public static string GetTrackitTowerStatusList(string towerListId, int? currentStatusId, string currentStatus, tower_list_statuses defaultStatus)
        {
            StringBuilder ret = new StringBuilder();
            string type = TowerListType.TrackiT.ToString();
            string currentWord = string.Empty;
            int currentId = 1;

            if (!currentStatusId.HasValue)
            {
                if (defaultStatus == null)
                {
                    currentStatus = string.Empty;
                    currentWord = string.Format(NULL_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Default On")
                {
                    currentWord = string.Format(DEFAULT_ON_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Default Off")
                {
                    currentWord = string.Format(DEFAULT_OFF_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Always On")
                {
                    currentWord = string.Format(ALWAYS_ON_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Always Off")
                {
                    currentWord = string.Format(ALWAYSOFF_STRING, towerListId, type);
                }
            }
            else
            {
                tower_list_statuses statusObj =  GetTowerListStatus(currentStatusId.Value);
                if(statusObj != null)
                {
                    currentId = currentStatusId.Value;

                    switch (statusObj.list_status)
                    {
                        case "Default On":
                            currentWord = string.Format(DEFAULT_ON_STRING, towerListId, type);
                            break;
                        case "Default Off":
                            currentWord = string.Format(DEFAULT_OFF_STRING, towerListId, type);
                            break;
                        case "Always On":
                            currentWord = string.Format(ALWAYS_ON_STRING, towerListId, type);
                            break;
                        case "Always Off":
                            currentWord = string.Format(ALWAYSOFF_STRING, towerListId, type);
                            break;
                        default:
                            currentWord = string.Format(NULL_STRING, towerListId, type);
                            break;
                    }
                }
            }

            ret = ret = GenarateLiList(currentStatus, currentId, type, towerListId, defaultStatus);

            string tmp = ret.ToString();
            tmp = string.Format(GRID_FORMAT, tmp);
            tmp = string.Format(TOWER_STATUS_BUTTON, currentWord, tmp).ToString();

            return tmp;
        }

        public static string GetMapTowerStatusList(string towerListId, int? currentStatusId, string currentStatus, tower_list_statuses defaultStatus)
        {
            StringBuilder ret = new StringBuilder();
            string type = TowerListType.Map.ToString();
            string currentWord = string.Empty;
            int currentId = 2;

            if (!currentStatusId.HasValue)
            {
                if (defaultStatus == null)
                {
                    currentStatus = string.Empty;
                    currentWord = string.Format(NULL_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Default On")
                {
                    currentWord = string.Format(DEFAULT_ON_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Default Off")
                {
                    currentWord = string.Format(DEFAULT_OFF_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Always On")
                {
                    currentWord = string.Format(ALWAYS_ON_STRING, towerListId, type);
                }
                else if (defaultStatus.list_status == "Always Off")
                {
                    currentWord = string.Format(ALWAYSOFF_STRING, towerListId, type);
                }
            }
            else
            {
                tower_list_statuses statusObj = GetTowerListStatus(currentStatusId.Value);
                currentId = currentStatusId.Value;

                switch (statusObj.list_status)
                {
                    case "Default On":
                        currentWord = string.Format(DEFAULT_ON_STRING, towerListId, type);
                        break;
                    case "Default Off":
                        currentWord = string.Format(DEFAULT_OFF_STRING, towerListId, type);
                        break;
                    case "Always On":
                        currentWord = string.Format(ALWAYS_ON_STRING, towerListId, type);
                        break;
                    case "Always Off":
                        currentWord = string.Format(ALWAYSOFF_STRING, towerListId, type);
                        break;
                    default:
                        currentWord = string.Format(NULL_STRING, towerListId, type);
                        break;
                }
            }

            ret = GenarateLiList(currentStatus, currentId, type, towerListId, defaultStatus);

            string tmp = ret.ToString();
            tmp = string.Format(GRID_FORMAT, tmp);
            tmp = string.Format(TOWER_STATUS_BUTTON, currentWord, tmp).ToString();

            return tmp;
        }

        public static StringBuilder GenarateLiList(string currentStatus, int currentId, string type, string towerListId, tower_list_statuses defaultStatus)
        {
            StringBuilder ret = new StringBuilder();
            
            if (defaultStatus != null)
            {
                string defaultString = string.Empty;
                if (defaultStatus.list_status == "Default On")
                {
                    defaultString = string.Format(TOWER_STATUS_DEFAULT_ON, defaultOnStatus.list_status, currentId, type, towerListId, "{0}");
                }
                else if (defaultStatus.list_status == "Default Off")
                {
                    defaultString = string.Format(TOWER_STATUS_DEFAULT_OFF, defaultOffStatus.list_status, currentId, type, towerListId, "{0}");
                }
                else if (defaultStatus.list_status == "Always On")
                {
                    defaultString = string.Format(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, "{0}");
                }
                else if (defaultStatus.list_status == "Always Off")
                {
                    defaultString = string.Format(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, "{0}");
                }

                if (defaultStatus.list_status == currentStatus)
                {
                    switch (currentStatus)
                    {
                        case "Always On":
                            ret.AppendFormat(defaultString, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, DISPLAY);
                            break;
                        case "Always Off":
                            ret.AppendFormat(defaultString, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                            break;
                        default:
                            ret.AppendFormat(defaultString, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, DISPLAY);
                            break;
                    }
                }
                else
                {
                    switch (currentStatus)
                    {
                        case "Always On":
                            ret.AppendFormat(defaultString, DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                            break;
                        case "Always Off":
                            ret.AppendFormat(defaultString, DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                            break;
                        default:
                            ret.AppendFormat(defaultString, NOT_DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, DISPLAY);
                            ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, DISPLAY);
                            break;
                    }
                }
            }
            else
            {
                switch (currentStatus)
                {
                    case "Always On":
                        ret.AppendFormat(TOWER_STATUS_NULL, currentId, type, towerListId, DISPLAY);
                        ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                        ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, DISPLAY);
                        break;
                    case "Always Off":
                        ret.AppendFormat(TOWER_STATUS_NULL, currentId, type, towerListId, DISPLAY);
                        ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, DISPLAY);
                        ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, NOT_DISPLAY);
                        break;
                    default:
                        ret.AppendFormat(TOWER_STATUS_NULL, currentId, type, towerListId, NOT_DISPLAY);
                        ret.AppendFormat(TOWER_STATUS_AWAYS_ON, alwaysOnStatus.list_status, currentId, type, towerListId, DISPLAY);
                        ret.AppendFormat(TOWER_STATUS_AWAYS_OFF, alwaysOffStatus.list_status, currentId, type, towerListId, DISPLAY);
                        break;
                }
            }

            return ret;
        }

        public static tower_list GetTowerList(string site_uid)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                tower_list towerObj = (from obj in ce.tower_list
                                       where obj.site_uid.Equals(site_uid)
                                       select obj).FirstOrDefault();

                return towerObj;
            }
        }

        public static tower_list_statuses GetTowerListStatus(int statusID)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                tower_list_statuses statusObj = (from obj in ce.tower_list_statuses
                                                 where obj.id.Equals(statusID)
                                                 select obj).FirstOrDefault();

                return statusObj;
            }
        }

        public static tower_list_statuses GetTowerListStatus(string list_status)
        {
            using (CPTTEntities ce = new CPTTEntities())
            {
                tower_list_statuses statusObj = (from obj in ce.tower_list_statuses
                                                 where obj.list_status.Equals(list_status)
                                                 select obj).FirstOrDefault();

                return statusObj;
            }
        }

        public static string Update(tower_list towerObj, TowerListUpdateAction siteAction, string type, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            if (type == TowerListType.TrackiT.ToString())
            {
                #region TrackiT
                CPTTEntities ce;
                if (ceRef == null)
                {
                    //using new entity object
                    ce = new CPTTEntities();
                }
                else
                {
                    //using current entity object
                    ce = ceRef;
                    isRootTransaction = false;
                }

                // Start transaction for update a Lease Admin record.
                try
                {
                    using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                    {
                        if (isRootTransaction)
                        {
                            // Manually open connection to prevent EF from auto closing
                            // connection and causing issues with future queries.
                            ce.Connection.Open();
                        }

                        // update tower list table
                        ce.AttachUpdated(towerObj);

                        // update site table
                        results = UpdateSiteTable(towerObj, siteAction, ce);

                        if (string.IsNullOrEmpty(results) && isRootTransaction)
                        {
                            ce.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
                            isSuccess = true;
                            trans.Complete();     // Transaction complete
                        }
                    }
                }
                catch (OptimisticConcurrencyException oex)
                {
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                    results = "The current record has been modifed since you have " +
                              "last retrieved it. Please reload the record and " +
                              "attempt to save it again.";
                }
                catch (Exception ex)
                {
                    // Error occurs
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    isSuccess = false;
                    results = "An error occurred saving the record. Please try again.";
                }
                finally
                {
                    if (isRootTransaction)
                    {
                        ce.Connection.Dispose();
                    }
                }

                if (isSuccess && isRootTransaction)
                {
                    ce.Dispose();
                }
                #endregion TrackiT
            }
            else if (type == TowerListType.Map.ToString())
            {
                #region Map
                try
                {
                    // update site table
                    results = UpdateSmSiteTable(towerObj, siteAction);
                }
                catch (Exception ex)
                {
                    // Error occurs
                    // Log error in ELMAH for any diagnostic needs.
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                    isSuccess = false;
                    results = "An error occurred saving the record. Please try again.";
                }
                #endregion Map
            }

            return results;

        }

        public static string Add(tower_list towerObj, CPTTEntities ceRef = null)
        {
            String results = null;
            bool isSuccess = false;
            bool isRootTransaction = true;

            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }

            try
            {
                using (TransactionScope trans = TransactionUtils.CreateTransactionScope())
                {
                    if (isRootTransaction)
                    {
                        // Manually open connection to prevent EF from auto closing
                        // connection and causing issues with future queries.
                        ce.Connection.Open();
                    }

                    ce.tower_list.AddObject(towerObj);
                    ce.SaveChanges();
                    isSuccess = true;
                    trans.Complete();     // Transaction complete
                }
            }
            catch (OptimisticConcurrencyException oex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(oex);

                results = "The current record has been modifed since you have " +
                          "last retrieved it. Please reload the record and " +
                          "attempt to save it again.";
            }
            catch (Exception ex)
            {
                // Error occurs
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                isSuccess = false;
                results = "An error occurred saving the record. Please try again.";
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }

            // Finally accept all changes from the transaction.
            if (isSuccess && isRootTransaction)
            {
                ce.Dispose();
            }

            return results;
        }

        public static string UpdateSiteTable(tower_list towerObj, TowerListUpdateAction action, CPTTEntities ceRef = null)
        {
            string returnResult = string.Empty;

            var siteObj = Site.SearchWithinTransaction(towerObj.site_uid, ceRef);
            //var smSiteObj = 
            switch (action)
            {
                case TowerListUpdateAction.ADD:
                    if (siteObj == null)
                    {
                        site newSiteObj = new site();

                        newSiteObj.site_uid = towerObj.site_uid;
                        newSiteObj.site_name = towerObj.site_name;
                        newSiteObj.site_class_desc = towerObj.site_class_desc;
                        newSiteObj.county = towerObj.county;
                        newSiteObj.state = towerObj.state;
                        newSiteObj.region_name = towerObj.region_name;
                        newSiteObj.market_code = towerObj.market_code;
                        newSiteObj.market_name = towerObj.market_name;
                        newSiteObj.site_latitude = towerObj.site_latitude;
                        newSiteObj.site_longitude = towerObj.site_longitude;
                        newSiteObj.structure_ht = towerObj.structure_ht;
                        newSiteObj.address = towerObj.address;
                        newSiteObj.site_status_desc = towerObj.site_status_desc;
                        newSiteObj.city = towerObj.city;
                        newSiteObj.zip = towerObj.zip;

                        returnResult = BLL.Site.Add(newSiteObj, ceRef);
                    }
                    break;
                case TowerListUpdateAction.DELETE:
                    if (siteObj != null && siteObj.lease_applications.FirstOrDefault() == null && siteObj.lease_admin_records.FirstOrDefault() == null
                        && siteObj.tower_modifications.FirstOrDefault() == null && siteObj.site_data_packages.FirstOrDefault() == null 
                        && siteObj.issue_tracker.FirstOrDefault() == null)
                    {
                            returnResult = BLL.Site.Remove(siteObj, ceRef);
                    }
                    else
                    {
                        returnResult = DELETE_ERROR;
                    }
                    break;
            }
            return returnResult;
        }

        public static string UpdateSmSiteTable(tower_list towerObj, TowerListUpdateAction action)
        {
            string returnResult = string.Empty;

            using (TrackitImportApplication.TrackiTImportServiceClient client = new TrackitImportApplication.TrackiTImportServiceClient())
            {
                
                string siteObj = client.CheckSmSite(HMACHelper.GenerateHMAC(), towerObj.site_uid);
                
                switch (action)
                {
                    case TowerListUpdateAction.ADD:
                        if (siteObj == "false")
                        {
                            decimal lat = new decimal();
                            decimal lng = new decimal();
                            string updateId = towerObj.updater_id.Value.ToString();
                            int? height = null;

                            if (towerObj.site_latitude.HasValue)
                            {
                                decimal.TryParse(towerObj.site_latitude.Value.ToString(), out lat);
                            }
                            if (towerObj.site_longitude.HasValue)
                            {
                                decimal.TryParse(towerObj.site_longitude.Value.ToString(), out lng);
                            }

                            if (towerObj.structure_ht.HasValue)
                            {
                                height = Convert.ToInt32(towerObj.structure_ht.Value);
                            }

                            returnResult = client.UpsertSmSite(HMACHelper.GenerateHMAC(), towerObj.region_name, towerObj.market_name, towerObj.market_code, towerObj.site_uid, towerObj.site_name,
                                towerObj.site_class_desc, lat, lng, updateId, height, towerObj.address, towerObj.site_status_desc, towerObj.state, towerObj.county,towerObj.city,towerObj.zip);

                        }
                        break;
                    case TowerListUpdateAction.DELETE:
                        returnResult = client.DeleteSmSite(HMACHelper.GenerateHMAC(), towerObj.site_uid);
                        break;
                }
                client.Close();
            }
            return returnResult;
        }
    }
}