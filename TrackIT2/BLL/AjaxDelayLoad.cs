﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Text;
using TrackIT2.DAL;
using System.Threading;
using Newtonsoft.Json;

namespace TrackIT2.BLL
{
    public class AjaxDelayLoad
    {
        #region Workflow
        #region const string
        public const string SOURCE_STRING_LEASE_APP = "LeaseApp";
        public const string SOURCE_STRING_TOWER_MOD = "TowerMod";
        public const string SOURCE_STRING_SDP = "SDP";
        public const string TEXT_STRING_LEASE_APP = "Lease Application";
        public const string TEXT_STRING_TOWER_MOD = "Tower Modification";
        public const string TEXT_STRING_SDP = "Site Data Package";
        public const string TEXT_STRING_ISSUE_TRACKER = "Open Issues";
        public const string TEXT_STRING_LEASE_ADMIN = "Lease Admin Tracker";
        public const string TEXT_STRING_DMS = "Site Documents";

        public const string PATH_LIST_LEASE_APP = @"../LeaseApplications/?filter=txtSiteID|";
        public const string PATH_EDIT_LEASE_APP = @"../LeaseApplications/Edit.aspx?id=";
        public const string PATH_LIST_TOWER_MOD = @"../TowerModifications/?filter=txtSiteID|";
        public const string PATH_EDIT_TOWER_MOD = @"../TowerModifications/Edit.aspx?id=";
        public const string PATH_LIST_SDP = @"../SiteDataPackages/?filter=txtSiteID|";
        public const string PATH_EDIT_SDP = @"../SiteDataPackages/Edit.aspx?id=";
        public const string PATH_LIST_ISSUE_TRACKER = @"../IssueTracker/?filter=txtSiteID|";
        public const string PATH_EDIT_ISSUE_TRACKER = @"../IssueTracker/Edit.aspx?id=";
        public const string PATH_LIST_LEASE_ADMIN = @"../LeaseAdminTracker/?filter=txtSiteID|";
        public const string PATH_EDIT_LEASE_ADMIN = @"../LeaseAdminTracker/Edit.aspx?id=";
        public const string PATH_DMS = @"../DMS/#/{0}/";

        public const string LIST_HEADER = "<li>Site Workflows</li>";
        public const string LIST_HEADER_APPLICATION = "<li>Application Workflows</li>";
        public const string LIST_LEASE_APP = "<li class=\"work-flow-item\"><a id=\"link_la\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_LEASE_APP_NONE = "<li>No Lease Application(s) Assigned.</li>";
        public const string LIST_TOWER_MOD = "<li class=\"work-flow-item\"><a id=\"link_tw\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_TOWER_MOD_NONE = "<li>No Tower Modification(s) Assigned.</li>";
        public const string LIST_SDP = "<li class=\"work-flow-item\"><a id=\"link_sdp\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_SDP_NONE = "<li>No Site Data Package(s) Assigned.</li>";
        public const string LIST_ISSUE_TRACKER = "<li class=\"work-flow-item\"><a id=\"link_issue\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_ISSUE_TRACKER_NONE = "<li>No Issue(s) Assigned.</li>";
        public const string LIST_LEASE_ADMIN = "<li class=\"work-flow-item\"><a id=\"link_lease_admin\" href=\"{0}\" >{1}</a></li>";
        public const string LIST_LEASE_ADMIN_NONE = "<li>No Lease Admin(s) Assigned.</li>";
        public const string LIST_DMS = "<li><a id=\"link_dms\" href=\"{0}\" >{1}</a></li>";

        public const string GRID_FORMAT = "<div><ul>{0}</ul></div>";
        #endregion const string

        /// <summary>
        /// Get the innerHTML of ul control to create list of links for site workflows
        /// </summary>
        /// <param name="site_uid">Site ID</param>
        /// <returns>html control in string</returns>
        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public static string GetSiteWorkflowsList(string site_uid, string sFirstLineText = LIST_HEADER, CPTTEntities ceRef = null)
        {
            StringBuilder ret = new StringBuilder(sFirstLineText);
            var result = new List<KeyValuePair<string, object[]>>();

            bool hasRecords = false;
            bool tower_modification_yn = false;
            bool issue_tracker_yn = false;

            bool isRootTransaction = true;
            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }
            if (isRootTransaction)
            {
                ce.Connection.Open();
            }

            try
            {
                //Lease Applications
                var queryLeaseApp = (from app in ce.lease_applications
                                    where app.site_uid == site_uid
                                    select app).ToList();
                if (queryLeaseApp.Count > 0)
                {
                    hasRecords = true;
                    if (queryLeaseApp.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_LEASE_APP, PATH_LIST_LEASE_APP + site_uid, "(" + queryLeaseApp.Count + ") " + TEXT_STRING_LEASE_APP + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_LEASE_APP, PATH_EDIT_LEASE_APP + ((lease_applications)queryLeaseApp[0]).id, "(1) " + TEXT_STRING_LEASE_APP);
                    }
                }
                //--> End Lease Applications

                //Tower Modifications
                var queryTwMod = (from tw in ce.tower_modifications
                                 where tw.site_uid == site_uid
                                 select tw).ToList();
                if (queryTwMod.Count > 0)
                {
                    hasRecords = true;
                    tower_modification_yn = true;
                    if (queryTwMod.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_TOWER_MOD, PATH_LIST_TOWER_MOD + site_uid, "(" + queryTwMod.Count + ") " + TEXT_STRING_TOWER_MOD + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_TOWER_MOD, PATH_EDIT_TOWER_MOD + ((tower_modifications)queryTwMod[0]).id, "(1) " + TEXT_STRING_TOWER_MOD);
                    }
                }
                //--> End Tower Modifications

                //SDP
                var querySDP = (from sdp in ce.site_data_packages
                               where sdp.site_uid == site_uid
                               select sdp).ToList();
                if (querySDP.Count > 0)
                {
                    hasRecords = true;
                    if (querySDP.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_SDP, PATH_LIST_SDP + site_uid, "(" + querySDP.Count + ") " + TEXT_STRING_SDP + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_SDP, PATH_EDIT_SDP + ((site_data_packages)querySDP[0]).id, "(1) " + TEXT_STRING_SDP);
                    }
                }
                //--> End SDP

                //Issue Tracker
                var queryIssue = (from issue in ce.issue_tracker
                                 where issue.site_uid == site_uid
                                 select issue).ToList();
                if (queryIssue.Count > 0)
                {
                    hasRecords = true;
                    issue_tracker_yn = true;
                    if (queryIssue.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_LIST_ISSUE_TRACKER + site_uid, "(" + queryIssue.Count + ") " + TEXT_STRING_ISSUE_TRACKER + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_EDIT_ISSUE_TRACKER + ((issue_tracker)queryIssue[0]).id, "(1) " + TEXT_STRING_ISSUE_TRACKER);
                    }
                }
                //Issue Tracker

                //Lease Admin
                var queryLeaseAdmin = (from le in ce.lease_admin_records
                                      from s in le.sites
                                      where s.site_uid == site_uid
                                      select le).ToList();
                if (queryLeaseAdmin.Any())
                {
                    hasRecords = true;
                    if (queryLeaseAdmin.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_LEASE_ADMIN, PATH_LIST_LEASE_ADMIN + site_uid, "(" + queryLeaseAdmin.Count + ") " + TEXT_STRING_LEASE_ADMIN + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_LEASE_ADMIN, PATH_EDIT_LEASE_ADMIN + ((lease_admin_records)queryLeaseAdmin[0]).id, "(1) " + TEXT_STRING_LEASE_ADMIN);
                    }
                }
                //Lease Admin

                //DMS
                ret.AppendFormat(LIST_DMS, PATH_DMS.Replace("{0}", site_uid), TEXT_STRING_DMS);

                // If no records were found, clear out the Application Workflows header
                // auto appendedin the start of the method.
                if (!hasRecords)
                {
                    ret.Clear();
                }
                else
                {
                    ret.Append("<li>&nbsp;</li>");
                }

                
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ret = null;
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }
            if (isRootTransaction)
            {
                ce.Dispose();
            }

            result.Add(new KeyValuePair<string, object[]>
                            (site_uid,
                                new object[]
                                { 
                                    ret.ToString(),
                                    tower_modification_yn.ToString(),
                                    issue_tracker_yn.ToString()
                                }
                            )
                          );

            if (result.Count > 0)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            }
            else
            {
                return string.Empty;
            }
        }

        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public static string GetSiteWorkflowsForGrid(string site_uid)
        {
            return string.Format(GRID_FORMAT, GetSiteWorkflowsList(site_uid, ""));
        }

        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public static string GetApplicationWorkflowsForLA(int la_id)
        {
            StringBuilder ret = new StringBuilder(LIST_HEADER_APPLICATION);
            bool hasRecords = false;
            var result = new List<KeyValuePair<string, object[]>>();
            bool tower_modification_yn = false;
            bool issue_tracker_yn = false;

            using (CPTTEntities ce = new CPTTEntities())
            {
                var querySDP = (from sdp in ce.site_data_packages
                               where sdp.lease_application_id == la_id
                               select sdp).ToList();
                if (querySDP.Count > 0)
                {
                    hasRecords = true;
                    if (querySDP.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_SDP, PATH_LIST_SDP + la_id, "(" + querySDP.Count + ") " + TEXT_STRING_SDP + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_SDP, PATH_EDIT_SDP + ((site_data_packages)querySDP[0]).id, "(1) " + TEXT_STRING_SDP);
                    }
                }
                //--> End SDP

                // Tower Modifications
                List<tower_modifications> lstTowerMod = new List<tower_modifications>();
                lstTowerMod = LeaseAppTowerMod.GetTowerModLA(la_id);

                if (lstTowerMod.Count > 0)
                {
                    hasRecords = true;
                    tower_modification_yn = true;
                    if (lstTowerMod.Count > 1)
                    {
                        //More than one
                        ret.AppendFormat(LIST_TOWER_MOD, PATH_LIST_TOWER_MOD + la_id, "(" + lstTowerMod.Count + ") " + TEXT_STRING_TOWER_MOD + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_TOWER_MOD, PATH_EDIT_TOWER_MOD + lstTowerMod[0].id, "(1) " + TEXT_STRING_TOWER_MOD);
                    }
                }
                //--> End Tower Modification

                //Issue Tracker
                List<issue_tracker> queryIssue = (from issue in ce.issue_tracker
                                                  join la in ce.lease_applications on la_id equals la.id
                                                  where issue.site_uid == la.site_uid
                                                  select issue).ToList();
                if (queryIssue.Count > 0)
                {
                    hasRecords = true;
                    issue_tracker_yn = true;

                    if (queryIssue.Count > 1)
                    {
                        //More than one
                        issue_tracker it = (issue_tracker)queryIssue[0];
                        ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_LIST_ISSUE_TRACKER + it.site_uid, "(" + queryIssue.Count + ") " + TEXT_STRING_ISSUE_TRACKER + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_ISSUE_TRACKER, PATH_EDIT_ISSUE_TRACKER + ((issue_tracker)queryIssue[0]).id, "(1) " + TEXT_STRING_ISSUE_TRACKER);
                    }
                }
                //Issue Tracker

                //Lease Admin
                List<lease_admin_records> queryLeaseAdmin = (from le in ce.lease_admin_records
                                                             where le.lease_application_id == la_id
                                                             select le).ToList();
                if (queryLeaseAdmin.Any())
                {
                    hasRecords = true;
                    if (queryLeaseAdmin.Count > 1)
                    {
                        //More than one * Abnormal case should have only 1
                        ret.AppendFormat(LIST_LEASE_ADMIN, PATH_LIST_LEASE_ADMIN + ((lease_admin_records)queryLeaseAdmin[0]).id, "(" + queryLeaseAdmin.ToList().Count + ") " + TEXT_STRING_LEASE_ADMIN + "s");
                    }
                    else
                    {
                        //Excatly 1 
                        ret.AppendFormat(LIST_LEASE_ADMIN, PATH_EDIT_LEASE_ADMIN + ((lease_admin_records)queryLeaseAdmin[0]).id, "(1) " + TEXT_STRING_LEASE_ADMIN);
                    }
                }
            }
            // If no records were found, clear out the Application Workflows header
            // auto appendedin the start of the method.
            if (!hasRecords)
            {
                ret.Clear();
            }
            else
            {
                ret.Append("<li>&nbsp;</li>");
            }

            result.Add(new KeyValuePair<string, object[]>
                            (la_id.ToString(),
                                new object[]
                                { 
                                    ret.ToString(),
                                    tower_modification_yn.ToString(),
                                    issue_tracker_yn.ToString()
                                }
                            )
                          );
            if (result.Count > 0)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion Workflow

        #region SA
        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public static int GetJsonSA(Boolean isNull, out String sJson,int iAppId, int saId = 0)
        {
            var iRet = 0;
            var sRet = "";
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        ce.Connection.Open();
                        if (!isNull && saId != 0)
                        {
                            var query = from sa in
                                            (
                                                from structure in ce.structural_analyses
                                                where structure.id == saId
                                                select structure).ToList()
                                        select new json_structural_analyses
                                        {
                                            ID = saId,
                                            LeaseAppID = iAppId,
                                            po_ordered_date = Utility.DateToString(sa.po_ordered_date),
                                            po_received_date = Utility.DateToString(sa.po_received_date),
                                            po_amount = sa.po_amount,
                                            po_number = sa.po_number,
                                            structural_analysis_type_id = sa.structural_analysis_type_id,
                                            sa_ordered_date = Utility.DateToString(sa.sa_ordered_date),
                                            sa_received_date = Utility.DateToString(sa.sa_received_date),
                                            sa_ordered_date_status_id = sa.sa_ordered_date_status_id,
                                            sa_tower_prcnt = sa.sa_tower_prcnt,
                                            sa_order_onhold_date = Utility.DateToString(sa.sa_order_onhold_date),
                                            sa_order_cancelled_date = Utility.DateToString(sa.sa_order_cancelled_date),
                                            sa_fee_received_date = Utility.DateToString(sa.sa_fee_received_date),
                                            sa_fee_amount = sa.sa_fee_amount,
                                            sa_check_nbr = sa.sa_check_nbr == null ? string.Empty : sa.sa_check_nbr.ToString(),
                                            structural_analysis_fee_payor_type_id = sa.structural_analysis_fee_payor_type_id,
                                            sa_vendor_id = sa.sa_vendor_id,
                                            sac_name_emp_id = sa.sac_name_emp_id,
                                            saw_to_customer_date = Utility.DateToString(sa.saw_to_customer_date),
                                            saw_approved_date = Utility.DateToString(sa.saw_approved_date),
                                            saw_to_sac_date = Utility.DateToString(sa.saw_to_sac_date),
                                            saw_request_date = Utility.DateToString(sa.saw_request_date),
                                            sac_priority_id = sa.sac_priority_id,
                                            sac_description_id = sa.sac_description_id,
                                            shopping_cart_number = string.IsNullOrEmpty(sa.shopping_cart_number) ? string.Empty : Utility.PrepareString(sa.shopping_cart_number),
                                            blanket_po_month = sa.blanket_po_month,
                                            blanket_po_year = sa.blanket_po_year,
                                            po_release_status_id = sa.po_release_status_id,
                                            po_release_date = Utility.DateToString(sa.po_release_date),
                                            po_notes = string.IsNullOrEmpty(sa.po_notes) ? string.Empty : Utility.PrepareString(sa.po_notes),
                                            sa_received_date_status_id = sa.sa_received_date_status_id,
                                            ps_received_date = Utility.DateToString(sa.ps_received_date),
                                            sa_updated_at = Utility.DateTimeToString(sa.updated_at),
                                            vendor_list = ((from v in ce.sa_vendors
                                                            where v.is_preferred == true
                                                            orderby v.sa_vendor
                                                            select new json_sa_vendors
                                                            {
                                                                id = v.id,
                                                                sa_vendor = v.sa_vendor,
                                                                is_preferred = v.is_preferred
                                                            }).Union(from v2 in ce.sa_vendors
                                                                     where (sa.sa_vendor_id != null) &&
                                                                     (v2.id == sa.sa_vendor_id.Value) &&
                                                                     (v2.is_preferred == false)
                                                                     select new json_sa_vendors
                                                                     {
                                                                         id = v2.id,
                                                                         sa_vendor = v2.sa_vendor + " (historical)",
                                                                         is_preferred = v2.is_preferred
                                                                     })).ToList(),
                                            sac_description_list = (from desc in ce.sac_descriptions
                                                                    where desc.is_preferred == true
                                                                    orderby desc.description
                                                                    select new json_sac_descriptions
                                                                    {
                                                                        id = desc.id,
                                                                        description = desc.description,
                                                                        is_preferred = desc.is_preferred
                                                                    }).Union(from desc2 in ce.sac_descriptions
                                                                             where (sa.sac_description_id != null) &&
                                                                             (desc2.id == sa.sac_description_id.Value) &&
                                                                             (desc2.is_preferred == false)
                                                                             select new json_sac_descriptions
                                                                             {
                                                                                 id = desc2.id,
                                                                                 description = desc2.description + " (historical)",
                                                                                 is_preferred = desc2.is_preferred
                                                                             }).ToList(),
                                            comments = (from result in
                                                            (from c in ce.structural_analysis_comments
                                                             where c.structural_analysis_id == sa.id
                                                             orderby c.structural_analysis_id
                                                             select c
                                                             ).ToList()
                                                        select new json_comments
                                                        {
                                                            comment_id = result.id,
                                                            sa_id = sa.id,
                                                            comment = result.comments,
                                                            creator = result.creator_user != null ? result.creator_user.last_name + result.creator_user.first_name : "No employee recorded.",
                                                            created_at = Utility.DateToString(result.created_at),
                                                            creator_id = result.creator_user != null ? result.creator_user.tmo_userid : "none",
                                                            updater = result.updater_user != null ? result.updater_user.last_name + result.updater_user.first_name : "No employee recorded.",
                                                            update_at = Utility.DateToString(result.updated_at),
                                                            exist = true
                                                        }).ToList(),
                                            comment_count = (from c in ce.structural_analysis_comments
                                                             where c.structural_analysis_id == sa.id
                                                             orderby c.structural_analysis_id
                                                             group c by c.structural_analysis_id into g
                                                             select g).Count(),
                                            sa_decision_date = Utility.DateToString(sa.sa_decision_date),
                                            sa_decision_type_id = sa.sa_decision_type_id,
                                            sa_decision_type_text = sa.sa_decision_type_id.HasValue ? (from decision_type in ce.struct_decision_types
                                                                                                       where decision_type.id == sa.sa_decision_type_id.Value
                                                                                                       select decision_type).FirstOrDefault().struct_decision_type : string.Empty,
                                            structural_analysis_type_text = sa.structural_analysis_type_id.HasValue ? (from structural_analysis_type in ce.structural_analysis_types
                                                                                                                       where structural_analysis_type.id == sa.structural_analysis_type_id.Value
                                                                                                                       select structural_analysis_type).FirstOrDefault().structural_analysis_type : string.Empty
                                        };

                            if (query.Any())
                            {
                                sRet = JsonConvert.SerializeObject(query);
                                iRet = query.Count();
                            }
                        }
                    }
                    sJson = sRet;
                    ce.Connection.Close();
                    ce.Dispose();
                }
            }
            catch
            {
                throw;
            }
            return iRet;
        }

        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public static int GetJsonSAComment(Boolean isNull, out String sJson, int iAppId, int saId = 0)
        {
            var iRet = 0;
            var sRet = "";
            try
            {
                using (CPTTEntities ce = new CPTTEntities())
                {
                    if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        ce.Connection.Open();
                        if (!isNull && saId != 0)
                        {
                            var query = from sa in
                                            (
                                                from structure in ce.structural_analyses
                                                where structure.id == saId
                                                select structure).ToList()
                                        select new json_structural_analyses
                                        {
                                            ID = saId,
                                            LeaseAppID = iAppId,
                                            comments = (from result in
                                                            (from c in ce.structural_analysis_comments
                                                             where c.structural_analysis_id == sa.id
                                                             orderby c.structural_analysis_id
                                                             select c
                                                             ).ToList()
                                                        select new json_comments
                                                        {
                                                            comment_id = result.id,
                                                            sa_id = sa.id,
                                                            comment = result.comments,
                                                            creator = result.creator_user != null ? result.creator_user.last_name + result.creator_user.first_name : "No employee recorded.",
                                                            created_at = Utility.DateToString(result.created_at),
                                                            creator_id = result.creator_user != null ? result.creator_user.tmo_userid : "none",
                                                            updater = result.updater_user != null ? result.updater_user.last_name + result.updater_user.first_name : "No employee recorded.",
                                                            update_at = Utility.DateToString(result.updated_at),
                                                            exist = true
                                                        }).ToList(),
                                            comment_count = (from c in ce.structural_analysis_comments
                                                             where c.structural_analysis_id == sa.id
                                                             orderby c.structural_analysis_id
                                                             group c by c.structural_analysis_id into g
                                                             select g).Count()
                                        };

                            if (query.Any())
                            {
                                sRet = JsonConvert.SerializeObject(query);
                                iRet = query.Count();
                            }
                        }
                    }
                    sJson = sRet;
                    ce.Connection.Close();
                    ce.Dispose();
                }
            }
            catch
            {
                throw;
            }
            return iRet;
        }

        [ActionSessionState(SessionStateBehavior.ReadOnly)]
        public static StructuralAnalysisTemplate GetSA(Boolean isNull, string iAppId, string saId, CPTTEntities ceRef = null)
        {
            bool hasRecords = false;
            StructuralAnalysisTemplate ret = new StructuralAnalysisTemplate();
            
            bool isRootTransaction = true;
            CPTTEntities ce;
            if (ceRef == null)
            {
                //using new entity object
                ce = new CPTTEntities();
            }
            else
            {
                //using current entity object
                ce = ceRef;
                isRootTransaction = false;
            }
            if (isRootTransaction)
            {
                ce.Connection.Open();
            }

            try
            {
                if (!isNull && saId != "0")
                {
                    int tmpSaId = int.Parse(saId);
                    var query = from sa in
                                    (
                                        from structure in ce.structural_analyses
                                        where structure.id == tmpSaId
                                        select structure).ToList()
                                select new StructuralAnalysisTemplate
                                {
                                    ID = saId,
                                    LeaseAppID = iAppId,
                                    po_ordered_date = Utility.DateToString(sa.po_ordered_date),
                                    po_received_date = Utility.DateToString(sa.po_received_date),
                                    po_amount = sa.po_amount == null ? string.Empty : Utility.PrepareString(sa.po_amount.ToString()),
                                    po_number = sa.po_number,
                                    structural_analysis_type_id = sa.structural_analysis_type_id == null ? string.Empty : Utility.PrepareString(sa.structural_analysis_type_id.ToString()),
                                    sa_ordered_date = Utility.DateToString(sa.sa_ordered_date),
                                    sa_received_date = Utility.DateToString(sa.sa_received_date),
                                    sa_ordered_date_status_id = sa.sa_ordered_date_status_id == null ? string.Empty : Utility.PrepareString(sa.sa_ordered_date_status_id.ToString()),
                                    sa_tower_prcnt = sa.sa_tower_prcnt == null ? string.Empty : Utility.PrepareString(sa.sa_tower_prcnt.ToString()),
                                    sa_order_onhold_date = Utility.DateToString(sa.sa_order_onhold_date),
                                    sa_order_cancelled_date = Utility.DateToString(sa.sa_order_cancelled_date),
                                    sa_fee_received_date = Utility.DateToString(sa.sa_fee_received_date),
                                    sa_fee_amount = sa.sa_fee_amount == null ? string.Empty : Utility.PrepareString(sa.sa_fee_amount.ToString()),
                                    sa_check_nbr = sa.sa_check_nbr == null ? string.Empty : Utility.PrepareString(sa.sa_check_nbr.ToString()),
                                    structural_analysis_fee_payor_type_id = sa.structural_analysis_fee_payor_type_id == null ? string.Empty : Utility.PrepareString(sa.structural_analysis_fee_payor_type_id.ToString()) ,
                                    sa_vendor_id = sa.sa_vendor_id == null ? string.Empty : Utility.PrepareString(sa.sa_vendor_id.ToString()),
                                    sac_name_emp_id = sa.sac_name_emp_id == null ? string.Empty : Utility.PrepareString(sa.sac_name_emp_id.ToString()),
                                    saw_to_customer_date = Utility.DateToString(sa.saw_to_customer_date),
                                    saw_approved_date = Utility.DateToString(sa.saw_approved_date),
                                    saw_to_sac_date = Utility.DateToString(sa.saw_to_sac_date),
                                    saw_request_date = Utility.DateToString(sa.saw_request_date),
                                    sac_priority_id = sa.sac_priority_id == null ? string.Empty : Utility.PrepareString(sa.sac_priority_id.ToString()),
                                    sac_description_id = sa.sac_description_id == null ? string.Empty : Utility.PrepareString(sa.sac_description_id.ToString()),
                                    shopping_cart_number = string.IsNullOrEmpty(sa.shopping_cart_number) ? string.Empty : Utility.PrepareString(sa.shopping_cart_number),
                                    blanket_po_month = sa.blanket_po_month == null ? string.Empty : Utility.PrepareString(sa.blanket_po_month.ToString()),
                                    blanket_po_year = sa.blanket_po_year == null ? string.Empty : Utility.PrepareString(sa.blanket_po_year.ToString()),
                                    po_release_status_id = sa.po_release_status_id == null ? string.Empty : Utility.PrepareString(sa.po_release_status_id.ToString()),
                                    po_release_date = Utility.DateToString(sa.po_release_date),
                                    po_notes = string.IsNullOrEmpty(sa.po_notes) ? string.Empty : Utility.PrepareString(sa.po_notes),
                                    sa_received_date_status_id = sa.sa_received_date_status_id == null ? string.Empty : Utility.PrepareString(sa.sa_received_date_status_id.ToString()),
                                    ps_received_date = Utility.DateToString(sa.ps_received_date),
                                    sa_updated_at = Utility.DateTimeToString(sa.updated_at),
                                    vendor_list = ((from v in ce.sa_vendors
                                                    where v.is_preferred == true
                                                    orderby v.sa_vendor
                                                    select new json_sa_vendors
                                                    {
                                                        id = v.id,
                                                        sa_vendor = v.sa_vendor,
                                                        is_preferred = v.is_preferred
                                                    }).Union(from v2 in ce.sa_vendors
                                                             where (sa.sa_vendor_id != null) &&
                                                             (v2.id == sa.sa_vendor_id.Value) &&
                                                             (v2.is_preferred == false)
                                                             select new json_sa_vendors
                                                             {
                                                                 id = v2.id,
                                                                 sa_vendor = v2.sa_vendor + " (historical)",
                                                                 is_preferred = v2.is_preferred
                                                             })).ToList(),
                                    sac_description_list = (from desc in ce.sac_descriptions
                                                            where desc.is_preferred == true
                                                            orderby desc.description
                                                            select new json_sac_descriptions
                                                            {
                                                                id = desc.id,
                                                                description = desc.description,
                                                                is_preferred = desc.is_preferred
                                                            }).Union(from desc2 in ce.sac_descriptions
                                                                     where (sa.sac_description_id != null) &&
                                                                     (desc2.id == sa.sac_description_id.Value) &&
                                                                     (desc2.is_preferred == false)
                                                                     select new json_sac_descriptions
                                                                     {
                                                                         id = desc2.id,
                                                                         description = desc2.description + " (historical)",
                                                                         is_preferred = desc2.is_preferred
                                                                     }).ToList(),
                                    comments = (from result in
                                                    (from c in ce.structural_analysis_comments
                                                     where c.structural_analysis_id == sa.id
                                                     orderby c.structural_analysis_id
                                                     select c
                                                     ).ToList()
                                                select new json_comments
                                                {
                                                    comment_id = result.id,
                                                    sa_id = sa.id,
                                                    comment = result.comments,
                                                    creator = result.creator_user != null ? result.creator_user.last_name + result.creator_user.first_name : "No employee recorded.",
                                                    created_at = Utility.DateToString(result.created_at),
                                                    creator_id = result.creator_user != null ? result.creator_user.tmo_userid : "none",
                                                    updater = result.updater_user != null ? result.updater_user.last_name + result.updater_user.first_name : "No employee recorded.",
                                                    update_at = Utility.DateToString(result.updated_at),
                                                    exist = true
                                                }).ToList(),
                                    comment_count = (from c in ce.structural_analysis_comments
                                                     where c.structural_analysis_id == sa.id
                                                     orderby c.structural_analysis_id
                                                     group c by c.structural_analysis_id into g
                                                     select g).Count(),
                                    sa_decision_date = Utility.DateToString(sa.sa_decision_date),
                                    sa_decision_type_id = sa.sa_decision_type_id == null ? string.Empty : Utility.PrepareString(sa.sa_decision_type_id.ToString())
                                };

                    if (query.Any())
                    {
                        ret = query.FirstOrDefault();
                        hasRecords = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error in ELMAH for any diagnostic needs.
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ret = null;
            }
            finally
            {
                if (isRootTransaction)
                {
                    ce.Connection.Dispose();
                }
            }
            if (isRootTransaction)
            {
                ce.Dispose();
            }

            if (hasRecords)
            {
                return ret;
            }
            else
            {
                return null;
            }
        }

        #endregion SA
    }

    public class LeaseAppWraper
    {
        public string site_uid { get; set; }
        public string site_name { get; set; }
        public string customer_name { get; set; }
        public string lease_application_type { get; set; }
        public DateTime? leaseapp_rcvd_date { get; set; }
        public string lease_application_status { get; set; }
        public bool tower_modification_yn { get; set; }
        public bool? rogue_equipment_yn { get; set; }
        public bool? issue_tracker_yn { get; set; }
        public string original_carrier { get; set; }
        public string lease_app_activity { get; set; }
        public bool? site_on_air_date_yn { get; set; }
        public DateTime? site_on_air_date { get; set; }
        public string site_status_desc { get; set; }
        public int id { get; set; }
    }
}