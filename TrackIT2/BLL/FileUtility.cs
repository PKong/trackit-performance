﻿using System.Linq;
using TrackIT2.DAL;

namespace TrackIT2.BLL
{
    /// <summary>
    /// File Utility
    /// </summary>
    public static class FileUtility
    {
        /// <summary>
        /// Get File Type
        /// </summary>
        /// <param name="fileName">string</param>
        /// <returns>string</returns>
        public static FileType GetFileType(string fileName, System.Collections.Generic.List<dms_file_types> dmsFileTypeList = null)
        {
            if (fileName.IndexOf('.').Equals(-1)) return GetFileTypeUnknown();
            var arrFileName = fileName.Split('.');

            if (arrFileName.Length <= 1) return GetFileTypeUnknown();
            var fileExtension = arrFileName[arrFileName.Length - 1].ToLower();

            return GetFileTypeFromExtension(fileExtension, dmsFileTypeList);
        }
        //-->

        /// <summary>
        /// Get File Type From Extension
        /// </summary>
        /// <param name="fileExtension">string</param>
        /// <returns>FileType</returns>
        public static FileType GetFileTypeFromExtension(string fileExtension, System.Collections.Generic.List<dms_file_types> dmsFileTypeList = null)
        {

            FileType objFileType;
            if (dmsFileTypeList == null)
            {
                using (var ce = new CPTTEntities())
                {
                    var objFile = ce.dms_file_types.FirstOrDefault(f => f.fileExtension.Equals(fileExtension));
                    objFileType = objFile != null ? new FileType(objFile.name, objFile.fileExtension, objFile.fileIcon) : GetFileTypeUnknown();
                }
            }
            else
            {
                var objFile = dmsFileTypeList.FirstOrDefault(f => f.fileExtension.Equals(fileExtension));
                objFileType = objFile != null ? new FileType(objFile.name, objFile.fileExtension, objFile.fileIcon) : GetFileTypeUnknown();
            }

            return objFileType;
        }
        //-->

        /// <summary>
        /// Get List of File Type
        /// </summary>
        /// <returns>List Of FileType</returns>
        public static System.Collections.Generic.List<dms_file_types> GetListDmsFileTypes()
        {
            using (var ce = new CPTTEntities())
            {
                var objFile = ce.dms_file_types.ToList();
                return objFile;
            }
        }

        /// <summary>
        /// Get File Type Unknown
        /// </summary>
        /// <returns>FileType</returns>
        public static FileType GetFileTypeUnknown()
        {
            return new FileType(FileTypes.Unknown.FILE_TYPE, FileTypes.Unknown.FILE_EXTENSION, FileTypes.Unknown.FILE_ICON);
        }
        //-->

        /// <summary>
        /// File Type
        /// </summary>
        public class FileType
        {
            public string TypeOfFile { get; set; }
            public string FileExtension { get; set; }
            public string FileIcon { get; set; }

            public FileType(string typeOfFile, string fileExtension, string fileIcon)
            {
                TypeOfFile = typeOfFile;
                FileExtension = fileExtension;
                FileIcon = fileIcon;
            }
        }
        //>

        /// <summary>
        /// File Types
        /// </summary>
        public struct FileTypes
        {
            public struct Unknown
            {
                public const string FILE_TYPE = "Unknown";
                public const string FILE_EXTENSION = "?";
                public const string FILE_ICON = "_blank.png";
            }
        }
        //>
    }
    //>
}