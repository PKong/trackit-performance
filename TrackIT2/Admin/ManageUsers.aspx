﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="ManageUsers.aspx.cs" Inherits="TrackIT2.Admin.ManageUsers" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register Assembly="skmValidators" Namespace="skmValidators" TagPrefix="skm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2>Manage Users</h2>

   <p style="font-style: italic; font-size: 10pt;">
      Note: If no TMO Id is specified, the user will log in with their e-mail address.
   </p>

   <asp:GridView ID="gvUsers" runat="server"                  
                 AllowPaging="True" 
                 AllowSorting="True"
                 AutoGenerateColumns="False"
                 DataKeyNames="id, tmo_userid, email"
                 PageSize="10" 
                 onrowdatabound="gvUsers_RowDataBound" 
                 onselectedindexchanged="gvUsers_SelectedIndexChanged" 
                 onrowdeleting="gvUsers_RowDeleting" 
                 onpageindexchanging="gvUsers_PageIndexChanging" 
                 onsorting="gvUsers_Sorting">
        
        <AlternatingRowStyle CssClass="GridViewAlternateRow" />      
        <SelectedRowStyle CssClass="GridViewSelectedRow" />         
        <EditRowStyle CssClass="GridViewEditRow" />

        <Columns>
            <asp:TemplateField HeaderText="Options">
                <ItemTemplate>
                    <asp:LinkButton ID="lnbEdit" runat="server" CommandName="Select" Text="Edit"></asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="lnbDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this user?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TMO Id" SortExpression="tmo_userid"></asp:TemplateField>
            <asp:TemplateField HeaderText="Active"></asp:TemplateField>
            <asp:TemplateField HeaderText="E-mail" SortExpression="email"></asp:TemplateField>
            <asp:TemplateField HeaderText="First Name" SortExpression="first_name"></asp:TemplateField>
            <asp:TemplateField HeaderText="Last Name" SortExpression="last_name"></asp:TemplateField>
            <asp:TemplateField HeaderText="Role"></asp:TemplateField>
            <asp:TemplateField HeaderText="Job Role"></asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Type"></asp:TemplateField>
            <asp:TemplateField HeaderText="Manager"></asp:TemplateField>
        </Columns>
    </asp:GridView>

    <p>&nbsp;</p>
    
    <asp:Panel ID="pnlUserDetails" runat="server">      
      <h3>
         <asp:Label ID="lblUserDetails" runat="server" Text="[Detail Header]"></asp:Label>
      </h3>

      <fieldset class="register">
         <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="RegisterUserValidationGroup"/>

         <p>
            <asp:Label ID="lblUserName" runat="server" AssociatedControlID="txtUserName">Login: (TMO Id):</asp:Label>
            <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valUserNameRequired" runat="server" 
                                        ControlToValidate="txtUserName" 
                                        CssClass="failureNotification" 
                                        ErrorMessage="UserName is required."
                                        Text="*"
                                        ToolTip="UserName is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
            <span style="font-size: 10pt;">
               <em>If the user does not have a TMO Id, use their e-mail address as a login.</em>
            </span>
         </p>

         <p>&nbsp;</p>

         <p>
            <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail">E-mail:</asp:Label>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valEmailRequired" runat="server" 
                                        ControlToValidate="txtEmail" 
                                        CssClass="failureNotification" 
                                        ErrorMessage="E-mail is required." 
                                        Text="*"
                                        ToolTip="E-mail is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="valEmailRegex" runat="server" 
                                               ControlToValidate="txtEmail" 
                                               CssClass="failureNotification"
                                               ErrorMessage="E-mail address is not in a valid format." 
                                               Text="*"
                                               ToolTip="E-mail address is not in a valid format."
                                               ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                               ValidationGroup="RegisterUserValidationGroup">
            </asp:RegularExpressionValidator>
&nbsp;</p>

         <p>&nbsp;</p>

         <asp:Panel ID="pnlPasswordDetails" runat="server">
            <p>
               <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword">Password:</asp:Label>
               <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" EnableViewState="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valPasswordRequired" runat="server" 
                                           ControlToValidate="txtPassword" 
                                           CssClass="failureNotification" 
                                           ErrorMessage="Password is required." 
                                           Text="*"
                                           ToolTip="Password is required." 
                                           ValidationGroup="RegisterUserValidationGroup">
               </asp:RequiredFieldValidator>
            </p>
                            
            <p>&nbsp;</p>
                            
            <p>
               <asp:Label ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword">Confirm Password:</asp:Label>
               <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" EnableViewState="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="valConfirmPasswordRequired" runat="server" 
                                           ControlToValidate="txtConfirmPassword" 
                                           CssClass="failureNotification"
                                           Display="Dynamic" 
                                           ErrorMessage="Confirm Password is required."  
                                           Text="*"
                                           ToolTip="Confirm Password is required." 
                                           ValidationGroup="RegisterUserValidationGroup">
               </asp:RequiredFieldValidator>
               <asp:CompareValidator ID="valPasswordCompare" runat="server" 
                                     ControlToCompare="txtPassword" 
                                     ControlToValidate="txtConfirmPassword" 
                                     CssClass="failureNotification" 
                                     Display="Dynamic" 
                                     ErrorMessage="The Password and Confirmation Password must match."
                                     Text="*"
                                     ValidationGroup="RegisterUserValidationGroup">
               </asp:CompareValidator>
            </p>
                            
            <p>&nbsp;</p>
         </asp:Panel>                           
                            
         <p>
            <asp:Label ID="lblActive" runat="server" AssociatedControlID="chkActive">Active Employee (Y/N):</asp:Label>
            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
         </p>
                           
         <p>&nbsp;</p>
                            
         <p>
            <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">First Name:</asp:Label>
            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valFirstNameRequired" runat="server" 
                                        ControlToValidate="txtFirstName" 
                                        CssClass="failureNotification" 
                                        ErrorMessage="First Name is required." 
                                        Text="*"
                                        ToolTip="First Name is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
         </p>
                            
         <p>&nbsp;</p>
                            
         <p>
            <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Last Name:</asp:Label>
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="valLastNameRequired" runat="server" 
                                        ControlToValidate="txtLastName" 
                                        CssClass="failureNotification" 
                                        ErrorMessage="Last Name is required." 
                                        Text="*"
                                        ToolTip="Last Name is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
         </p>         
                            
         <p>&nbsp;</p>
                            
         <p>
            <asp:Label ID="lblJobRole" runat="server" AssociatedControlID="ddlJobRole">Job Role:</asp:Label>
            <asp:DropDownList ID="ddlJobRole" runat="server" AppendDataBoundItems="true">
               <asp:ListItem Text="Select a role ..." Value="-99"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="valJobRoleRequired" runat="server" 
                                        ControlToValidate="ddlJobRole"                                         
                                        CssClass="failureNotification"
                                        ErrorMessage="Job Role is required."
                                        InitialValue="-99"
                                        Text="*"
                                        ToolTip="Job Role is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
         </p>
                            
         <p>&nbsp;</p>
                            
         <p>
            <asp:Label ID="lblEmployeeType" runat="server" AssociatedControlID="ddlEmployeeType">Employee Type:</asp:Label>
            <asp:DropDownList ID="ddlEmployeeType" runat="server" AppendDataBoundItems="true">
               <asp:ListItem Text="Select a type ..." Value="-99"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="valEmployeeTypeRequired" runat="server" 
                                        ControlToValidate="ddlEmployeeType"  
                                        CssClass="failureNotification" 
                                        ErrorMessage="Employee Type is required." 
                                        InitialValue="-99" 
                                        Text="*"
                                        ToolTip="Employee Type is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
         </p>
                            
         <p>&nbsp;</p>
                            
         <p>
            <asp:Label ID="lblManager" runat="server" AssociatedControlID="ddlManager">Manager:</asp:Label>
            <asp:DropDownList ID="ddlManager" runat="server" AppendDataBoundItems="true">
               <asp:ListItem Text="Select a manager ..." Value="-99"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="valManagerRequired" runat="server" 
                                        ControlToValidate="ddlManager" 
                                        CssClass="failureNotification" 
                                        ErrorMessage="Manager is required." 
                                        InitialValue="-99"
                                        Text="*"
                                        ToolTip="Manager is required." 
                                        ValidationGroup="RegisterUserValidationGroup">
            </asp:RequiredFieldValidator>
         </p>

         <p>&nbsp;</p>
                            
         <p>
            <asp:Label ID="lblRole" runat="server" AssociatedControlID="cblRole">Application Role(s):</asp:Label>
            <skm:CheckBoxListValidator ID="calRoleRequired" runat="server" 
                                       ControlToValidate="cblRole" 
                                       CssClass="failureNotification"
                                       ErrorMessage="Please specify at least 1 role for the user within the application."
                                       MinimumNumberOfSelectedCheckBoxes="1"
                                       Text="*" 
                                       ToolTip="Role is required." 
                                       ValidationGroup="RegisterUserValidationGroup">
            </skm:CheckBoxListValidator>
            <asp:CheckBoxList ID="cblRole" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table">
            </asp:CheckBoxList>            
         </p>

         <p>&nbsp;</p>

         <p class="submitButton">
            <asp:Button ID="btnCreateUser" runat="server" Text="Create User" CausesValidation="true" ValidationGroup="RegisterUserValidationGroup" onclick="btnCreateUser_Click"/>
            <asp:Button ID="btnUpdateUser" runat="server" Text="Update User" CausesValidation="true" ValidationGroup="RegisterUserValidationGroup" onclick="btnUpdateUser_Click"/>
            <asp:Button ID="btnCancelUpdate" runat="server" Text="Cancel" CausesValidation="false" ValidationGroup="RegisterUserValidationGroup" onclick="btnCancelUpdate_Click"/>
         </p>
      </fieldset>
   </asp:Panel>

</asp:Content>