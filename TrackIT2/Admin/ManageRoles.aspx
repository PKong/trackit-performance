﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="TrackIT2.Admin.ManageRoles" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h2>Manage Roles</h2>

   <p>
      <h4>Add New Role</h4>
      <b>Name: </b><asp:TextBox ID="txtAddName" runat="server" MaxLength="256"></asp:TextBox>
      &nbsp;&nbsp;
      <asp:Button ID="btnAddRole" runat="server" Text="Add" onclick="btnAddRole_Click" />
   </p>

   <asp:GridView ID="gvRoles" runat="server"                  
                 AllowPaging="True" 
                 AllowSorting="True"
                 AutoGenerateColumns="False"
                 DataKeyNames="RoleId, RoleName"
                 PageSize="10" 
                 onpageindexchanging="gvRoles_PageIndexChanging"
                 onrowdatabound="gvRoles_RowDataBound"                  
                 onrowdeleting="gvRoles_RowDeleting"                  
                 onselectedindexchanged="gvRoles_SelectedIndexChanged" 
                 onsorting="gvRoles_Sorting">
        
        <AlternatingRowStyle CssClass="GridViewAlternateRow" />      
        <SelectedRowStyle CssClass="GridViewSelectedRow" />         
        <EditRowStyle CssClass="GridViewEditRow" />
               
        <Columns>
            <asp:TemplateField HeaderText="Options">
                <ItemTemplate>
                    <asp:LinkButton ID="lnbSelectRole" runat="server" CommandName="Select" Text="Users"></asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="lnbDeleteRole" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this role?');"></asp:LinkButton>
                </ItemTemplate>

                <EditItemTemplate>
                  <asp:LinkButton ID="lnbCancelRole" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                  &nbsp;&nbsp;
                  <asp:LinkButton ID="lnbUpdateRole" runat="server" CommandName="Edit" Text="Update"></asp:LinkButton>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Name" SortExpression="RoleName">
               <EditItemTemplate>
                  <asp:TextBox ID="txtEditName" runat="server" MaxLength="256"></asp:TextBox>
               </EditItemTemplate>
            </asp:TemplateField>                  
        </Columns>
    </asp:GridView>

    <p>&nbsp;</p>

    <h2>Users in Role</h2>

    <asp:Panel ID="pnlAddUserToRole" runat="server">
      <h4>Add User to Role</h4>
      <b>User: </b>
      <asp:DropDownList ID="ddlAddUser" runat="server" AppendDataBoundItems="true" >
         <asp:ListItem Text="Select a user ..." Value="-99"></asp:ListItem>
      </asp:DropDownList>
      &nbsp;&nbsp;
      <asp:Button ID="btnAddUserToRole" runat="server" Text="Add" onclick="btnAddUserToRole_Click" />
    </asp:Panel>

    <asp:GridView ID="gvRoleUsers" runat="server" 
                 AllowSorting="True"
                 AutoGenerateColumns="False"
                 DataKeyNames="email"
                 onrowdatabound="gvRoleUsers_RowDataBound" 
                 onrowdeleting="gvRoleUsers_RowDeleting"
                 onsorting="gvRoleUsers_Sorting">
        <AlternatingRowStyle CssClass="listAlternatingRow" />
        <SelectedRowStyle BackColor="#F9F900" />
        <Columns>
            <asp:TemplateField HeaderText="Options">
                <ItemTemplate>
                    <asp:LinkButton ID="lnbDelete" runat="server" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this user from the role?');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TMO Id" SortExpression="tmo_userid"></asp:TemplateField>
            <asp:TemplateField HeaderText="E-Mail" SortExpression="email"></asp:TemplateField>
            <asp:TemplateField HeaderText="First Name" SortExpression="first_name"></asp:TemplateField>     
            <asp:TemplateField HeaderText="Last Name" SortExpression="last_name"></asp:TemplateField>      
        </Columns>
    </asp:GridView>
</asp:Content>
