﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using Utility = TrackIT2.BLL.Utility;

namespace TrackIT2.Admin
{
   public partial class ManageRoles : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            Session["RoleSortCol"] = "RoleName";
            Session["RoleSortDir"] = "ASC";
            Session["RoleUserSortCol"] = "tmo_userid";
            Session["RoleUserSortDir"] = "ASC";

            gvRoleUsers.Visible = false;
            pnlAddUserToRole.Visible = false;
            BindRoles();
         }
      }

      protected void BindRoles()
      {
         CPTTEntities ce = new CPTTEntities();
         List<aspnet_Roles> roles = new List<aspnet_Roles>();
         string sortCol = (string) Session["RoleSortCol"];
         string sortDir = (string) Session["RoleSortDir"];

         if (sortDir == "ASC")
         {
            roles = ce.aspnet_Roles
                      .OrderBy(Utility.GetField<aspnet_Roles>(sortCol))
                      .ToList();
         }
         else
         {
            roles = ce.aspnet_Roles
                      .OrderByDescending(Utility.GetField<aspnet_Roles>(sortCol))
                      .ToList();
         }

         gvRoles.DataSource = roles;
         gvRoles.DataBind();
      }

      protected void BindRoleUsers(string roleName)
      {
         CPTTEntities ce = new CPTTEntities();
         List<user> roleUsers = new List<user>();
         List<string> excludeUsers = new List<string>();
         string[] userIds;
         string sortCol = (string) Session["RoleUserSortCol"];
         string sortDir = (string) Session["RoleUserSortDir"];
         
         userIds = Roles.GetUsersInRole(roleName);

         var users = from user in ce.users
                     where userIds.Contains(user.tmo_userid)
                     select user;

         if (sortDir == "ASC")
         {
            roleUsers = users
                        .OrderBy(Utility.GetField<user>(sortCol))
                        .ToList<user>();
         }
         else
         {
            roleUsers = users
                        .OrderByDescending(Utility.GetField<user>(sortCol))
                        .ToList<user>();
         }
         
         gvRoleUsers.DataSource = roleUsers;
         gvRoleUsers.DataBind();

         foreach (user currUser in roleUsers)
         {
            excludeUsers.Add(currUser.email);
         }

         BindUserList(excludeUsers);
         
         gvRoleUsers.Visible = true;
         pnlAddUserToRole.Visible = true;
      }

      protected void BindUserList(List<string> usersToExclude)
      {
         CPTTEntities ce = new CPTTEntities();
         ListItem userItem;

         ddlAddUser.ClearSelection();
         ddlAddUser.Items.Clear();

         List<user> allUsers = ce.users
                                 .OrderBy(Utility.GetField<user>("first_name"))                     
                                 .OrderBy(Utility.GetField<user>("last_name"))                                 
                                 .ToList();

         userItem = new ListItem();
         userItem.Text = "Select a user ...";
         userItem.Value = "-99";
         ddlAddUser.Items.Add(userItem);

         foreach (user currUser in allUsers)
         {
            if (!usersToExclude.Contains(currUser.email))
            {
               userItem = new ListItem();
               userItem.Text = currUser.last_name + ", " + currUser.first_name;
               userItem.Value = currUser.email;               
               ddlAddUser.Items.Add(userItem);
            }
         }
      }

      protected void gvRoles_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
         * 0 - Actions (Edit)
         * 1 - Name
         */
         if (e.Row.RowType == DataControlRowType.Header)
         {
            LinkButton sortLink;
            string sortCol = (string) Session["RoleSortCol"];
            string sortDir = (string) Session["RoleSortDir"];

            // Use the HTML safe codes for the up arrow ? and down arrow ?.
            string sortArrow = sortDir == "ASC" ? "&#9650;" : "&#9660;";

            // GridView rows with sortable columns will have a linkbutton
            // generated. Compare to the CommandArgument since this is what
            // we set our sortColumn based on.
            foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
            {
               if (currCell.HasControls())
               {
                  sortLink = ((LinkButton)currCell.Controls[0]);
                  if (sortLink.CommandArgument == sortCol)
                  {
                     sortLink.Text = sortLink.Text + " " + sortArrow;
                  }
               }
            }
         }

         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            aspnet_Roles currRole = (aspnet_Roles)e.Row.DataItem;
            LinkButton selectButton = (LinkButton)e.Row.Cells[0].FindControl("lnbSelectRole");
            LinkButton deleteButton = (LinkButton)e.Row.Cells[0].FindControl("lnbDeleteRole");

            if (gvRoles.EditIndex == -1)
            {                              
               selectButton.CommandArgument = currRole.RoleName;
               deleteButton.CommandArgument = currRole.RoleName;
            }
            else
            {
               selectButton.Visible = false;
               deleteButton.Visible = false;
            }

            if (gvRoles.EditIndex != e.Row.RowIndex)
            {
               e.Row.Cells[1].Text = currRole.RoleName;
            }
            else
            {
               TextBox txtEditName = (TextBox)e.Row.Cells[1].FindControl("txtEditName");
            }
         }
      }

      protected void gvRoles_SelectedIndexChanged(object sender, EventArgs e)
      {
         string roleName = (string) gvRoles.SelectedDataKey["RoleName"];

         BindRoles();
         BindRoleUsers(roleName);
      }

      protected void gvRoles_RowDeleting(object sender, GridViewDeleteEventArgs e)
      {
         string roleName;
         string[] roleUsers;

         roleName = (string) gvRoles.DataKeys[e.RowIndex]["RoleName"];
         roleUsers = Roles.GetUsersInRole(roleName);

         if (roleUsers.Length == 0)
         {            
            Roles.DeleteRole(roleName);            
            
            pnlAddUserToRole.Visible = false;
            gvRoleUsers.Visible = false;
            BindRoles();

            Master.StatusBox.showStatusMessage("Role removed.");
         }
         else
         {
            e.Cancel = true;
            Master.StatusBox.showErrorMessage
                             ("<p>Users exist in the role specified.</p>" + 
                              "<p>&nbsp;</p>" + 
                              "<p>Please remove the users from the role " +
                              "before continuing.</p>");
         }
      }

      protected void gvRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
      {
         if (gvRoles.EditIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Cannot change pages while a record is being edited.");
            e.Cancel = true;
         }
         else
         {
            gvRoles.PageIndex = e.NewPageIndex;
            BindRoles();
         }
      }

      protected void gvRoles_Sorting(object sender, GridViewSortEventArgs e)
      {
         string sortCol = (string) Session["RoleSortCol"];
         string sortDir = (string) Session["RoleSortDir"];

         if (gvRoles.EditIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Cannot change sorting while a record is being edited.");
            e.Cancel = true;
         }
         else
         {
            if (sortCol != e.SortExpression)
            {
               Session["RoleSortDir"] = "ASC";
            }
            else
            {
               if (sortDir == "ASC")
               {
                  Session["RoleSortDir"] = "DESC";
               }
               else
               {
                  Session["RoleSortDir"] = "ASC";
               }
            }

            Session["RoleSortCol"] = e.SortExpression;
            BindRoles();
         }
      }

      protected void gvRoleUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
      {
         string userEmail;
         string userName;
         string roleName;
         
         userEmail = (string) gvRoleUsers.DataKeys[e.RowIndex]["email"];         
         roleName = (string) gvRoles.SelectedDataKey["RoleName"];
         userName = Membership.GetUserNameByEmail(userEmail);

         Roles.RemoveUserFromRole(userName, roleName);

         BindRoleUsers(roleName);
         Master.StatusBox.showStatusMessage("User removed from role.");
      }

      protected void gvRoleUsers_Sorting(object sender, GridViewSortEventArgs e)
      {
         string sortCol = (string) Session["RoleUserSortCol"];
         string sortDir = (string) Session["RoleUserSortDir"];
         string roleName = (string) gvRoles.SelectedDataKey["RoleName"];

         if (sortCol != e.SortExpression)
         {
            Session["RoleUserSortDir"] = "ASC";
         }
         else
         {
            if (sortDir == "ASC")
            {
               Session["RoleUserSortDir"] = "DESC";
            }
            else
            {
               Session["RoleUserSortDir"] = "ASC";
            }
         }

         Session["RoleUserSortCol"] = e.SortExpression;         
         BindRoleUsers(roleName);
      }

      protected void gvRoleUsers_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
         * 0 - Actions (Edit)
         * 1 - TMO Id
         * 2 - Email
         * 3 - First Name
         * 4 - Last Name
         */
         if (e.Row.RowType == DataControlRowType.Header)
         {
            LinkButton sortLink;
            string sortCol = (string) Session["RoleUserSortCol"];
            string sortDir = (string) Session["RoleUserSortDir"];

            // Use the HTML safe codes for the up arrow ? and down arrow ?.
            string sortArrow = sortDir == "ASC" ? "&#9650;" : "&#9660;";

            // GridView rows with sortable columns will have a linkbutton
            // generated. Compare to the CommandArgument since this is what
            // we set our sortColumn based on.
            foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
            {
               if (currCell.HasControls())
               {
                  sortLink = ((LinkButton)currCell.Controls[0]);
                  if (sortLink.CommandArgument == sortCol)
                  {
                     sortLink.Text = sortLink.Text + " " + sortArrow;
                  }
               }
            }
         }

         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            user currUser = (user)e.Row.DataItem;

            LinkButton deleteButton = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");

            deleteButton.CommandArgument = currUser.aspnet_user_id.ToString();
            e.Row.Cells[1].Text = currUser.tmo_userid;
            e.Row.Cells[2].Text = currUser.email;
            e.Row.Cells[3].Text = currUser.first_name;
            e.Row.Cells[4].Text = currUser.last_name;
         }
      }

      protected void btnAddRole_Click(object sender, EventArgs e)
      {
         if (!String.IsNullOrWhiteSpace(txtAddName.Text))
         {
            if (txtAddName.Text.Contains(","))
            {
               Master.StatusBox.showErrorMessage
                                ("Role cannot contain a ',' character.");
            }
            else
            {
               if (!Roles.RoleExists(txtAddName.Text.Trim()))
               {
                  Roles.CreateRole(txtAddName.Text.Trim());
                  Master.StatusBox.showStatusMessage("Role created.");

                  gvRoles.SelectedIndex = -1;
                  BindRoles();
                  pnlAddUserToRole.Visible = false;
                  gvRoleUsers.Visible = false;
               }
               else
               {
                  Master.StatusBox.showErrorMessage("Role already exists.");
               }
            }
         }
         else
         {
            Master.StatusBox.showErrorMessage("Please sepcify a role.");
         }
      }

      protected void btnAddUserToRole_Click(object sender, EventArgs e)
      {
         string roleName;
         string userName;

         if (ddlAddUser.SelectedIndex > 0)
         {
            roleName = (string) gvRoles.SelectedDataKey["RoleName"];
            userName = Membership.GetUserNameByEmail(ddlAddUser.SelectedValue);

            Roles.AddUserToRole(userName, roleName);

            BindRoleUsers(roleName);
            Master.StatusBox.showStatusMessage("User added to role.");
         }
         else
         {
            Master.StatusBox.showErrorMessage("Please specify user to add.");
         }
      }
   }
}