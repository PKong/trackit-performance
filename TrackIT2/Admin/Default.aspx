﻿<%@ Page Title="Administrator Menu | TrackIT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.Admin.Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center;">
        <h2>Administrator Menu</h2>

        <ul class="admin-nav">            
            <li><a href="ManageUsers.aspx">Manage Users</a> </li>
            <li><a href="ManageRoles.aspx">Manage Roles</a> </li>
            <li><a href="ManageFieldAccess.aspx">Manage Field Access</a> </li>
            <li><a href="ManageMetadata.aspx">Manage Display Groups</a> </li>
            <li><a href="ManageProjectCategories.aspx">Manage Project Categories</a></li>            
        </ul>
    </div>
</asp:Content>
