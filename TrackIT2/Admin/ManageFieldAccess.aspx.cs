﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.Admin
{
   public partial class ManageFieldAccess : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            Session["AccessSortCol"] = "type";
            Session["AccessSortDir"] = "ASC";
            Session["AccessFilter"] = "Administrator";

            BindFilter();
            BindAccess();            
         }
      }

      protected void BindAccess()
      {
         CPTTEntities ce = new CPTTEntities();
         List<application_field_access> access = new List<application_field_access>();
         string sortCol = (string)Session["AccessSortCol"];
         string sortDir = (string)Session["AccessSortDir"];
         string filterName = (string)Session["AccessFilter"];

         if (sortDir == "ASC")
         {
            access = ce.application_field_access
                       .Where(a => a.name.Equals(filterName))
                       .OrderBy(Utility.GetField<application_field_access>(sortCol))
                       .ToList();
         }
         else
         {
            access = ce.application_field_access
                       .Where(a => a.name.Equals(filterName))
                       .OrderByDescending(Utility.GetField<application_field_access>(sortCol))
                       .ToList();
         }

         gvAccess.DataSource = access;
         gvAccess.DataBind();
      }

      protected void BindFilter()
      {
         CPTTEntities ce = new CPTTEntities();
         List<string> filterNames = new List<string>();
         string currFilter;
         
         currFilter = (string)Session["AccessFilter"];
         
         filterNames = ce.application_field_access
                         .OrderBy(Utility.GetField<application_field_access>("name"))
                         .Select(a => a.name).Distinct()
                         .ToList();

         ddlFilterByName.DataSource = filterNames;
         ddlFilterByName.DataBind();
         ddlFilterByName.ClearSelection();
         ddlFilterByName.Items.FindByValue(currFilter).Selected = true;
      }

      protected void ResetForm()
      {
         rblAccessType.SelectedIndex = 0;
         txtAccessName.Text = "";
         txtTableName.Text = "";
         txtFieldName.Text = "";
         chkCanView.Checked = true;
         chkCanEdit.Checked = false;
      }

      protected void btnAddAccess_Click(object sender, EventArgs e)
      {
         if (!string.IsNullOrEmpty(txtAccessName.Text) &&
             !string.IsNullOrEmpty(txtTableName.Text) &&
             !string.IsNullOrEmpty(txtFieldName.Text))
         {
            ApplicationFieldAccess.Add(rblAccessType.SelectedValue, txtAccessName.Text.Trim(), 
                                       txtTableName.Text.Trim(), txtFieldName.Text.Trim(), 
                                       chkCanView.Checked, chkCanEdit.Checked);

            ResetForm();
            BindAccess();
            Master.StatusBox.showStatusMessage("Field Access created.");            
         }
         else
         {
            Master.StatusBox.showErrorMessage("Please specify access name, table name, and field name.");
         }
      }

      protected void gvAccess_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Access Type (Role, User)
          * 2 - Access Name
          * 3 - Table Name
          * 4 - Field Name
          * 5 - Can View
          * 6 - Can Edit
         */
         if (e.Row.RowType == DataControlRowType.Header)
         {
            LinkButton sortLink;
            string sortCol = (string)Session["AccessSortCol"];
            string sortDir = (string)Session["AccessSortDir"];

            // Use the HTML safe codes for the up arrow ? and down arrow ?.
            string sortArrow = sortDir == "ASC" ? "&#9650;" : "&#9660;";

            // GridView rows with sortable columns will have a linkbutton
            // generated. Compare to the CommandArgument since this is what
            // we set our sortColumn based on.
            foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
            {
               if (currCell.HasControls())
               {
                  sortLink = ((LinkButton)currCell.Controls[0]);
                  if (sortLink.CommandArgument == sortCol)
                  {
                     sortLink.Text = sortLink.Text + " " + sortArrow;
                  }
               }
            }
         }

         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            application_field_access currAccess = (application_field_access)e.Row.DataItem;

            LinkButton editButton = (LinkButton)e.Row.Cells[0].FindControl("lnbEdit");
            LinkButton deleteButton = (LinkButton)e.Row.Cells[0].FindControl("lnbDelete");
            
            if (gvAccess.EditIndex != e.Row.RowIndex)
            {
               if (gvAccess.EditIndex == -1)
               {
                  editButton.CommandArgument = currAccess.id.ToString();
                  deleteButton.CommandArgument = currAccess.id.ToString();
               }
               else
               {
                  editButton.Visible = false;
                  deleteButton.Visible = false;
               }
               
               e.Row.Cells[1].Text = currAccess.type;
               e.Row.Cells[2].Text = currAccess.name;
               e.Row.Cells[3].Text = currAccess.table_name;
               e.Row.Cells[4].Text = currAccess.field_name;

               if (currAccess.can_view)
               {
                  e.Row.Cells[5].Text = "Yes";
               }
               else
               {
                  e.Row.Cells[5].Text = "No";
               }

               if (currAccess.can_edit)
               {
                  e.Row.Cells[6].Text = "Yes";
               }
               else
               {
                  e.Row.Cells[6].Text = "No";
               }
            }
            else
            {
               RadioButtonList rblEditAccessType = (RadioButtonList) e.Row.Cells[1].FindControl("rblEditAccessType");
               rblEditAccessType.ClearSelection();
               rblEditAccessType.Items.FindByValue(currAccess.type).Selected = true;

               TextBox txtEditAccessName = (TextBox)e.Row.Cells[2].FindControl("txtEditAccessName");
               txtEditAccessName.Text = currAccess.name;

               TextBox txtEditTableName = (TextBox)e.Row.Cells[3].FindControl("txtEditTableName");
               txtEditTableName.Text = currAccess.table_name;

               TextBox txtEditFieldName = (TextBox)e.Row.Cells[4].FindControl("txtEditFieldName");
               txtEditFieldName.Text = currAccess.field_name;

               CheckBox chkEditCanView = (CheckBox)e.Row.Cells[5].FindControl("chkEditCanView");
               if (currAccess.can_view)
               {
                  chkEditCanView.Checked = true;
               }
               else
               {
                  chkEditCanView.Checked = false;
               }

               CheckBox chkEditCanEdit = (CheckBox)e.Row.Cells[6].FindControl("chkEditCanEdit");
               if (currAccess.can_edit)
               {
                  chkEditCanEdit.Checked = true;
               }
               else
               {
                  chkEditCanEdit.Checked = false;
               }
            }
         }
      }

      protected void gvAccess_PageIndexChanging(object sender, GridViewPageEventArgs e)
      {
         if (gvAccess.EditIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Cannot change pages while a record is being edited.");
            e.Cancel = true;
         }
         else
         {
            gvAccess.PageIndex = e.NewPageIndex;
            BindAccess();
         }
      }

      protected void gvAccess_RowDeleting(object sender, GridViewDeleteEventArgs e)
      {
         int id;

         id = (int)gvAccess.DataKeys[e.RowIndex]["id"];
         ApplicationFieldAccess.Delete(id);

         BindAccess();
         Master.StatusBox.showStatusMessage("Field Access Security Removed.");
      }

      protected void gvAccess_Sorting(object sender, GridViewSortEventArgs e)
      {
         string sortCol = (string)Session["AccessSortCol"];
         string sortDir = (string)Session["AccessSortDir"];

         if (gvAccess.EditIndex > -1)
         {
            Master.StatusBox.showErrorMessage("Cannot change sorting while a record is being edited.");
            e.Cancel = true;
         }
         else
         {
            if (sortCol != e.SortExpression)
            {
               Session["AccessSortDir"] = "ASC";
            }
            else
            {
               if (sortDir == "ASC")
               {
                  Session["AccessSortDir"] = "DESC";
               }
               else
               {
                  Session["AccessSortDir"] = "ASC";
               }
            }

            Session["AccessSortCol"] = e.SortExpression;
            BindAccess();
         }
      }

      protected void gvAccess_RowEditing(object sender, GridViewEditEventArgs e)
      {
         CPTTEntities ce = new CPTTEntities();
         application_field_access access = new application_field_access();
         int id = (int)gvAccess.DataKeys[e.NewEditIndex].Value;

         access = ce.application_field_access
                   .Where(a => a.id.Equals(id))
                   .First();

         // Detach the record from the entity context so it can be used
         // for subsequent updates.
         ce.Detach(access);
         Session["currentAccess"] = access;
         
         gvAccess.EditIndex = e.NewEditIndex;
         BindAccess();
      }

      protected void gvAccess_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
      {
         gvAccess.EditIndex = -1;
         Master.StatusBox.showStatusMessage("Edit cancelled.");
         BindAccess();
      }

      protected void gvAccess_RowUpdating(object sender, GridViewUpdateEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Access Type (Role, User)
          * 2 - Access Name
          * 3 - Table Name
          * 4 - Field Name
          * 5 - Can View
          * 6 - Can Edit
         */
         if (Page.IsValid)
         {
            int id = (int)gvAccess.DataKeys[e.RowIndex].Value;
            application_field_access access = (application_field_access)Session["currentAccess"];
            GridViewRow editRow = gvAccess.Rows[e.RowIndex];

            RadioButtonList rblEditAccessType = (RadioButtonList)editRow.Cells[1].FindControl("rblEditAccessType");
            TextBox txtEditAccessName = (TextBox)editRow.Cells[2].FindControl("txtEditAccessName");
            TextBox txtEditTableName = (TextBox)editRow.Cells[3].FindControl("txtEditTableName");
            TextBox txtEditFieldName = (TextBox)editRow.Cells[4].FindControl("txtEditFieldName");
            CheckBox chkEditCanView = (CheckBox)editRow.Cells[2].FindControl("chkEditCanView");
            CheckBox chkEditCanEdit = (CheckBox)editRow.Cells[2].FindControl("chkEditCanEdit");

            try
            {
               ApplicationFieldAccess.Update(access, rblEditAccessType.SelectedValue, 
                                             txtEditAccessName.Text.Trim(), txtEditTableName.Text.Trim(), 
                                             txtEditFieldName.Text.Trim(), chkEditCanView.Checked, 
                                             chkEditCanEdit.Checked);
               gvAccess.EditIndex = -1;
               BindAccess();
               Session["currentAccess"] = null;

               Master.StatusBox.showStatusMessage("Field Access Updated.");
            }
            catch (OptimisticConcurrencyException)
            {
               string message = "<p>This record has been modified since you last loaded it.</p>" +
                                "<p>There may be new changes to the record.</p>" +
                                "<p>Please reload</a> the record and try again.</p>";
               Master.StatusBox.showErrorMessage(message);
            }
         }
      }

      protected void btnFilterByName_Click(object sender, EventArgs e)
      {
         Session["AccessFilter"] = ddlFilterByName.SelectedValue;
         BindAccess();
      }
   }
}