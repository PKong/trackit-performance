﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.Reports._Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   <div style="text-align: center;">
      <h2>Reports</h2>

      <p>
         Note: You will need to provide your reporting server credentials on your 
         <asp:Hyperlink ID="hypProfile" runat="server" NavigateUrl="~/Account/Profile.aspx">profile</asp:Hyperlink> 
         page in order to be able to access the reports.
      </p>

      <p>&nbsp;</p>

      <ul class="admin-nav">            
         <li><a href="http://tmtref01.tmtcpt.com/Reports" target="_blank">Reporting Server</a></li>
         <li><a href="Assumptions.aspx" target="_blank">Assumptions</a></li>
         <li><a href="BlankDates.aspx" target="_blank">Blank Dates Report</a></li>
         <li><a href="ExpiredProjectedDetail.aspx" target="_blank">Expired Projected Detail</a></li>   
         <li><a href="FutureActualDetail.aspx" target="_blank">Future Actual Detail</a></li>
      </ul>
   </div>

</asp:Content>
