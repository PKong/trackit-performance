﻿using System;

namespace TrackIT2.Objects
{
   public class SearchParameter
   {
      public string VariableName { get; set; }
      public string WhereClause { get; set; }      
      public bool IsWildcardSearch { get; set; }
      public bool IsRangeSearch { get; set; }
      public bool AllowsMultipleValues { get; set; }      
      public Type ParameterType { get; set; }
      public bool IgnoreProcessing { get; set; }
   }
}