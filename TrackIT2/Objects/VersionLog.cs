﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackIT2
{
   public class VersionLog : IComparer<VersionLog>
   {
      public enum ComparisonType
      {
         Version = 0,
         UpdatedBy = 1,
         UpdatedAt = 2,
         FieldChanged = 3
      }
            
      public int version { get; set; }
      public int groupId { get; set; }
      public string updatedBy { get; set; }
      public DateTime updatedAt { get; set; }
      public string fieldChanged { get; set; }
      public string oldValue { get; set; }
      public string newValue { get; set; }
      public ComparisonType ComparisonMethod { get; set; }
      public string UserAvatarImageUrl { get; set; }

      public VersionLog()
      {
         ComparisonMethod = ComparisonType.Version;
      }

      public VersionLog(ComparisonType comparisonMethod)
      {
         ComparisonMethod = comparisonMethod;
      }

      public int CompareTo(VersionLog y)
      {
         return version.CompareTo(y.version);
      }

      public int Compare(VersionLog x, VersionLog y)
      {
         switch (ComparisonMethod)
         {
            case ComparisonType.Version:
               return x.version.CompareTo(y.version);

            case ComparisonType.UpdatedBy:
               return x.updatedBy.CompareTo(y.updatedBy);

            case ComparisonType.UpdatedAt:
               return x.updatedAt.CompareTo(y.updatedAt);

            case ComparisonType.FieldChanged:
               return x.fieldChanged.CompareTo(y.fieldChanged);
               
            default:
               return x.version.CompareTo(y.version);
         }
      }

      public static Predicate<VersionLog> FindPredicate(VersionLog verlog)
      {
         return (VersionLog verlog2) => verlog.version == verlog2.version;
      }

      public static Predicate<VersionLog> FindPredicateByVersionNumber(int versionNumber)
      {
         return (VersionLog verlog2) => versionNumber == verlog2.version;
      }

      public static Predicate<VersionLog> FindPredicateByUpdatedBy(string updatedBy)
      {
         return (VersionLog verlog2) => updatedBy == verlog2.updatedBy;
      }

      public static Predicate<VersionLog> FindPredicateByUpdatedAt(DateTime updatedAt)
      {
         return (VersionLog verlog2) => updatedAt == verlog2.updatedAt;
      }

      public static Predicate<VersionLog> FindPredicateByFieldChanged(string fieldChanged)
      {
         return (VersionLog verlog2) => fieldChanged == verlog2.fieldChanged;
      }
   }
}