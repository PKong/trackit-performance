﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Data;
using System.Text;
using System.Web;
using TrackIT2.SiteMaster;

namespace TrackIT2.Objects
{
   public class DMSHelper : System.Web.UI.Page
   {
      private const string privateKey = "M8F>M=B*E0D(\\AU)&A)I\\K-S1^D4]F8ME7:<%QE&4[!?0X],/0;YMH#2.6=-!";
      private const string publicKey = "1E8220D9-3516-4026-8D34-CF57393819D5";
      private string time  = DateTime.Now.ToUniversalTime().ToString();    
    
      string siteId = string.Empty;

      public DataTable GetPublicDocuments(string siteId)
      {
         if(!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["towerid"]))
         {
            siteId = HttpContext.Current.Request.QueryString["towerid"];
         }

         List<string> docParams = new List<string>();
         docParams.Add(siteId);
         docParams.Add(publicKey);
         docParams.Add(time);
        
         DmsSubscriptionContractClient siteMastersClient = new DmsSubscriptionContractClient();
         string encodedSignature = HttpContext.Current.Server.UrlEncode(CreateSignature(BuildSignatureString(docParams)));
         DataTable dt = new DataTable();

         try
         {
            dt = siteMastersClient.ListPublicSiteDocuments(docParams[0], docParams[1], docParams[2], encodedSignature);
         }
         catch(System.ServiceModel.FaultException<DmsSubScriptionFaultException>)
         {
            dt.Columns.Add("DMSFiles_Id", typeof(Int32));
            dt.Columns.Add("OriginalFileName", typeof(String));
            dt.Columns.Add("DocumentType_Id", typeof(Int32));
            dt.Columns.Add("DocumentStatus", typeof(String));

         DataRow dr = dt.NewRow();
         dr["DMSFiles_Id"] = "-1";
         dr["OriginalFileName"] = "Docs temporarily unavailable";
         dt.Rows.Add(dr);
         }

         return dt;
      }

      public string GetDocument(int id, string documentName, DataTable documentNames)
      {
         List<string> linkParams = new List<string>();
         linkParams.Add(id.ToString());
         linkParams.Add(publicKey);
         linkParams.Add(DateTime.Now.ToUniversalTime().ToString());

         DmsSubscriptionContractClient siteMastersClient = new DmsSubscriptionContractClient();
         string encodedSignature = Server.UrlEncode(CreateSignature(BuildSignatureString(linkParams)));
         string downloadUrl = string.Empty;

         try
         {
            downloadUrl = siteMastersClient.GetDocument(Convert.ToInt32(linkParams[0]), linkParams[1], linkParams[2], encodedSignature);
            downloadUrl += "&PublicKey=" + publicKey + "&TimeStamp=" + linkParams[2] + "&Sig=" + encodedSignature + "&OriginalFileName=" + documentName.ToString() + "&Doc_Id=" + linkParams[0];
         }
         catch(System.ServiceModel.FaultException<DmsSubScriptionFaultException>)
         {
            downloadUrl = string.Empty;
         }

         return downloadUrl;
      }

      public string DownloadZipDocuments(DataTable datatbl)
      {
         string documentIds = string.Empty;
         int[] docArray = new int[datatbl.Rows.Count];

         for(int i = 0; i <= datatbl.Rows.Count - 1; i++)
         {
            documentIds += datatbl.Rows[i][0].ToString();
            docArray[i] = Convert.ToInt32(datatbl.Rows[i][0].ToString());
         }

         List<string> docLinkParams = new List<string>();
         docLinkParams.Add(documentIds);
         docLinkParams.Add(publicKey);
         docLinkParams.Add(DateTime.Now.ToUniversalTime().ToString());

         DmsSubscriptionContractClient siteMastersClient = new DmsSubscriptionContractClient();
         string encodedSignature = Server.UrlEncode(CreateSignature(BuildSignatureString(docLinkParams)));
         string docUrl = string.Empty;

         try
         {
            docUrl = siteMastersClient.GetDocuments(docArray, docLinkParams[1], docLinkParams[2], encodedSignature);
            docUrl += "&PublicKey=" + publicKey + "&TimeStamp=" + docLinkParams[2] + "&Sig=" + encodedSignature + "&OriginalFileName=Download.zip" + "&Doc_Id=" + docLinkParams[0];
         }
         catch(Exception)
         {
            docUrl = string.Empty;
         }

         return docUrl;
      }

      public DataTable GetSDPDocuments(string siteId)
      {
         List<string> docParams = new List<string>();
         docParams.Add(siteId);
         docParams.Add(publicKey);
         docParams.Add(time);

         DmsSubscriptionContractClient siteMastersClient = new DmsSubscriptionContractClient();
         string encodedSignature = Server.UrlEncode(CreateSignature(BuildSignatureString(docParams)));
         DataTable listSDPDocuments = new DataTable();

         try
         {
            listSDPDocuments = siteMastersClient.ListSDPDocuments(docParams[0], docParams[1], docParams[2], encodedSignature);
         }
         catch(Exception)
         {
            listSDPDocuments.Clear();
         }

         return listSDPDocuments;
      }

      public List<PhotoInfo>GetSitePhotos(string siteId, bool isThumbnail, string photoId)
      {
         List<PhotoInfo> urlInfo = new List<PhotoInfo>();
         List<string> listParameters = new List<string>();       
         bool isThumb = isThumbnail;
        
         listParameters.Add(siteId);
         listParameters.Add(publicKey);
         listParameters.Add(time);

         DmsSubscriptionContractClient siteMastersClient = new DmsSubscriptionContractClient();
         string encodedSignature = Server.UrlEncode(CreateSignature(BuildSignatureString(listParameters)));
         DataTable dt = new DataTable();

         if (string.IsNullOrEmpty(photoId))
         {
            try
            {
                // TODO: DEBUG ONLY
               //dt = siteMastersClient.ListSitePhotos(siteId, publicKey, time, encodedSignature);
                throw new Exception("DEBUG ONLY");
            }
            catch (Exception)
            {
               dt.Columns.Add("PhotoID", typeof(string));
               dt.Columns.Add("PhotoUrl", typeof(string));

               for (int i = 0; i <= 4; i++)
               {
                  DataRow dr = dt.NewRow();
                  dr["PhotoID"] = -1;
                  dr["PhotoUrl"] = isThumb ? "/images/NoService_small.jpg" : "/images/NoService_large.jpg";
                  dt.Rows.Add(dr);
               }
            }
         }

         string urlFormat = "~/ImageSource.aspx?Photo_id={0}&PublicKey={1}&TimeStamp={2}&Sig={3}&SiteID={4}";
        
         if(isThumb)
         {
            for(int i = 0; i <= dt.Rows.Count - 1; i++)
            {
               PhotoInfo imageUrlInfo = new PhotoInfo();
             
               if (dt.Rows[i][0].ToString() == "-1")
               {
                  imageUrlInfo.photoID = string.Empty;
                  imageUrlInfo.photoURL = dt.Rows[i]["PhotoUrl"].ToString();
               }
               else
               {
                  imageUrlInfo.photoID = dt.Rows[i][0].ToString();
                  imageUrlInfo.photoURL = String.Format(urlFormat, dt.Rows[i][0].ToString(), publicKey, time, encodedSignature, siteId) + "&Thumbnail=yes";
               }

               urlInfo.Add(imageUrlInfo);
            }
         }
         else
         {
            PhotoInfo imageUrlInfo = new PhotoInfo();
            if (string.IsNullOrEmpty(photoId))
            {
                  if (dt.Rows[0][0].ToString() == "-1")
                  {
                  imageUrlInfo.photoID = dt.Rows[0]["PhotoID"].ToString();
                  imageUrlInfo.photoURL = dt.Rows[0]["PhotoUrl"].ToString();
                  }
                  else
                  {
                  imageUrlInfo.photoID = dt.Rows[0][0].ToString();
                  imageUrlInfo.photoURL = String.Format(urlFormat, dt.Rows[0][0].ToString(), publicKey, time, encodedSignature, siteId) + "&Thumbnail=no";
                  }
            }
            else
            {
                  imageUrlInfo.photoID = photoId;
                  imageUrlInfo.photoURL = String.Format(urlFormat, photoId.ToString(), publicKey, time, encodedSignature, siteId) + "&Thumbnail=no";
            }

            urlInfo.Add(imageUrlInfo);
         }

         return urlInfo;
      }

      public string CreateSignature(string signatureUnencrypted)
      {
         ASCIIEncoding encodedString = new ASCIIEncoding();
         HMACSHA256 myHash = new HMACSHA256(encodedString.GetBytes(privateKey));

         Byte[] compareSig = myHash.ComputeHash(encodedString.GetBytes(signatureUnencrypted));
         return Convert.ToBase64String(compareSig);
      }

      public string BuildSignatureString(List<string> parameters)
      {
         try
         {
         string sReturn;
         sReturn = String.Empty;

         foreach(string sParameter in parameters)
         {
            sReturn += sParameter + "\n";
         }
         
            return sReturn;
         }
         catch (Exception)
         {
            return string.Empty;
         }
      }
   }
}