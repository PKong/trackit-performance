﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrackIT2.Objects
{
   [Serializable]
   public class SavedLinks
   {
      public List<SavedLink> LinkItems;
   }
   
   [Serializable]
   public class SavedLink
   {
      public string title;
      public string url;
      public string description;

      public SavedLink() { }

      public SavedLink(string newTitle, string newUrl, string newDescription)
      {
         title = newTitle;
         url = newUrl;
         description = newDescription;
      }
   }
}