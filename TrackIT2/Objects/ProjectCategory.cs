﻿namespace TrackIT2.Objects
{
   public enum ProjectCategory
   {
      Initial, 
      Mod,
      GroundModFiber,
      TMOPremod, 
      CostShareOnly, 
      SprintNV
   }
}