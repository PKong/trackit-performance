﻿// Implementation of profile provider provided by Jon Galloway:
// http://weblogs.asp.net/jgalloway/archive/2008/01/19/writing-a-custom-asp-net-profile-class.aspx
// Additional documentation on working with more complex objects can be found
// on the MSDN site:
// http://msdn.microsoft.com/en-us/library/d8b58y5d.aspx
using System.Web.Profile;
using System.Web.Security;

namespace TrackIT2.Objects
{
   public class UserProfile : ProfileBase
   {
      public static UserProfile GetUserProfile(string username)
      {
         return Create(username) as UserProfile;
      }

      public static UserProfile GetUserProfile() 
      { 
         return Create(Membership.GetUser().UserName) as UserProfile; 
      } 
      
      [SettingsAllowAnonymous(false)]
      public SavedFilters SavedFilters
      {
          get { return base["SavedFilters"] as SavedFilters; }
          set { base["SavedFilters"] = value; }
      }

      [SettingsAllowAnonymous(false)]
      public SavedLinks SavedLinks
      {
         get { return base["SavedLinks"] as SavedLinks; }
         set { base["SavedLinks"] = value; }
      }

      [SettingsAllowAnonymous(false)]
      public JSONWrapper.MyWidgets UserWidgets
      {
          get { return base["UserWidgets"] as JSONWrapper.MyWidgets; }
          set { base["UserWidgets"] = value; }
      }

      [SettingsAllowAnonymous(false)]
      public string ReportLogin
      {
         get { return base["ReportLogin"] as string; }
         set { base["ReportLogin"] = value; }
      }

      [SettingsAllowAnonymous(false)]
      public string ReportPassword
      {
         get { return base["ReportPassword"] as string; }
         set { base["ReportPassword"] = value; }
      }
   }
}