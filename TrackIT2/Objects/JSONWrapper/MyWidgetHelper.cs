﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TrackIT2.DAL;

namespace TrackIT2.Objects.JSONWrapper
{
   
    public class DashboardJSONHelper
    {
        #region Members + Accessors
        private MyWidgets m_objMyWidgets;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialize the helper with defined MyWidgets class.
        /// </summary>
        /// <param name="objMyWidgets">MyWidgets Class from user profile.</param>
        public DashboardJSONHelper(MyWidgets objMyWidgets)
        {
            m_objMyWidgets = objMyWidgets;
        }

        /// <summary>
        /// Initialize the helper with default json string from Dashboard/jsonfeed.
        /// </summary>
        /// <param name="sMyWidgetsJSON">JSON string that loaded from Dashboard/jsonfeed.</param>
        public DashboardJSONHelper(string sMyWidgetsJSON)
        {
            var JSONResult = Newtonsoft.Json.JsonConvert.DeserializeObject<JSONMyWidgetResult>(sMyWidgetsJSON);
            m_objMyWidgets = new MyWidgets();
            if (JSONResult != null && JSONResult.result != null)
            {
                m_objMyWidgets.MyWidgetsResult =JSONResult;
            }
            else
            {
                //Some error occur. JSON didn't have properly format.
            }
        }

        
        #endregion


        #region Methods
        /// <summary>
        /// Get current MyWidgets Object
        /// </summary>
        /// <returns></returns>
        public MyWidgets GetMyWidgets()
        {
            return m_objMyWidgets;
        }
        /// <summary>
        /// Get Current MyWidgets Object in JSON format.
        /// </summary>
        /// <returns></returns>
        public string GetMyWidgetJSON()
        {
            string ret = null;
            if (m_objMyWidgets != null)
            {
                ret = Newtonsoft.Json.JsonConvert.SerializeObject(m_objMyWidgets.MyWidgetsResult).ToString();
            }
            return ret;
        }
        /// <summary>
        /// Set Current MyWidgets Object from JSON string.
        /// </summary>
        /// <param name="sJSON"></param>
        /// <returns></returns>
        public Boolean SetMyWidgetWithJSON(string sJSON)
        {
            Boolean result = false;
            if (!sJSON.Contains("{\"result\":"))
            {
                sJSON = "{\"result\": " + sJSON + "}";
            }
            var objMyWidget = Newtonsoft.Json.JsonConvert.DeserializeObject<JSONMyWidgetResult>(sJSON);
            if (objMyWidget != null && objMyWidget.result != null)
            {
                m_objMyWidgets.MyWidgetsResult = objMyWidget;
                result = true;
            }

            return result;
        }

        #endregion
    }

    [Serializable]
    public class MyWidgets
    {
        public JSONMyWidgetResult MyWidgetsResult { get; set; }
        
    }

#region Class Template for JSON object
    //For MyWidget JSON
    [Serializable]
    public class JSONMyWidgetResult
    {
        public JSONMyWidgets result { get; set; }
        //Constructor
        public JSONMyWidgetResult()
        {
            result = new JSONMyWidgets();
        }

        //Inner Class
        public class JSONMyWidgets
        {
            public string layout { get; set; }
            public List<JSONWidgetInfo> data { get; set; }

            //Constructor
            public JSONMyWidgets()
            {
                data = new List<JSONWidgetInfo>();
            }
            //Inner Class
            public class JSONWidgetInfo
            {
                
                public string title { get; set; }
                public string column { get; set; }
                public string id { get; set; }
                public string url { get; set; }
                public string editurl { get; set; }
                public Boolean open { get; set; }
                public Boolean double_height { get; set; }

                public WidgetMetadataInfo metadata { get; set; }

                //Not sure properties
                public int new_position { get; set; }
                
                
            }
        }
    }

    [Serializable]
    public class WidgetMetadataInfo
    {
        public string defaults { get; set; }
    }
#endregion
    
}