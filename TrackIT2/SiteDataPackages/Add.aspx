﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Site Data Package | TrackiT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TrackIT2.SiteDataPackage.Add" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" Src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/site_data_package_add") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/site_data_package_add") %>   

    <script type="text/javascript">
        $(document).ready(function () {
            $("li.ui-menu-item").live("click", function () {
                document.forms["Form1"].submit();
            });

            $("#txtSiteID").live("focusOut", function () {
                document.forms["Form1"].submit();
            });

            // Setup Upload Document Modal
            setupDmsModal("Upload Document");
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
    <div id="message_alert">
    </div>
    <asp:Panel ID="pnlSDPContainer" CssClass="create-container-small form_element" runat="server">
        <h4>Select Site</h4>
        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID" 
            LabelFieldClientID="lblSiteID" 
            LabelFieldValue="Site ID "
            LabelFieldCssClass="label" 
            TextFieldClientID="txtSiteID" 
            TextFieldCssClass="input-autocomplete"
            TextFieldWidth="188" 
            TextFieldValue="" 
            ScriptKeyName="ScriptAutoCompleteSiteID"
            DataSourceUrl="SiteIdAutoComplete.axd" 
            ClientIDMode="Static" 
            runat="server" />
        <br />
        <div id="HideForNow" runat="server" visible="true">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ctrSiteID" />
                </Triggers>
                <ContentTemplate>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="cb"></div>
            <br />
            <h4>Select Associated Lease App</h4>
                <asp:Label ID="Label1" runat="server" Text="" Font-Size="Small"></asp:Label>
                <asp:DropDownList ID="ddlAssociatedLeaseApps" CssClass="select-field" runat="server">
                </asp:DropDownList>
            <div class="cb"></div>
        </div>
        <asp:Panel ID="divSDPInfo" runat="server" Visible="false">
            <div class="form_element" style="margin-top:20px;">
                <h4>Site Data Package Info</h4>
                <asp:Label ID="labSDPRequestDate" runat="server" >SDP Request</asp:Label>
                <asp:TextBox ID="txtSDPRequestDate" runat="server" 
                                           CssClass="datepicker split"></asp:TextBox>
            </div>
        </asp:Panel>
        <div id="EditAbleField" runat="server" visible="false">
            <div class="form_element" style="margin-top:20px;">
                <h4>Summary</h4>
                <label>
                    SDP Priority</label>
                <asp:DropDownList ID="ddlSDPPriority" CssClass="select-field" runat="server">
                </asp:DropDownList>
                <div id="divNonLACreationFields_1" runat="server" >
                    <label id="lblCustomer" runat="server">
                        Customer</label>
                    <asp:DropDownList ID="ddlCustomer" CssClass="select-field" runat="server">
                    </asp:DropDownList>
                    <label id="lblSpecialProject" runat="server">
                        Special Project</label>
                    <asp:DropDownList ID="ddlSpecialProject" CssClass="select-field" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="divNonLACreationFields_2" runat="server" class="form_element" style="margin-top:10px;">
                <h4>Team Assigned</h4>
                <label>
                    SDP Assigned to Emp</label>
                <asp:DropDownList ID="ddlSDPAssignedToEMP" CssClass="select-field" runat="server">
                </asp:DropDownList>
                <label id="lblSDPTMOPMEmp" runat="server">
                    SDP TMO PM Emp</label>
                <asp:DropDownList ID="ddlSDPTMOPMEmp" CssClass="select-field" runat="server">
                </asp:DropDownList>
                <label id="lblSDPTMOSpecialistCurrentEmp" runat="server">
                    SDP TMO Specialist Current Emp</label>
                <asp:DropDownList ID="ddlSDPTMOSpecialistCurrentEmp" CssClass="select-field" runat="server">
                </asp:DropDownList>
                <label id="lblSDPTMOAdminEmp" runat="server">
                    SDP TMO Admin Emp</label>
                <asp:DropDownList ID="ddlSDPTMOAdminEmp" CssClass="select-field" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <asp:Button ID="btnSubmitCreateSite" runat="server" Text="Create" CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all" UseSubmitBehavior="false" OnClick="btnSubmitCreateSite_Click" OnClientClick="if (!validateSiteDataPackage()) { return false;}" />
        <div class="cb">
        </div>
    </asp:Panel>
</asp:Content>
