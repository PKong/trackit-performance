﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.SiteDataPackage
{
   public partial class Add : System.Web.UI.Page
   {
        
      public static TextBox txtSiteID = new TextBox();

      public site_data_packages CurrentSiteDataPackage
      {
         get
         {
               return (site_data_packages)this.ViewState["CurrentSiteDataPackage"];
         }
         set
         {
               this.ViewState["CurrentSiteDataPackage"] = value;
         }
      }

      protected void Page_Init(object sender, EventArgs e)
      {
         // Set Date Time Culture to en-US, some laptop set to local culture
         System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

         Control ctrl = PageUtility.FindControlRecursive(this.pnlSDPContainer, this.ctrSiteID.TextFieldClientID);
         if (ctrl != null)
         {
               TextBox t = ctrl as TextBox;
               t.TextChanged += new EventHandler(txtSiteID_TextChanged);

         }
      }

      protected void Page_Load(object sender, EventArgs e)
      {
         int iSDPID = 0;
         ddlAssociatedLeaseApps.Visible = false;
         Label1.Text = "Please select Site first.";

         if (!Page.IsPostBack && Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iSDPID))
         {
               Utility.ClearSubmitValidator();
               EditAbleField.Visible = true;
               ViewState["id"] = iSDPID;
               Control ctrl = PageUtility.FindControlRecursive(this.pnlSDPContainer, this.ctrSiteID.TextFieldClientID);
               if (ctrl != null)
               {
                  TextBox t = ctrl as TextBox;
                  site_data_packages objSDP = BLL.SiteDataPackage.Search(iSDPID);
                  CurrentSiteDataPackage = objSDP;
                  if (objSDP != null)
                  {
                     t.Text = objSDP.site_uid;
                     BindListBox(objSDP.site_uid);
                     Utility.ControlValueSetter(this.ddlAssociatedLeaseApps, objSDP.lease_application_id);

                     BindLookUpList();
                     BindSummaryData();
                     if (objSDP.lease_application_id.HasValue && LeaseApplication.Search(objSDP.lease_application_id.Value) != null  )
                     {
                           lblCustomer.Visible = false;
                           ddlCustomer.Visible = false;
                           lblSDPTMOAdminEmp.Visible = false;
                           ddlSDPTMOAdminEmp.Visible = false;
                           lblSDPTMOPMEmp.Visible = false;
                           ddlSDPTMOPMEmp.Visible = false;
                           lblSDPTMOSpecialistCurrentEmp.Visible = false;
                           ddlSDPTMOSpecialistCurrentEmp.Visible = false;
                           lblSpecialProject.Visible = false;
                           ddlSpecialProject.Visible = false;
                     }
                  }
               }

               btnSubmitCreateSite.Text = "Save";
         }
         else if (!Page.IsPostBack && Request.QueryString["laid"] != null && int.TryParse(Request.QueryString["laid"], out iSDPID))
         {
               Utility.ClearSubmitValidator();
               ViewState["laid"] = iSDPID;
               lease_applications objLA = LeaseApplication.Search(iSDPID);
               Control ctrl = PageUtility.FindControlRecursive(this.pnlSDPContainer, this.ctrSiteID.TextFieldClientID);
               if (ctrl != null)
               {
                  TextBox t = ctrl as TextBox;
                  t.Text = objLA.site_uid;
                  BindListBox(objLA.site_uid);
                  Utility.ControlValueSetter(this.ddlAssociatedLeaseApps, iSDPID);
               }
               divSDPInfo.Visible = true;
               EditAbleField.Visible = true;
               divNonLACreationFields_1.Visible = false;
               divNonLACreationFields_2.Visible = false;
               BindLookUpList(true);
         }
      }

      public void txtSiteID_TextChanged(object sender, EventArgs e)
      {
         Control ctrl = PageUtility.FindControlRecursive(this.pnlSDPContainer, this.ctrSiteID.TextFieldClientID);
         if (ctrl != null)
         {
               // Get text from 'txtSiteID'
               TextBox t = ctrl as TextBox;

               if (t.Text != null || t.Text != "")
               {
                  // Create string to save site_id
                  string site_id = t.Text.ToUpper();

                  // Check SitID to return LeaseApp, then assign to ListBox
                  BindListBox(site_id);
               }
         }
      }

      #region Binding
      protected void BindListBox(string site_id)
      {   
         List<AssociatedLeaseAppInfo> lstAssociatedLA = AssociatedLeaseApp.GetAssociatedLeaseApp(site_id);
         if (lstAssociatedLA.Count > 0)
         {
               ddlAssociatedLeaseApps.Visible = true;
               var sort_lstAssociatedLA = lstAssociatedLA.ToList();
               var dynamic_lstAssociatedLA = new DynamicComparer<AssociatedLeaseAppInfo>();
               dynamic_lstAssociatedLA.SortOrder(x => x.Descriptions);
               sort_lstAssociatedLA.Sort(dynamic_lstAssociatedLA);
               Utility.BindDDLDataSource(ddlAssociatedLeaseApps, sort_lstAssociatedLA, "id", "Descriptions", " Click to Select an Application ");
               Label1.Text = "";
         }
         else
         {
               Label1.Text = "Associated applications not found.";
               ddlAssociatedLeaseApps.Visible = false;
         }
      }
      protected void BindLookUpList(Boolean blnIsLACreation = false)
      {
         using (CPTTEntities ce = new CPTTEntities())
         {
               //Summary
             
               var sdpPriorities = ce.sdp_priorities.ToList();
               var sortPriorities = new DynamicComparer<sdp_priorities>();
               sortPriorities.SortOrder(x => x.sdp_priority);
               sdpPriorities.Sort(sortPriorities);
               Utility.BindDDLDataSource(this.ddlSDPPriority, sdpPriorities, "id", "sdp_priority", "- Select -");

               if (!blnIsLACreation)
               {

                  Utility.BindDDLDataSource(this.ddlCustomer,
                                    ce.customers.OrderBy(Utility.GetField<customer>("customer_name")).OrderBy(Utility.GetIntField<customer>("sort_order")),
                                    "id", "customer_name", "- Select -");

                  Utility.BindDDLDataSource(this.ddlSDPPriority, sdpPriorities, "id", "sdp_priority", null);
                   
                  var sdpSpecialProjects = ce.sdp_special_projects.ToList();
                  var sortSpecialProjects = new DynamicComparer<sdp_special_projects>();
                  sortSpecialProjects.SortOrder(x => x.sdp_special_project);
                  sdpSpecialProjects.Sort(sortSpecialProjects);
                  Utility.BindDDLDataSource(this.ddlSpecialProject, sdpSpecialProjects, "id", "sdp_special_project", "- Select -");

                  //Team Assigned
                  List<UserInfo> lstUser = BLL.User.GetUserInfo();
                  var sortUsers = new DynamicComparer<UserInfo>();
                  sortUsers.SortOrder(x => x.FullName);
                  lstUser.Sort(sortUsers);

                  Utility.BindDDLDataSource(this.ddlSDPAssignedToEMP, lstUser, "ID", "FullName", "- Select -");
                  Utility.BindDDLDataSource(this.ddlSDPTMOPMEmp, lstUser, "ID", "FullName", "- Select -");
                  Utility.BindDDLDataSource(this.ddlSDPTMOSpecialistCurrentEmp, lstUser, "ID", "FullName", "- Select -");
                  Utility.BindDDLDataSource(this.ddlSDPTMOAdminEmp, lstUser, "ID", "FullName", "- Select -");
               }
         }
      }
      protected void BindSummaryData()
      {
         using(CPTTEntities ce = new CPTTEntities())
         {
               site_data_packages objSDP = CurrentSiteDataPackage;
               if (objSDP != null)
               {
                  Utility.ControlValueSetter(this.ddlSDPPriority, objSDP.sdp_priority_id);
                  Utility.ControlValueSetter(this.ddlCustomer, objSDP.customer_id);
                  Utility.ControlValueSetter(this.ddlSpecialProject, objSDP.sdp_special_project_id);
                  Utility.ControlValueSetter(this.ddlSDPAssignedToEMP, objSDP.sdp_assigned_to_emp_id);
                  Utility.ControlValueSetter(this.ddlSDPTMOPMEmp, objSDP.tmo_pm_emp_id);
                  Utility.ControlValueSetter(this.ddlSDPTMOSpecialistCurrentEmp, objSDP.tmo_specialist_current_emp_id);
                  Utility.ControlValueSetter(this.ddlSDPTMOAdminEmp, objSDP.tmo_admin_emp_id);
               }
         }
      }
      #endregion
        

      public void ResetForm()
      {
      }

      protected void btnSubmitCreateSite_Click(object sender, EventArgs e)
      {            
         site_data_packages objSDP = null;
         string status;
         string mode = btnSubmitCreateSite.Text == "Save" ? "edit" : "add";

         if (mode == "add")
         {
            objSDP =  new site_data_packages();
         }
         else
         {
            objSDP = CurrentSiteDataPackage;
         }

         Control ctrl = PageUtility.FindControlRecursive(this.pnlSDPContainer, this.ctrSiteID.TextFieldClientID);
         if (objSDP != null && ctrl != null)
         {
            TextBox t = ctrl as TextBox;
            if (BLL.Site.Search(t.Text) != null)
            {
               Utility.ClearErrorColorForTextbox(t);
               if (ViewState["laid"] != null)
               {
                     objSDP.sdp_request_date = Utility.PrepareDate(txtSDPRequestDate.Text);
                     objSDP.sdp_priority_id = Utility.PrepareInt(this.ddlSDPPriority.SelectedValue);
               }
               objSDP.site_uid = Utility.PrepareString(t.Text);

               objSDP.lease_application_id = Utility.PrepareInt(this.ddlAssociatedLeaseApps.SelectedValue);

               if (btnSubmitCreateSite.Text != "Save")
               {
                  status = BLL.SiteDataPackage.Add(objSDP);
               }
               else
               {
                  objSDP.sdp_priority_id = Utility.PrepareInt(this.ddlSDPPriority.SelectedValue);
                  objSDP.customer_id = Utility.PrepareInt(this.ddlCustomer.SelectedValue);
                  objSDP.sdp_assigned_to_emp_id = Utility.PrepareInt(this.ddlSDPAssignedToEMP.SelectedValue);
                  objSDP.tmo_pm_emp_id = Utility.PrepareInt(this.ddlSDPTMOPMEmp.SelectedValue);
                  objSDP.tmo_specialist_current_emp_id = Utility.PrepareInt(this.ddlSDPTMOSpecialistCurrentEmp.SelectedValue);
                  objSDP.tmo_admin_emp_id = Utility.PrepareInt(this.ddlSDPTMOAdminEmp.SelectedValue);
                  objSDP.sdp_special_project_id = Utility.PrepareInt(this.ddlSpecialProject.SelectedValue);

                  status = BLL.SiteDataPackage.Update(objSDP, null);
               }

               // A null or empty status indicates success.
               if (String.IsNullOrEmpty(status))
               {
                  // Redirect to edit page with created id.
                  if (mode == "add")
                  {
                     Master.StatusBox.showStatusMessage("Site Data Package added successfully.");
                  }
                  else
                  {
                     Master.StatusBox.showStatusMessage("Site Data Package updated successfully.");
                  }
                     
                  ClientScript.RegisterStartupScript(GetType(), "Load",
                  String.Format("<script type='text/javascript'>" +
                              "   $(\"#MainContent_btnSubmitCreateSite\").val(\"Processing...\");" +
                              "   $(\"#MainContent_btnSubmitCreateSite\").attr(\"disabled\", true);" +
                              "   window.parent.location.href = 'Edit.aspx?id={0}';" +
                              "</script>",
                  objSDP.id.ToString()));
               }
               else
               {
                  Master.StatusBox.showErrorMessage(status);
               }
            }
            else
            {
               //Invalid site UID
               Master.StatusBox.showStatusMessage(Globals.ERROR_TXT_INVALID_UID);
               Utility.SettingErrorColorForTextbox(t);
            }                    
         }
      }        
   }
}