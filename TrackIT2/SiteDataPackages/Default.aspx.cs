﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Collections.Specialized;
using System.Web.Script.Serialization;
using System.IO;
using TrackIT2.Handlers;
using System.ComponentModel;

namespace TrackIT2.SiteDataPackages
{
    /// <summary>
    /// Default List page for SiteDataPackages
    /// </summary>
    public partial class _Default : System.Web.UI.Page
    {
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLookupLists();
                PageUtility.SetupSearchResultFilters(this, panelFilterBox);
                this.SetupFlexiGrid();

                // Clear Session
                DefaultFilterSession.ClearSession(HttpContext.Current, DefaultFilterSession.FilterType.SdpFilter);
            }
        }
        //-->

        /// <summary>
        /// Bind Lookup Lists
        /// </summary>
        protected void BindLookupLists()
        {

            CPTTEntities ce = new CPTTEntities();

            // Region Name
            var regions = ce.sites.Select(x => new { region_name = x.region_name }).Distinct().ToList();
            regions.Sort((x,y) => new AlphanumComparatorFast().Compare(x.region_name,y.region_name));
            Utility.BindDDLDataSource(this.lsbSiteRegionName, regions, "region_name", "region_name", null);

            // Site Market Code
            var marketCodes = ce.sites.Select(x => new { market_code = x.market_code }).Distinct().ToList();
            marketCodes.Sort((x, y) => new AlphanumComparatorFast().Compare(x.market_code, y.market_code));
            Utility.BindDDLDataSource(this.lsbSiteMarketCode, marketCodes, "market_code", "market_code", null);

            // SDP Priority
            var sdpPriorities = ce.sdp_priorities.ToList();
            var sortPriorities = new DynamicComparer<sdp_priorities>();
            sortPriorities.SortOrder(x => x.sdp_priority);
            sdpPriorities.Sort(sortPriorities);
            Utility.BindDDLDataSource(this.lsbSDPPriority, sdpPriorities, "id", "sdp_priority", null);

            //// SDP Request Type
            //var sdpRequestTypes = ce.sdp_request_types.OrderBy(Utility.GetField<sdp_request_types>("sdp_request_type"));
            //Utility.BindDDLDataSource(this.ddlSDPRequestType, sdpRequestTypes, "id", "sdp_request_type", "- Select -");

            // Special Project Type
            var sdpSpecialProjectType = ce.sdp_special_projects.ToList();
            var sortSpecialProjectType = new DynamicComparer<sdp_special_projects>();
            sortSpecialProjectType.SortOrder(x => x.sdp_special_project);
            sdpSpecialProjectType.Sort(sortSpecialProjectType);
            Utility.BindDDLDataSource(this.lsbSDPSpecialProjectType, sdpSpecialProjectType, "id", "sdp_special_project", null);

            // SDP TMO PM Emp
            List<UserInfo> employees = BLL.User.GetUserInfo();
            var sortEmployees = new DynamicComparer<UserInfo>();
            sortEmployees.SortOrder(x => x.FullName);
            employees.Sort(sortEmployees);
            Utility.BindDDLDataSource(this.lsbSDPTMOPMEmp, employees, "ID", "FullName", null);

            // SDP TMO Specialist Current Emp
            Utility.BindDDLDataSource(this.lsbSDPTMOSpecialistCurrentEmp, employees, "ID", "FullName", null);

            // Advance Search 
            BindAdvanceSearch();
        }
        //-->

        /// <summary>
        /// Setup Flexi Grid
        /// </summary>
        protected void SetupFlexiGrid()
        {
            fgPackages.SortColumn = fgPackages.Columns.Find(col => col.Code == "site_uid");
            fgPackages.SortDirection = FlexigridASPNET.GridSortOrder.Asc;
            fgPackages.RowPerPageOptions = new int[] { 10, 20, 30, 40, 50 };
            fgPackages.NoItemMessage = "No Data";
        }
        //-->

        protected void BindAdvanceSearch()
        {
            ddlCriteria.Items.Clear();
            AdvanceSearch.SiteDatePackageFilter advSearch = new AdvanceSearch.SiteDatePackageFilter();

            List<ListItem> criteria = new List<ListItem>();
            foreach (AdvanceSearch.CritereaInfo item in advSearch.LstCriterea)
            {
                criteria.Add(new ListItem(item.NameInDDL, item.GetDDLValue()));
            }
            var sort_criteria = criteria;
            sort_criteria.Sort((obj1, obj2) => new AlphanumComparatorFast().Compare(obj1.Text, obj2.Text));
            ddlCriteria.Items.AddRange(sort_criteria.Cast<ListItem>().ToArray());

        }

        public void ExportClick(object s, EventArgs e)
        {
            try
            {
                ExportFile ef = new ExportFile();
                var searchData = hiddenSearchValue.Value;
                NameValueCollection data = new NameValueCollection();
                if (!string.IsNullOrEmpty(searchData))
                {
                    var jss = new JavaScriptSerializer();
                    var table = jss.Deserialize<dynamic>(searchData);

                    for (int x = 0; x < table.Length; x++)
                    {
                        data.Add(table[x]["name"], table[x]["value"]);
                    }
                }

                const int startIndex = 4;
                var parameters = new NameValueCollection();

                // If we have form values, extract them to build the object query.
                if (data.Count > startIndex)
                {
                   for (var i = startIndex; i < data.Count; i++)
                   {
                      var itemKey = data.GetKey(i);
                      var itemValue = data.Get(i);

                      parameters.Add(itemKey, itemValue);
                   }
                }

                NameValueCollection columnValue = ExportFile.GetColumn(hiddenColumnValue.Value);
                ExportFileHandler extHand;
                extHand = new ExportFileHandler(ExportFile.eExportType.eSiteDataPackage, data, columnValue, parameters);
                extHand.Load_Data_OnComplete += new ExportFileHandler.Load_Data_OnComplete_Handler(Load_Data_OnComplete);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        protected void Load_Data_OnComplete(List<ExportClass> loadDataResult, NameValueCollection data, NameValueCollection column, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    ExportFile ef = new ExportFile();
                    MemoryStream mem = ef.WriteReport(loadDataResult, column, ExportFile.eExportType.eSiteDataPackage, Server.MapPath("/"));

                    if (mem != null)
                    {
                        string fileName = ExportFile.GenarateFileName(data["fileName"]);
                        DownloadFile(mem, fileName);
                    }
                    else
                    {
                        Master.StatusBox.showStatusMessage("Export report error.");
                    }
                }
                else
                {
                    throw e.Error;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }

        private void DownloadFile(MemoryStream mstream, string reportName)
        {
            try
            {
                byte[] byteArray = mstream.ToArray();
                Response.Clear();

                if (Response.Cookies.Count > 0)
                {
                    bool flag = true;
                    foreach (string c in Response.Cookies)
                    {
                        if (c == "fileDownloadToken")
                        {
                            Response.Cookies["fileDownloadToken"].Value = hiddenDownloadFlag.Value;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                    }
                }
                else
                {
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", hiddenDownloadFlag.Value));
                }
                Response.AddHeader("Content-Disposition", "attachment; filename=" + reportName + ".xls");
                Response.AddHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/vnd.msexcel";
                Response.BinaryWrite(byteArray);
                Response.Flush();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                Master.StatusBox.showStatusMessage("Export report error.");
            }
        }
    }
}