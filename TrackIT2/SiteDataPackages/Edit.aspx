﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Edit Site Data Package | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TrackIT2.SiteDataPackage.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Styles -->
   <%: Styles.Render("~/Styles/site_data_package_edit") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/site_data_package_edit") %>
   

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(function () {
            var tab_index = '<%= Session["cur_tab_index"] %>';
            // Select Tabs	
            $(function () {
                $("#tabs2").tabs({ selected: parseInt(tab_index) });
            });

            upadateShowDocument();
            // Set the modal size
            setModalSize(600, 400, 640);

            // Setup List Create Modal
            setupListCreateModal("Edit Package", window.location.toString().substr(window.location.toString().indexOf('=') + 1));

            var id = window.location.toString().substr(window.location.toString().indexOf('=') + 1);

            //Setup Document link Dialog
            setupDocumentLinkDialog("Document Links");

            // Comments
            var prmComments = Sys.WebForms.PageRequestManager.getInstance();
            prmComments.add_pageLoaded(setupCommentsLocal);
            //Create Link document for all comments
            CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');

            jumpToSelectedSdpTab();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandlerSDP)

            // Setup Upload Document Modal
            setupDmsModal("Upload Document");
        });
        //-->


        function EndRequestHandlerSDP() {
            CreateCommentLinkDocument('MainContent_hidden_document_all_document_comments', 'document-comment-');
        }

        // Setup Comments Local
        function setupCommentsLocal() {
            setupComments();
            setShowCommentBox();
        }

        function setShowCommentBox() {
            var mainCommentCount = $("#MainContent_hidden_count_main_comment").val();
            if (parseInt(mainCommentCount) == 0 || $("#MainContent_divComment1").length == 0) {
                showCommentBox();
            }
        }
        //-->

    </script>

    <asp:Panel ID="pnlNoID" runat="server" CssClass="align-center">
      <h1>Not Found (404)</h1>

      <p>The site data package specified is invalid or no longer exists.</p>

      <p>&nbsp;</p>

      <p>
         <a href="Default.aspx">&laquo; Go back to list page.</a>
      </p>

      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </asp:Panel>

    <asp:Panel ID="pnlSectionContent" runat="server">        
       <div class="section content">
           <div class="wrapper">
               <div class="action-tab">
                    <div class="left-side">
                        <asp:HyperLink ID="backToList" runat="server" Text="« Return to List" NavigateUrl="~/SiteDataPackages/" CssClass="site-link-no-underline"></asp:HyperLink>
                    </div>
                    <div class="right-side">
                        <asp:LinkButton ID="btnSdpDelete" CssClass="link-button site-link-no-underline" runat="server" ClientIDMode="Static" OnClientClick="return openDeleteAppButtonPopup('Site Data Package',event);" onclick="btnSdpDelete_Click">Delete</asp:LinkButton>
                    </div>
               </div>
               <div class="application-information">
                   <h1>
                       Site Data Package <span>
                           <asp:HyperLink ID="lblSiteID" runat="server" Text="" CssClass="h1-span-site-link site-link-no-underline"></asp:HyperLink>
                           /
                           <asp:Label ID="lblSiteName" runat="server" Text=""></asp:Label>
                           /
                           <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                       </span>
                   </h1>
                   <div id="tabs" class="toggle-me">
                       <ul class="tabs">
                           <li><a href="#general-information">Related Leases and Tower</a></li>
                       </ul>
                       <div id="general-information">
                           <div style="padding: 20px;">
                               <div class="i-1-3">
                                   <div class="general-information-column">
                                       <div style="float: left;">
                                           <asp:GridView ID="grdMain" CssClass="related-apps TableGenInfo" runat="server" AutoGenerateColumns="False">
                                               <Columns>
                                                   <asp:BoundField HeaderText="Site ID" DataField="SiteID" />
                                                   <asp:BoundField HeaderText="Customer" DataField="CustomerName" />
                                                   <asp:BoundField HeaderText="App Received" DataField="LAReceived" DataFormatString="{0:MM/dd/yyyy}"
                                                       HtmlEncode="false" />
                                                   <asp:HyperLinkField DataNavigateUrlFields="Link" Text="Go" />
                                               </Columns>
                                           </asp:GridView>
                                           <div id="divNoRelatedLeaseApps" class="related-apps TableGenInfo" style="margin-right: 20px;width: 420px;
                                               background: #f0f0f0;" runat="server">
                                               <div style="margin: 0 auto 0 auto; width: 200px; padding: 30px 0 0 0; text-align: center;
                                                   color: #888;">
                                                   <em>
                                                       <asp:Label ID="lblNoLeaseAppStatus" runat="server">No Associated Lease Application</asp:Label>
                                                   </em>
                                               </div>
                                           </div>
                                       </div>
                                       <ul class="information-list" style="width: 180px; margin-bottom: 10px;">
                                           <li>Tower Address</li>
                                           <li>
                                               <asp:HyperLink ID="mybtn" Target="_blank" runat="server">
                                               </asp:HyperLink>
                                               <asp:Label ID="lblAddressNA" runat="server" Visible="false"></asp:Label>
                                           </li>
                                           <li>&nbsp;</li>
                                           <li>Latitude, Longitude:
                                              <br />
                                              <asp:HyperLink ID="lblSiteLatLong" Text="" Target="_blank" runat="server">
                                                  <asp:Label ID="lblDegreeLatitude" runat="server" ></asp:Label>,&nbsp;<asp:Label ID="lblDegreeLongtitude" runat="server"></asp:Label><br/>
                                                  <asp:Label ID="lblSiteLatitude" runat="server" Text="Label"></asp:Label>,&nbsp;<asp:Label ID="lblSiteLongitude" runat="server" Text="Label"></asp:Label>
                                              </asp:HyperLink>
                                           </li>
                                       </ul>
                                   </div>
                                   <div class="cb"></div>
                                   <div class="general-information-column">
                                       <ul class="information-list" style="width: 180px;">
                                           <li>Summary</li>
                                           <li>Site ID:
                                               <asp:Label ID="lblSummarySiteID" runat="server"></asp:Label></li>
                                           <li>SDP Priority:
                                               <asp:Label ID="lblSummarySDPPriority" runat="server"></asp:Label></li>
                                           <li>
                                               <asp:Label ID="lblSummaryCustomerFront" runat="server">Customer:</asp:Label>
                                               <asp:Label ID="lblSummaryCustomer" runat="server"></asp:Label></li>
                                           <li>
                                               <asp:Label ID="lblSpecialProjectFront" runat="server">Sp Project:</asp:Label>
                                               <asp:Label ID="lblSpecialProject" runat="server"></asp:Label></li>
                                           <li>
                                               <asp:Label ID="lblLeaseAppTypeFront" runat="server" Visible="false">LA Type:</asp:Label>    
                                               <asp:Label ID="lblLeaseAppType" runat="server" Visible="false"></asp:Label>
                                           </li>
                                           <li>
                                               <asp:Label ID="lblLeaseAppActivityFront" runat="server" Visible="false">LA Activity:</asp:Label>    
                                               <asp:Label ID="lblLeaseAppActivity" runat="server" Visible="false"></asp:Label>
                                           </li>
                                       </ul>
                                       <ul class="information-list" style="width: 260px;">
                                           <li>Team Assigned</li>
                                           <li>Assigned: <asp:Label ID="lblSDPAssignedToEmp" runat="server"></asp:Label></li>
                                           <li>PM: <asp:Label ID="lblSDPTMOPMEmp" runat="server"></asp:Label></li>
                                           <li>Specialist Current: <asp:Label ID="lblSDPTMOSpecialistCurrentEmp" runat="server"></asp:Label></li>
                                           <li>Admin Emp: <asp:Label ID="lblSDPTMOAdminEmp" runat="server"></asp:Label></li>
                                       </ul>
                                       <ul class="information-list" style="width: 180px; margin-bottom: 10px;">
                                           <li>Structure Details</li>
                                           <li>Type:
                                               <asp:Label ID="lblType" runat="server" Text=""></asp:Label></li>
                                           <li>Height:
                                               <asp:Label ID="lblHeight" runat="server" Text=""></asp:Label></li>
                                           <li>Rogue Equip:
                                               <asp:Label ID="lblRogueEquip" runat="server" Text=""></asp:Label></li>
                                           <li>Status:
                                               <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                               <asp:Image ID="imgStructureDetails_Construction" runat="server" AlternateText="Construction" CssClass="construction-img" /></li>
                                       </ul>
                                   </div>
                                   <div class="cb"></div>
                               </div>
                               <!-- .i-2-3 -->
                               <div class="i-1-3">
                                   <div class="map">
                                       <asp:Image ID="Image2" Width="310" Height="235" frameborder="0" scrolling="no" marginheight="0"
                                           runat="server" ImageUrl="" AlternateText="" />
                                   </div>
                                   <!-- .map -->
                               </div>
                               <!-- .i-1-3 -->
                               <div class="cb">
                               </div>                            
                               <a id="modalBtnExternal" class="edit-button" href="javascript:void(0)">edit</a>
                               <!-- Modal -->
                               <div id="modalExternal" class="modal-container" style="overflow: hidden;">
                               <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
                               </div>  
                               <asp:HiddenField ID="hidDocumentDownload" runat="server" ClientIDMode="Static" /> 
                               <div class="cb">
                               </div>
                           </div>
                           <!-- padding -->
                       </div>
                       <!-- #general-information -->
                   </div>
                   <!-- #tabs -->
                   <div class="cb">
                   </div>
               </div>
               <!-- .application-information -->
               <div class="application-process" style="float: left;">
                   <h1>
                       Site Data Package Process</h1>
                   <div id="tabs2">
                       <ul class="tabs">
                           <li><a href="#general">General</a></li>
                           <li><a href="#document-statuses">Document Status</a></li>
                           <li><a href="#abstract">Abstract</a></li>
                       </ul>
                       <div id="general">
                           <div style="padding: 20px;">
                           <div id="error_general"></div>
                               <!--Document Link-->
                               <div class="document-link" id="SDPCheckList"  >
                               <label>
                                       SDP Checklist</label>
                                   <asp:HiddenField  ID="hidden_document_SDPCheckList" ClientIDMode="Static" runat="server" />
                               </div>

                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitSDP_General2" runat="server" style="float:right" 
                                               Text="Save" 
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                               OnClick="btnSubmitSDP_General_Click"
                                               OnClientClick="if (!generalBeforeSave()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="left-col">
                                   <h4>
                                       Process Dates</h4>
                                   <label>
                                       SDP Request</label>
                                   <asp:TextBox ID="txtSDP_RequestDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       SDP Due By</label>
                                   <asp:TextBox ID="txtSDP_DueDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       SDP Due Date Notes</label>
                                   <asp:TextBox ID="txtSDP_DueDateNotes" CssClass="txtareaComment" TextMode="MultiLine" Rows="2" Columns="20" runat="server"></asp:TextBox>

                                   <label>
                                       First Sweep Completed</label>
                                   <asp:TextBox ID="txtFirstSweepDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       SDP Notice Of Completion</label>
                                   <asp:TextBox ID="txtSDP_NoticeOfCompletionDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       SDP Completed</label>
                                   <asp:TextBox ID="txtSDP_CompletedDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <h4>
                                       Market Notice</h4>
                                       <!--Document Link-->
                                    <div class="document-link" id="SDPFirstNoticeToMarket"  >
                                        <label>
                                            First Notice to Market</label>
                                            <asp:TextBox ID="txtFirstMarketDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                            <asp:HiddenField  ID="hidden_document_SDPFirstNoticeToMarket" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <!--Document Link-->
                                    <div class="document-link" id="SDPSecondNoticeToMarket"  >
                                        <label>
                                            Second Notice to Market</label>
                                        <asp:TextBox ID="txtSecondMarketDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                        <asp:HiddenField  ID="hidden_document_SDPSecondNoticeToMarket" ClientIDMode="Static" runat="server" />
                                   </div>
                                   <!--Document Link-->
                                   <div class="document-link" id="SDPThirdNoticeToMarket"  >
                                       <label>
                                           Third Notice to Market</label>
                                       <asp:TextBox ID="txtThirdMarketDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                       <asp:HiddenField  ID="hidden_document_SDPThirdNoticeToMarket" ClientIDMode="Static" runat="server" />
                                   </div>
                               </div>
                               <div class="right-col">
                                   <h4>
                                       Escalation and Doc Replacement</h4>
                                   <label>
                                       SDP Escalated By Emp</label>
                                   <asp:DropDownList ID="ddlSDP_EscalatedByEmp" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Escalation</label>
                                   <asp:TextBox ID="txtEscalationDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       SDP Escalation Completed by Emp</label>
                                   <asp:DropDownList ID="ddlSDP_EscalationCompletedByEmp" CssClass="select-field" runat="server">
                                   </asp:DropDownList>
                                   <label>
                                       Escalation Completed</label>
                                   <asp:TextBox ID="txtEscalationCompletedDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       Vendor Order Received</label>
                                   <asp:TextBox ID="txtVendorOrderReceived" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       Vendor Order PO Received</label>
                                   <asp:TextBox ID="txtVendorOrderPOReceived" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <h4>
                                       Key InSite</h4>
                                   <label>
                                       Hard Cost Approval</label>
                                   <asp:TextBox ID="txtCostApprovalDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       Prime Lease Exec</label>
                                   <asp:TextBox ID="txtPrimeLeaseExecDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       Permit Applied</label>
                                   <asp:TextBox ID="txtPermitAppliedDate" CssClass="datepicker" runat="server"></asp:TextBox>
                                   <label>
                                       Construction Start</label>
                                   <asp:TextBox ID="txtConstructionStartDate" CssClass="datepicker" runat="server"></asp:TextBox>
                               </div>
                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitSDP_General" runat="server" 
                                               Text="Save"
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"
                                               OnClick="btnSubmitSDP_General_Click"
                                               OnClientClick="if (!generalBeforeSave()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="cb">
                               </div>
                           </div>
                           <!-- padding -->
                       </div>
                       <!-- #general -->
                       <div id="document-statuses">
                           <div style="padding: 20px;">
                               <div class="document-link" id="AdditionalPrimeDocuments"  >
                                   <label style="width: 157px;">
                                           Additional Prime Documents</label>
                                       <asp:HiddenField  ID="hidden_document_AdditionalPrimeDocuments" ClientIDMode="Static" runat="server" />
                               </div>

                               <div class="full-col align-right">
                                    <asp:UpdatePanel ID="updateShowDocument" runat="server" style="float: left;margin-left:460px" >
                                        <ContentTemplate>
                                                <div class="onoffswitch">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" >
                                                </div>
                                                <asp:CheckBox ID="chkShowDocument" name="onoffswitch"  runat="server" style="display:none"  AutoPostBack="true" EnableViewState="true" Enabled="false"
                                                OnCheckedChanged="chkShowDocument_CheckedChanged" />                                             
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                   <asp:Button ID="btnSubmitDocStatus2" runat="server"
                                               Text="Save"
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all" 
                                               OnClick="btnSubmitDocStatus_Click"
                                               OnClientClick="if (!documentStatusBeforeSave()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="left-col">
                                   <h4>
                                       Document Status</h4>
                                   <!--Document Link-->
                                   <div class="document-link" id="TowerDrawing_DocTypes"  >
                                       <label>
                                           Tower Drawing Doc Types</label>
                                       <%--<asp:ListBox ID="lstTowerDrawing_DocTypes" runat="server" SelectionMode="Multiple"
                                           ToolTip="Click to Select a reason"></asp:ListBox>--%>
                                        <asp:HiddenField  ID="hidden_document_TowerDrawing_DocTypes" ClientIDMode="Static" runat="server" />
                                   </div>
                                   <asp:ListBox ID="lstTowerDrawing_DocTypes" runat="server" SelectionMode="Multiple"
                                           ToolTip="Click to Select a reason"></asp:ListBox>

                                   <!--Document Link-->
                                   <div class="document-link" id="TowerDrawing_DocStatus" > 
                                       <label>Tower Drawing Doc Status</label>
                                       <asp:DropDownList ID="ddlTowerDrawing_DocStatus" CssClass="select-field" runat="server" Width="180"></asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_TowerDrawing_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                  
                                   <!--Document Link-->
                                   <div class="document-link" id="TowerTagPhoto_DocStatus"  > 
                                       <label>Tower Tag Photo Doc Status</label>
                                       <asp:DropDownList ID="ddlTowerTagPhoto_DocStatus" CssClass="select-field" runat="server" Width="180"></asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_TowerTagPhoto_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="StructuralCalcs_DocStatus" >
                                       <label>
                                           Structural Calcs Doc Status</label>
                                       <asp:DropDownList ID="ddlStructuralCalcs_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_StructuralCalcs_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>

                                   <!--Document Link-->
                                   <div class="document-link" id="StructuralAnalysis_DocStatus"  >
                                       <label>
                                           Structural Analysis Current Doc Status</label>
                                       <asp:DropDownList ID="ddlStructuralAnalysis_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_StructuralAnalysis_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   

                                   <!--Document Link-->
                                   <div class="document-link" id="TowerErectionDetail_DocStatus" >
                                       <label>
                                           Tower Erection Detail Doc Status</label>
                                       <asp:DropDownList ID="ddlTowerErectionDetail_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_TowerErectionDetail_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>

                                   <!--Document Link-->
                                   <div class="document-link" id="FoundationDesign_DocStatus"  >
                                       <label>
                                           Foundation Design Doc Status</label>
                                       <asp:DropDownList ID="ddlFoundationDesign_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_FoundationDesign_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   

                                   <!--Document Link-->
                                   <div class="document-link" id="ConstructionDrawings_DocStatus" >
                                       <label>
                                           Construction Drawings Doc Status</label>
                                       <asp:DropDownList ID="ddlConstructionDrawings_DocStatus" CssClass="select-field"
                                           runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_ConstructionDrawings_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>

                                   <!--Document Link-->
                                   <div class="document-link" id="GeotechReport_DocStatus"  >
                                       <label>
                                           Geotech Report Doc Status</label>
                                       <asp:DropDownList ID="ddlGeotechReport_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_GeotechReport_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>

                                   <!--Document Link-->
                                   <div class="document-link" id="BuildingPermit_DocStatus"  >
                                       <label>
                                           Building Permit Doc Status</label>
                                       <asp:DropDownList ID="ddlBuildingPermit_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_BuildingPermit_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                               </div>
                               <div class="right-col">
                               <!--Document Link-->
                                   <div class="document-link" id="ZoningApproval_DocStatus"  >
                                       <label>
                                           Zoning Approval Doc Status</label>
                                       <asp:DropDownList ID="ddlZoningApproval_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_ZoningApproval_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="PhaseI_DocStatus" >
                                       <label>
                                           Phase I Doc Status</label>
                                       <asp:DropDownList ID="ddlPhaseI_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_PhaseI_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="PhaseII_DocStatus" >
                                       <label>
                                           Phase II Doc Status</label>
                                       <asp:DropDownList ID="ddlPhaseII_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_PhaseII_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="NEPA_DocStatus"  >
                                       <label>
                                           NEPA Doc Status</label>
                                       <asp:DropDownList ID="ddlNEPA_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_NEPA_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="SHPO_DocStatus"  >
                                       <label>
                                           SHPO Doc Status</label>
                                       <asp:DropDownList ID="ddlSHPO_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_SHPO_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="GPS1a2c_DocStatus" >
                                       <label>
                                           GPS 1a2c Doc Status</label>
                                       <asp:DropDownList ID="ddlGPS1a2c_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_GPS1a2c_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="Airspace_DocStatus" >
                                       <label>
                                           Airspace Doc Status</label>
                                       <asp:DropDownList ID="ddlAirspace_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_Airspace_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="FAAStudyDet_DocStatus" >
                                       <label>
                                           FAA Study Determination Doc Status</label>
                                       <asp:DropDownList ID="ddlFAAStudyDet_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_FAAStudyDet_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="FCC_DocStatus" >
                                       <label>
                                           FCC Doc Status</label>
                                       <asp:DropDownList ID="ddlFCC_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_FCC_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="AMCertification_DocStatus" >
                                       <label>
                                           AMCertification Doc Status</label>
                                       <asp:DropDownList ID="ddlAMCertification_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_AMCertification_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="Towair_DocStatus" >
                                       <label>
                                           Towair Document Status</label>
                                       <asp:DropDownList ID="ddlTowair_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_Towair_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="PrimeLease_DocStatus" >
                                       <label>
                                           Prime Lease Doc Status</label>
                                       <asp:DropDownList ID="ddlPrimeLease_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_PrimeLease_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="Memorandum_DocStatus" >
                                       <label>
                                           Memorandum of Lease Doc Status</label>
                                       <asp:DropDownList ID="ddlMemorandum_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_Memorandum_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                                   <!--Document Link-->
                                   <div class="document-link" id="Title_DocStatus" >
                                       <label>
                                           Title Doc Status</label>
                                       <asp:DropDownList ID="ddlTitle_DocStatus" CssClass="select-field" runat="server" Width="180">
                                       </asp:DropDownList>
                                       <asp:HiddenField  ID="hidden_document_Title_DocStatus" ClientIDMode="Static" runat="server" />
                                   </div>
                                   
                               </div>
                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitDocStatus" runat="server" 
                                               Text="Save"
                                               CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all" 
                                               OnClick="btnSubmitDocStatus_Click"
                                               OnClientClick="if (!documentStatusBeforeSave()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="cb">
                               </div>
                           </div>
                           <!-- padding -->
                       </div>
                       <!-- #towermod-process -->
                       <div id="abstract">
                           <div style="padding: 20px;">
                               <div id="error_abstract"></div>
                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitAbstract2" runat="server"
                                               Text="Save"
                                               CssClass="ui-button ui-widget ui-state-default ui-corner-all"  
                                               OnClick="btnSubmitAbstract_Click" 
                                               OnClientClick="if (!abstractBeforeSave()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <h4>
                                   Abstract</h4>
                               <div class="left-col">
                                   <asp:Label ID="lbl_EmployeeAssigned" Text="Employee Assigned" runat="server" />
                                   <asp:DropDownList ID="ddl_EmployeeAssigned" CssClass="select-field" runat="server" />
                                   <br />
                                   <asp:Label ID="lbl_AbstractVendor" Text="Abstract Vendor" runat="server" />
                                   <asp:DropDownList ID="ddl_AbstractVendor" CssClass="select-field" runat="server" />
                                   <br />
                                   <asp:Label ID="lbl_Assigned" Text="Assigned" runat="server" />
                                   <asp:TextBox ID="txt_Assigned" CssClass="datepicker" runat="server" /><br />

                                   <!--Document Link-->
                                   <div class="document-link" id="AbstractCompleted" >
                                       <asp:Label ID="lbl_Completed" Text="Completed" runat="server" />
                                       <asp:TextBox ID="txt_Completed" CssClass="datepicker" runat="server" />
                                       <asp:HiddenField  ID="hidden_document_AbstractCompleted" ClientIDMode="Static" runat="server" />
                                   </div>
                                   <br />
                                   <asp:Label ID="lbl_AbstractNotes" Text="Abstract Note" runat="server" />
                                   <asp:TextBox ID="txt_AbstractNotes" runat="server" TextMode="MultiLine" Style="border: 1px solid #E1E1E1;
                                       width: 190px; height: 90px" /><br />
                               </div>
                               <div class="right-col">
                               </div>
                               <div class="full-col align-right">
                                   <asp:Button ID="btnSubmitAbstract" runat="server"
                                               Text="Save"
                                               CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all"  
                                               OnClick="btnSubmitAbstract_Click" 
                                               OnClientClick="if (!abstractBeforeSave()) {return false;}"
                                               UseSubmitBehavior="false" />
                               </div>
                               <div class="cb">
                               </div>
                           </div>
                           <!-- padding -->
                       </div>
                       <!-- #Abstract -->
                   </div>
                   <!-- #tabs2 -->
                   <div class="cb">
                   </div>
               </div>
               <!-- .application-comments -->
               <div class="application-comments">
                   <h2 id="h2-comments" style="width: 140px;">
                       Comments</h2>
                   <div class="comments-wrapper">
                       <asp:UpdatePanel ID="upComments" UpdateMode="Always" runat="server">
                           <ContentTemplate>
                               <div class="blah-wrapper" style="width: 100%; height: 0px; position: relative; overflow: hidden">
                                   <div style="position: absolute; top: -200px; width: 100%; height: 200px; background: none;"
                                       id="blah">
                                       <div class="new-comment" style="display: none;">
                                           <div class="balloon">
                                               <asp:TextBox ID="txtComments" Rows="2" Columns="5" CssClass="comment-textarea" runat="server"
                                                   TextMode="MultiLine"></asp:TextBox>
                                               <div class="balloon-bottom">
                                               </div>
                                           </div>
                                           <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                               <asp:Image ID="imgAuthor" Width="25" ImageUrl="/images/avatars/default.png" AlternateText="profile-picture"
                                                   runat="server" />
                                               <asp:Button ID="btnPost" ClientIDMode="Static" runat="server" CssClass="ui-button ui-widget ui-state-default ui-corner-all" OnClientClick="this.disabled = true; this.value = 'Processing...';showLoadingSpinner('h2-comments');" OnClick="btnPost_Click" Text="Post" UseSubmitBehavior="false"/>
                                               <asp:Button ID="btnCommentCancel" CssClass="ui-button ui-widget ui-state-default ui-corner-all comment-cancel" ClientIDMode="Static" runat="server" OnClientClick="return false;" Text="Cancel" />                                                                                       
                                           </div>
                                       </div>
                                   </div>
                                   <div class="clear-all">
                                   </div>
                               </div>
                               <div runat="server" id="divComment1" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment1">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment1" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter1">
                                           </p>
                                        <div id="pnlDocument1" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment1" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <div runat="server" id="divComment2" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment2">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment2" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter2">
                                           </p>
                                           <div id="pnlDocument2" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment2" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <div runat="server" id="divComment3" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment3">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment3" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter3">
                                           </p>
                                           <div id="pnlDocument3" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment3" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <div runat="server" id="divComment4" class="comment">
                                   <div class="balloon">
                                       <p runat="server" id="pComment4">
                                       </p>
                                       <div class="balloon-bottom">
                                       </div>
                                   </div>
                                   <div class="author-info" style="margin-top: 15px; font-size: 10pt; color: #a1a1a1;">
                                       <asp:Image ID="imgUserComment4" Width="25" ImageUrl="/images/avatars/default.png"
                                           ToolTip="profile-picture" runat="server" />
                                       <p runat="server" id="pCommenter4">
                                           </p>
                                           <div id="pnlDocument4" runat="server" style="width:100%;text-align:left;" ></div>
                                       <asp:ImageButton ID="btnDeleteComment4" ImageUrl="/images/icons/Erase-Greyscale.png"
                                           ToolTip="Delete Comment" Width="12" runat="server" OnClick="btnDeleteComment_Click"
                                           CssClass="btn-comment-delete" />
                                   </div>
                               </div>
                               <a class="add-new" href="javascript:void(0)">add new</a>
                               <%--<asp:LinkButton ID="btnPost" CssClass="post" NavigateUrl="javascript:void(0)" runat="server" OnClientClick="showLoadingSpinner('h2-comments');"
                                   OnClick="btnPost_Click">post</asp:LinkButton>--%>
                               <%--<a class="cancel" href="javascript:void(0)">cancel</a> --%>
                               <a class="fancy-button" style="display: inline-block;
                                   text-align: center; width: 88%; margin: 0 auto;" id="btnAllComment" runat="server">
                                   more comments</a>
                                   <asp:Button ID="btnAllRelateComment" OnClick="btnAllRelateComment_Click" OnClientClick="showLoadingSpinner('h2-comments');" ClientIDMode="Static" runat="server" CssClass="fancy-button AllReleateCommentButton" Text="Show related comments" />
                              <asp:HiddenField ID="hidden_count_main_comment" runat="server"/>
                              <asp:HiddenField ID="hidden_document_all_document_comments" runat="server"/>
                              <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
                              <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
                           </ContentTemplate>
                       </asp:UpdatePanel>
                     
                   </div>
                   <!-- .comments-wrapper -->
               </div>
               <div class="cb">
               </div>
           </div>
           <!-- .wrapper -->
       </div>
    </asp:Panel>
    <!-- .content -->
</asp:Content>
