﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Site Data Packages | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master"  Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.SiteDataPackages._Default" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/site_data_package") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/site_data_package") %>
   
    <script type="text/javascript" language="javascript">

        // Global Var using ddlSDPSpecialProjectType instead of ddlSDPRequestType
        //searchFilterFields = "txtSiteID,ddlSiteRegionName,ddlSiteMarketCode,ddlSDPPriority,ddlSDPSpecialProjectType,ddlSDPTMOPMEmp,ddlSDPTMOSpecialistCurrentEmp,txtSiteOnAirDateFrom,txtSiteOnAirDateTo,txtSDPRequestDateFrom,txtSDPRequestDateTo";
        searchFilterFields = "txtSiteID,lsbSiteRegionName,lsbSiteMarketCode,lsbSDPPriority,lsbSDPSpecialProjectType,lsbSDPTMOPMEmp,lsbSDPTMOSpecialistCurrentEmp,txtSiteOnAirDateFrom,txtSiteOnAirDateTo,txtSDPRequestDateFrom,txtSDPRequestDateTo";
        currentFlexiGridID = '<%= this.fgPackages.ClientID %>';

        // ADV Search Var
        var GETdatas = new Array();
        var blnHaveGET = false;
        //ADV Search + Global Search Added by Nam
        var blnExec = false;

        // Doc Ready
        $(function ()
        {

            // Get query string.(query string, out arrayGET data)
            blnHaveGET = GrabGETData(window.location.search.substring(1), GETdatas);
            // Load Advance Search
            if (typeof $("#ddlCriteria") != "undefined") {
                ADVSearchInit($("#ddlCriteria"), "/SiteDataPackages");
            }
            if (typeof GETdatas['filter'] == "string") {
                blnExec = LoadAdvanceSearch(GETdatas['filter'], searchFilterFields);
                blnExec = blnExec | CheckGSFilter(GETdatas['filter']);
            }
            else {
                //Added 2012/02/28 for checking global search
                if (typeof GETdatas['gs'] == "string") {
                    var txtGS = GETdatas['gs'];
                    if (txtGS != undefined && txtGS != null) {
                        setCurrentGS(txtGS);
                        blnExec = true;
                    }
                }
            }

            if (GETdatas['filter'] == '') {
                $("#hiddenFilterParam").val('true');
                blnExec = false
            }

            // Set the modal size
            setModalSize(600, 400, 330);

            // Setup List Create Modal
            setupListCreateModal("Create New SDP");

            // Setup Search Filter
            setupSearchFilter();

            // Setup Saved Filter Modal 
            setupSavedFilterModal(SavedFilterConstants.buttonCreateNew, SavedFilterConstants.listViews.sdp.name, SavedFilterConstants.listViews.sdp.url);
        });
        //-->

        // Flexigrid
        //
        // FG Before Send Data
        function fgBeforeSendData(data)
        {

            // Show - Loading Spinner
            $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

            // disable export button
            $("#MainContent_exportButton").val("Processing...");
            $("#MainContent_exportButton").attr("disabled", true);

            // Global Search
            var gs = $.urlParam(GlobalSearch.searchQueryString);
            //
            addGlobal:
            if (!String(gs).isNullOrEmpty())
            {
                for (i = 0; i < arrayOfFilters.length; i++)
                {
                    if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
                }
                addFilterPostParam(GlobalSearch.searchQueryString, gs);
            }
            //>

            // Set Default filter
            removeFilterPostParam(ConstGlobal.DEFAULT.returnPage);
            addFilterPostParam(ConstGlobal.DEFAULT.returnPage, savedFiltersQueryString);

            // Search Filter Data
            sendSearchFilterDataToHandler(data);
        }
        //
        // FG Data Load
        function fgDataLoad(sender, args)
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Enable export button
            $("#MainContent_exportButton").val("Export to Excel");
            $('#MainContent_exportButton').removeAttr("disabled");

        }
        //
        // FG Row Click
        function fgRowClick(sender, args)
        {

            return false;
        }
        //
        // FG Row Render
        function fgRowRender(tdCell, data, dataIdx)
        {

            var cVal = new String(data.cell[dataIdx]);

            if (cVal.isNullOrEmpty())
            {
                tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
            } else
            {
                // Type?
                switch (Number(dataIdx))
                {

                    // Date 
                    case 3:
                        tdCell.innerHTML = cVal;
                        break;

                    // Date  
                    case 5:
                        tdCell.innerHTML = cVal;
                        break;

                    // Standard String  
                    default:
                        tdCell.innerHTML = cVal;
                        break;
                }
                //>
            }
        }
        //
        // FG No Data
        function fgNoData()
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Disable export button
            $("#MainContent_exportButton").val("Export to Excel");
        }
        //-->


        // Window Load
        $(window).load(function () {

            // Apply Search Filter?
            if (blnExec) {

                // Wait For Last Field To Load
                var waitForLastFieldToLoadLimit = 0;
                var waitForLastFieldToLoad = setInterval(function () {
                    if (($('#hiddenLastFieldToCheckLoaded').val().length === 0) || (waitForLastFieldToLoadLimit === 50)) {
                        clearInterval(waitForLastFieldToLoad);

                        // Apply filters and reload the DataGrid
                        applySearchFilter(true);
                    }
                    waitForLastFieldToLoadLimit++;
                }, 100);
                //>

            } else {
                // Normal DataGrid Load
                if ($('.filter-tag').length > 0 || $("#hiddenFilterParam").val() == "true") {
                    reloadDataGrid();
                }
                else {
                // Skip Normal load and show filter
                    $('.filter-box').slideToggle();
                    $("#MainContent_exportButton").attr("disabled", true);
                }
            }
        });
        //-->
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 id="h1" style="float:left; padding-top:3px; padding-right:30px;">Site Data Packages</h1>
	        <div class="page-options-nav">
                <a class="fancy-button clear-filter-button" href="javascript:void(0)">Clear Filters</a>
                <a class="fancy-button filter-button arrow-down" href="javascript:void(0)">Filter Results<span class="arrow-down-icon"></span></a> 
                <a id="modalBtnExternalSaveFilter" class="fancy-button save-filter-button" href="javascript:void(0)">Save Filter</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
		        <a id="modalBtnExternal" class="fancy-button" href="javascript:void(0)">Create New SDP</a>
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->

    <!-- Search Filter Box -->
    <div class="filter-box">
	    <div style="padding:20px">
            <div class="filter-button-close"><a class="fancy-button close-filter" style="padding:2px 6px 2px 6px;" href="javascript:void(0)">x</a></div>
                <asp:Panel ID="panelFilterBox" runat="server">
                    <div class="left-col">

                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                            LabelFieldClientID="lblSiteID"
                            LabelFieldValue="Site ID"
                            LabelFieldCssClass="label"
                            TextFieldClientID="txtSiteID"
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188"
                            ScriptKeyName="ScriptAutoCompleteSiteID"
                            DataSourceUrl="SiteIdAutoComplete.axd"
                            ClientIDMode="Static"
                            runat="server" />
		                <br />

                        <label>Site Region Name</label>
                        <%--<asp:DropDownList ID="ddlSiteRegionName" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="Site Region Name 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="Site Region Name 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divSiteRegionName" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSiteRegionName" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
		
		                <label>Site Market Code</label>
                        <%--<asp:DropDownList ID="ddlSiteMarketCode" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="Site Market Code 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="Site Market Code 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divSiteMarketCode" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSiteMarketCode" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
		
		                <label>SDP Priority</label>
                        <%--<asp:DropDownList ID="ddlSDPPriority" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="SDP Priority 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="SDP Priority 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divSDPPriority" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSDPPriority" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>Special Project Type</label>
                        <%--<asp:DropDownList ID="ddlSDPSpecialProjectType" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="SDP Request Type 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="SDP Request Type 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divSDPSpecialProjectType" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSDPSpecialProjectType" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>TMO PM Emp</label>
                        <%--<asp:DropDownList ID="ddlSDPTMOPMEmp" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="SDP TMO PM Emp 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="SDP TMO PM Emp 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divSDPTMOPMEmp" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSDPTMOPMEmp" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                        <label>TMO Spec Curr Emp</label>
                        <%--<asp:DropDownList ID="ddlSDPTMOSpecialistCurrentEmp" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                            <asp:ListItem Text="SDP TMO Specialist Current Emp 1" Value="item1"></asp:ListItem>
                            <asp:ListItem Text="SDP TMO Specialist Current Emp 2" Value="item2"></asp:ListItem>
                        </asp:DropDownList>--%>
                        <div id="divSDPTMOSpecialistCurrentEmp" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbSDPTMOSpecialistCurrentEmp" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />
                    </div>

		            <div class="right-col">
		                <label>Site On Air Date</label>
		                <ul class="field-list">
                          <li>
                             <asp:TextBox ID="txtSiteOnAirDateFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valSiteOnAirDateFrom" runat="server"
                                                   ControlToValidate="txtSiteOnAirDateFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
		                    <li>&nbsp;to&nbsp;</li>
		                    <li>
                             <asp:TextBox ID="txtSiteOnAirDateTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valSiteOnAirDateto" runat="server"
                                                   ControlToValidate="txtSiteOnAirDateTo"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
                        </ul>
                        <br />
		
		                <label>SDP Request Date</label>
		                <ul class="field-list">
                          <li>
                             <asp:TextBox ID="txtSDPRequestDateFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valSDPRequestDateFrom" runat="server"
                                                   ControlToValidate="txtSDPRequestDateFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
		                    <li>&nbsp;to&nbsp;</li>
		                    <li>
                             <asp:TextBox ID="txtSDPRequestDateTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valSDPRequestDateTo" runat="server"
                                                   ControlToValidate="txtSDPRequestDateTo"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
                        </ul>
                    </div>

                    <div class="cb"></div>
                    <!--Advance search -->
                    <div id="pnlAdvanceCriteria"></div>

                    <div class="cb"></div>
                    <asp:Label ID="lblAdvanceCriteria" CssClass="critesria-title" runat="server" >Add More Filter Criteria >></asp:Label>
                    <asp:DropDownList ID="ddlCriteria" CssClass="criteria-select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    <button id="blnAddAdvanceCriteria" class="fancy-button" type="button" onclick="OnAddingCriteria(null,null,null);" runat="server" >Add Criteria</button>
                    
                    <div class="cb"></div>
                    <div style="padding:20px 0 15px 0; overflow:visible;">
                        <div style="width:auto;float:left;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                        <div style="width:auto;float:right;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                    </div>
                </asp:Panel>
	    </div>
    </div>
    <!-- /Search Filter Box -->

    <div class="cb"></div>

    <!-- Filter Tags -->
    <div id="filterTagsContainer" class="filter-tags-container not-displayed"  runat="server"></div>
    <!-- /Filter Tags -->

    <div class="cb"></div>

    <!-- Site Data Packages Grid -->
    <div style="overflow:auto; margin-bottom: 20px; min-height:580px;">

        <fx:Flexigrid ID="fgPackages"
            AutoLoadData="False"
            Width="990"
            ResultsPerPage="20"
			   ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			   HandlerUrl="~/SdpList.axd"
            runat="server">
		    <Columns>
			    <fx:FlexiColumn Code="site_uid" Text="Site ID" Width="80" />
                <fx:FlexiColumn Code="site.region_name" Text="Site Region Name" Width="110" OnRowRender="fgRowRender"  />
                <fx:FlexiColumn Code="site.market_code" Text="Site Mkt Code" Width="81" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="site.site_on_air_date" Text="Site On Air Date" Width="90" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="sdp_priorities.sdp_priority" Text="Priority" Width="50" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="sdp_request_date" Text="SDP Req Date" Width="80" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="sdp_special_projects.sdp_special_project" Text="SP Project Type" Width="90" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="user6.last_name" Text="TMO PM Emp" Width="160" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="user7.last_name" Text="TMO Spec Cur Emp" Width="160" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="id" Text="Actions" Width="65" AllowSort="false"
                                Format="<a class='fancy-button' href='/SiteDataPackages/Edit.aspx?id={0}'>Edit</a>" />
		    </Columns>
	    </fx:Flexigrid>
        <br />
        <div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgPackages')" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenLastFieldToCheckLoaded" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilterParam" runat="server" ClientIDMode="Static" Value="" />
        <div class="cb"></div>

    </div>
    <!-- /Site Data Packages Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->

</asp:Content>
