﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.DAL;
using TrackIT2.BLL;

namespace TrackIT2
{
    /// <summary>
    /// Comment
    /// </summary>
    public partial class Comment : System.Web.UI.Page
    {
        // Var
        private const string QS_COMMENT_TYPE = "ct";
        private const string QS_ID = "id";
        private const string COMMENT_TYPE_LEASE_APPS = "la";
        private const string COMMENT_TYPE_LEASE_APPS_LABEL = "Lease Application";
        private const string COMMENT_TYPE_TOWER_MODS = "tm";
        private const string COMMENT_TYPE_TOWER_MODS_LABEL = "Tower Modification";
        private const string COMMENT_TYPE_LEASE_ADMIN = "le";
        private const string COMMENT_TYPE_LEASE_ADMIN_LABEL = "Lease Admin";
        private const string COMMENT_TYPE_SITE_DATA_PACKAGE = "sdp";
        private const string COMMENT_TYPE_SITE_DATA_PACKAGE_LABEL = "Site Data Package";
        private const string COMMENT_AREA_GEN = "General";
        private const string COMMENT_AREA_SA = "Structural<br />Analysis";
        private const string COMMENT_AREA_LEASEADMIN = "Lease Admin";
        private const string COMMENT_AREA_TOWER_MOD = "Tower Mod";
        private const string COMMENT_AREA_SDP = "SDP";
        private const string H1_LABEL_STRING_FORMAT = " / {0} / {1}";
        private bool isGetAllReleate = false;
        //
        private int mId;
        //>

        /// <summary>
        /// Current ID
        /// </summary>
        private int CurrentID
        {
            get
            {
                return (int)this.ViewState["CurrentID"];
            }
            set
            {
                this.ViewState["CurrentID"] = value;
            }
        }
        //>

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[QS_COMMENT_TYPE]) && !string.IsNullOrEmpty(Request.QueryString[QS_ID]))
                {
                    bool isValidId = Int32.TryParse(Request.QueryString[QS_ID],
                                                    out mId);
                    if (isValidId)
                    {
                        //bind back button 
                        if (Request.QueryString[QS_COMMENT_TYPE] == COMMENT_TYPE_LEASE_APPS)
                        {
                            this.btnGoBack.Attributes.Add("href", "/LeaseApplications/Edit.aspx?id=" + Request.QueryString[QS_ID]);
                            btnGoBack.InnerText = "« Back to Lease App";
                        }
                        else if (Request.QueryString[QS_COMMENT_TYPE] == COMMENT_TYPE_TOWER_MODS)
                        {
                            this.btnGoBack.Attributes.Add("href", "/TowerModifications/Edit.aspx?id=" + Request.QueryString[QS_ID]);
                            btnGoBack.InnerText = "« Back to Tower Mod";
                        }
                        else if (Request.QueryString[QS_COMMENT_TYPE] == COMMENT_TYPE_SITE_DATA_PACKAGE)
                        {
                            this.btnGoBack.Attributes.Add("href", "/SiteDataPackages/Edit.aspx?id=" + Request.QueryString[QS_ID]);
                            btnGoBack.InnerText = "« Back to Site Data Package";
                        }

                        CurrentID = mId;
                        isGetAllReleate = true;

                        BindComments();
                    }
                }

            }
            else
            {
                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    if (!string.IsNullOrEmpty(keyName))
                    {
                         AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                    }
                }
            }
            
        }
        //-->

        /// <summary>
        /// Bind Comments
        /// </summary>
        protected void BindComments()
        {
            CPTTEntities ce = new CPTTEntities();
            string site_uid = string.Empty;

            // Type of Comments?
            if (Request.QueryString[QS_COMMENT_TYPE] == COMMENT_TYPE_LEASE_APPS)
            {
                #region Lease App
                // Lease App Comments
                List<AllComments> leaseAppComments = AllCommentsHelper.GetAllCommentLeaseApp(mId,null, false, isGetAllReleate);//Normal order by request (http://tickets.t-mobiletowers.com/browse/TRACKIT-237)

                if (leaseAppComments.Count > 0)
                {
                    // H1 Title
                    spnCommentsType.InnerHtml = COMMENT_TYPE_LEASE_APPS_LABEL;
                    lblSiteID.Text = leaseAppComments[0].SiteID;
                    SiteID.Text = String.Format(H1_LABEL_STRING_FORMAT, 
                                        leaseAppComments[0].SiteName, leaseAppComments[0].CustomerName);

                    if (!string.IsNullOrEmpty(leaseAppComments[0].SiteID))
                    {
                        lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + leaseAppComments[0].SiteID;
                    }

                    // Binding
                    rptComments.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_All);
                    rptComments.DataSource = leaseAppComments;
                    rptComments.DataBind();

                    var laId = leaseAppComments.FirstOrDefault().LeaseApplicationID.Value;
                    var leaseApp = ce.lease_applications.Where(la => la.id == laId).FirstOrDefault();
                    if (leaseApp != null)
                    {
                        site_uid = leaseApp.site_uid;
                    }
                }
                else
                {
                    HideUpComments();
                }

                //bind link document
                GetCommentsLinkDocument(leaseAppComments);
                #endregion Lease App
            }
            else if (Request.QueryString[QS_COMMENT_TYPE] == COMMENT_TYPE_TOWER_MODS)
            {
                #region TowerMod
                // Tower Mods Comments
                List<AllComments> allComment = new List<AllComments>();
                List<tower_mod_comments> towerModComments = ce.tower_mod_comments
                                                                .Where(tmc => tmc.tower_modification_id == mId)
                                                                .OrderByDescending(Utility.GetDateField<tower_mod_comments>("created_at"))
                                                                .ToList();

                List<LeaseAppTowerModInfo> lstResult = LeaseAppTowerMod.GetRalatedLeaseApp(mId);

                if (towerModComments.Count > 0 || lstResult.Count >0 )
                {
                    // H1 Title
                    spnCommentsType.InnerHtml = COMMENT_TYPE_TOWER_MODS_LABEL;
                    string siteName = "";
                    string cusName = "";
                    if (towerModComments.Count > 0 && towerModComments[0].tower_modifications.site != null)
                    {
                        siteName = towerModComments[0].tower_modifications.site.site_name;
                        site_uid = towerModComments.FirstOrDefault().tower_modifications.site_uid;
                    }
                    else if (lstResult.Count > 0) 
                    {
                        site_uid = lstResult[0].SiteID;
                        var site = TrackIT2.BLL.Site.Search(site_uid);
                        siteName = site.site_name;
                    }

                    if (lstResult.Count > 0)
                    {
                        String sCustomerNames = "";
                        foreach (LeaseAppTowerModInfo item in lstResult)
                        {
                            if (isGetAllReleate)
                            {
                                allComment.AddRange(AllCommentsHelper.GetAllCommentLeaseApp(item.LAID,mId, false, isGetAllReleate));
                            }

                            if (item.CustomerName != null)
                            {
                                if (sCustomerNames == "")
                                {
                                    sCustomerNames += item.CustomerName;
                                }
                                else
                                {
                                    sCustomerNames += ", " + item.CustomerName;
                                }
                            }
                        }
                        if (sCustomerNames == "") sCustomerNames =  "(no customer found)";
                        cusName = sCustomerNames;
                    }
                    else
                    {
                        if (towerModComments[0].tower_modifications.customer != null) cusName = towerModComments[0].tower_modifications.customer.customer_name;                        
                    }

                    lblSiteID.Text = site_uid;
                    SiteID.Text = String.Format(H1_LABEL_STRING_FORMAT,
                                        siteName, cusName);

                    lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + site_uid;

                    // Binding
                    rptComments.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_All);
                    if (allComment.Count > 0)
                    {
                        // releate with leaseapp
                        rptComments.DataSource = allComment;
                    }
                    else
                    {
                        // no releate with leaseApp
                        allComment = TowerModComment.GetTowerModCommentByTowerModId(mId);
                        rptComments.DataSource = allComment;
                    }
                    rptComments.DataBind();
                }
                else
                {
                    HideUpComments();
                }
                GetCommentsLinkDocument(allComment);
                
                #endregion TowerMod
            }
            else if (Request.QueryString[QS_COMMENT_TYPE] == COMMENT_TYPE_SITE_DATA_PACKAGE)
            {
                #region SDP
                // SDP Comments
                List<AllComments> allComment = new List<AllComments>();
                List<sdp_comments> sdpComments = ce.sdp_comments
                                                                .Where(sdpc => sdpc.site_data_package_id == mId)
                                                                .OrderByDescending(Utility.GetDateField<sdp_comments>("created_at"))
                                                                .ToList();
                List<SDPRelatedLeaseApplication> lstResult = TrackIT2.BLL.SiteDataPackage.GetRelatedLeaseApplication(null, mId);

                if (sdpComments.Count > 0 || lstResult.Count > 0)
                {
                    // H1 Title
                    spnCommentsType.InnerHtml = COMMENT_TYPE_SITE_DATA_PACKAGE_LABEL;
                    string siteName = "";
                    string cusName = "";
                    if (sdpComments.Count > 0 && sdpComments[0].site_data_packages.site != null)
                    {
                        siteName = sdpComments[0].site_data_packages.site.site_name;
                        var sdpId = sdpComments.FirstOrDefault().site_data_package_id;
                        var sdpObj = ce.site_data_packages.Where(sdp => sdp.id == sdpId).FirstOrDefault();
                        if (sdpObj != null)
                        {
                            site_uid = sdpObj.site_uid;
                        }
                    }
                    else
                    {
                        site_uid = lstResult.FirstOrDefault().SiteID;
                        var site = TrackIT2.BLL.Site.Search(site_uid);
                        siteName = site.site_name;
                    }


                    if (lstResult != null)
                    {
                            String sCustomerNames = "";
                            foreach (SDPRelatedLeaseApplication item in lstResult)
                            {
                                if (isGetAllReleate)
                                {
                                    allComment.AddRange(AllCommentsHelper.GetAllCommentLeaseApp(item.LAID,mId, false, isGetAllReleate));
                                }

                                if (item.CustomerName != null)
                                {
                                    if (sCustomerNames == "")
                                    {
                                        sCustomerNames += item.CustomerName;
                                    }
                                    else
                                    {
                                        sCustomerNames += ", " + item.CustomerName;
                                    }
                                }
                            }

                            if (sCustomerNames == "") sCustomerNames = "(no customer found)";
                            cusName = sCustomerNames;
                    }
                    else
                    {
                        if (sdpComments[0].site_data_packages.customer != null) cusName = sdpComments[0].site_data_packages.customer.customer_name;
                    }

                    lblSiteID.Text = site_uid;
                    SiteID.Text = String.Format(H1_LABEL_STRING_FORMAT,
                                        siteName, cusName);

                    if (!string.IsNullOrEmpty(site_uid))
                    {
                        lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + site_uid;
                    }

                    // Binding
                    rptComments.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_All);
                    if (allComment.Count > 0)
                    {
                        // releate with leaseapp
                        rptComments.DataSource = allComment;
                    }
                    else
                    {
                        // no releate with leaseApp
                        allComment = SdpComment.GetSdpCommentBySdp(mId);
                        rptComments.DataSource = allComment;
                    }
                    rptComments.DataBind();
                }
                else
                {
                    HideUpComments();
                }
                GetCommentsLinkDocument(allComment);

                #endregion SDP
            }
        }
        //-->

        protected void HideUpComments()
        {
            upComments.Visible = false;
            divNoComment.Visible = true;
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteComment_Click(object sender, EventArgs e)
        {
            ImageButton btnDelete = sender as ImageButton;
            if (btnDelete != null)
            {
                string[] sTmp;
                sTmp = btnDelete.AlternateText.Split('/');
                if (sTmp.Length == 2)
                {
                    string source = sTmp[0];
                    int id = int.Parse(sTmp[1]);
                    switch (source)
                    {
                        case AllComments.LEASE_APPLICATION :
                            LeaseAppComment.Delete(id);
                            break;
                        case AllComments.STRUCTURAL_ANALYSIS :
                            LeaseAppSAComment.Delete(id);
                            break;
                        case COMMENT_TYPE_TOWER_MODS :
                            TowerModComment.Delete(id);
                            break;
                        case COMMENT_TYPE_SITE_DATA_PACKAGE:
                            SdpComment.Delete(id);
                            break;
                        case AllComments.LEASE_ADMIN:
                            LeaseAdminComments.Delete(id);
                            break;
                        default:
                            break;
                    }
                    mId = CurrentID;
                    BindComments();
                }

            }
        }

        /// <summary>
        /// rptComments ItemDataBound - Lease Apps
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptComments_ItemDataBound_All(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                AllComments comment = (AllComments)e.Item.DataItem;

                Label lblDateCreated = (Label)e.Item.FindControl("lblDateCreated");
                lblDateCreated.Text = Utility.DateToString(comment.CreateAt);

                Label lblArea = (Label)e.Item.FindControl("lblArea");

                switch (comment.Mode)
                {
                    case "eSA":
                        lblArea.Text = COMMENT_AREA_SA;
                        break;
                    case "eLE":
                        lblArea.Text = COMMENT_AREA_LEASEADMIN;
                        break;
                    case "eLA":
                        lblArea.Text = COMMENT_AREA_GEN;
                        break;
                    case "eTM":
                        lblArea.Text = COMMENT_AREA_TOWER_MOD;
                        break;
                    case "eSDP":
                        lblArea.Text = COMMENT_AREA_SDP;
                        break;
                    default:
                        lblArea.Text = COMMENT_AREA_GEN;
                        break;
                }


                Label lblEmployee = (Label)e.Item.FindControl("lblEmployee");
                Image imgEmployee = (Image)e.Item.FindControl("imgEmployee");

                if (!string.IsNullOrEmpty(comment.CreatorName))
                {
                    lblEmployee.Text = comment.CreatorName;
                }
                else
                {
                    lblEmployee.Text = "No employee recorded.";
                }

                imgEmployee.ImageUrl = BLL.User.GetUserAvatarImage(comment.CreatorUsername);

                Label lblComment = (Label)e.Item.FindControl("lblComment");
                lblComment.Text = comment.Comments;


                var pnlDocument = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("pnlDocument");
                pnlDocument.Attributes.Add("class", "document-comment-" + comment.CommentID);

                ImageButton btnDeleteComment = (ImageButton)e.Item.FindControl("btnDeleteComment");
                btnDeleteComment.AlternateText = comment.Source + "/" + comment.CommentID;
            }
        }
        //-->

        #region Releate Comment

        #endregion Releate Comment
     
        #region Document Link

        //private void GetAllTMOCommentsLinkDocument(List<tower_mod_comments> lstTMOComment)
        //{
        //    this.MainContent_hidden_document_all_document_comments.Value = String.Empty;
        //    List<List<Dms_Document>> allComments = new List<List<Dms_Document>>();
        //    foreach (tower_mod_comments comment in lstTMOComment)
        //    {
        //        List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(comment.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.TMO_Comments.ToString()));
        //        if (docData.Count == 0)
        //        {
        //            Dms_Document doc = new Dms_Document();
        //            doc.ID = comment.id;
        //            doc.ref_field = DMSDocumentLinkHelper.eDocumentField.TMO_Comments.ToString();
        //            docData.Add(doc);
        //        }
        //        allComments.Add(docData);
        //    }
        //    this.MainContent_hidden_document_all_document_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(allComments);
        //}

        private void GetCommentsLinkDocument(List<AllComments> lstLeaseAppComment)
        {
            this.MainContent_hidden_document_all_document_comments.Value = String.Empty;
            List<List<Dms_Document>> allComments = new List<List<Dms_Document>>();
            foreach (AllComments comment in lstLeaseAppComment)
            {
                DMSDocumentLinkHelper.eDocumentField efield;

                if (comment.Mode == "eSA")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.SA_Comments;
                }
                else if (comment.Mode == "eLE")
                {
                    //Lease admin set field null because not document link
                    efield = DMSDocumentLinkHelper.eDocumentField.LA_Comments;
                }
                else if (comment.Mode == "eTM")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.TMO_Comments;
                }
                else if (comment.Mode == "eSDP")
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.SDP_Comments;
                }
                else
                {
                    efield = DMSDocumentLinkHelper.eDocumentField.LE_Comments;
                }
                if (efield != DMSDocumentLinkHelper.eDocumentField.LA_Comments && efield != DMSDocumentLinkHelper.eDocumentField.SDP_Comments)
                {
                    List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(comment.CommentID, DMSDocumentLinkHelper.DocumentField(efield.ToString()));
                    if (docData.Count == 0)
                    {
                        Dms_Document doc = new Dms_Document();
                        doc.ID = comment.CommentID;
                        doc.ref_field = efield.ToString();
                        docData.Add(doc);
                    }
                    allComments.Add(docData);
                }
            }
            this.MainContent_hidden_document_all_document_comments.Value = Newtonsoft.Json.JsonConvert.SerializeObject(allComments);
        }

        private List<Dms_Document> GetLinkDocument(int commentID, DMSDocumentLinkHelper.eDocumentField efield)
        {
            List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(commentID, DMSDocumentLinkHelper.DocumentField(efield.ToString()));
            if (docData.Count == 0)
            {
                Dms_Document doc = new Dms_Document();
                doc.ID = commentID;
                doc.ref_field = efield.ToString();
                docData.Add(doc);
            }
            return docData;
        }
        #endregion
    }
}