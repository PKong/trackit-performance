﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Edit Issue Tracker Item | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TrackIT2.IssueTracker.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    <!-- Styles -->
    <%: Styles.Render("~/Styles/issue_tracker_edit") %>    

    <!-- Scripts -->
    <%: Scripts.Render("~/Scripts/issue_tracker_edit") %>
    
    <!--TODO: Move to use script bundle -->
    <script src="../Scripts/documentLink.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(function () {
            // Setup List Create Modal
            setupListCreateIssueModalWithID("Edit Issue", $.urlParam("id"));
            //Setup Document link Dialog
            setupDocumentLinkDialog("Document Links", true);

            //Setup Issue actions 
            CreateCommentLinkDocument('hidden_document_Issue_Action','document-comment-');
            generateInfoLinkDocument();

            // Setup Upload Document Modal
            setupDmsModal("Upload Document");
        });
        //-->

        // Page Load (different to Doc Ready!)
        function pageLoad() {
            setupIssueDiscussionItems();
            CreateCommentLinkDocument('hidden_document_Issue_Action', 'document-comment-');
        }
        //-->


        function generateInfoLinkDocument() {
            var jsonStr = $('#hidden_document_Issue').val();
            if (jsonStr != null || jsonStr != "") {
                jsonData = jQuery.parseJSON(jsonStr);
            }
            $('.document-link-info').empty();
            if (jsonData != null) {
                if (jsonData.length > 0) {
                    var ulelement = $('<ul style="margin-left:20px;" ></ul>');
                    $('.document-link-info').append(ulelement);
                    for (i = 0; i < jsonData.length; i++) {
                        if ((jsonData[i].ref_key != "") && (jsonData[i].documentName != "")) {
                            var lidocument = $('<li style="word-wrap:break-word;margin-bottom:10px;"><a style="font-weight:normal;font-size:10pt;" href="#" ref_field="' + jsonData[i].ref_field + '" ref_key="' + jsonData[i].ref_key + '">' + jsonData[i].documentName + '</a>');
                           $(lidocument).click(function() {
                              OnDownloadDocClick($(this).find('a').attr('ref_key'));
                           });
                            $('.document-link-info').find('ul').append(lidocument);
                        }
                    }
                }
            }
        }
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <asp:Panel ID="pnlNoID" runat="server" CssClass="align-center">
      <h1>Not Found (404)</h1>

      <p>The issues specified is invalid or no longer exists.</p>

      <p>&nbsp;</p>

      <p>
         <a href="Default.aspx">&laquo; Go back to list page.</a>
      </p>

      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </asp:Panel>
              
    <asp:Panel ID="pnlSectionContent" runat="server">    
       <div class="section content">
           <div class="wrapper">
           <div class="action-tab">
                    <div class="left-side">
                        <asp:HyperLink ID="backToList" runat="server" Text="« Return to List" NavigateUrl="~/IssueTracker/" CssClass="site-link-no-underline"></asp:HyperLink>
                    </div>
               </div>
               <!-- Issue and Site Information -->
               <div class="site-information">
                   <h1 id="h1">Issue <asp:Label ID="lblIssueNumber" CssClass="h1-caps" runat="server" /> <span><asp:HyperLink ID="lblSiteID" runat="server" CssClass="h1-span-site-link site-link-no-underline"></asp:HyperLink><span ID="spnSiteIdNameSep" runat="server"> / </span><asp:Label ID="lblSiteName" runat="server" /></span>
                   </h1>
                   <div id="tabs" class="">
                       <ul class="tabs">
                           <li>
                               <a href="#issue-and-site-information">Issue and Site Information</a>
                           </li>
                       </ul>
                       <div id="issue-and-site-information">
                           <div style="padding: 20px;overflow: auto;">
                               <div class="i-2-3">
                                   <ul class="information-list" style="width: 220px; padding-right: 10px;">
                                       <li>Issue Information</li>
                                       <li>Priority: <asp:Label ID="lblPriority" runat="server" Text=""></asp:Label></li>
                                       <li>Status: <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label></li>
                                       <li>Submitted: <asp:Label ID="lblSubmitted" runat="server">Previous UID:</asp:Label></li>
                                       <li>Submit User: <asp:Label ID="lblSubmittedBy" runat="server" Text=""></asp:Label></li>
                                   </ul>
                                   <ul class="information-list" style="width: 220px; padding-right: 10px;">
                                       <li>Team & Tenants</li>
                                       <li>Current Specialist: <asp:Label ID="lblCurrentSpecialist" runat="server"></asp:Label></li>
                                       <li>Project Manager: <asp:Label ID="lblProjectManager" runat="server"></asp:Label></li>
                                       <li>Tenants: <asp:Label ID="lblTenants" runat="server"></asp:Label></li>
                                   </ul>
                                   <ul class="information-list" style="width: 170px;">
                                       <li>Tower Address</li>
                                       <li ID="liTowerAddressDetails" runat="server">
                                           <asp:HyperLink ID="link_googlemaps" NavigateUrl="javascript:void(0)" Target="_blank" runat="server"> </asp:HyperLink>
                                           <br/>
                                           <asp:Label ID="lblSiteLatLongHeader" CssClass="lbl-v-spaced-top" runat="server">Latitude, Longitude:<br /></asp:Label>
                                           <asp:HyperLink ID="hypSiteLatLong" Target="_blank" CssClass="lbl-v-spaced-bottom" runat="server">
                                              <asp:Label ID="lblDegreeLatitude" runat="server" ></asp:Label>,&nbsp;<asp:Label ID="lblDegreeLongtitude" runat="server"></asp:Label><br/> 
                                              <asp:Label ID="lblSiteLatitude" runat="server"/>,&nbsp;<asp:Label ID="lblSiteLongitude" runat="server" />
                                           </asp:HyperLink>
                                       </li>
                                       <li ID="liNoTowerAddressDetails" Visible="False" runat="server"><em>(No Details)</em></li>
                                   </ul>
                                   <div class="information-list" style="width: 440px; margin-right: 20px; overflow: auto;">
                                       <span class="info-list-title" style="display: inline-block; padding-bottom: 4px;">Issue Summary</span>
                                       <div class="ui-tabs-panel ui-widget-content" style="overflow: auto;">
                                           <div style="margin: 0 auto 0 auto; padding: 10px; overflow: auto;">
                                               <asp:Label ID="lblIssueSummary" runat="server"></asp:Label></div></div><div class="document-link-info" style="width:100%;" ></div>
                                   </div>
                                   <ul ID="ulStructureDetails" class="information-list" style="width: 170px;" runat="server">
                                       <li>Structure Details</li>
                                       <li>Type: <asp:Label ID="lblStructureDetails_Type" runat="server" Text="Monopole"></asp:Label></li>
                                       <li>Height: <asp:Label ID="lblStructureDetails_Height" runat="server" Text="100"></asp:Label></li>
                                       <li>Status: <asp:Label ID="lblStructureDetails_Status" runat="server" Text="On-Air"></asp:Label>
                                       <asp:Image ID="imgStructureDetails_Construction" runat="server" AlternateText="Construction" CssClass="construction-img" /></li>
                                   </ul>
                                   <ul ID="ulNoStructureDetails" class="information-list" style="width: 150px;" Visible="False" runat="server">
                                       <li>Structure Details</li>
                                       <li><em>(No Details)</em></li>
                                   </ul>
                               </div>
                               <div class="i-1-3">
                                   <div class="map">
                                       <asp:Image ID="imgSite" Width="310" Height="235" scrolling="no" marginheight="0" runat="server" />
                                   </div>
                                   <div class="cb"></div>
                               </div>

                               <a id="modalBtnExternal" class="edit-button" href="javascript:void(0)">edit issue</a> <!-- Modal --><div id="modalExternal" class="modal-container" style="overflow: hidden;"></div>
                               <div id="modalDocument" class="modal-container" style="overflow:hidden;"></div>
                               <!-- /Modal -->
                       </div>
                   </div>
                   <!-- #tab -->
                   <div class="cb">
                   </div>
               </div>

               </div>
               <!-- /Issue and Site Information -->
            
               <br/><br/>

               <!-- Issue Tasks and Comments -->
               <div class="all-comments" style="position: relative;">
                   <span id="h1-discussion" style="padding-right: 20px;" class="h1">Discussion</span>                   
                   <!-- Main Discussion Items List -->  
                   <asp:UpdatePanel ClientIDMode="Static" id="upDiscussion" runat="server" style="padding: 0; margin: -22px 0 0 0;" updatemode="Always">
                       <ContentTemplate>
                           <a id="add-discussion-item" class="edit-button a-discussion" href="javascript:void(0);">add discussion item</a>
                           &nbsp;
                           <div class="div-add-discussion-action-btns" style="display: none;">
                                <asp:Button ID="btnCancelNewDiscItem"
                                           CssClass="comment-cancel ui-button ui-widget ui-state-default ui-corner-all"
                                           ClientIDMode="Static" OnClientClick="handleAddDiscussionItemHide(); return false;"
                                           Text="Cancel" runat="server" />
                                 &nbsp; 
                                <asp:Button ID="btnPostNewDiscItem" NavigateUrl="javascript:void(0)"
                                            CssClass="ui-button ui-widget ui-state-default ui-corner-all" 
                                            ClientIDMode="Static" OnClientClick="return handleAddDiscussionItemPost();"
                                            OnClick="BtnPostNewDiscItemClick"
                                            Text="Create Item" runat="server" />
                           </div>
                           <p class="inline-error-add-disc-item" style="display: none;">Error Occurred!</p>                              
                           <div id="discussion-repeater-container">
                               <asp:Repeater ID="rptDiscussion" ClientIDMode="Static" runat="server">
                                   <HeaderTemplate>
                                       <table width="100%" cellspacing="0" cellpadding="0" class="sort-me all-comments-table">
	                                       <thead>
		                                       <tr>
			                                       <th scope="col" style="width:70px;height:30px;">Date</th>
			                                       <th scope="col" style="width:100px;">Type</th>
			                                       <th scope="col" style="width:140px;">Employee</th>
			                                       <th scope="col" style="width:630px;">&nbsp;</th>
		                                       </tr>
	                                       </thead>
                                           <tbody>
                                               <tr id="tr-new-discussion-item" style="display: none;">
						                           <td width="width:70px;" class="td-new-discussion-item" style="border-bottom: none;">
						                               <div class="div-new-discussion-item" style="display: none;">
                                                           <label id="lblNewDiscItemDateCreated"></label>
						                               </div>
						                           </td>
               	                                   <td width="100px" class="td-new-discussion-item" style="border-bottom: none;">
               	                                       <div class="div-new-discussion-item" style="display: none;">
						                                   <asp:DropDownList ID="ddlNewDiscItemType" CssClass="form-select" ClientIDMode="Static" runat="server" />
						                               </div>
               	                                   </td>
						                           <td width="140px" class="td-new-discussion-item" style="border-bottom: none;">
						                               <div class="div-new-discussion-item" style="display: none;">
							                               <div class="author-info">
                                                               <asp:Image ID="imgNewDiscItemEmployee"  Width="50" Height="50" ImageUrl="/images/avatars/default.png"
                                                                   ToolTip="Employee Profile Image" runat="server" />
								                               <asp:Label id="lblNewDiscItemEmployee" runat="server" />
							                               </div>
						                               </div>
						                           </td>
						                           <td width="630px" class="td-new-discussion-item" style="border-bottom: none;">
						                               <div class="div-new-discussion-item" style="display: none;">
							                               <div class="comment">				
								                               <div class="balloon">
								                                   <asp:TextBox ID="txtNewDiscItemActionItem" Rows="5"
								                                                   CssClass="textarea-new-discuss-item comment-text-unselected italic"
								                                                   runat="server" TextMode="MultiLine" Text="Enter your new discussion item text here."
                                                                                   ClientIDMode="Static" />
									                               <div class="balloon-left-side"></div>
								                               </div>
							                               </div>
						                               </div>
						                           </td>
                                               </tr>
                                   </HeaderTemplate> 
                                   <ItemTemplate>
                	                           <tr>
						                           <td><asp:Label id="lblDateCreated" runat="server" /></td>
						                           <td><asp:Label id="lblType" runat="server" /></td>
						                           <td>
							                           <div class="author-info">
                                                           <asp:Image ID="imgEmployee"  Width="50" Height="50" ImageUrl="/images/avatars/default.png"
                                                               ToolTip="Employee Profile Image" runat="server" />
								                           <asp:Label id="lblEmployee" runat="server" />
							                           </div>
						                           </td>
						                           <td>
							                           <div class="comment">				
								                           <div class="balloon">
									                           <asp:Label id="lblComment" runat="server" />
									                           <div class="balloon-left-side"></div>
								                           </div>
                                                           <div id="pnlDocument" runat="server" style="width:100%;text-align:left;" ></div>
                                                           <asp:ImageButton id="btnDeleteComment" Width="12" ImageUrl="/images/icons/Erase-Greyscale.png"
                                                                   ToolTip="Delete Item" CssClass="btn-comment-delete" runat="server" />
							                           </div>
						                           </td>
					                           </tr>
                                   </ItemTemplate>
                                   <FooterTemplate>
                                               <tr>
                                                   <td id="tdDiscussionNoData" colspan="4" style="border-bottom: none;" Visible="False" runat="server">
                                                       <asp:Label ID="lblDiscussionNoData"
                                                               Text="There is no Discussion data to display."
                                                               runat="server"
                                                               CssClass="italic" />
                                                   </td>
                                               </tr>
                	                       </tbody>	
			                           </table>
                                   </FooterTemplate>
                               </asp:Repeater>
                           </div>
                        <asp:HiddenField ID="hidden_document_Issue_Action" runat="server" ClientIDMode="Static" Value="" />
                       </ContentTemplate>
                   </asp:UpdatePanel>
                   <asp:HiddenField ID="hidden_document_Issue" runat="server" ClientIDMode="Static" Value="" />
                   <asp:HiddenField ID="hidDocumentDownload" runat="server" ClientIDMode="Static" /> 
                   <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
                   <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
                   <!-- /Main Discussion Items List -->
               </div>
               <!-- /Issue Tasks and Comments -->
           </div>
       </div>
    </asp:Panel>
</asp:Content>