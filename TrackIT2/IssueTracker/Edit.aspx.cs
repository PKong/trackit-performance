﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Web.UI.HtmlControls;
using System.Web;

namespace TrackIT2.IssueTracker
{
    /// <summary>
    /// Edit
    /// </summary>
    public partial class Edit : Page
    {
        // Var
        private bool _issueFound;
        private int _issueId;
        private const string LABEL_NONE = "None";
        private string _issueTitle;
        private const string ACTION_TYPE_DISCUSSION_ITEM = "DiscussionItem";
        private string _requestTarget;
        private string _requestArgument;
        //>

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {   
            if (!Page.IsPostBack)
            {
                // Get Issue ID
                var blIsValidId = int.TryParse(Request.QueryString["Id"], out _issueId);

                // Clear Submit Validator
                Utility.ClearSubmitValidator();

                // Valid ID?
                if (blIsValidId)
                {
                   if (BLL.IssueTracker.Search(_issueId) != null)
                   {
                      // Bind Issue
                      BindIssue();
                      pnlNoID.Visible = false;
                      pnlSectionContent.Visible = true;
                      _issueFound = true;
                   }
                   else
                   {
                      // Page render event will write the 404 response code. This part
                      // of the code will display the No Id panel and hide the edit
                      // panel.
                      pnlSectionContent.Visible = false;
                      pnlNoID.Visible = true;
                      _issueFound = false;
                   }
                }
                else
                {
                   // Page render event will write the 404 response code. This part
                   // of the code will display the No Id panel and hide the edit
                   // panel.
                   pnlSectionContent.Visible = false;
                   pnlNoID.Visible = true;
                   _issueFound = false;
                }
                //>

                GetSessionFilter();
            }
            else
            {
                // Re-set Issue ID
                if (IssueTracker != null) _issueId = IssueTracker.id;

                // Form Action - Deleting an Issue Action Item? (Discussion Item)
                _requestTarget = Request.Form["__EVENTTARGET"];
                _requestArgument = Request.Form["__EVENTARGUMENT"];
                //
                if ((!string.IsNullOrEmpty(_requestTarget)) && (!string.IsNullOrEmpty(_requestArgument)))
                {
                    if(_requestArgument.IndexOf(ACTION_TYPE_DISCUSSION_ITEM, System.StringComparison.Ordinal) > -1)
                    {
                        int issueActionItemId = 0;
                        var arrDiscussionItemToDelete = _requestArgument.Split('|');
                        if (arrDiscussionItemToDelete.Length == 2) issueActionItemId = int.Parse(arrDiscussionItemToDelete[1]);
                        if (issueActionItemId > 0) DeleteIssueActionItem(issueActionItemId);
                    }
                }

                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    if (!string.IsNullOrEmpty(keyName))
                    {
                        try
                        {
                            AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                        }
                        catch (Amazon.S3.AmazonS3Exception ex)
                        {
                            Master.StatusBox.showErrorMessage(ex.Message);
                        }
                    }
                }
                //>
            }

            // Page Title
            _issueTitle = Globals.ISSUE_TRACKER_ID_PREFIX + _issueId.ToString(CultureInfo.InvariantCulture);
            Page.Title = "Edit Issue: " + _issueTitle + " | TrackiT2";
            Utility.ControlValueSetter(lblIssueNumber, _issueTitle);
        }
        //-->

        protected override void Render(HtmlTextWriter writer)
        {
           base.Render(writer);

           if (!_issueFound)
           {
              Response.StatusCode = 404;
           }
        }

        #region Properties

        /// <summary>
        /// Issue Tracker
        /// </summary>
        public issue_tracker IssueTracker
        {
            get
            {
                return (issue_tracker)ViewState["IssueTracker"];
            }
            set
            {
                ViewState["IssueTracker"] = value;
            }
        }
        //>

        #endregion

        #region Binding

        /// <summary>
        /// Bind Issue
        /// </summary>
        protected void BindIssue()
        {

            using (var ce = new CPTTEntities())
            {
                // Get the Entity Record
                var issue = BLL.IssueTracker.Search(_issueId);
                IssueTracker = issue;
                ce.Attach(issue);

                // Have Site?
                if (issue.site != null)
                {
                    // Yes- Have Site
                    //-------------

                    // Item Title
                    Utility.ControlValueSetter(lblSiteID, issue.site.site_uid);
                    if (!string.IsNullOrEmpty(issue.site.site_uid))
                    {
                        lblSiteID.NavigateUrl = Globals.SITE_LINK_URL + issue.site.site_uid;
                    }

                    Utility.ControlValueSetter(lblSiteName, issue.site.site_name);

                    // Google Maps Link
                    if (!string.IsNullOrEmpty(issue.site.address))
                    {
                        // Link Address
                        const string S_ADDRESS_FORMAT = "{0}<br />{1}, {2}, {3}<br />{4}";
                        link_googlemaps.Text = string.Format(S_ADDRESS_FORMAT
                            , Utility.PrepareString(issue.site.address)
                            , Utility.PrepareString(issue.site.city)
                            , Utility.PrepareString(issue.site.state)
                            , Utility.PrepareString(issue.site.zip)
                            , Utility.PrepareString(issue.site.county));

                        // URL
                        var sbGoogleSearch = new StringBuilder();
                        sbGoogleSearch.Append(issue.site.address.ToString(CultureInfo.InvariantCulture));
                        if (!String.IsNullOrWhiteSpace(issue.site.city)) sbGoogleSearch.Append(", " + issue.site.city);
                        if (!String.IsNullOrWhiteSpace(issue.site.state)) sbGoogleSearch.Append(", " + issue.site.state);
                        if (!String.IsNullOrWhiteSpace(issue.site.zip)) sbGoogleSearch.Append(", " + issue.site.zip);
                        if (!String.IsNullOrWhiteSpace(issue.site.county)) sbGoogleSearch.Append(", " + issue.site.county);
                        link_googlemaps.NavigateUrl = Utility.GetGoogleMapLink(sbGoogleSearch.ToString());
                    }
                    else
                    {
                        link_googlemaps.Visible = false;
                    }
                    //>

                    // Latitude & Longitude                
                    if (issue.site.site_latitude.HasValue || issue.site.site_longitude.HasValue)
                    {
                        Utility.ControlValueSetter(lblDegreeLatitude, Utility.ConvertLatLongCoorToDegree(issue.site.site_latitude), (0.0).ToString());
                        Utility.ControlValueSetter(lblSiteLatitude, issue.site.site_latitude, (0.0).ToString(CultureInfo.InvariantCulture));
                        Utility.ControlValueSetter(lblDegreeLongtitude, Utility.ConvertLatLongCoorToDegree(issue.site.site_longitude), (0.0).ToString());
                        Utility.ControlValueSetter(lblSiteLongitude, issue.site.site_longitude, (0.0).ToString(CultureInfo.InvariantCulture));
                        hypSiteLatLong.NavigateUrl = Utility.GetGooleMapLatLong(lblSiteLatitude.Text, lblSiteLongitude.Text);
                        lblSiteLatLongHeader.Visible = true;
                        hypSiteLatLong.Visible = true;
                    }
                    else
                    {
                        lblSiteLatLongHeader.Visible = false;
                        hypSiteLatLong.Visible = false;
                    }
                    //>

                    // Structure Details
                    Utility.ControlValueSetter(lblStructureDetails_Type, issue.site.site_class_desc);
                    Utility.ControlValueSetter(lblStructureDetails_Height, issue.site.structure_ht, (0).ToString(CultureInfo.InvariantCulture));
                    Utility.ControlValueSetter(lblStructureDetails_Status, issue.site.site_status_desc);
                    if (BLL.Site.IsNotOnAir(issue.site.site_on_air_date, issue.site.site_status_desc))
                    {
                        imgStructureDetails_Construction.ImageUrl = "/images/icons/Construction.png";
                    }
                    else
                    {
                        imgStructureDetails_Construction.Visible = false;
                    }

                    // Remove no Tower display list item from view to prevent gap.
                    liNoTowerAddressDetails.Style.Add("display,", "none");

                    // Site Image
                    BindTowerImage(issue.site.site_uid);
                    // Bind document link for issue action items case issus have sites
                    GetLinkDocument();
                    GetCommentsLinkDocument();
                }
                else
                {
                    // No Site
                    //-------------

                    // Item Title
                    lblSiteID.Visible = false;
                    spnSiteIdNameSep.Visible = false;
                    Utility.ControlValueSetter(lblSiteName, Globals.ISSUE_TRACKER_NO_SITE_NAME);

                    // Tower Address - Hide
                    liTowerAddressDetails.Visible = false;
                    liNoTowerAddressDetails.Visible = true;

                    // Structure Details - Hide
                    ulStructureDetails.Visible = false;
                    ulNoStructureDetails.Visible = true;

                    // Site Image
                    BindTowerImage(null);
                }
                //>

                // Issue Information & Summary
                Utility.ControlValueSetter(lblPriority, "(" + issue.issue_priority_types.issue_priority_number + ") " + issue.issue_priority_types.issue_priority_type);
                Utility.ControlValueSetter(lblStatus, issue.issue_statuses.issue_status, LABEL_NONE);
                Utility.ControlValueSetter(lblSubmitted, issue.created_at);
                Utility.ControlValueSetter(lblSubmittedBy, new UserInfo(issue.user).FullName);
                Utility.ControlValueSetter(lblIssueSummary, issue.issue_summary);
                //>

                // Team
                Utility.ControlValueSetter(lblCurrentSpecialist, new UserInfo(issue.user2).FullName, LABEL_NONE);
                Utility.ControlValueSetter(lblProjectManager, new UserInfo(issue.user1).FullName, LABEL_NONE);

                // Tenants
                if ((issue.issue_tracker_tenants != null) && (issue.issue_tracker_tenants.Count > 0))
                {
                    string sTenants = "";
                    sTenants += issue.issue_tracker_tenants.Aggregate(sTenants, (current, tennant) => current + (tennant.customer.customer_name + "<br />"));
                    lblTenants.Text = sTenants;
                }else
                {
                    lblTenants.Text = LABEL_NONE;
                }
                //>

                // Bind Issue Action Items (Discussion)
                BindIssueActionItems(issue);
            }
            //>
        }
        //-->

        /// <summary>
        /// Bind Issue Action Items
        /// </summary>
        /// <param name="issue"> </param>
        protected void BindIssueActionItems(issue_tracker issue)
        {
            rptDiscussion.ItemDataBound += new RepeaterItemEventHandler(RptDiscussionItemDataBound);
            rptDiscussion.DataSource = issue.issue_action_items.OrderByDescending(i => i.created_at);
            rptDiscussion.DataBind();   
        }
        //-->

        /// <summary>
        /// Bind Tower Image
        /// </summary>
        /// <param name="siteId"></param>
        protected void BindTowerImage(string siteId)
        {
            var imageHelper = new DMSHelper();
            List<PhotoInfo> siteImages = imageHelper.GetSitePhotos(siteId, false, string.Empty);
            imgSite.ImageUrl = siteImages[0].photoURL;
            imgSite.AlternateText = siteImages[0].photoID;
        }
        //-->

        /// <summary>
        /// Rpt Discussion Item DataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RptDiscussionItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Process Header Items
            if (e.Item.ItemType == ListItemType.Header)
            {
                // Create Type
                using (var ce = new CPTTEntities())
                {
                    var ddlNewDiscItemType = (DropDownList)e.Item.FindControl("ddlNewDiscItemType");
                    var actionTypes = ce.issue_action_item_types;
                    var sort_actionTypes = actionTypes.ToList();
                    var dynamic_actionTypes = new DynamicComparer<issue_action_item_types>();
                    dynamic_actionTypes.SortOrder(x => x.issue_action_item_type);
                    sort_actionTypes.Sort(dynamic_actionTypes);
                    Utility.BindDDLDataSource(ddlNewDiscItemType, sort_actionTypes, "id", "issue_action_item_type", "- Select -");
                }
                //>

                // Create Employee Info
                var lblNewDiscItemEmployee = (Label)e.Item.FindControl("lblNewDiscItemEmployee");
                var user = BLL.User.GetUserInfo((int?) Session["userId"]);
                if (user.Count > 0)
                {
                    lblNewDiscItemEmployee.Text = user[0].FullName;
                    var imgNewDiscItemEmployee = (Image)e.Item.FindControl("imgNewDiscItemEmployee");
                    imgNewDiscItemEmployee.ImageUrl = BLL.User.GetUserAvatarImage(user[0].UserUID);
                }else
                {
                    lblNewDiscItemEmployee.Text = LABEL_NONE;
                }
                lblNewDiscItemEmployee.Text = user.Count > 0 ? user[0].FullName : LABEL_NONE;
                //>
            }
            //>

            // Process Data Items
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // Get Issue Action
                var issueAction = (issue_action_items)e.Item.DataItem;

                // Create At
                var lblDateCreated = (Label)e.Item.FindControl("lblDateCreated");
                lblDateCreated.Text = issueAction.created_at != null ? Utility.DateToString(issueAction.created_at) : LABEL_NONE;

                // Type
                var lblType = (Label)e.Item.FindControl("lblType");
                lblType.Text = issueAction.issue_action_item_types.issue_action_item_type;

                // Comment or Task Text
                var lblComment = (Label)e.Item.FindControl("lblComment");
                lblComment.Text = issueAction.issue_action_item;

                var pnlDocument = (System.Web.UI.HtmlControls.HtmlGenericControl)e.Item.FindControl("pnlDocument");
                pnlDocument.Attributes.Add("class", "document-comment-" + issueAction.id); 

                // Employee
                var employee = new UserInfo(issueAction.user);
                var lblEmployee = (Label)e.Item.FindControl("lblEmployee");
                var imgEmployee = (Image)e.Item.FindControl("imgEmployee");
                lblEmployee.Text = !string.IsNullOrEmpty(employee.FullName) ? employee.FullName : LABEL_NONE;
                imgEmployee.ImageUrl = BLL.User.GetUserAvatarImage(employee.UserUID);

                // Hidden Field - Type|ID
                var btnDeleteComment = (ImageButton)e.Item.FindControl("btnDeleteComment");
                btnDeleteComment.AlternateText = ACTION_TYPE_DISCUSSION_ITEM + "|" + issueAction.id.ToString(CultureInfo.InvariantCulture);

                // Hide button if user is not allowed to delete comments.
                if (!Security.UserIsInRole("Administrator") && issueAction.creator_id != (int)Session["userId"])
                {
                   btnDeleteComment.Visible = false;
                }

            }
            //>

            // No Data Handler
            if (rptDiscussion.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    var tdDiscussionNoData = (HtmlTableCell)e.Item.FindControl("tdDiscussionNoData");
                    tdDiscussionNoData.Visible = true;
                }
            }
            //>
        }
        //-->

        #endregion

        #region Data Processing / Actions

        /// <summary>
        /// Btn Post New Disc Item Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnPostNewDiscItemClick(object sender, EventArgs e)
        {
            // Get the Issue
            var issue = IssueTracker;
            if (issue == null) return;

            var actionItems = new issue_action_items { issue_tracker_id = issue.id };

            // Action Item
            var ctrlTxtNewDiscItemActionItem = PageUtility.FindControlRecursive(rptDiscussion, "txtNewDiscItemActionItem");
            if (ctrlTxtNewDiscItemActionItem != null)
            {
                var txtNewDiscItemActionItem = ctrlTxtNewDiscItemActionItem as TextBox;
                if (txtNewDiscItemActionItem != null) actionItems.issue_action_item = Server.HtmlEncode(txtNewDiscItemActionItem.Text);
            }
            //>

            // Type
            var ctrlDdlNewDiscItemType = PageUtility.FindControlRecursive(rptDiscussion, "ddlNewDiscItemType");
            if (ctrlDdlNewDiscItemType != null)
            {
                var ddlNewDiscItemType = ctrlDdlNewDiscItemType as DropDownList;
                if (ddlNewDiscItemType != null) actionItems.issue_action_item_type_id = int.Parse(ddlNewDiscItemType.SelectedValue);
            }
            //>

            // Add Issue Action Item
            //-----------------------
            string result = BLL.IssueTracker.AddIssueActionItem(actionItems);
            
            // Regardless of success/faul, the issueFound variable is set to 
            // avoid a 404 code interferring with the page render.
            _issueFound = true;

            // Success?
            if (result == null)
            {
                // Yes - Re-bind Issue (to refresh page data)               
                BindIssue();
            }
            else
            {
                // No - Throw Error
                Master.StatusBox.showStatusMessage("Error: a problem occurred while adding the Issue Action Item (Discussion Item), this operation has been aborted.");
            }
            //>
        }
        //-->


        /// <summary>
        /// Delete Issue Action Item
        /// </summary>
        /// <param name="issueActionItemId"> </param>
        protected void DeleteIssueActionItem(int issueActionItemId)
        {
            // Delete Item
            //-------------------
            string result = BLL.IssueTracker.DeleteIssueActionItem(issueActionItemId);

            // Regardless of success/fail, the issueFound variable is set to 
            // avoid a 404 code interferring with the page render.
            _issueFound = true;

            // Success?
            if (result == null)
            {
                // Yes - Re-bind Issue (to refresh page data)             
               BindIssue();                
            }
            else
            {
                // No - Throw Error
                Master.StatusBox.showStatusMessage("Error: a problem occurred while deleting the Issue Action Item (Discussion Item), this operation has been aborted.");
            }
            //>
        }
        //-->

        #endregion


        #region Document Link 
        private void GetCommentsLinkDocument()
        {
            this.hidden_document_Issue_Action.Value = "";
            List<List<Dms_Document>> allIssueAction = new List<List<Dms_Document>>();
            List<issue_action_items> issue_action = this.IssueTracker.issue_action_items.ToList(); ;
            foreach (issue_action_items action in issue_action)
            {
                List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(action.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Issue_Action.ToString()));
                if (docData.Count == 0)
                {
                    Dms_Document doc = new Dms_Document();
                    doc.ID = action.id;
                    doc.ref_field = DMSDocumentLinkHelper.eDocumentField.Issue_Action.ToString();
                    docData.Add(doc);
                }
                allIssueAction.Add(docData);
            }
            this.hidden_document_Issue_Action.Value = Newtonsoft.Json.JsonConvert.SerializeObject(allIssueAction);

        }
        private void GetLinkDocument()
        {
            this.hidden_document_Issue.Value = "";
            List<Dms_Document> docData = DMSDocumentLinkHelper.GetDocumentLink(IssueTracker.id, DMSDocumentLinkHelper.DocumentField(DMSDocumentLinkHelper.eDocumentField.Issue.ToString()));
            if (docData.Count == 0)
            {
                Dms_Document doc = new Dms_Document();
                doc.ID = IssueTracker.id;
                doc.ref_field = DMSDocumentLinkHelper.eDocumentField.Issue.ToString();
                docData.Add(doc);
            }
            this.hidden_document_Issue.Value += Newtonsoft.Json.JsonConvert.SerializeObject(docData);
        }
        #endregion

        private void GetSessionFilter()
        {
            // Check session filter
            DefaultFilterSession filterSession = new DefaultFilterSession();

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[DefaultFilterSession.SessionName] != null)
            {
                filterSession = (DefaultFilterSession)HttpContext.Current.Session[DefaultFilterSession.SessionName];

                string hiddenFilter = DefaultFilterSession.GetFilterSession(filterSession, DefaultFilterSession.FilterType.IssueFilter);
                backToList.NavigateUrl += "?filter=" + hiddenFilter;
            }
        }
    }
    //>
}