﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Issue Tracker | TrackiT2" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2.IssueTracker.DefaultIssueTracker" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Styles -->
    <%: Styles.Render("~/Styles/issue_tracker") %>
    
    <!-- Scripts -->
    <%: Scripts.Render("~/Scripts/issue_tracker") %>

    <script type="text/javascript" language="javascript">
        // Global Var
        searchFilterFields = "ddlIssueID,txtSiteID,txtSiteName,lsbPriority,txtIssueCreatedFrom,txtIssueCreatedTo,lsbCreateUser,lsbCurrentSpecialist,lsbProjectManager,lsbCustomer,lsbStatus";
        currentFlexiGridID = '<%= fgIssueTracker.ClientID %>';

        // Doc Ready
        $(function () {

            // Set the modal size
            //setModalSize(490, 450, 330);

            // Setup List Create Modal
            setupListCreateModal("Create New Issue");

            // Setup Search Filter
            setupSearchFilter();

            // Setup Saved Filter Modal 
            setupSavedFilterModal(SavedFilterConstants.buttonCreateNew, SavedFilterConstants.listViews.issueTracker.name, SavedFilterConstants.listViews.issueTracker.url);

        });
        //-->

        // Flexigrid
        //
        // FG Before Send Data
        function fgBeforeSendData(data)
        {

            // Show - Loading Spinner
            $('#h1').activity({ segments: 8, width: 2, space: 0, length: 3, speed: 1.5, align: 'right' });

            // disable export button
            $("#MainContent_exportButton").val("Processing...");
            $("#MainContent_exportButton").attr("disabled", true);

            // Global Search
            var gs = $.urlParam(GlobalSearch.searchQueryString);
            //
            addGlobal:
            if (!String(gs).isNullOrEmpty())
            {
                for (var i = 0; i < arrayOfFilters.length; i++)
                {
                    if (arrayOfFilters[i].name == GlobalSearch.searchQueryString) break addGlobal;
                }
                addFilterPostParam(GlobalSearch.searchQueryString, gs);
            }
            //>

            // Set Default filter
            removeFilterPostParam(ConstGlobal.DEFAULT.returnPage);
            addFilterPostParam(ConstGlobal.DEFAULT.returnPage, savedFiltersQueryString);

            // Search Filter Data
            sendSearchFilterDataToHandler(data);
        }
        //
        // FG Data Load
        function fgDataLoad(sender, args)
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Enable export button
            $("#MainContent_exportButton").val("Export to Excel");
            $('#MainContent_exportButton').removeAttr("disabled");

        }
        //
        // FG Row Click
        function fgRowClick(sender, args)
        {

            return false;
        }
        //
        // FG Row Render
        function fgRowRender(tdCell, data, dataIdx)
        {

            var cVal = new String(data.cell[dataIdx]);

            // Type?
            switch (Number(dataIdx))
            {

                // Date
                case 5:
                    if (cVal.isNullOrEmpty()) {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else {
                        //alert(cVal);
                        tdCell.innerHTML = cVal;
                    }
                    break;

                // Standard String 
                default:
                    if (cVal.isNullOrEmpty())
                    {
                        tdCell.innerHTML = ConstGlobal.DATA.noDataHtml;
                    } else
                    {
                        tdCell.innerHTML = cVal;
                    }
                    break;
            }
            //>
        }
        //
        // FG No Data
        function fgNoData()
        {

            // Hide - Loading Spinner 
            $('#h1').activity(false);

            // Disable export button
            $("#MainContent_exportButton").val("Export to Excel");
        }
        //-->
    </script>    

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="message_alert" class="full-col"></div>

    <!-- Page Content Header -->
    <div class="o-1" style="margin:0;">
	    <h1 id="h1" style="float:left; padding-top:3px; padding-right:30px;">Issue Tracker</h1>
	        <div class="page-options-nav">
                <a class="fancy-button clear-filter-button" href="javascript:void(0)">Clear Filters</a>
                <a class="fancy-button filter-button arrow-down" href="javascript:void(0)">Filter Results<span class="arrow-down-icon"></span></a> 
                <a id="modalBtnExternalSaveFilter" class="fancy-button save-filter-button" href="javascript:void(0)">Save Filter</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
		        <a id="modalBtnExternal" class="fancy-button" href="javascript:void(0)">Create New Issue</a>
	        </div>
	    <div class="cb"></div>
    </div>
    <!-- /Page Content Header -->

    <!-- Search Filter Box -->
    <div class="filter-box">
	    <div style="padding:20px">
            <div class="filter-button-close"><a class="fancy-button close-filter" style="padding:2px 6px 2px 6px;" href="javascript:void(0)">x</a></div>
                <asp:Panel ID="panelFilterBox" runat="server">
                    <div class="left-col">

                        <label>Issue ID</label>
                        <asp:DropDownList ID="ddlIssueID" ClientIDMode="Static" CssClass="select-field" runat="server">
                            <asp:ListItem Text="- Select -" Value=""></asp:ListItem>
                        </asp:DropDownList>
		                <br />
                        
                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                            LabelFieldClientID="lblSiteID"
                            LabelFieldValue="Site ID"
                            LabelFieldCssClass="label"
                            TextFieldClientID="txtSiteID"
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188"
                            ScriptKeyName="ScriptAutoCompleteSiteID"
                            DataSourceUrl="SiteIdAutoComplete.axd"
                            ClientIDMode="Static"
                            runat="server" />
		                <br />
                        
                        <TrackIT2:TextFieldAutoComplete ID="ctrSiteName"
                            LabelFieldClientID="lblSiteName"
                            LabelFieldValue="Site Name"
                            LabelFieldCssClass="label"
                            TextFieldClientID="txtSiteName"
                            TextFieldCssClass="input-autocomplete"
                            TextFieldWidth="188"
                            ScriptKeyName="ScriptAutoCompleteSiteName"
                            DataSourceUrl="SiteNameAutoComplete.axd"
                            IsWholeTextUsage="True"
                            ClientIDMode="Static"
                            runat="server" />
		                <br />
                        
                        <label>Priority</label>

                        <div id="divPriority" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbPriority" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
                        
		                <label>Created</label>
		                <ul class="field-list">
                          <li>
                             <asp:TextBox ID="txtIssueCreatedFrom" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valIssueCreatedFrom" runat="server"
                                                   ControlToValidate="txtIssueCreatedFrom"
                                                   ClientIDMode="Static" 
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                              </asp:CompareValidator>
                          </li>
		                    <li>&nbsp;to&nbsp;</li>
		                    <li>
                             <asp:TextBox ID="txtIssueCreatedTo" ClientIDMode="Static" CssClass="datepicker" runat="server" />
                             <asp:CompareValidator ID="valIssueCreatedTo" runat="server"
                                                   ControlToValidate="txtIssueCreatedTo" 
                                                   ClientIDMode="Static"
                                                   CssClass="inline-error"
                                                   Display="Dynamic"
                                                   Type="Date"
                                                   Operator="DataTypeCheck"
                                                   ErrorMessage="*">
                             </asp:CompareValidator>
                          </li>
                        </ul>                                            
                        <br />
                        
                        <label>Create User</label>

                        <div id="divCreateUser" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCreateUser" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                    </div>

		            <div class="right-col">
		                
                        <label>Status</label>
                        <div id="divStatus" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbStatus" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />

                        <label>Current Specialist</label>
                        
                        <div id="divCurrentSpecialist" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCurrentSpecialist" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
                        
                        <label>Project Manager</label>
                        
                        <div id="divProjectManager" style="width: 240px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbProjectManager" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
		                <br />
                        
                        <label style="display: inline-block; vertical-align:top;">Tenants</label>
                        
                        <div id="divCustomer" style="width: 188px;margin-left: 188px; vertical-align:top;margin-top:-32px" >
                            <asp:ListBox ID="lsbCustomer" SelectionMode="Multiple" ToolTip="- Select -" CssClass="multi-local-config" ClientIDMode="Static" runat="server" />
                        </div>
                        <br />

                    </div>

                    <div class="cb"></div>
                    <div style="padding:20px 0 15px 0; overflow:visible;">
                        <div style="width:auto;float:left;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                        <div style="width:auto;float:right;"><a class="fancy-button apply-search-filter" href="javascript:void(0)">Apply Filter</a></div>
                    </div>
                </asp:Panel>
	    </div>
    </div>
    <!-- /Search Filter Box -->

    <div class="cb"></div>

    <!-- Filter Tags -->
    <div id="filterTagsContainer" class="filter-tags-container not-displayed"  runat="server"></div>
    <!-- /Filter Tags -->

    <div class="cb"></div>

    <!-- Issue Tracker Grid -->
    <div style="overflow:auto; margin-bottom: 20px; min-height:580px;">

        <fx:Flexigrid ID="fgIssueTracker"
            Width="990"
            ResultsPerPage="20"
			ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			HandlerUrl="~/IssueTrackerList.axd"
            runat="server">
		    <Columns>
		        <fx:FlexiColumn Code="id" Text="Issue ID" Width="70" Format="TMIT-{0}" />
			    <fx:FlexiColumn Code="site.site_uid" Text="Site ID" Width="80" />
                <fx:FlexiColumn Code="site.site_name" Text="Site Name" Width="200"  />
		        <fx:FlexiColumn Code="issue_summary" Text="Issue Summary" Width="340" Format="<span style='display:block;padding:0 5px 0 5px; text-align:left;'>{0}</span>" />
                <fx:FlexiColumn Code="issue_priority_types.id" Text="Priority" Width="70" />
                <fx:FlexiColumn Code="created_at" Text="Created" Width="80" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="issue_statuses.id" Text="Status" Width="70" />
                <fx:FlexiColumn Code="id" Text="Actions" Width="60" AllowSort="false"
                                Format="<a class='fancy-button' href='/IssueTracker/Edit.aspx?id={0}'>Edit</a>" />
		    </Columns>
	    </fx:Flexigrid>
        <br />
        <div>
            <div id="exportLoadSpinner" style="display: block; width:25px; height:25px;margin-left:80%; float:left;"></div>
            <asp:Button ID="exportButton" CssClass="ui-button ui-widget ui-state-default ui-corner-all excelExportButton" runat="server" Text="Export to Excel" UseSubmitBehavior="false" OnClientClick="FrontExportClick('fgIssueTracker');" OnClick="ExportClick" />
        </div>
        <asp:HiddenField ID="hiddenSearchValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenColumnValue" runat="server" ClientIDMode="Static" Value="null" />
        <asp:HiddenField ID="hiddenDownloadFlag" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilterParam" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hiddenFilter" runat="server" ClientIDMode="Static" Value="" />

        <div class="cb"></div>

    </div>
    <!-- /Issue Tracker Grid -->

    <!-- Modal -->
    <div id="modalExternal" class="modal-container" style="overflow:hidden;"></div>
    <div id="modalExternalSavedFilterManage" class="modal-container" style="overflow:hidden;"></div>
    <!-- /Modal -->

</asp:Content>
