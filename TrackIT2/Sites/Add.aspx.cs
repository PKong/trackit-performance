﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.Sites
{
    public partial class Add : System.Web.UI.Page
    {
        public site CurrentSite
        {
            get
            {
                return (site)this.ViewState["CurrentSite"];
            }
            set
            {
                this.ViewState["CurrentSite"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && Request.QueryString["id"] != null && ! String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                Utility.ClearSubmitValidator();
                site objSite = BLL.Site.Search(Request.QueryString["id"]);
                if (objSite != null)
                {
                    CurrentSite = objSite;
                    BindData();
                    GetLinkDocument();                    
                }
            }
            else if(Page.IsPostBack)
            {
                // Initialise Download from Amazon
                if (Request.Params["__EVENTARGUMENT"] == "DownloadDocument")
                {
                    string keyName = hidDocumentDownload.Value;
                    if (!string.IsNullOrEmpty(keyName))
                    {
                        AmazonDMS.DownloadObject(Server.UrlDecode(keyName));
                    }
                }
            }
        }
        protected void BindData()
        {
            site objSite = CurrentSite;
            if (objSite != null)
            {
                Utility.ControlValueSetter(ddl_RogueEquip, objSite.rogue_equipment_yn);
            }
        }

        protected void btn_Save_RogueEquip_Click(object sender, EventArgs e)
        {
            if (CurrentSite != null)
            {
                using(CPTTEntities ce = new CPTTEntities())
                {
                    var querySite = from s in ce.sites
                                    where s.site_uid == CurrentSite.site_uid
                                    select s;
                    if (querySite.ToList().Count > 0)
                    {
                        site objSite = querySite.ToList()[0];
                        ce.sites.Detach(objSite);
                        objSite.rogue_equipment_yn = Utility.ConvertYNToBoolean(ddl_RogueEquip.SelectedValue);
                        ce.AttachUpdated(objSite);
                        ce.SaveChanges();

                        // Redirect to edit page with uid. Keep save button disabled
                        // to prevent double clicks.
                        ClientScript.RegisterStartupScript(GetType(), "Load",
                        String.Format("<script type='text/javascript'>" +
                                          "   $(\"#MainContent_btn_Save_RogueEquip\").val(\"Processing...\");" +
                                          "   $(\"#MainContent_btn_Save_RogueEquip\").attr(\"disabled\", true);" + 
                                          "   window.parent.location.href = 'SiteDashboard.aspx?id={0}';" +
                                          "</script>",
                        objSite.site_uid.ToString()));
                    }
                }
                
            }
        }
        #region Documnent Link
        private void GetLinkDocument()
        {

            List<Dms_Document_Site> RogueEquipment = DMSDocumentLinkHelper.GetDocumentLink(CurrentSite.site_uid);
            SetLinkDocument(CurrentSite.site_uid, RogueEquipment);

        }

        private void SetLinkDocument(String ID, List<Dms_Document_Site> docData)
        {
            //insert template data in case no documents link
            if (docData.Count == 0)
            {
                Dms_Document_Site doc = new Dms_Document_Site();
                doc.ID = CurrentSite.site_uid;
                doc.ref_field = "RogueEquipment";
                docData.Add(doc);
            }

            this.hidden_document_RogueEquipment.Value = Newtonsoft.Json.JsonConvert.SerializeObject(docData);
        }
        #endregion
    }
}