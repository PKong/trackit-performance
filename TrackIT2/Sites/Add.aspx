﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Sites | TrackiT2" Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs"
 Inherits="TrackIT2.Sites.Add" MasterPageFile="~/Templates/Modal.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/sites_add") %>
   <!--TODO: Move to use script bundle -->
   <script src="../Scripts/documentLink.js" type="text/javascript"></script>
   <script type="text/javascript">
       $(document).ready(function () {
           parent.$('#modalIframeId').height(parent.$('#modalIframeId').contents().find('.modal-page-wrapper').outerHeight(true) + 30);
           // Setup Upload Document Modal
           parent.setupDmsModal("Upload Document");
       });
   </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4>Select Equipment</h4>
    <br/>
    <asp:Label ID="lbl_RogueEquip" runat="server" Text="Rogue Equipment"></asp:Label>&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="ddl_RogueEquip" runat="server">
        <asp:ListItem Text=" - Select - " value=""></asp:ListItem>
        <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
        <asp:ListItem Text="No" Value="no"></asp:ListItem>
    </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_Save_RogueEquip" runat="server" CssClass="ui-button ui-widget ui-state-default ui-corner-all" Text="Save" UseSubmitBehavior="false" OnClick="btn_Save_RogueEquip_Click" OnClientClick="this.disabled = true; this.value = 'Processing...';" />
    
    <div class="document-link" id="RogueEquipment" >
        <asp:HiddenField  ID="hidden_document_RogueEquipment" ClientIDMode="Static" runat="server" />
    </div>
    <asp:HiddenField ID="hidDocumentDownload" runat="server" ClientIDMode="Static" />
    <div id="modalExternalDms" class="modal-container" style="overflow:hidden;"></div>
    <asp:HiddenField id="hidBasePath" ClientIDMode="Static" runat="server"/>
</asp:Content>
