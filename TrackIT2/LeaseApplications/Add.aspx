﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="Add Lease Application | TrackIT2" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="TrackIT2.LeaseApplications.Add" %>
<%@ Register TagPrefix="TrackIT2" TagName="TextFieldAutoComplete" src="~/Controls/TextFieldAutoComplete.ascx" %>
<%@ Register TagPrefix="TrackIT2" TagName="TmoAppIDFieldAutoComplete" src="~/Controls/TmoAppIDFieldAutoComplete.ascx" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_application_add") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_application_add") %>   

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="width:710px;margin: 0 auto 0 auto;">
        <div class="left-col form_element">
            <div id="divApplicationDetails" runat="server">
                <h4>Lease Application Details</h4>                
                <label ID="lblAppType" runat="server">App Type</label>
                <asp:DropDownList ID="ddlAppType" runat="server" CssClass="select-field">
                </asp:DropDownList>
                <br />
                <label ID="lblActivityType" runat="server">Activity Type</label>
                <asp:DropDownList ID="ddlActivityType" runat="server" CssClass="select-field"></asp:DropDownList>
                <br />
                <label ID="lblAppReceived" runat="server">Received</label>
                <asp:TextBox ID="txtReceived" runat="server" CssClass="datepicker"></asp:TextBox>
                <br />
                <label ID="lblAppStatus" runat="server">App Status</label>
                <asp:DropDownList ID="ddlAppStatus" runat="server" CssClass="select-field"></asp:DropDownList>
                <label ID="lblAppStatusDate" runat="server">App Status Date</label>
                <asp:TextBox ID="txtAppStatusDate" runat="server" CssClass="datepicker" Style="margin-bottom: 10px;"></asp:TextBox>
                <br />
            </div>
            <div id="divCustomerDetails" runat="server">
                <h4>Customer Details</h4>
                <label ID="lblCustomer" runat="server">
                    Customer</label>
                <asp:DropDownList ID="ddlCustomer" CssClass="select-field" runat="server"></asp:DropDownList><br />
                <label ID="lblCarrierProject" runat="server">Carrier Project</label>
                <asp:DropDownList ID="ddlCarrierProject" runat="server" CssClass="select-field"></asp:DropDownList>
                <label ID="lblPriority" runat="server">Priority</label>
                <asp:DropDownList ID="ddlPriority" runat="server" CssClass="select-field"></asp:DropDownList>
                <label ID="lblCustomerUID" runat="server">Customer UID</label>
                <asp:TextBox ID="txtCustomerUID" runat="server"></asp:TextBox><br />
                <label ID="lblCustomerSiteName" runat="server">Customer Site Name</label>
                <asp:TextBox ID="txtCustomerSiteName" runat="server"></asp:TextBox><br />
                <label ID="lblOriginalCarrier" runat="server">Original Carrier</label>
                <asp:TextBox ID="txtOriginalCarrier" runat="server"></asp:TextBox><br />                
            </div>
            <div id="divRiskLevel" runat="server">
               <h4>Risk Level</h4>
               <label ID="lblRisk" runat="server">
                  Risk Level</label>
               <asp:DropDownList ID="ddlRiskLevel" runat="server" CssClass="select-field"></asp:DropDownList><br />
               <label id="lblRiskLevelNotes" runat="server">Notes</label>
               <br />
               <asp:TextBox ID="txtRiskLevelNotes" runat="server" CssClass="risklevel-notes" TextMode="MultiLine" Rows="3" Width="200"></asp:TextBox><br />
            </div>
        </div>
        <div class="right-col form_element">
            <div id="divSite" runat="server">
                <h4>Site</h4>
                <label>Select Site</label>
                <TrackIT2:TextFieldAutoComplete ID="ctrSiteID"
                                        LabelFieldClientID="lblSiteID"
                                        LabelFieldValue=""
                                        LabelFieldCssClass="label"
                                        TextFieldClientID="txtSiteID"
                                        TextFieldCssClass="input-autocomplete"
                                        TextFieldWidth="188"
                                        ScriptKeyName="ScriptAutoCompleteSiteID"
                                        DataSourceUrl="SiteIdAutoComplete.axd"
                                        ClientIDMode="Static"
                                        runat="server" />
                <p style="float: left; width: 90%; padding: 5px 5px 0; text-align: left; text-decoration: italic; font-size: 12px; font-style:italic; margin-bottom:20px;color:#666;">
                    Note: Site ID, Name, Address and Structure Details will show up as a result of selecting the Site.</p>
            </div>
            <div id="divTMOApp" runat="server">
               <h4>TMO Application</h4>
               <TrackIT2:TmoAppIDFieldAutoComplete ID="txtTMOAppId"
                                        LabelFieldClientID="lblTMOAppId"
                                        LabelFieldValue="TMO App Id"
                                        LabelFieldCssClass="label"
                                        TextFieldClientID="txtTMOAppId"
                                        TextFieldCssClass="input-autocomplete"
                                        TextFieldWidth="188"
                                        ScriptKeyName="ScriptAutoCompleteTmoAppID"
                                        DataSourceUrl="TMOAppIdAutoCompleteHandler.axd"
                                        ClientIDMode="Static"
                                        runat="server"
                                        TargetTextField="txtSiteID"
                                        />
            </div>
            <div id="divTeamAssigned" runat="server">
                <h4>Team Assigned</h4>
                <label ID="lblInitSpecialist" runat="server">Initial Specialist</label>
                <asp:DropDownList ID="ddlInitialSpecialist" runat="server" CssClass="select-field">
                </asp:DropDownList><br />
                <label ID="lblCurrentSpecialist" runat="server">Current Specialist</label>
                <asp:DropDownList ID="ddlCurrentSpecialist" runat="server" CssClass="select-field">
                </asp:DropDownList><br />
                <label ID="lblProjectManager" runat="server">Project Manager</label>
                <asp:DropDownList ID="ddlProjectManager" runat="server" CssClass="select-field">
                </asp:DropDownList><br />
                <label ID="lblColocationCoordinator" runat="server">Colocation Coordinator</label>
                <asp:DropDownList ID="ddlColocationCoordinator" runat="server" CssClass="select-field">
                </asp:DropDownList>
                <label ID="lblAccountManager" runat="server">Account Manager</label>
                <asp:DropDownList ID="ddlAccountManager" runat="server" CssClass="select-field">
                </asp:DropDownList>
            </div>
            <div id="divPaymentDetails" runat="server">
                <h4>Payment Details</h4>
                <label ID="lblFeeReceived" runat="server">Fee Received</label>
                <asp:TextBox ID="txtFeeReceived" runat="server" CssClass="datepicker"></asp:TextBox><br />
                <label ID="lblFeeAmount" runat="server">Fee Amount</label>
                <asp:TextBox ID="txtFeeAmount" CssClass="moneyfield" runat="server"></asp:TextBox><br />
                <label ID="lblCheckNo" runat="server">Check #</label>
                <asp:TextBox ID="txtCheck" runat="server"></asp:TextBox>                                
            </div>   
        </div> 
        <asp:Button ID="btnSubmitCreateApps" runat="server" CssClass="buttonLASubmit ui-button ui-widget ui-state-default ui-corner-all" UseSubmitBehavior="false" Text="Create" OnClick="btnAdd_Click" OnClientClick="if (!validateCreateApp()) { return false;}" />                
    </div>
</asp:Content>
