﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="LASummaryInfo.aspx.cs" Inherits="TrackIT2.LeaseApplications.Summary.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_application_summary") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_application_summary") %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="width:700px;margin: 0 auto 0 auto;">
        <h4>Summary</h4>
        <div class="left-col form_element">
            <asp:Label ID="lbl_antenna_count" runat="server">Antenna Count</asp:Label>
            <asp:TextBox ID="txt_antenna_count" runat="server"/><br/>

            <asp:Label ID="lbl_antenna_dimensions" runat="server">Antenna dimensions</asp:Label>
            <asp:TextBox ID="txt_antenna_dimensions" runat="server"/><br/>

            <asp:Label ID="lbl_antenna_weight" runat="server">Antenna Weight</asp:Label>
            <asp:TextBox ID="txt_antenna_weight" runat="server"/><br/>

            <asp:Label ID="lbl_antenna_rad" runat="server">Antenna RAD</asp:Label>
            <asp:TextBox ID="txt_antenna_rad" runat="server"/><br/>

            <asp:Label ID="lbl_antenna_mount_type" runat="server">Antenna Mount Type</asp:Label>
            <asp:TextBox ID="txt_antenna_mount_type" runat="server"/><br/>

            <asp:Label ID="lbl_mw_diameter" runat="server">MW Diameter</asp:Label>
            <asp:TextBox ID="txt_mw_diameter" runat="server"/><br/>

            <asp:Label ID="lbl_mw_rad" runat="server">MW RAD</asp:Label>
            <asp:TextBox ID="txt_mw_rad" runat="server"/><br/>           
        </div>
        <div class="right-col form_element">
            <asp:Label id="lbl_line_count" runat="server">Line Count</asp:Label>
            <asp:TextBox ID="txt_line_count" runat="server"/><br/>

            <asp:Label ID="lbl_line_diameter" runat="server">Line Diameter</asp:Label>
            <asp:TextBox ID="txt_line_diameter" runat="server"/><br/>

            <asp:Label ID="lbl_line_length" runat="server">Line Length</asp:Label>
            <asp:TextBox ID="txt_line_length" runat="server"/><br/>

            <asp:Label id="lbl_lna_rdu_count" runat="server">LNA RDU Count</asp:Label>
            <asp:TextBox ID="txt_lna_rdu_count" runat="server"/><br/>

            <asp:Label ID="lbl_lna_rdu_dimensions" runat="server">LNA RDU Dimensions</asp:Label>
            <asp:TextBox ID="txt_lna_rdu_dimensions" runat="server"/><br/>

            <asp:Label ID="lbl_lna_rdu_weight" runat="server">LNA RDU Weight</asp:Label>
            <asp:TextBox ID="txt_lna_rdu_weight" runat="server"/><br/>

            <asp:Label ID="lbl_lna_rdu_rad" runat="server">LNA RDU RAD</asp:Label>
            <asp:TextBox ID="txt_lna_rdu_rad" runat="server"/><br/>

            <asp:Label ID="lbl_ground_expansion_required_yn" runat="server">Ground Expansion Required</asp:Label>
            <asp:DropDownList ID="ddl_ground_expansion_required_yn" runat="server">
                <asp:ListItem Value="" Text="- Select -"></asp:ListItem>
                <asp:ListItem Value="yes" Text="Yes"></asp:ListItem>
                <asp:ListItem Value="no" Text="No"></asp:ListItem>
            </asp:DropDownList><br/>            
        </div>
        
        <div class="form_element">
           <asp:Label id="lbl_other_equipment" runat="server" style="float:left;">Other Equipment</asp:Label>
           <br />
           <asp:TextBox ID="txt_other_equipment" runat="server" TextMode="MultiLine" Rows="7" style="width: 565px; max-width:565px; float:left; border: 1px solid #e1e1e1;" />
        </div>

        <br />

        <asp:Button ID="btn_submit_edit_summary" runat="server" CssClass="buttonSubmit" Text="Save"
            OnClick="btn_submit_edit_summary_Click" OnClientClick="return validateEditSummary();"/>
        <div class="cb"></div>
    </div>
</asp:Content>
