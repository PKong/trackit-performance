﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="LAClientInfo.aspx.cs" Inherits="TrackIT2.LeaseApplications.CustomerContacts.Edit" %>
<%@ MasterType TypeName="TrackIT2.Templates.Modal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_application_client_info") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_application_client_info") %>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="message_alert"></div>
    <div class="create-container" style="width:350px;margin: 0 auto 0 auto;">
        <h4>Customer Contacts</h4>
        <div class="left-col form_element">
            <asp:Label ID="lbl_site_acq_contact_name" runat="server">Name</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_name" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_phone" runat="server">Phone</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_phone" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_email" runat="server">Email</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_email" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_company" runat="server">Company</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_company" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_address" runat="server">Address</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_address" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_city" runat="server">City</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_city" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_state" runat="server">State</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_state" runat="server"/><br/>
            <asp:Label ID="lbl_site_acq_contact_zip" runat="server">Zip Code</asp:Label>
            <asp:TextBox ID="txt_site_acq_contact_zip" runat="server"/><br/>
        </div>
        
        <div class="cb"></div>

        <asp:Button ID="btn_submit_edit_contact" runat="server" 
                    Text="Save"
                    CssClass="buttonSubmit ui-button ui-widget ui-state-default ui-corner-all"
                    OnClick="btn_submit_edit_contact_Click" 
                    OnClientClick="if (!validateEditCustomerContacts()) {return false;}" 
                    UseSubmitBehavior="false"/>
        <div class="cb"></div>
    </div>
</asp:Content>