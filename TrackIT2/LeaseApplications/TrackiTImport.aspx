﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="TrackiTImport.aspx.cs" Inherits="TrackIT2.LeaseApplications.TrackiTImport" %>
<%@ MasterType TypeName="TrackIT2.Templates.TrackIT2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Styles -->
   <%: Styles.Render("~/Styles/trackit_import")%>
    
   <!-- Scripts -->
    
   <%: Scripts.Render("~/Scripts/trackit_import")%>
   <script src="../Scripts/list.view.js" type="text/javascript"></script>
   <script type="text/javascript" src="../Scripts/saved.filters.js"></script>
   <script src="../Scripts/global.search.js" type="text/javascript"></script>
   <script type="text/javascript" src="../Scripts/activity-indicator.js"></script>
   <script type="text/javascript" src="../Scripts/import.application.js"></script>

   <script type="text/javascript" >

       // ADV Search Var
       var GETdatas = new Array();
       var blnHaveGET = false;
       var blnExec = false;
       currentFlexiGridID = '<%= fgImportApplications.ClientID %>';
       searchFilterFields = "";

       // Doc Ready
       $(function () {
           searchFilterFields = "MainContent_txtSearch";
           $('#MainContent_txtSearch').bind("keydown", function (event) {
               if (event.keyCode == 13) {
                   removeFilterPostParam('search');
                   addFilterPostParam('search', $('#MainContent_txtSearch').val());
                   isNewSearchFilter = true;
                   reloadDataGrid();
               }
           });
       });

       function closeDialog() {
           removeFilterPostParam('search');
           addFilterPostParam('search', $('#MainContent_txtSearch').val());
           isNewSearchFilter = false;
           reloadDataGrid();
       }
       //-->

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="h1" style="float:left; padding-top:3px; padding-right:30px;"></h1>
    <div>
        <asp:Label CssClass="label" ID="lblSearch" Text="Search" runat="server" style="margin-left:430px;" ></asp:Label>
        <asp:TextBox ID="txtSearch" runat="server" Width="160px" ></asp:TextBox>
    </div>
    <!-- Lease Applications Grid -->
    <div style="overflow:visible; margin-bottom: 20px; min-height:400px; position:relative;">

           <fx:Flexigrid ID="fgImportApplications"
            Width="750"
            ResultsPerPage="10"
			ShowPager="true"
            Resizable="false"
            ShowToggleButton="false"
            ShowTableToggleButton="false"
            SearchEnabled="false"
            UseCustomTheme="true"
            CssClass="tmobile"
            WrapCellText="true"
            DoNotIncludeJQuery="true"
            OnClientBeforeSendData="fgBeforeSendData"
            OnClientDataLoad="fgDataLoad"
            OnClientRowClick="fgRowClick"
            OnClientNoDataLoad="fgNoData"
			HandlerUrl="~/ImportApplicationsList.axd"
            runat="server">
		    <Columns>
			    <fx:FlexiColumn Code="CreateDate" Text="App Started" Width="165"  OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="StartDate" Text="DBA" Width="150" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="SiteID" Text="Site" Width="108" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="ApplicationType" Text="Type" Width="101"  OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="View" Text=" " Width="70" AllowSort="false" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="Import" Text=" " Width="70" AllowSort="false" OnRowRender="fgRowRender" />
                <fx:FlexiColumn Code="Hidden" Text=" " Width="70" AllowSort="false" OnRowRender="fgRowRender" />
		    </Columns>
	    </fx:Flexigrid>
        <br />
        <div class="cb"></div>
    </div>
</asp:Content>
