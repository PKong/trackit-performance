﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Modal.Master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="TrackIT2.LeaseApplications.History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   <!-- Styles -->
   <%: Styles.Render("~/Styles/lease_application_history") %>   
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/lease_application_history") %>

    <script type="text/javascript" language="javascript">
        // Doc Ready
        $(document).ready(function () {
            $(function () {
                if ($('#ApplicationID').val() != "null" && $('#ApplicationID').val() != undefined) {
                    $("#history-tabs").tabs({ selected: 2 });
                    $('#MainContent_pnlNoHistory').css('display', 'none');

                    $('#leaseAppHisTab').css('display', 'none');
                    $('#saHisTab').css('display', 'none');
                    $('#equipThisTab').css('display', 'block');
                }
                else {
                    $("#history-tabs").tabs({ selected: 0 });

                    $('#leaseAppHisTab').css('display', 'block');
                    $('#saHisTab').css('display', 'block');
                    $('#equipThisTab').css('display', 'none');
                }

            });

            // Bind Table Sorting for Lease Applications
            $("#la_history_table").tablesorter({
                headers:
                {
                    // Disable sortable on column "Old", "New"
                    3: { sorter: false },
                    4: { sorter: false }
                }
            });

            // Bind Table Sorting for Structural Analysis

            $("#sa_history_table").tablesorter({
                headers:
                {
                    // Disable sortable on column "Old", "New"
                    4: { sorter: false },
                    5: { sorter: false }
                }
            });

            // Bind Table Sorting for Equipment
            $("#equip_history_table").tablesorter();

        });
    </script>

    <style type="text/css">
        .tableContainer
        {
            height: 100%;
            overflow-x: hidden;
            overflow-y: auto;
        }
        
        div.tableContainer table
        {
            background-color: white;
            overflow-x: hidden;
            overflow-y: auto;
            width: 100%;
        }
        
        .th-inner
        {
            overflow-y: hidden;
            line-height: 30px;
            text-align: left;
            margin-left: -5px;
            padding-left: 5px;
            position: absolute;
            text-align: left;
            top: 0;
        }
        
        .header .th-inner
        {
            width: 100%;
        }
        
        .sortArrow
        {
            padding: 1px 10px;
            line-height: 30px;
        }
        
        /* Override table header and sortable sign img, work on all=browser */
        
        table.mytable
        {
            background-color: white;
            overflow-x: hidden;
            overflow-y: auto;
            width: 100%;
        }
        
        .mytable th.header, .mytable th.headerSortUp, .mytable th.headerSortDown
        {
            background: white;
        }
        
        .mytable th.header .sortArrow
        {
            background: url("/Styles/images/asc_desc.gif") no-repeat scroll right center transparent;
            cursor: pointer;
        }
        .mytable th.headerSortUp .sortArrow
        {
            background-image: url("/Styles/images/asc.gif");
            background-position: 100% 50%;
        }
        .mytable th.headerSortDown .sortArrow
        {
            background-image: url("/Styles/images/desc.gif");
            background-position: 100% 50%;
        }
        
        .headerSortUp .th-inner, .headerSortDown .th-inner
        {
            /*background-color: white;*/
        }
        
        .header .th-inner
        {
            /*background-color: white;*/
        }
        
        .header-background
        {
            background-color: #D5ECFF;
            height: 30px;
            left: 0;
            position: absolute;
            right: 0;
            top: 0;
        }
        
        .history-container
        {
            width: 100%;
            margin: 0 auto;
            /* Reduce height to meet the height of tab */
            height: 330px;
            background-color: white;
            margin: 10px auto;
            padding-top: 30px;
            position: relative;
        }
       .application-history .ui-tabs .ui-tabs-panel
        {
            margin-top: 0px !important;
        }
        .application-history .ui-tabs .ui-tabs-nav li
        {
            top: 3px; 
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlNoHistory" runat="server">
        <h2 style="text-indent: 15px;">
            No history found.</h2>
    </asp:Panel>
    <asp:Panel ID="pnlHistory" CssClass="application-history" runat="server">
        <div id="history-tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
            <ul class="ui-tabs ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
                <li id="leaseAppHisTab"><a href="#leaseapplicationHistory">Lease Application History</a> </li>
                <li id="saHisTab"><a href="#structuralanalysisHistory">Structural Analysis History</a> </li>
                <li id="equipThisTab"><a href="#equipmentHistory">Equipment Information History</a> </li>
            </ul>
            <div id="leaseapplicationHistory" style="margin-top: 0;">
                <div class="create-container history-container" style="">
                    <div class="all-comments all-history tableContainer" id="leaseAppTableContainer">
                        <div id="divNoLAHistory" runat="server" clientidmode="Static">
                            <h2>
                                No lease application history found.</h2>
                        </div>
                        <asp:Repeater ID="rptLAHistory" runat="server">
                            <HeaderTemplate>
                                <table id="la_history_table" width="900px" cellspacing="0" cellpadding="0" class="sort-me all-comments-table mytable">
                                    <thead>
                                        <tr class="">
                                            <th scope="col" style="width: 150px;">
                                                <div class="th-inner" style="text-indent: 55px;">
                                                    <span>User </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 100px;">
                                                <div class="th-inner" style="text-indent: 35px;">
                                                    <span>Date </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 65px;">
                                                    <span>Field Changed </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 100px;">
                                                    <span>Old </span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 100px;">
                                                    <span>New </span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <div class="cb"></div>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <div class="author-info">
                                            <img id="imgAvatar" runat="server" alt="Employee Profile Image" src="" />
                                            <asp:Label ID="lblEmployee" runat="server" Text="" />
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDate" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFieldChanged" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOld" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblNew" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div id="structuralanalysisHistory" style="margin-top: 0;">
                <div class="create-container history-container" style="">
                    <div class="all-comments all-history tableContainer" id="saTableContainer">
                        <div id="divNoSaHistory" runat="server" clientidmode="Static">
                            <h2 style="text-indent: 15px;">
                                No structural analysis history found.</h2>
                        </div>
                        <asp:Repeater ID="rptSAHistory" runat="server">
                            <HeaderTemplate>
                                <table id="sa_history_table" width="900px" cellspacing="0" cellpadding="0" class="sort-me all-comments-table mytable">
                                    <thead>
                                        <tr class="">
                                            <th scope="col" style="width: 150px;">
                                                <div class="th-inner" style="text-indent: 55px;">
                                                    <span>User </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 100px;">
                                                <div class="th-inner" style="text-indent: 35px;">
                                                    <span>SA Id </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 100px;">
                                                <div class="th-inner" style="text-indent: 35px;">
                                                    <span>Date </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 65px;">
                                                    <span>Field Changed </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 170px;">
                                                <div class="th-inner" style="text-indent: 80px;">
                                                    <span>Old </span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 170px;">
                                                <div class="th-inner" style="text-indent: 75px;">
                                                    <span>New </span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <div class="cb">
                                    </div>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <div class="author-info">
                                            <img id="imgSAAvatar" runat="server" alt="Employee Profile Image" src="" />
                                            <asp:Label ID="lblSAEmployee" runat="server" Text="" />
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSAGroupId" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSADate" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSAFieldChanged" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSAOld" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSANew" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="ApplicationID" runat="server" ClientIDMode="Static" Value="null" />

            <div id="equipmentHistory" style="margin-top: 0;" visibility="false">
                <div class="create-container history-container" style="width:940px;">
                    <div class="all-comments all-history tableContainer" id="Div2" >
                        <div id="divNoEquipMentHistory" runat="server" clientidmode="Static">
                            <h2 style="text-indent: 15px;">
                                No equipment information history found.
                            </h2>
                        </div>
                        <asp:Repeater ID="equipHistoryRepeater" runat="server">
                            <HeaderTemplate>
                                <table id="equip_history_table" width="900px" cellspacing="0" cellpadding="0" class="sort-me all-comments-table mytable">
                                    <thead>
                                        <tr class="">
                                            <th scope="col" style="width: 150px;">
                                                <div class="th-inner" style="text-indent: 25px;">
                                                    <span>Equipment Type </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 150px;">
                                                <div class="th-inner" style="text-indent: 40px;">
                                                    <span>Operation </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 100px;">
                                                <div class="th-inner" style="text-indent: 30px;">
                                                    <span>Date </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 65px;">
                                                    <span>Field Changed </span><span class="sortArrow">&nbsp;</span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 80px;">
                                                    <span>Old </span>
                                                </div>
                                            </th>
                                            <th scope="col" style="width: 220px;">
                                                <div class="th-inner" style="text-indent: 85px;">
                                                    <span>New </span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <div class="cb"></div>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <div class="author-info">
                                            <asp:Label ID="lblEquipmentType" runat="server" Text="" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="author-info">
                                            <asp:Label ID="lblEquipmentOperation" runat="server" Text="" />
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEquipmentChangeDate" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEquipmentFieldChanged" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEquipmentOldValue" runat="server" Text="" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEquipmentNewValue" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody></table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
