﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text;

namespace TrackIT2.LeaseApplications.CustomerContacts
{
    public partial class Edit : System.Web.UI.Page
    {
        public lease_applications CurrentLeaseApplication
        {
            get
            {
                return (lease_applications)this.ViewState["CurrentLeaseApplication"];
            }
            set
            {
                this.ViewState["CurrentLeaseApplication"] = value;
            }
        }

        private Hashtable m_htVarCharInvalidField;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //this.InitialControl();
                int iLeaseAppID = 0;
                Utility.ClearSubmitValidator();
                //this.BindLookupLists();
                if (Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out iLeaseAppID))
                {
                    LoadData(iLeaseAppID);
                    //btnSubmitCreateApps.Text = "Save";
                }
            }
        }

      protected void LoadData(int leaseAppID)
      {
         lease_applications app = LeaseApplication.Search(leaseAppID);
         CurrentLeaseApplication = app;
         ProjectCategory category = LeaseApplication.GetProjectCategory(app);
         List<metadata> viewFields = MetaData.GetViewableFieldsByCategory(category);

         if (app != null)
         {
            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_address"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_address, app.site_acq_contact_address);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_address"))
               {
                  txt_site_acq_contact_address.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_address.Visible = false;
               this.txt_site_acq_contact_address.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_city"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_city, app.site_acq_contact_city);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_city"))
               {
                  txt_site_acq_contact_city.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_city.Visible = false;
               this.txt_site_acq_contact_city.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_company"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_company, app.site_acq_contact_company);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_company"))
               {
                  txt_site_acq_contact_company.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_company.Visible = false;
               this.txt_site_acq_contact_company.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_email"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_email, app.site_acq_contact_email);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_email"))
               {
                  txt_site_acq_contact_email.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_email.Visible = false;
               this.txt_site_acq_contact_email.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_name"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_name, app.site_acq_contact_name);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_name"))
               {
                  txt_site_acq_contact_name.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_name.Visible = false;
               this.txt_site_acq_contact_name.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_phone"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_phone, app.site_acq_contact_phone);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_phone"))
               {
                  txt_site_acq_contact_phone.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_phone.Visible = false;
               this.txt_site_acq_contact_phone.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_state"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_state, app.site_acq_contact_state);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_state"))
               {
                  txt_site_acq_contact_state.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_state.Visible = false;
               this.txt_site_acq_contact_state.Visible = false;
            }

            if (Security.IsFieldViewable(viewFields, "lease_applications", "site_acq_contact_zip"))
            {
               Utility.ControlValueSetter(this.txt_site_acq_contact_zip, app.site_acq_contact_zip);

               if (!Security.UserCanEditField(User.Identity.Name, "lease_applications", "site_acq_contact_zip"))
               {
                  txt_site_acq_contact_zip.Enabled = false;
               }
            }
            else
            {
               this.lbl_site_acq_contact_zip.Visible = false;
               this.txt_site_acq_contact_zip.Visible = false;
            }                                 
          }
      }

        protected void btn_submit_edit_contact_Click(Object sender, EventArgs e)
        {
            if (CurrentLeaseApplication != null)
            {
                Boolean blnIsValid = true;
                lease_applications app = CurrentLeaseApplication;
                m_htVarCharInvalidField = new Hashtable();

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_address.Text), app, m_htVarCharInvalidField, "site_acq_contact_address", "Address");
                if (blnIsValid) app.site_acq_contact_address = Utility.PrepareString(this.txt_site_acq_contact_address.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_city.Text), app, m_htVarCharInvalidField, "site_acq_contact_city", "City");
                if (blnIsValid) app.site_acq_contact_city = Utility.PrepareString(this.txt_site_acq_contact_city.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_company.Text), app, m_htVarCharInvalidField, "site_acq_contact_company", "Company");
                if (blnIsValid) app.site_acq_contact_company = Utility.PrepareString(this.txt_site_acq_contact_company.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_email.Text), app, m_htVarCharInvalidField, "site_acq_contact_email", "E-Mail");
                if (blnIsValid) app.site_acq_contact_email = Utility.PrepareString(this.txt_site_acq_contact_email.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_name.Text), app, m_htVarCharInvalidField, "site_acq_contact_name", "Name");
                if (blnIsValid) app.site_acq_contact_name = Utility.PrepareString(this.txt_site_acq_contact_name.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_phone.Text), app, m_htVarCharInvalidField, "site_acq_contact_phone", "Phone");
                if (blnIsValid) app.site_acq_contact_phone = Utility.PrepareString(this.txt_site_acq_contact_phone.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_state.Text), app, m_htVarCharInvalidField, "site_acq_contact_state", "State");
                if (blnIsValid) app.site_acq_contact_state = Utility.PrepareString(this.txt_site_acq_contact_state.Text);

                blnIsValid = blnIsValid & Utility.ValidateVarchar(Utility.PrepareString(this.txt_site_acq_contact_zip.Text), app, m_htVarCharInvalidField, "site_acq_contact_zip", "Zip");
                if (blnIsValid) app.site_acq_contact_zip = Utility.PrepareString(this.txt_site_acq_contact_zip.Text);

                if (blnIsValid)
                {
                    if (String.IsNullOrEmpty(LeaseApplication.Update(app)))
                    {
                        Master.StatusBox.showStatusMessage("Update Complete.");

                        // Redirect to edit page with created id. Keep save button disabled
                        // to prevent double clicks.
                        ClientScript.RegisterStartupScript(GetType(), "Load",
                        String.Format("<script type='text/javascript'>" +
                                      "   $(\"#MainContent_btn_submit_edit_contact\").val(\"Processing...\");" + "   $(\"#MainContent_btnSubmitCreateApps\").attr(\"disabled\", true);" +
                                      "   window.parent.location.href = 'Edit.aspx?id={0}';" +
                                      "</script>",
                                      app.id.ToString()));
                    }
                    else
                    {
                        //Do msgbox here.
                        Master.StatusBox.showStatusMessage("Unexpected error occur.");
                    }
                }
                else
                {
                    StringBuilder sErrorMsg = new StringBuilder(Globals.ERROR_HEADER);
                    foreach (string item in m_htVarCharInvalidField.Keys)
                    {
                        if (m_htVarCharInvalidField[item] != null)
                        {
                            sErrorMsg.AppendLine(String.Format(Globals.ERROR_INVALID_LENGTH_FORMAT, item, m_htVarCharInvalidField[item]));
                        }
                    }

                    Master.StatusBox.showStatusMessage(sErrorMsg.ToString());
                }
                
                
            }
        }
        
    }
}