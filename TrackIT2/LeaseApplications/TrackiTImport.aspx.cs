﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TrackIT2.LeaseApplications
{
    public partial class TrackiTImport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.User.IsInRole("Administrator"))
                {
                    int iExtenWidth = ((fgImportApplications.Columns[6].Width)/5)-2;
                    fgImportApplications.Columns[0].Width = fgImportApplications.Columns[0].Width + iExtenWidth;
                    fgImportApplications.Columns[1].Width = fgImportApplications.Columns[1].Width + iExtenWidth;
                    fgImportApplications.Columns[2].Width = fgImportApplications.Columns[2].Width + iExtenWidth;
                    fgImportApplications.Columns[3].Width = fgImportApplications.Columns[3].Width + iExtenWidth;
                    fgImportApplications.Columns[4].Width = fgImportApplications.Columns[4].Width + iExtenWidth;
                    fgImportApplications.Columns[5].Width = fgImportApplications.Columns[5].Width + iExtenWidth;
                    fgImportApplications.Columns[6].IsVisible = false;
                }
            }
        }
    }
}