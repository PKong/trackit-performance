﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;

namespace TrackIT2.LeaseApplications
{
   public partial class Versions : System.Web.UI.Page
   {
      protected void Page_Load(object sender, EventArgs e)
      {
         if (!Page.IsPostBack)
         {
            int leaseAppId;
            bool isValidId = Int32.TryParse(Request.QueryString["Id"],
                                            out leaseAppId);

            if (isValidId)
            {
               pnlNoId.Visible = false;
               pnlVersions.Visible = true;

               Session["sortColumn"] = VersionLog.ComparisonType.Version;
               Session["sortDirection"] = "ASC";

               BindVersions(leaseAppId);
            }
            else
            {
               pnlVersions.Visible = false;
               pnlNoId.Visible = true;
            }
         }
      }

      protected void gvVersions_RowDataBound(object sender, GridViewRowEventArgs e)
      {
         /* GridView Row has following column layout
          * 0 - Actions (Edit)
          * 1 - Revision
          * 2 - Updated By
          * 3 - Updated At
          * 4 - Field Changed
          * 5 - Old Value
          * 6 - New Value
          */         
         if (e.Row.RowType == DataControlRowType.Header)
         {
            LinkButton sortLink;
            string sortBy = Session["sortColumn"].ToString();
            string sortDir = Session["sortDirection"].ToString();            

            // Use the HTML safe codes for the up arrow ▲ and down arrow ▼.
            string sortArrow = sortDir == "ASC" ? "&#9650;" : "&#9660;";
            
            // GridView rows with sortable columns will have a linkbutton
            // generated. Compare to the CommandArgument since this is what
            // we set our sortColumn based on.
            foreach (System.Web.UI.WebControls.TableCell currCell in e.Row.Cells)
            {
               if (currCell.HasControls())
               {
                  sortLink = ((LinkButton) currCell.Controls[0]);
                  
                  if (sortLink.CommandArgument == sortBy)
                  {
                     sortLink.Text = sortLink.Text + " " + sortArrow;
                  }
               }
            }
         }

         if (e.Row.RowType == DataControlRowType.DataRow)
         {
            VersionLog versionLog = (VersionLog) e.Row.DataItem;

            e.Row.Cells[1].Text = versionLog.version.ToString();
            e.Row.Cells[2].Text = versionLog.updatedBy;
            e.Row.Cells[3].Text = versionLog.updatedAt.ToString("MM/dd/yyyy hh:mm:ss tt");
            e.Row.Cells[4].Text = versionLog.fieldChanged;

            if (versionLog.oldValue != null)
            {
               e.Row.Cells[5].Text = versionLog.oldValue;
            }
            
            if (versionLog.newValue != null)
            {
               e.Row.Cells[6].Text = versionLog.newValue;
            }            
         }
      }

      protected void BindVersions(int id)
      {
         List<VersionLog> versions = new List<VersionLog>();
         versions = LeaseApplicationVersion.GenerateVersionLog(id);
         string sortDirection = Session["sortDirection"].ToString();

         if (Session["sortColumn"] != null)
         {
            VersionLog.ComparisonType sortBy = (VersionLog.ComparisonType)Session["sortColumn"];
            versions.Sort(new VersionLog(sortBy));

            if (sortDirection == "DESC")
            {
               versions.Reverse();
            }            
         }

         if (versions.Count > 0)
         {
            gvVersions.DataSource = versions;
            gvVersions.DataBind();
            
            lblNoVerions.Visible = false;
            gvVersions.Visible = true;
         }
         else
         {
            gvVersions.Visible = false;
            lblNoVerions.Visible = true;
         }
      }

      protected void gvVersions_Sorting(object sender, GridViewSortEventArgs e)
      {
         string currDirection = Session["sortDirection"].ToString();
         
         VersionLog.ComparisonType newSort = 
               (VersionLog.ComparisonType) Enum.Parse(typeof(VersionLog.ComparisonType), e.SortExpression);
         
         VersionLog.ComparisonType currSort = (VersionLog.ComparisonType) Session["sortColumn"];

         Session["sortColumn"] = newSort;

         // If we're sorting a new column, reset the direction to ascending.
         // Otherwise flip the sort direction.
         if (newSort != currSort)
         {
            Session["sortDirection"] = "ASC";  
         }
         else
         {
            Session["sortDirection"] = currDirection == "ASC" ? "DESC" : "ASC"; 
         }

         BindVersions(Int32.Parse(Request.QueryString["Id"]));
      }
   }
}