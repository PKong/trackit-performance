﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackIT2.BLL;
using TrackIT2.DAL;
using System.Web.UI.HtmlControls;

namespace TrackIT2.LeaseApplications
{
    public partial class History : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!Page.IsPostBack)
            {
                int leaseAppId;
                bool isValidId = Int32.TryParse(Request.QueryString["Id"], out leaseAppId);

                if (isValidId)
                {
                    if (string.IsNullOrEmpty(Request.QueryString["ApplicationID"]))
                    {
                        ApplicationID.Value = "null";
                        BindHistory(leaseAppId);
                    }
                    else
                    {
                        ApplicationID.Value = Request.QueryString["ApplicationID"];
                        int applicationID = 0;
                        if (int.TryParse(ApplicationID.Value, out applicationID))
                        {
                            BindEquipMentHistory(applicationID);
                        }
                        else
                        {
                            divNoEquipMentHistory.Visible = true;
                        }
                    }
                }                
            }
        }

        protected void BindEquipMentHistory(int applicationID)
        {
            List<t_ChangeLog> equipLog = new List<t_ChangeLog>();
            equipLog = EquipmentChangeLog.GetEquipmentChangeLog(applicationID);
            equipLog.Reverse();

            if (equipLog.Count > 0)
            {
                divNoEquipMentHistory.Visible = false;
                equipHistoryRepeater.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_EquipmentHistory);
                equipHistoryRepeater.DataSource = equipLog;
                equipHistoryRepeater.DataBind();
            }
            else
            {
                divNoEquipMentHistory.Visible = true;
                equipHistoryRepeater.Visible = false;
            }
        }

        protected void BindHistory(int leaseAppId)
        {
           List<VersionLog> laHistory = new List<VersionLog>();
           List<VersionLog> saHistory = new List<VersionLog>();
           
           // Retrieve lease app and structural analysis history. Sort in 
           // reverse order based on the updated date.
           laHistory = LeaseApplicationVersion.GenerateVersionLog(leaseAppId);
           laHistory.Sort(new VersionLog(VersionLog.ComparisonType.UpdatedAt));
           laHistory.Reverse();

           saHistory = StructuralAnalysisVersion.GenerateVersionLogByLeaseApplication(leaseAppId);
           saHistory.Sort(new VersionLog(VersionLog.ComparisonType.UpdatedAt));
           saHistory.Reverse();
                                                                        
           if (laHistory.Count > 0 || saHistory.Count > 0)
           {
              pnlNoHistory.Visible = false;
              pnlHistory.Visible = true;

              if (laHistory.Count > 0)
              {
                 rptLAHistory.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_LeaseAppsHistory);
                 rptLAHistory.DataSource = laHistory;
                 rptLAHistory.DataBind();

                 divNoLAHistory.Style.Add("display", "none");
                 rptLAHistory.Visible = true;
              }
              else
              {
                 rptLAHistory.Visible = false;
                 divNoLAHistory.Style.Add("display", "block");                 
              }

              if (saHistory.Count > 0)
              {
                 rptSAHistory.ItemDataBound += new RepeaterItemEventHandler(rptComments_ItemDataBound_SAHistory);
                 rptSAHistory.DataSource = saHistory;
                 rptSAHistory.DataBind();

                 divNoSaHistory.Style.Add("display", "none");
                 rptSAHistory.Visible = true;
              }
              else
              {
                 rptSAHistory.Visible = false;
                 divNoSaHistory.Style.Add("display", "block");                 
              }
           }
           else
           {
              pnlHistory.Visible = false;
              pnlNoHistory.Visible = true;
           }
        }

        /// <summary>
        /// rptComments ItemDataBound - Lease Apps
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptComments_ItemDataBound_LeaseAppsHistory(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                VersionLog version_item = (VersionLog)e.Item.DataItem;
                HtmlImage imgAvatar = e.Item.FindControl("imgAvatar") as HtmlImage;

                // Some version records are legacy records, which do not have
                // the last modified user/date. In these cases set default
                // values.
                if (imgAvatar != null)
                {
                   if (!String.IsNullOrEmpty(version_item.UserAvatarImageUrl))
                   {
                      imgAvatar.Src = version_item.UserAvatarImageUrl;
                   }
                   else
                   {
                      imgAvatar.Src = "~/images/avatars/default.png";
                   }
                   
                }
                
                Label lblEmployee = (Label)e.Item.FindControl("lblEmployee");
                Label lblDate = (Label)e.Item.FindControl("lblDate");

                if (!String.IsNullOrEmpty(version_item.updatedBy))
                {
                   lblEmployee.Text = version_item.updatedBy;
                }
                else
                {
                   lblEmployee.Text = "No employee recorded.";
                }

                if (version_item.updatedAt != null)
                {
                   lblDate.Text = Utility.DateToString(version_item.updatedAt);
                }
                else
                {
                   lblDate.Text = "No date recorded.";
                }

                Label lblFieldChanged = (Label)e.Item.FindControl("lblFieldChanged");
                lblFieldChanged.Text = version_item.fieldChanged;

                Label lblOld = (Label)e.Item.FindControl("lblOld");
                lblOld.Text = version_item.oldValue;

                Label lblNew = (Label)e.Item.FindControl("lblNew");
                lblNew.Text = version_item.newValue;
            }
        }
        //-->

        /// <summary>
        /// rptComments ItemDataBound - Structural Analysis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptComments_ItemDataBound_SAHistory(object sender, RepeaterItemEventArgs e)
        {
           if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
           {
              VersionLog version_item = (VersionLog)e.Item.DataItem;
              HtmlImage imgSAAvatar = e.Item.FindControl("imgSAAvatar") as HtmlImage;

              // Some version records are legacy records, which do not have
              // the last modified user/date. In these cases set default
              // values.
              if (imgSAAvatar != null)
              {
                 if (!String.IsNullOrEmpty(version_item.UserAvatarImageUrl))
                 {
                    imgSAAvatar.Src = version_item.UserAvatarImageUrl;
                 }
                 else
                 {
                    imgSAAvatar.Src = "~/images/avatars/default.png";
                 }

              }

              Label lblSAEmployee = (Label)e.Item.FindControl("lblSAEmployee");
              Label lblSADate = (Label)e.Item.FindControl("lblSADate");              

              if (!String.IsNullOrEmpty(version_item.updatedBy))
              {
                 lblSAEmployee.Text = version_item.updatedBy;
              }
              else
              {
                 lblSAEmployee.Text = "No employee recorded.";
              }

              if (version_item.updatedAt != null)
              {
                 lblSADate.Text = Utility.DateToString(version_item.updatedAt);
              }
              else
              {
                 lblSADate.Text = "No date recorded.";
              }

              Label lblSAGroupID = (Label)e.Item.FindControl("lblSAGroupId");
              lblSAGroupID.Text = version_item.groupId.ToString();

              Label lblFieldChanged = (Label)e.Item.FindControl("lblSAFieldChanged");
              lblFieldChanged.Text = version_item.fieldChanged;

              Label lblOld = (Label)e.Item.FindControl("lblSAOld");
              lblOld.Text = version_item.oldValue;

              Label lblNew = (Label)e.Item.FindControl("lblSANew");
              lblNew.Text = version_item.newValue;
           }
        }

        /// <summary>
        /// rptComment ItemDataBound - Equipment 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptComments_ItemDataBound_EquipmentHistory(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                t_ChangeLog log = (t_ChangeLog)e.Item.DataItem;
                Label lblEquipmentType = (Label)e.Item.FindControl("lblEquipmentType");
                string equipType = log.TableName;
                if (equipType.IndexOf("t_") != - 1)
                {
                    equipType = equipType.Replace("t_", "");
                }

                lblEquipmentType.Text = equipType;

                Label lblEquipmentOperation = (Label)e.Item.FindControl("lblEquipmentOperation");
                lblEquipmentOperation.Text = log.OperationType;

                Label lblEquipmentChangeDate = (Label)e.Item.FindControl("lblEquipmentChangeDate");
                lblEquipmentChangeDate.Text = Utility.DateToString(log.ChangeDate);

                Label lblEquipmentFieldChanged = (Label)e.Item.FindControl("lblEquipmentFieldChanged");
                lblEquipmentFieldChanged.Text = log.FieldName;

                Label lblEquipmentOldValue = (Label)e.Item.FindControl("lblEquipmentOldValue");
                lblEquipmentOldValue.Text = log.OldValue;

                Label lblEquipmentNewValue = (Label)e.Item.FindControl("lblEquipmentNewValue");
                lblEquipmentNewValue.Text = log.NewValue;
            }
        }
    }
}