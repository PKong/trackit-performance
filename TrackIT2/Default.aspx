﻿<%@ Import Namespace="System.Web.Optimization" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/TrackIT2.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrackIT2._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

   <!-- Styles -->
   <%: Styles.Render("~/Styles/dashboard") %>
    
   <!-- Scripts -->
   <%: Scripts.Render("~/Scripts/dashboard") %>

    <script type="text/javascript">
        // Doc Ready!
        $(document).ready(function ()
        {

            // Dashboard Templates
            $('body').append('<div id="templates"></div>');
            $("#templates").hide();
            $("#templates").load("/Dashboard/templates.html", initDashboard);

            // Init Dashboard
            function initDashboard()
            {

                // TODO: Generate IDs server-side?
                var widgetPrefix = "widget";
                var jsonStr = $("#hidMyWidgetJSON").val();
                var objJSON = jQuery.parseJSON(jsonStr.toString());
                //console.log(objJSON);

                // Setup Dashboard
                var dashboard = $('#dashboard').dashboard({
                    layoutClass: 'layout',
                    addWidgetSettings: {
                        widgetDirectoryUrl: "/Dashboard/jsonfeed/widgetcategories.json"
                    },
                    json_data: {
                        data: objJSON
                    },
                    layouts:
		                [
			                { title: "Layout1",
			                    id: "layout1",
			                    image: "/Styles/images/layout1.png",
			                    html: '<div class="layout layout-a"><div class="column first column-first"></div></div>',
			                    classname: 'layout-a'
			                },
			                { title: "Layout2",
			                    id: "layout2",
			                    image: "/Styles/images/layout2.png",
			                    html: '<div class="layout layout-aa"><div class="column first column-first"></div><div class="column second column-second"></div></div>',
			                    classname: 'layout-aa'
			                },
			                { title: "Layout3",
			                    id: "layout3",
			                    image: "/Styles/images/layout3.png",
			                    html: '<div class="layout layout-ba"><div class="column first column-first"></div><div class="column second column-second"></div></div>',
			                    classname: 'layout-ba'
			                },
			                { title: "Layout4",
			                    id: "layout4",
			                    image: "/Styles/images/layout4.png",
			                    html: '<div class="layout layout-ab"><div class="column first column-first"></div><div class="column second column-second"></div></div>',
			                    classname: 'layout-ab'
			                },
			                { title: "Layout5",
			                    id: "layout5",
			                    image: "/Styles/images/layout5.png",
			                    html: '<div class="layout layout-aaa"><div class="column first column-first"></div><div class="column second column-second"></div><div class="column third column-third"></div></div>',
			                    classname: 'layout-aaa'
			                }
		                ]
                });
                //>

                // Widgets Added Handler
                dashboard.element.live('dashboardAddWidget', function (e, obj)
                {
                    startId++;
                    var widgetId = (widgetPrefix + startId);
                    var widget = obj.widget;
                    dashboard.addWidget({
                        "id": widgetId,
                        "title": widget.title,
                        "url": widget.url,
                        "metadata": widget.metadata,
                        "maximum": widget.maximum
                    }, dashboard.element.find('.column:first'));

                    // Add New Widget To User Widget JSON
                    addNewWidgetToUserWidgetJSON(widget.title, "first", widgetId, widget.url, null, true, 0, widget.metadata);

                    // Save Dashboard & Widgets
                    saveDashboardAndWidgets();

                });
                //>

                // Dashboard Layout Changed
                dashboard.element.live('dashboardLayoutChanged', function ()
                {

                    // Save Dashboard & Widgets
                    saveDashboardAndWidgets();
                });
                //-->

                // Init!
                dashboard.init();

                // Set the widgetID for the new one

                var startId;
                var tmp_max = 0;
                $('.widget').each(function ()
                {
                    var each_id = $(this).attr("id");
                    each_id = each_id.substring(each_id.lastIndexOf("t") + 1);
                    //alert(each_id + "?" + tmp_max);
                    if (parseInt(each_id) > parseInt(tmp_max))
                    {
                        tmp_max = each_id;
                    }
                });

                startId = tmp_max;
                //alert("New Widget Id starts at: " + startId);
            }

            // Expand Layout Button
            $(".toggle-width-button").click(function ()
            {
                $(".wrapper").toggleClass("fluid-width");
                $(".dashboard").trigger("resize");
            });
            //>
        });
        //-->
    </script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="overflow:auto;padding:23px 0 0 0;">
        <h1 class="h1-header">Dashboard</h1>
        <div class="headerlinks">
            <a class="toggle-width-button" href="#">Expand</a> 
            <a class="openaddwidgetdialog headerlink" href="#">Add Dashboard Item</a>
            <a class="editlayout headerlink" href="#">Edit Layout</a>
        </div>
    </div>
    <div id="dashboard" class="dashboard">
        <div class="layout">
            <div class="column first column-first">
            </div>
            <div class="column second column-second">
            </div>
            <div class="column third column-third">
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hidMyWidgetJSON" ClientIDMode="Static" />
</asp:Content>
