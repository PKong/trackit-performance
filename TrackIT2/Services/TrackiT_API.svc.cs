﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TrackIT2.BLL;
using TrackIT2.DAL;
using TrackIT2.Objects;
using System.Collections;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;
using System.Web;

namespace TrackIT2.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TrackiT_API" in code, svc and config file together.

    public class TrackiT_API : ITrackiT_API
    {
        private const String ACCESS_DENIED = "Access Denied";
        
        public string InsertLeaseAppRevision(byte[] signature, String TMOAppId)
        {
            string result = String.Empty;
            if (HMACHelper.HMAC_Authen(signature))
            {
                try
                {
                    lease_applications leaseApp = LeaseApplication.SearchByTMOAppId(TMOAppId.Trim());
                    if (leaseApp != null)
                    {
                        leaseapp_revisions objRV = new leaseapp_revisions();
                        objRV.lease_application_id = leaseApp.id;
                        objRV.revision_rcvd_date = DateTime.Now;
                        result = LeaseAppRevisions.Add(objRV);
                    }
                    else
                    {
                        //Write log edit not exist TMO-AppId from T-Mobile site
                        var elmahCon = Elmah.ErrorLog.GetDefault(null);
                        System.ArgumentException argEx = new System.ArgumentException("Cannot insert lease application revision, TMOAppID: '" + TMOAppId + "' does not exist.", "TMOAppId", null);
                        elmahCon.Log(new Elmah.Error(argEx));
                    }
                }
                catch (Exception ex)
                {
                    //Write log exception while Insert Lease app revision
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(ex));
                }
            }
            else
            {
                result = ACCESS_DENIED;
            }
            return result;
        }

        public int CheckExistedTrakItTMOApp(byte[] signature, String TMOAppId)
        {
            int result = -1;
            if (HMACHelper.HMAC_Authen(signature))
            { 
                try
                {
                    lease_applications leaseApp = LeaseApplication.SearchByTMOAppId(TMOAppId);
                    if (leaseApp != null)
                    {
                        result = leaseApp.id;
                    }
                }
                catch (Exception ex)
                {
                    //Write log exception when service CheckExistedTrakItTMOApp 
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(ex));
                }
            }
            else
            {
                result = -99;
            }
            return result;
        }

        public bool CanShowSDPDocs(byte[] signature, String TMO_AppID)
        {
            bool result = false;
            if (HMACHelper.HMAC_Authen(signature))
            {
                try
                {
                    List<site_data_packages> sdp = BLL.SiteDataPackage.SearchByTMPAppID(TMO_AppID);
                    if (sdp.Any())
                    {
                        result = sdp.Any(x => x.publish_document == true);
                    }
                   
                }
                catch (Exception ex)
                {
                    //Write log exception when service CheckExistedTrakItTMOApp 
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(ex));
                }
            }
            return result;
        }

        public List<string[]> GetSDPDocuments(byte[] signature, String TMO_AppID)
        {
            List<string[]> result = new List<string[]>();

            if (HMACHelper.HMAC_Authen(signature))
            {
                try
                {
                    List<site_data_packages> sdp = BLL.SiteDataPackage.SearchByTMPAppID(TMO_AppID);
                    if (sdp.Any())
                    {
                        List<int> sdpId = sdp.Where(x => x.publish_document ==true).Select(x=>x.id).ToList();
                        result = DMSDocumentLinkHelper.GetListDocumentByApp(sdpId, DMSDocumentLinkHelper.eListDocumentType.SiteDataPackage);
                    }
                }
                catch (Exception ex)
                {
                    //Write log exception when service CheckExistedTrakItTMOApp 
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(ex));
                }
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }


        public byte[] DowloadDocument(byte[] signature, String keyName,int sdpId)
        {
            byte[] result = null;

            if (HMACHelper.HMAC_Authen(signature))
            {
                try
                {
                    site_data_packages sdp = BLL.SiteDataPackage.Search(sdpId);
                    if (sdp != null && sdp.publish_document)
                    {
                        result = AmazonDMS.DownloadObjectStream(keyName.Replace('|', '+'));
                    }
                }
                catch (Exception ex)
                {
                    //Write log exception when service CheckExistedTrakItTMOApp 
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(ex));
                }
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }


        public byte[] DowloadAllDocument(byte[] signature, List<string> listFileKey, String TMO_AppID, string zipFileName, ref bool isRefreshData)
        {
            byte[] result = null;
            if (HMACHelper.HMAC_Authen(signature))
            {
                try
                {
                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        string zipPath = Path.Combine(HttpRuntime.CodegenDir, zipFileName);
                        List<site_data_packages> sdp = BLL.SiteDataPackage.SearchByTMPAppID(TMO_AppID);
                        if (sdp.Any())
                        {
                            List<int> sdpIds = sdp.Where(x => x.publish_document == true).Select(x => x.id).ToList();
                            List<string[]> lstPublishDocument = DMSDocumentLinkHelper.GetListDocumentByApp(sdpIds, DMSDocumentLinkHelper.eListDocumentType.SiteDataPackage);
                            //immediately return when not found any document
                            if (!lstPublishDocument.Any())
                            {
                                return result;
                            }
                            foreach (string[] key in lstPublishDocument)
                            {
                                string decodeKey = HttpUtility.UrlDecode(key[1]).Replace("|","+");
                                string fileName = decodeKey.Split('/')[decodeKey.Split('/').Length - 1];
                                byte[] buffer = AmazonDMS.DownloadObjectStream(decodeKey);
                                string dest = Path.Combine(HttpRuntime.CodegenDir, fileName);
                                if (buffer != null)
                                {
                                    File.WriteAllBytes(dest, buffer);
                                    zip.AddFile(dest, @"\");
                                }
                            }
                            zip.Save(zipPath);
                            FileStream fs = File.OpenRead(zipPath);
                            result = new byte[fs.Length];
                            fs.Read(result, 0, Convert.ToInt32(fs.Length));
                            fs.Close();
                            fs.Dispose();
                            File.Delete(zipPath);

                            //Check all key retriee is equal with request or not?
                            List<string> lstPublishDocumentKey = lstPublishDocument.Select(x => x[1].Replace('|','+')).ToList();
                            isRefreshData = lstPublishDocumentKey.Where(a => !listFileKey.Contains(a)).Union(listFileKey.Where(a => !lstPublishDocumentKey.Contains(a))).Any();
                        }
                        else 
                        {
                            isRefreshData = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Write log exception when service CheckExistedTrakItTMOApp 
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(ex));
                }
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return result;
        }
    }

}
